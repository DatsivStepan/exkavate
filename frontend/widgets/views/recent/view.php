<?php

use yii\helpers\Html;
use common\models\Likes;
?>

<?php foreach ($modelResentProducts as $product) { ?>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 product-container-padding product-one-all-no-home nth-chil-prod">
        <?= $this->render('//templates/_product', ['product' => $product])?>
    </div>
<?php } ?>