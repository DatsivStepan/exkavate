<?php

use yii\helpers\Html;
use common\models\Likes;

?>

<?php foreach ($modelResentNews as $news) { ?>
    <div class="row">
    <p class="recenr-new-data"><?= $news->getDateCreate('M. d.Y');; ?></p>
    <p class="recenr-new-title"><?= Html::a($news->title, $news->getLink()); ?></p>
    <p class="recenr-new-desc"><?= $news->short_description; ?></p>
    </div>
<?php } ?>