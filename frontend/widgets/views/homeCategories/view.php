<section class="categories">
    <div class="container">
      <div class="categories-title">Popular Categories<br></div>
      <div class="categories-box">
        <div class="row">
            <?php foreach($categories as $category){ ?>
                <?php if ($category->getProductCount() > 0) { ?>
                    <div class="col-xs-12 col-sm-offset-0 col-sm-6 col-md-3">
                        <div class="category-items">
                            <a href="<?= $category->getLink(); ?>">
                                <img src="<?= $category->img_src; ?>" alt="<?= $category->name; ?>">
                                <span><?= $category->name; ?></span>
                            </a>
                        </div>    
                    </div>
                <?php } ?>
            <?php } ?>
          <div class="bottom-block"></div>
        </div>
      </div>
      <div class="toggle-categories">Expand All Categories  <i class="fa fa-angle-right" aria-hidden="true"></i></div>
    </div>
</section>
