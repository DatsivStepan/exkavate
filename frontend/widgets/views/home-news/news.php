<?php

use yii\helpers\Html;
?>
<?php if (!empty($modelNews)) { ?>
<div class="owl-carousel owl-theme hidden-sm hidden-md hidden-lg" id="owl-news-mobile">
    <?php foreach ($modelNews as $key => $newsArray) { ?>
        <?php foreach($newsArray as $keyN => $news){ ?>
            <div class="item">
                <div style="width: 70vw; overflow: hidden;margin-top:13px; margin-right: 10px">
                    <div class="mobile-news-img-container">
                        <?= Html::tag('img', '', ['src' => $news->getImages(), 'style' => 'width:100%;height:100%']); ?>
                    </div>    
                    <div class="mobile-news-date-container">
                        <?= $news->getDateCreate(); ?>
                    </div>
                    <div class="mobile-news-title-container">
                        <?= Html::a($news->title, $news->getLink()); ?>
                    </div>
                    <div class="mobile-news-description-container">
                        <?= $news->short_description; ?>
                    </div>
                </div>
            </div>    
        <?php } ?>
    <?php } ?>
</div>
<div class="container">
<div class="owl-carousel owl-theme hidden-xs" id="owl-news">
    <?php foreach ($modelNews as $key => $newsArray) { ?>
        <div class="item">
            <div class="row">
            <?php if ($key == 1) { ?>
                <!-- // тут повинен бути шаблон і однієї великої і чотирьох маленьких -->
                    <?php foreach($newsArray as $keyN => $news){ ?>
                        <!-- // тут перевірка на то чи перша новина, щоб дати інші класи -->
                            <?php if ($keyN == 0) { ?>
                                <div class="home-first-new col-sm-6 col-md-6">
                            <?php } else { ?>
                                <div class="home-last-fourth-new col-sm-3 col-md-3">
                            <?php }  ?>
                        <!-- //  -->
                        <div class="img-container" style="position:relative;">
                            <?= Html::tag('img', '', ['src' => $news->getImages(), 'style' => 'width:100%;']); ?>
                            <div style="display: none; background-color: white; position:absolute;left: 16px; top: 0px;">
                                <?= $news->categoryModel ? $news->categoryModel->name : ''; ?>
                            </div>
                        </div>
                        <div class="home-news-descriptioon">
                            <p class="home-news-date-create"><?= $news->getDateCreate('M. d. Y'); ?></p>
                            <h3 class="home-news-title">
                                <?= Html::a($news->title, $news->getLink()); ?>
                            </h3>
                            <div class="home-news-short-description"><?= $news->short_description; ?></div>
                        </div>
                                </div>
                    <?php } ?>
                </div>
            </div>             
            <?php } else { ?>
                <!-- // тут повинен бути шаблон і вісьми маленьких -->
                    <?php foreach($newsArray as $news){ ?>
                        <div class="col-sm-3 col-lg-3">
                            <div class="home-img-container" style="position:relative;">
                                <?= Html::tag('img', '', ['src' => $news->getImages(), 'style' => 'width:100%;']); ?>
<!--                                <div style="background-color: white; position:absolute;left: 16px; top: 0px;">
                                    <?= $news->categoryModel ? $news->categoryModel->name : ''; ?>
                                </div>-->
                            </div>
                            <div class="home-news-descriptioon">
                                <p class="home-news-date-create"><?= $news->getDateCreate();; ?></p>
                                <h3 class="home-news-title">
                                    <?= Html::a($news->title, $news->getLink()); ?>
                                </h3>
                                <div class="home-news-short-description"><?= $news->short_description; ?></div>
                            </div>
                        </div>
                        
                    <?php } ?>
            <?php } ?>        
    <?php } ?>
   </div>             
 </div>          
<?php } ?>