<?php

use yii\helpers\Html;
use common\models\Likes;
?>

<?php foreach ($modelProducts as $product) { ?>
    <div class="item">
        <?= $this->render('//templates/_product', ['product' => $product, 'type' => 'home']); ?>
    </div>
<?php } ?>