<?php
namespace frontend\widgets;
use common\models\Category;


class Categories extends \yii\bootstrap\Widget
{
    private $model;
    
    public function init(){
        $this->model = new Category();
    }

    public function run() {
        return $this->render('homeCategories/view', [
            'categories' => $this->model->getCategories()
        ]);
    }
}