<?php
namespace frontend\widgets;
use common\models\News;


class RecentNews extends \yii\bootstrap\Widget
{
    public $limit;

    public function init(){
    }

    public function run() {
        $modelResentNews = News::find()->orderBy('created_at ASC')->limit($this->limit)->all();        
        
        return $this->render('recent/news-view', [
            'modelResentNews' => $modelResentNews
        ]);
    }
}