<?php
namespace frontend\widgets;
use common\models\Products;


class RecentProduct extends \yii\bootstrap\Widget
{
    public $limit;

    public function init(){
    }

    public function run() {
        $modelResentProducts = Products::find()->orderBy('created_at ASC')->limit($this->limit)->all();        
        
        return $this->render('recent/view', [
            'modelResentProducts' => $modelResentProducts
        ]);
    }
}