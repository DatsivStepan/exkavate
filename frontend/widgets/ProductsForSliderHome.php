<?php
namespace frontend\widgets;
use common\models\Products;
use common\models\Views;
use common\models\Likes;
use common\models\ProductsComments;

class ProductsForSliderHome extends \yii\bootstrap\Widget
{
    public $type;
    public $limit;

    public function init(){
    }

    public function run() {
        $query = Products::find()->alias('product');
        switch ($this->type) {
            case 'latest':
                $query->orderBy('created_at ASC');
                break;
            case 'fuatured':
                $query->orderBy('created_at ASC');
                break;
            case 'popular':
                $query->select([
                    'product.*', 
                    'comCount' => "(SELECT COUNT(comments.id) FROM " . ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                    'likeCount' => 
                        "(SELECT COUNT(likes.id) FROM " . Likes::tableName() .
                        " likes WHERE likes.object_id = product.id AND likes.type = '" . Likes::TYPE_PRODUCT . "')",
                    'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() . " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
                ])
                ->orderBy([
                    'comCount' => SORT_DESC,
                    'viewCount'=> SORT_DESC,
                    'likeCount'=> SORT_DESC,
                  ]);
                break;
        }

        return $this->render('home-product/view', [
            'modelProducts' => $query->limit($this->limit)->all()
        ]);
    }
}