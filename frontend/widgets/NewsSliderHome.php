<?php
namespace frontend\widgets;
use common\models\News;

class NewsSliderHome extends \yii\bootstrap\Widget
{
    public $limit;

    public function init(){
    }

    public function run() {
        $query = News::find()->orderBy(['created_at' => SORT_DESC])->limit($this->limit)->all();

        $resultNews = [];
        foreach ($query as $key => $news) {
            if ($key <= 4) {
                $resultNews[1][] = $news;
            }elseif ($key <= 12) {
                $resultNews[2][] = $news;
            }elseif ($key <= 20) {
                $resultNews[3][] = $news;
            }
        }
        
        return $this->render('home-news/news', [
            'modelNews' => $resultNews
        ]);
    }
}