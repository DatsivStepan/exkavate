<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Exkavate',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'frame' => [
            'class' => 'frontend\modules\frame\Module',
        ],
    ],
    'components' => [
        'language'=>'en-EN',
        'i18n' => [
            'class'=> Zelenin\yii\modules\I18n\components\I18N::className(),
            'languages' => ['ru-RU', 'en-EN'],
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::className()
                ],
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ]
        ],
        'eauth' => [
                'class' => 'nodge\eauth\EAuth',
                'popup' => true, // Use the popup window instead of redirecting.
                'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
                'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                'httpClient' => [
                        // uncomment this to use streams in safe_mode
                        //'useStreamsFallback' => true,
                ],
                'services' => [ // You can change the providers and their classes.
//                        'goo
                ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
            'class' => 'frontend\components\LangRequest',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
//            'class'=>'frontend\components\LangUrlManager',
            'rules' => [
                '' => 'site/index',
                
                'products/view/<id:\d+>' => 'products/view',

                'category/index' => 'category/index',

                'pages/<slug:\w+>' => 'site/pages',
                '<_c:[\w-]+>' => '<_c>/index',
                '<_c:[\w-]+>/<id:\d+>' => '<_c>/view',
                'search/tags/<tag:\w+>' => 'search/tags',
                'news/search/<tag:\w+>' => 'news/search',
                '<_c:[\w-]+>/<id:\d+>/<_a:[\w-]+>' => '<_c>/<_a>',
                '<_c:[\w-]+>/<_a:[\w-]+>/<id:\d+>' => '<_c>/<_a>',
            ],
        ],
    ],
    'params' => $params,
];