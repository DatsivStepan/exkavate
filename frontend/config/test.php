<?php
return [
    'id' => 'app-frontend-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LfhATIUAAAAAD6jLtKEDAgG0BZj_8weyfDovLtA',
            'secret' => '6LfhATIUAAAAAPhlAugbS3gGs9IRYPT6GL7L7G2a',
        ]
    ],
];
