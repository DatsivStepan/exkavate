<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Company;
use common\models\Likes;
use common\models\Views;
use common\models\Products;
use common\models\Follower;

/**
 * PublicationsSearch represents the model behind the search form about `common\models\Company`.
 */
class PublicationsSearch extends Company
{
    public $likesCount;
    public $viewsCount;
    public $followCount;
    public $search_value;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'slug'], 'integer'],
            [
                [
                    'name', 'description', 'viewsCount', 'likesCount', 'followCount',
                    'logo_src', 'address', 'site', 'phone', 'created_at',
                    'updated_at', 'search_value'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find()
            ->alias('company')
            ->select([
                'company.*', 
                'likeCount' => 
                    "(SELECT DISTINCT COUNT(*) 
                        FROM " . Likes::tableName() . " lk
                        LEFT JOIN " . Products::tableName() . " product ON product.id = lk.object_id
                        WHERE (product.`company_id`= company.id) AND (lk.`type` = '" . Likes::TYPE_PRODUCT . "'))",
                'viewCount' =>
                    "(SELECT DISTINCT COUNT(*) 
                        FROM " . Views::tableName() . " vw
                        LEFT JOIN " . Products::tableName() . " product ON product.id = vw.object_id
                        WHERE (product.`company_id`= company.id) AND (vw.`type` = '" . Views::TYPE_PRODUCT . "'))",
                'folCount' => 
                    "(SELECT COUNT(*) FROM " . Follower::tableName() . " follower
                        WHERE follower.`object_id` = company.id AND follower.`type` = '" . Follower::TYPE_COMPANY . "')",
                'productsCount' => 
                        "(SELECT COUNT(*) FROM " . Products::tableName() . " prC
                            WHERE prC.`company_id`= company.id)",
                ])
                ->having(['>', 'productsCount', 0]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'likesCount' => [
                        'asc' => ['likeCount' => SORT_ASC],
                        'desc' => ['likeCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'followCount' => [
                        'asc' => ['folCount' => SORT_ASC],
                        'desc' => ['folCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);
        // add conditions that should always apply here

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'slug' => $this->slug,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->search_value])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'logo_src', $this->logo_src])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
