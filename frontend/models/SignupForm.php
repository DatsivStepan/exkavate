<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $type;
    public $reCaptcha;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['reCaptcha', 'required'],
            ['password', 'string', 'min' => 6],
            //[['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LfhATIUAAAAAPhlAugbS3gGs9IRYPT6GL7L7G2a', 'uncheckedMessage' => 'Please confirm that you are not a bot.']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $status = false;
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->status = User::STATUS_ACTIVE;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->type = 'user';
        $user->password = $this->password;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if (($status = $user->save()) && $user->email) {
            \Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'signup-html'],
                        ['user' => $user]
                )
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' support'])
                ->setTo($user->email)
                ->setSubject('You register on Exkavate')
                ->send();
        }
        return $status ? $user : null;
    }
}
