<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\News;

/**
 * SearchNews represents the model behind the search form about `common\models\News`.
 */
class SearchNews extends News
{
    public $tags;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inHome', 'category_id'], 'integer'],
            [
                [
                    'title', 'short_description', 'description', 'img_src',
                    'created_at', 'updated_at', 'tags'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find()
                ->alias('news')
                ->joinWith(['newsTagsModel tag']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'news.inHome' => $this->inHome,
            'news.category_id' => $this->category_id,
            'news.created_at' => $this->created_at,
            'news.updated_at' => $this->updated_at,
            'tag.name' => $this->tags
        ]);

        $query->andFilterWhere(['like', 'news.title', $this->title])
            ->andFilterWhere(['like', 'news.short_description', $this->short_description])
            ->andFilterWhere(['like', 'news.description', $this->description])
            ->andFilterWhere(['like', 'news.img_src', $this->img_src]);

        return $dataProvider;
    }
}
