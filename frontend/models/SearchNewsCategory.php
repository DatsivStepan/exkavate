<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NewsCategory;

/**
 * SearchNews represents the model behind the search form about `common\models\News`.
 */
class SearchNewsCategory extends NewsCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'featured'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsCategory::find()
                ->alias('newsCategory');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'news.inHome' => $this->inHome,
//            'news.category_id' => $this->category_id,
//            'news.created_at' => $this->created_at,
//            'news.updated_at' => $this->updated_at,
//            'tag.name' => $this->tags
//        ]);
//
//        $query->andFilterWhere(['like', 'news.title', $this->title])
//            ->andFilterWhere(['like', 'news.short_description', $this->short_description])
//            ->andFilterWhere(['like', 'news.description', $this->description])
//            ->andFilterWhere(['like', 'news.img_src', $this->img_src]);

        return $dataProvider;
    }
}
