<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Collections;
use common\models\Likes;
use common\models\Views;
use common\models\Products;
use common\models\ProductsCollections;

/**
 * CollectionsSearch represents the model behind the search form about `common\models\Collections`.
 */
class ProductsCollectionsSearch extends Collections
{
    public $likesCount;
    public $viewsCount;
    public $uploadCount;
    public $product_id;
    public $search_value;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'user_id'], 'integer'],
            [
                [
                    'name', 'slug', 'uploadCount', 'viewsCount', 'search_value',
                    'likesCount', 'description', 'created_at', 'updated_at'
                ], 
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Collections::find()
            ->alias('collection')
            ->select([
                'collection.*', 
                'likeCount' => 
                    "(SELECT DISTINCT COUNT(*) 
                        FROM " . Likes::tableName() . " lk
                        LEFT JOIN " . Products::tableName() . " product ON product.id = lk.object_id
                        LEFT JOIN " . ProductsCollections::tableName() . " prC ON prC.product_id = product.id
                        WHERE (prC.`collection_id`= collection.id) AND (lk.`type` = '" . Likes::TYPE_PRODUCT . "'))",
                'viewCount' =>
                    "(SELECT DISTINCT COUNT(*) 
                        FROM " . Views::tableName() . " vw
                        LEFT JOIN " . Products::tableName() . " product ON product.id = vw.object_id
                        LEFT JOIN " . ProductsCollections::tableName() . " prC ON prC.product_id = product.id
                        WHERE (prC.`collection_id`= collection.id) AND (vw.`type` = '" . Views::TYPE_PRODUCT . "'))",
                'uplCount' => 
                    "(SELECT COUNT(*) FROM " . ProductsCollections::tableName() . " prC
                        WHERE prC.`collection_id`= collection.id)",
                'productsCount' => 
                    "(SELECT COUNT(*) FROM " . ProductsCollections::tableName() . " prC
                        WHERE prC.`collection_id`= collection.id)",
            ])
                ->leftJoin('products_collections', 'products_collections.collection_id = collection.id')
                ->leftJoin('products', 'products.id = products_collections.product_id')
                ->where([
                        'products_collections.product_id' => $this->product_id,
                        'products_collections.company_id' => $this->company_id
                    ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'likesCount' => [
                        'asc' => ['likeCount' => SORT_ASC],
                        'desc' => ['likeCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'uploadCount' => [
                        'asc' => ['uplCount' => SORT_ASC],
                        'desc' => ['uplCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'collection.id' => $this->id,
//            'collection.company_id' => $this->company_id,
//            'collection.user_id' => $this->user_id,
//            'collection.created_at' => $this->created_at,
//            'collection.updated_at' => $this->updated_at,
//        ]);

        $query->andFilterWhere(['like', 'collection.name', $this->search_value])
            ->andFilterWhere(['like', 'collection.slug', $this->slug])
            ->andFilterWhere(['like', 'collection.description', $this->description]);

        return $dataProvider;
    }
    
}
