<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\Views;
use common\models\ProductsComments;

/**
 * ProductsSearch represents the model behind the search form about `common\models\Products`.
 */
class ProductsSearch extends Products
{
    public $commentsCount;
    public $viewsCount;
    public $sort;
    public $search_value;
    public $collection_id;
    public $advertiser_id;
    public $publication_id;
    public $trend_id;
    public $dates;
    public $tag;
    public $paginCount;
    
    public static $datesArray = [
        'week' => 'Last week',
        '30days' => 'Last 30 days',
        'year' => 'Last year'
    ];

    public static $arrayPagination = [
        12 => '12 per page',
        24 => '24 per page',
        36 => '36 per page',
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'company_id', 'paginCount'
                ], 
                'integer'
            ],
            [
                [
                    'name', 'sort_date', 'category_id', 'viewsCount', 'commentsCount',
                    'content', 'img_src', 'file_src', 'created_at', 'updated_at',
                    'sort', 'search_value', 'collection_id', 'advertiser_id', 'publication_id', 'dates'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function reloadDataId($attribute)
    {
        $resArray = [];
            if (is_array($this->{$attribute})) {
            foreach ($this->{$attribute} as $id => $value) {
                if ($value == 1) {
                    $resArray[] = $id;
                }
            }
        }
        $this->{$attribute} = $resArray;
    }
    
    public function searchS($params, $productId = null)
    {
        $query = Products::find()
            ->alias('product')
            ->select([
                'product.*', 
                'comCount' => "(SELECT COUNT(comments.id) FROM " . ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() .
                " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
            ]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'commentsCount' => [
                        'asc' => ['comCount' => SORT_ASC],
                        'desc' => ['comCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $this->load($params);
        if ($this->category_id) {
            $this->reloadDataId('category_id');
        }

        if ($this->publication_id) {
            $this->reloadDataId('publication_id');
        }
        if ($productId) {
            $query->andFilterWhere([
                'product.id' => $productId,
            ]);
        }
        if ($this->dates) {
            switch ($this->dates) {
                case 'week':
                    $date = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') - 7, date('Y')));
                    break;
                case '30days':
                    $date = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') - 30, date('Y')));
                    break;
                case 'year':
                    $date = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d'), date('Y')-1));
                    break;
                default:
                    $date = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d'), date('Y')-5));
            }
//            var_dump($date);exit;
                    $query->andFilterWhere([
                        '>', 'product.created_at', $date
                    ]);
        }
        $query->andFilterWhere([
            'product.category_id' => $this->category_id,
            'product.company_id' => $this->publication_id,
        ]);
        
        $query->andFilterWhere(['like', 'product.name', $this->search_value]);

        return $dataProvider;
    }
    
    public function searchLastR($params, $productId)
    {
        $query = Products::find()
                ->where(['id' => $productId]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'sort' => [
//                'attributes' => [
//                    'commentsCount' => [
//                        'asc' => ['comCount' => SORT_ASC],
//                        'desc' => ['comCount' => SORT_DESC],
//                    ],
//                    'viewsCount' => [
//                        'asc' => ['viewCount' => SORT_ASC],
//                        'desc' => ['viewCount' => SORT_DESC],
//                    ],
//                    'created_at'
//                ],
//            ],
        ]);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $this->load($params);
        
//        if ($productId) {
//            $query->andFilterWhere([
//                'product.id' => $productId,
//            ]);
//        }
//        $query->andFilterWhere([
//            'product.category_id' => $this->category_id,
//        ]);
//        
//        $query->andFilterWhere(['like', 'product.name', $this->search_value]);

        return $dataProvider;
    }
    
    public function search($params, $productId = null)
    {
        $query = Products::find()
            ->alias('product')
            ->select([
                'product.*', 
                'comCount' => "(SELECT COUNT(comments.id) FROM " . ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() .
                " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
            ]);
        
        // add conditions that should always apply here

        $this->load($params);

        $paginCount = 10;
        if ($this->paginCount) {
            $paginCount = $this->paginCount;
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'commentsCount' => [
                        'asc' => ['comCount' => SORT_ASC],
                        'desc' => ['comCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => $paginCount,
            ],
        ]);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        
        if ($productId) {
            $query->andFilterWhere([
                'product.id' => $productId,
            ]);
        }
        $query->andFilterWhere([
            'product.category_id' => $this->category_id,
        ]);
        
        $query->andFilterWhere(['like', 'product.name', $this->search_value]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCollectionProduct($params)
    {
        $query = Products::find()
            ->alias('product')
            ->select([
                'product.*', 
                'comCount' => "(SELECT COUNT(comments.id) FROM " . ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() .
                " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
            ])
            ->leftJoin('products_collections', 'products_collections.product_id = product.id')
            ->where(['products_collections.collection_id' => $this->collection_id]);;
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'commentsCount' => [
                        'asc' => ['comCount' => SORT_ASC],
                        'desc' => ['comCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $this->load($params);
       
        $query->andFilterWhere([
            'product.category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->search_value]);

        return $dataProvider;
    }

    public function searchAdvertisersProducts($params)
    {
        $query = Products::find()
            ->alias('product')
            ->select([
                'product.*', 
                'comCount' => "(SELECT COUNT(comments.id) FROM " . ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() . " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
            ])
            ->where(['product.brand_id' => $this->advertiser_id]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'commentsCount' => [
                        'asc' => ['comCount' => SORT_ASC],
                        'desc' => ['comCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);
        $this->load($params);
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
        
        $query->andFilterWhere([
            'product.category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->search_value]);

        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchTags($params)
    {
        $query = Products::find()
            ->alias('product')
            ->select([
                'product.*', 
                'comCount' => "(SELECT COUNT(comments.id) FROM " . ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() .
                " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
            ])
            ->joinWith(['productsTagsModel tag']);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'commentsCount' => [
                        'asc' => ['comCount' => SORT_ASC],
                        'desc' => ['comCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $this->load($params);
        
        $this->category_id = $category_id;
        $query->andFilterWhere([
            'product.category_id' => $this->category_id,
        ]);
        $query->andFilterWhere(['like', 'product.name', $this->search_value]);
        $query->andFilterWhere(['like', 'tag.name', $this->tag]);

        return $dataProvider;
    }
    
    public function searchPublicationsProducts($params)
    {
        $query = Products::find()
            ->alias('product')
            ->select([
                'product.*', 
                'comCount' => "(SELECT COUNT(comments.id) FROM " . \common\models\ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() .
                " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
            ])
            ->where(['product.company_id' => $this->publication_id]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'commentsCount' => [
                        'asc' => ['comCount' => SORT_ASC],
                        'desc' => ['comCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);
        
        $this->load($params);

        $query->andFilterWhere([
            'product.category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->search_value]);

        return $dataProvider;
    }
    
    public function searchTrendingSearch($params)
    {
        $query = Products::find()
            ->alias('product')
            ->select([
                'product.*', 
                'comCount' => "(SELECT COUNT(comments.id) FROM " . \common\models\ProductsComments::tableName() . " comments WHERE comments.product_id = product.id)",
                'viewCount' => "(SELECT COUNT(views.id) FROM " . Views::tableName() .
                " views WHERE views.object_id = product.id AND views.type = '" . Views::TYPE_PRODUCT . "' )"
            ])
            ->leftJoin('products_trends', 'products_trends.product_id = product.id')
            ->where(['products_trends.trend_id' => $this->trend_id]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'commentsCount' => [
                        'asc' => ['comCount' => SORT_ASC],
                        'desc' => ['comCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $this->load($params);

        $query->andFilterWhere([
            'product.category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->search_value]);

        return $dataProvider;
    }
    
}
