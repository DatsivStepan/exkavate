<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Brands;
use common\models\Likes;
use common\models\Views;
use common\models\Products;
use common\models\Follower;
/**
 * AdvertisersSearch represents the model behind the search form about `common\models\Brands`.
 */
class AdvertisersSearch extends Brands
{
    public $likesCount;
    public $viewsCount;
    public $followCount;
    public $search_value;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'company_id'], 'integer'],
            [
                [
                    'name', 'slug', 'viewsCount', 'likesCount', 'followCount', 
                    'description', 'img_src', 'created_at', 'updated_at', 'search_value'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Brands::find()
            ->alias('brand')
            ->select([
                'brand.*', 
                'likeCount' => 
                    "(SELECT DISTINCT COUNT(*) 
                        FROM " . Likes::tableName() . " lk
                        LEFT JOIN " . Products::tableName() . " product ON product.id = lk.object_id
                        WHERE (product.`brand_id`= brand.id) AND (lk.`type` = '" . Likes::TYPE_PRODUCT . "'))",
                'viewCount' =>
                    "(SELECT DISTINCT COUNT(*) 
                        FROM " . Views::tableName() . " vw
                        LEFT JOIN " . Products::tableName() . " product ON product.id = vw.object_id
                        WHERE (product.`brand_id`= brand.id) AND (vw.`type` = '" . Views::TYPE_PRODUCT . "'))",
                'folCount' => 
                    "(SELECT COUNT(*) FROM " . Follower::tableName() . " follower
                        WHERE follower.`object_id` = brand.id AND follower.`type` = '" . Follower::TYPE_BRAND . "')",
                'productsCount' => 
                        "(SELECT COUNT(*) FROM " . Products::tableName() . " prC
                            WHERE prC.`brand_id`= brand.id)",
                ])
                ->having(['>', 'productsCount', 0]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'likesCount' => [
                        'asc' => ['likeCount' => SORT_ASC],
                        'desc' => ['likeCount' => SORT_DESC],
                    ],
                    'viewsCount' => [
                        'asc' => ['viewCount' => SORT_ASC],
                        'desc' => ['viewCount' => SORT_DESC],
                    ],
                    'followCount' => [
                        'asc' => ['folCount' => SORT_ASC],
                        'desc' => ['folCount' => SORT_DESC],
                    ],
                    'created_at'
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'company_id' => $this->company_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->search_value])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
}
