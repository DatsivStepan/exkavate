<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Trends;

/**
 * ProductsSearch represents the model behind the search form about `common\models\Products`.
 */
class TrendsFollowingSearch extends Trends
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['company_id', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trends::find()
                ->alias('trends')
                ->leftJoin('follower', "follower.object_id = trends.id AND follower.type = '" . \common\models\Follower::TYPE_TRENDING . "'" );
//                ->leftJoin('follower', "follower.object_id = trends.company_id AND follower.type = '" . \common\models\Follower::TYPE_COMPANY . "'" );
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }
        
        $this->load($params);
       
        $query->andFilterWhere([
            'follower.user_id' => \Yii::$app->user->id,
        ]);
        
        return $dataProvider;
    }
}
