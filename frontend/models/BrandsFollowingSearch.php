<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Brands;

/**
 * ProductsSearch represents the model behind the search form about `common\models\Products`.
 */
class BrandsFollowingSearch extends Brands
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'category_id'], 'integer'],
            [['name', 'sort_date', 'sort_dimensions', 'sort_comment', 'sort_views', 'short_content', 'content', 'img_src', 'file_src', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Brands::find()->joinWith([
                'brandsFollowers followers'
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }
        
        $this->load($params);
       
        $query->andFilterWhere([
            'followers.user_id' => \Yii::$app->user->id,
        ]);
        
        return $dataProvider;
    }
}
