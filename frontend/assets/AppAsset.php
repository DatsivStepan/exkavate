<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;
/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css",
        'css/plugins/sweetalert.css',
        'css/site.css',
        'css/main.css',
        'css/profile.css'
    ];
    public $js = [
        'js/plugins/sweetalert.min.js',
        'js/plugins/dropzone.js',
        'js/plugins/jquery.cookie.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = array(
        'position' => View::POS_HEAD
    );
}
