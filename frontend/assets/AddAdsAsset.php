<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;
/**
 * Main frontend application asset bundle.
 */

class AddAdsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web';
    public $basePath = '@frontend';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/shablon/demo-kanban.css',
        'css/plugins/shablon/demo-grid.css',
        'css/plugins/shablon/main.css',
    ];
    public $js = [
        'js/plugins/shablon/muuri-0.5.1.js'
    ];
    public $jsOptions = array(
        'position' => View::POS_END
    );
}
