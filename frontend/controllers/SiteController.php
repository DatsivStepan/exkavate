<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\Response;
use common\models\Follower;
use common\models\Likes;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;  
use common\models\Pages;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return $this;
    }
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    
    public function actionFollow()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $object_id = Yii::$app->request->post('object_id');
        $type = Yii::$app->request->post('type');
        if ($object_id) {
            $modelFollow = Follower::findOne(['object_id' => $object_id, 'type' => $type, 'user_id' => \Yii::$app->user->id]);
            if (!$modelFollow) {
                $modelFollow = new Follower;
                $modelFollow->type = $type;
                $modelFollow->object_id = $object_id;
                $modelFollow->user_id = \Yii::$app->user->id;
                if ($modelFollow->save()) {
                    $status = 'success';
                } else {
                    $status = 'error';
                }
            } else {
                $status = 'success';
            }
        } else {
            $status = 'error';
        }
        
        return ['status' => $status];
    }

    public function actionUnfollow()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $object_id = Yii::$app->request->post('object_id');
        $type = Yii::$app->request->post('type');
        if ($object_id) {
            $modelFollow = Follower::findOne(['object_id' => $object_id, 'type' => $type, 'user_id' => \Yii::$app->user->id]);
            if ($modelFollow) {
                if ($modelFollow->delete()) {
                    $status = 'success';
                } else {
                    $status = 'error';
                }
            } else {
                $status = 'success';
            }
        } else {
            $status = 'error';
        }
        
        return ['status' => $status];
    }

    public function actionLike()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $object_id = Yii::$app->request->post('object_id');
        $type = Yii::$app->request->post('type');
        if ($object_id) {
            $modelLikes = Likes::findOne(['object_id' => $object_id, 'type' => $type, 'user_id' => \Yii::$app->user->id]);
            if (!$modelLikes) {
                $modelLikes = new Likes;
                $modelLikes->type = $type;
                $modelLikes->object_id = $object_id;
                $modelLikes->user_id = \Yii::$app->user->id;
                if ($modelLikes->save()) {
                    $status = 'success';
                        if ($modelLikes->type == Likes::TYPE_BRAND) {
                            $modelFollow = Follower::findOne(['object_id' => $object_id, 'type' => $type, 'user_id' => \Yii::$app->user->id]);
                            if (!$modelFollow) {
                                $modelFollow = new Follower;
                                $modelFollow->type = $type;
                                $modelFollow->object_id = $object_id;
                                $modelFollow->user_id = \Yii::$app->user->id;
                                if ($modelFollow->save()) {
                                    $status = 'success';
                                } else {
                                    $status = 'error';
                                }
                            } else {
                                $status = 'success';
                            }
                        }
                    
                } else {
                    $status = 'error';
                }
            } else {
                $status = 'success';
            }
        } else {
            $status = 'error';
        }
        
        return ['status' => $status];
    }

    public function actionUnlike()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $object_id = Yii::$app->request->post('object_id');
        $type = Yii::$app->request->post('type');
        if ($object_id) {
            $modelLikes = Likes::findOne(['object_id' => $object_id, 'type' => $type, 'user_id' => \Yii::$app->user->id]);
            if ($modelLikes) {
                if ($modelLikes->delete()) {
                    $status = 'success';
                        $modelFollow = Follower::findOne(['object_id' => $object_id, 'type' => $type, 'user_id' => \Yii::$app->user->id]);
                        if ($modelFollow) {
                            if ($modelFollow->delete()) {
                                $status = 'success';
                            } else {
                                $status = 'error';
                            }
                        } else {
                            $status = 'success';
                        }
                } else {
                    $status = 'error';
                }
            } else {
                $status = 'success';
            }
        } else {
            $status = 'error';
        }
        
        return ['status' => $status];
    }
    
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {    
        $publications = \common\models\Company::getCompanyWithLimit(20);
        $trends = \common\models\Trends::getTrendWithLimit(10);
        $sliderItems = \common\models\Sliders::find()->where(['type' => \common\models\Sliders::SLIDER_TYPE_HOME])->all();

        return $this->render('index', [
            'publications' => $publications,
            'trends' => $trends,
            'sliderItems' => $sliderItems,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    
    public function actionAjaxLogin() {
        Yii::$app->response->format = Response::FORMAT_JSON;  

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->loginUser()) {
            return ['status' => true];
        }else{
            return ['status' => false];            
        }
    }
    
    public function actionLogin() {
        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {

                    $identity = User::findByEAuth($eauth);
                    $profile = $eauth->getAttributes();
                    //var_dump($profile);exit;
                    if(!User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->exists()){
                        $model = new User();
                        $model->username = (isset($profile['name']))?$profile['name']:$profile['id'];
                        $model->password = \Yii::$app->security->generateRandomString(8);
                        $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
                        $model->auth_key = \Yii::$app->security->generateRandomString(8);
                        $model->username = $profile['name'];
                        $model->email = (isset($profile['email']))?$profile['email']:$profile['id'];
                        $model->signup_type = $eauth->getServiceName();
                        $model->social_id = (isset($profile['id']))?$profile['id']:'';
                        if ($model->save()) {
                          if (Yii::$app->getUser()->login($model)) {
                            $eauth->redirect();
                          }
                        }
                    }else{
                        $model = User::find()->where(['social_id' => (isset($profile['id']))?$profile['id']:$profile['id'],'username' => $profile['name'], 'email' => (isset($profile['email']))?$profile['email']:$profile['id']])->one();
                        if (Yii::$app->getUser()->login($model)) {
                          $eauth->redirect();
                        }
                    }
                    //Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
//                    $eauth->redirect();
                }else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
        
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $href = Yii::$app->request->get('href');
        Yii::$app->user->logout();
//        var_dump($href);exit;
        if ($href) {
            return $this->redirect($href);
        }
        return $this->goBack();
    }

    /**
     * @return mixed
     */
    public function actionSaveNewslaterEmail()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;  
        $res = ['status' => false, 'message' => ''];
        if (Yii::$app->request->isPost && Yii::$app->request->post('email')){
            $modelNewslaterEmail = \common\models\NewslaterEmail::findOne(['email' => Yii::$app->request->post('email')]);
            if (!$modelNewslaterEmail) {
                $modelNewslaterEmail = new \common\models\NewslaterEmail();
                $modelNewslaterEmail->email = Yii::$app->request->post('email');
                if ($modelNewslaterEmail->save()) {
                    $res['status'] = true;
                    $res['message'] = 'Subscribed successfully!';
                } else {
                    $res['status'] = false;
                    $res['message'] = 'There was a problem saving, try again or contact the administration of the site.';
                }
            } else {
                $res['status'] = false;
                $res['message'] = 'Subscribed successfully!';
            }
        }

        return $res;
    }
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionAjaxSignup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;  
        
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return ['status' => true];
                }else{
                    return ['status' => false];                    
                }
            }else{
                return ['status' => false];
            }
        }else{
            return ['status' => false];
        }
    }
    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionPages($slug)
    {
        $model = Pages::findOne(['slug' => $slug]);
        if ($model) {
            return $this->render('page', [
                'model' => $model
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
