<?php

namespace frontend\controllers;

use Yii;
use common\models\Category;
use common\models\Products;
use frontend\models\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionGetProductsById()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = ['status' => false, 'data' => null];
        $products_id = Yii::$app->request->post('products_id');
        if ($products_id) {
            $products = Products::findAll($products_id);
            $productsA = [];
            foreach ($products as $product) {
                $productsA[] = [
                    'name' => $product->name,
                    'link' => $product->getLink(),
                    'image' => $product->getImageSrc(),
                ];
            };
            $res['status'] = true;
            $res['data'] = $productsA;
        }
        
        return $res;
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelCategories = Category::find()->all();

        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [], null);
 
        return $this->render('index', [
            'searchModel' => $searchModel,
            'modelCategories' => $modelCategories,
            'productsModel' => $dataProvider->getModels(),
            'pagination' => $dataProvider->pagination,
        ]);
    }
}
