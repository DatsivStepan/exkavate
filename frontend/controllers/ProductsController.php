<?php

namespace frontend\controllers;

use Yii;
use common\models\Products;
use frontend\models\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use frontend\models\ProductsCollectionsSearch;
use common\models\Views;
use common\models\Follower;
use common\models\ProductsComments;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionLastReviewed()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->searchLastR(Yii::$app->request->queryParams, json_decode($_COOKIE['last_reviewed_data']));
        
        return $this->render('/category/last-reviewed', [
            'searchModel' => $searchModel,
            'productsModel' => $dataProvider->getModels(),
            'pagination' => $dataProvider->pagination,
        ]);
    }

    public function actionCollections($id)
    {
        $model = $this->findModel($id);
        
        $searchModel = new ProductsCollectionsSearch();
        $searchModel->product_id = $model->id;
        $searchModel->company_id = $model->company_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('collections', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionEmbed($id)
    {
        $model = $this->findModel($id);
        $this->layout = 'iframe';
        return $this->render('_embed', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $user_ip = Yii::$app->request->userIP;
        $modeViews = Views::find()->where([
            'object_id' => $model->id,
            'type' => 'product'
        ])->andWhere([
            'OR',
            ['user_ip' => $user_ip],
            ['user_id' => !\Yii::$app->user->isGuest ? \Yii::$app->user->id : null]
        ])->one();
        if(!$modeViews){
            $modeViews = new Views();
            $modeViews->object_id = $model->id;
            $modeViews->user_ip = $user_ip;
            $modeViews->user_id = !\Yii::$app->user->isGuest ? \Yii::$app->user->id : null;
            $modeViews->type = 'product';
            $modeViews->save();
        }
        
        $modelProductComment = ProductsComments::find()->where(['product_id' => $id,'parent_id' => null])->orderBy('created_at DESC')->all();
        $modelNewProductComment = new ProductsComments();
        return $this->render('view', [
            'model' => $model,
            'modelNewProductComment' => $modelNewProductComment,
            'modelProductComment' => $modelProductComment,
        ]);
    }

    public function actionGetProductData($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = ['status' => false, 'data' => null];
        
        $data = [];
        $model = Products::findOne(['id' => $id]);
        if ($model) {
            $data['name'] = $model->name;
            $data['img'] = $model->getImageSrc();
            $data['date'] = $model->getCreatedDate();
            $data['link'] = $model->getLink();
            $data['like'] = $model->getLikeCount();
            $data['view'] = $model->getViewCount();
            $data['comment'] = $model->getCommentCount();
            $data['collection'] = $model->getCollectionCount();
            $result = [
                'status' => true,
                'data' => $data
            ];
        }

        return $result;
    }
    
    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionCreateProductComment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $modelNewProductComment = new ProductsComments();
        $modelNewProductComment->description = $_POST['description'];
        $modelNewProductComment->product_id = $_POST['product_id'];
        $modelNewProductComment->user_id = \Yii::$app->user->id;
        $modelNewProductComment->parent_id = $_POST['parent_id'];
        $result = [];
        if($modelNewProductComment->save()){
            $result['status'] = true;
            $result['comment'] = [
                'id' => $modelNewProductComment->id,
                'description' => $modelNewProductComment->description,
                'user_id' => $modelNewProductComment->user_id,
                'created_at' => $modelNewProductComment->created_at,
                'username' => $modelNewProductComment->user->getNameAndSurname(),
            ];
        }else{
            $result['comment'] = false;
            $result['status'] = false;
        }
        return $result;
    }
    
    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
