<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use common\models\Likes;
use frontend\models\ProductsLikesSearch;
use frontend\models\PublicationsFollowingSearch;
use frontend\models\BrandsFollowingSearch;
use common\models\Collections;
use common\models\Products;
use common\models\ProductsCollections;
use frontend\models\CollectionsFollowingSearch;
use frontend\models\MyCollectionsSearch;
use frontend\models\TrendsFollowingSearch;
use yii\web\NotFoundHttpException;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
//        if (\Yii::$app->user->can('user')){
            $this->enableCsrfValidation = false;
//        } else {
//            throw new \yii\web\NotFoundHttpException();
//        }
        return $this;
    }
    
    public function actionIndex()
    {
        return $this->redirect(['likes']);
    }
    
    public function actionLikes()
    {
        $model = $this->findModel();
        
        $searchModel = new ProductsLikesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('likes', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionFollowingAdvertisers()
    {
        $model = $this->findModel();
        
        $searchModel = new BrandsFollowingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('following-advertisers', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionFollowingPublications()
    {
        $model = $this->findModel();
        
        $searchModel = new PublicationsFollowingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('following-publications', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionFollowingCollections()
    {
        $model = $this->findModel();
        
        $searchModel = new CollectionsFollowingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('following-collections', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMyCollections()
    {
        $model = $this->findModel();
        
        $searchModel = new MyCollectionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('my-collections', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionFollowingTrends()
    {
        $model = $this->findModel();
        
        $searchModel = new TrendsFollowingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('following-trends', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreateCollection()
    {
        $this->layout = '/iframe';
        $create = '';
        $model = new Collections();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = \Yii::$app->user->id;
            if ($model->save()) {
                $create = 'create';
                return $this->render('_close');
                \Yii::$app->session->setFlash('collection_save');
            } else {
                \Yii::$app->session->setFlash('collection_not_save');
            }
        }
        return $this->render('_create-collection', [
            'model' => $model,
            'create' => $create,
        ]);
    }
    
    public function actionAddMyCollection()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $res = ['status' => false, 'id' => 0, 'message' => ''];
        if (\Yii::$app->request->isPost && \Yii::$app->request->post('value')) {
            $model = Collections::findOne(['user_id' => \Yii::$app->user->id, 'name' => \Yii::$app->request->post('value')]);
            if (!$model) {
                $model = new Collections();
                $model->user_id = \Yii::$app->user->id;
                $model->name = \Yii::$app->request->post('value');
                if($model->save()){
                    $res['status']= true;
                    $res['id']= $model->id;
                    $res['message']= 'Collection created';
                }
            } else {
                $res['status']= false;
                $res['message']= 'Collection with that name already exists';
            }
        } else {
            $res['status']= false;
            $res['message']= 'Error';
        }
        return $res;
    }
    
    public function actionGetCollectionChoise($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $res = ['status' => false, 'collectionArray' => [], 'choiseCollectionArray' => []];
        $modelProduct = Products::findOne($id);
        if ($modelProduct) {
            $choiseCollectionsArray = Collections::getMyChoiseForProduct($id);
            $collectionsArray = Collections::getMyInArrayMap();
            $res = [
                'status' => true, 
                'choiseCollectionArray' => $choiseCollectionsArray,
                'collectionArray' => $collectionsArray
            ];
        }
        
        return $res;
    }

    public function actionSaveCollectionChoise($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $res = ['status' => false, 'count' => 0];
        if (\Yii::$app->request->isPost && \Yii::$app->request->post('arrayCollection')) {
            ProductsCollections::deleteAll(['product_id' => $id, 'user_id' => \Yii::$app->user->id]);
            $arrayCollection = \Yii::$app->request->post('arrayCollection');
            foreach ($arrayCollection as $collection_id) {
                $model = new ProductsCollections();
                $model->collection_id = $collection_id;
                $model->user_id = \Yii::$app->user->id;
                $model->product_id = $id;
                $model->save();
            }
            $res = ['status' => true, 'count' => Collections::getMyChoiseForProductCount($id)];
        }
        
        return $res;
    }
    
    public function actionAddToCollection($id)
    {
        $this->layout = '/iframe';
        $modelProduct = Products::findOne($id);
        
        if (Yii::$app->request->isPost) {
            if (Yii::$app->request->post('collections')) {
                Collections::deleteMyChoiseForProduct($id);
                
                foreach (Yii::$app->request->post('collections') as  $collection_id) {
                    $model = new ProductsCollections();
                    $model->product_id = $id;
                    $model->collection_id = $collection_id;
                    $model->save();
                }
                return $this->render('_close');
                \Yii::$app->session->setFlash('collection_save');
            } else {
                \Yii::$app->session->setFlash('collection_not_save');
            }
        }
        
        $choiseCollectionsArray = Collections::getMyChoiseForProduct($id);

        $collectionsArray = Collections::getMyInArrayMap();
        return $this->render('_add-to-collection', [
            'choiseCollectionsArray' => $choiseCollectionsArray,
            'modelProduct' => $modelProduct,
            'collectionsArray' => $collectionsArray,
        ]);
    }
    
    
    public function actionUpdate()
    {
        $model = $this->findModel();

        if (Yii::$app->request->isPost) {
            if (Yii::$app->request->post('updatePassword') === '') {
                $old_password = Yii::$app->request->post('User')['old_password'];
                if ($model->load(Yii::$app->request->post()) && $model->validatePassword($old_password)) {
                    $model->setPassword($model->password);
                    if ($model->save()) {
                        \Yii::$app->session->setFlash('passwordSave');
                        $model->old_password = null;
                        $model->password = null;
                        $model->password_repeat = null;
                    }
                } else {
                    $model->password = null;
                    $model->password_repeat = null;
                    \Yii::$app->session->setFlash('passwordNotSave');
                }
            } else {
                $model->scenario = 'user_data';
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    \Yii::$app->session->setFlash('dataSave');
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete()
    {
        $this->findModel()->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel()
    {
        if (($model = User::findOne(Yii::$app->user->id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
