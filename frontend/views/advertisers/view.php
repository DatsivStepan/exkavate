<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Likes;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model common\models\Brands */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog">
    <div class="container">
        <div class="category-index">
            <div class="">
                <div class="col_sm-12 catal_cat_public-view padding_0">
                    <div class="public_view-img">
                        <div class="block-pub-left">
                            <a href="<?= $model->getLink(); ?>">
                                <img src="<?= $model->getImageSrc(); ?>">
                            </a>
                            <span class="div_catal_public-view">
                                    <img class="class-img-public-view" src="/images/pub1.png">
                                    <span class="pad_span_right">
                                       <?= $model->getViewsCount(); ?>
                                    </span>
                                </span>
                        </div>
                        <div class="block-pub-right">
                            <span class="a_name-adv"><?= $model->name; ?></span>
                            <span class="a_name-categ-pub"><?= $model->getCategoryName(); ?></span>
                            <?php if (!\Yii::$app->user->isGuest) { ?>
                                <?php if ($model->getFollowStatus()) { ?>
                                    <button class="btn js-follow public_bottom113 active" data-type="brand"
                                            data-object_id="<?= $model->id; ?>"><span
                                                class="follow">Unfollow</span>
                                    </button>
                                <?php } else { ?>
                                    <button class="btn js-follow public_bottom113 not_active" data-type="brand"
                                            data-object_id="<?= $model->id; ?>"><span
                                                class="follow">Follow</span>
                                    </button>
                                <?php } ?>
                            <?php } else { ?>
                                <button class="btn not_active js-login-modal"><span class="follow">Follow</span>
                                </button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog">
    <div class="container">
        <div class="category-index row">
            <?= $this->render('/category/_search', ['model' => $searchModel, 'modelCategories' => $modelCategories]); ?>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog back_gray">
    <div class="container">
        <div class="category-index row">
            <div class='col-lg-2 col-md-3 col-sm-3 padding-right-null pos-n-categ'>
                <?= $this->render('/category/_lelf_sidebar_categories', ['model' => $modelCategories, 'category_id' => $searchModel->category_id ? $searchModel->category_id : []]); ?>
            </div>
            <div class='col-lg-10 col-md-9 col-sm-9'>
                <div class="col-ld-12 padding_0 wight_med_catal">
                    <?php foreach ($dataProvider->getModels() as $product) { ?>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-container-padding product-one-all-no-home">
                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                        </div>
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                </div>
                <div class="bottom_pagination" style="clear: both;display: block">
                    <div class="col-sm-2 col-xs-12" style="padding:0px;">
                        <?= Html::dropDownList('paginCount',
                            $searchModel->paginCount,
                            \frontend\models\ProductsSearch::$arrayPagination,
                            [
                                'class' => 'form-control js-pagin-count-select class-plag-count',
                                'style' => 'margin:20px 0 45px 0;',
                            ]) ?>
                    </div>
                    <div class="col-sm-10 col-xs-12"
                        <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                    </div>
            </div>
        </div>

    </div>
</div>
</div>
