<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Likes;
use common\models\Configuration;
use yii\widgets\LinkPager;
use frontend\assets\HomeAsset;
use common\models\Follower;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AdvertisersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
HomeAsset::register($this);

$this->title = Yii::t('app', 'Advertisers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <div class="row">
            <div class="mar_bat-crie"></div>
        </div>
        <div class="col-sm-12 padding_0">
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <?php foreach ($dataProvider->getModels() as $brand) { ?>
                        <div class="row row_mar_00">
                            <div class="block-left-brand padding-collec-nul">
                                <?= $this->render('//templates/_brand', ['brand' => $brand]) ?>
                            </div>
                            <div class="block-right-brand whit_co2 block-band-rig">
                                <div class="owl-carousel owl-theme owl-loaded js-products-carousel"
                                     data-slides="<?= count($brand->getBrandProductWithLimit(12)); ?>"
                                     data-id="<?= $brand->id ?>" id="owl-products<?= $brand->id ?>">
                                    <?php foreach ($brand->getBrandProductWithLimit(12) as $product) { ?>
                                        <div class="item product-one-all-no-home product-one-all-adverts">
                                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!--                            <a class="a_colecction a_col3 a_col3_coll2" href="-->
                            <? //= $brand->getLink(); ?><!--"><img src="/images/ar_lef1.png">View all</a>-->
                        </div>
                        <hr class="hidden-xs hr-product-col">
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".js-products-carousel").each(function () {
            var id = $(this).data("id");
            var slides = $(this).data("slides");
            var width = $( window ).width();
            $('#owl-products' + id).owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: true,
                center: (slides == 1 && width <= 768)?true:false,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 2
                    },
                    1200: {
                        items: 4
                    }
                }
            });
        });
    });
</script>