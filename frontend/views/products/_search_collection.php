<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollectionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contant">
    <div class="row height_row">
        <?php $search_ads = '?search-collection=' . (isset($_GET['search-collection']) ? $_GET['search-collection'] : ''); ?>
        <div class="whith_and_marg_clas">
            <div class="div-float-left"><span class="span_text_sort">Sort:</span><span class="span_text_by">By</span></div>
            <div class="div-float-left">
                <?php $href = 'created_at'; ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <?php if ($_GET['sort'] == 'created_at') { ?>
                        <?php $href = '-created_at'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_top"></span></a>
                    <?php } elseif ($_GET['sort'] == '-created_at') { ?>
                        <?php $href = 'created_at'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_bottom"></span></a>
                    <?php } ?>
                <?php } ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>">
                    <span class="span_text_by_date">Date</span>
                </a>
            </div>
            <div class="div-float-left">
                <?php $href = 'likesCount'; ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <?php if ($_GET['sort'] == 'likesCount') { ?>
                        <?php $href = '-likesCount'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_top"></span></a>
                    <?php } elseif ($_GET['sort'] == '-likesCount') { ?>
                        <?php $href = 'likesCount'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_bottom"></span></a>
                    <?php } ?>
                <?php } ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>">
                    <span class="span_text_comments bold">Oks</span>
                </a>
            </div>
            <div class="div-float-left">
                <?php $href = 'viewsCount'; ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <?php if ($_GET['sort'] == 'viewsCount') { ?>
                        <?php $href = '-viewsCount'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_top"></span></a>
                    <?php } elseif ($_GET['sort'] == '-viewsCount') { ?>
                        <?php $href = 'viewsCount'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_bottom"></span></a>
                    <?php } ?>
                <?php } ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>">
                    <span class="span_text_comments">Views</span>
                </a>
            </div>
            <div class="div-float-left">
                <?php $href = 'uploadCount'; ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <?php if ($_GET['sort'] == 'uploadCount') { ?>
                        <?php $href = '-uploadCount'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_top"></span></a>
                    <?php } elseif ($_GET['sort'] == '-uploadCount') { ?>
                        <?php $href = 'uploadCount'; ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_bottom"></span></a>
                    <?php } ?>
                <?php } ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>">
                    <span class="span_text_comments">Uploads</span>
                </a>
            </div>
        </div>
    </div>
</div>
