<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Likes;
use common\models\Follower;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Configuration;
use common\components\BootstrapModal;
use app\assets\ProductViewerAsset;

ProductViewerAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;

$modelSetting = ArrayHelper::map(Configuration::find()->all(), 'key', 'value');
BootstrapModal::widget([
    ['id' => 'create-collection-modal', 'header' => 'Create collection', 'footer' => true, 'size' => BootstrapModal::SIZE_LARGE],
]);
?>
<div class="backgroung_page_catalog back_page-product-view">
    <div class="container container_product-view">
        <div class="dis_web">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 padding_0">
                <div class="left_product-block">
                    <?php if ($model->brandModel) { ?>
                    <div class="hidden-lg hidden-md bord_boot ">
                        <div class="clas_opus_adver">
                                <span class="sp_adv_opus">
                                    <?= date('M d, Y', strtotime($model->created_at)); ?>
                                </span>
                        </div>
                    </div>
                    <div class="hidden-lg hidden-md  bord_boot-advert">
                        <div class="div_img_2">
                            <a href="<?= $model->brandModel->getLink(); ?>">
                                <img class="cla_im_advert" src="<?= $model->brandModel->getImageSrc(); ?>">
                            </a>
                            <div style="clear: both"></div>
                        </div>
                        <div class="float_le_advert_hover" style="width: 100%">
                            <p>
                                <a href="<?= $model->brandModel->getLink(); ?>">
                                    <?= $model->brandModel->name; ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="img-product">
                        <?= $this->render('_view_pdf', ['model' => $model]); ?>
                    </div>
                    <div class="border-righ">
                        <div class="advert_div-left_block">
                            <div class="float_advert_ico-left">
                                <a target="_blank" href="<?= $modelSetting['facebook']; ?>">
                                    <img class="" src="/images/fecbook.png">
                                </a>
                                <a target="_blank" href="<?= $modelSetting['twitter']; ?>">
                                    <img class="" src="/images/twiter.png">
                                </a>
                            </div>
                            <div class="float_advert_link-rigth">
                                <?php $class = !\Yii::$app->user->isGuest ? 'js-add-to-collection-modal' : 'js-login-modal' ?>
                                <a>
                                    <img class="" src="/images/icon_plus.png" data-product_id="<?= $model->id ?>">
                                    <span class="pad_span_right pad_collec_icon <?= $class; ?>"
                                          data-product_id="<?= $model->id ?>">Add to collection</span>
                                </a>
                                <a class="embed-wrap hidden-xs">
                                    <img class="" src="/images/dild.png">
                                    <span class="pad_span_right pad_collec_icon js-copy-link" 
                                          data-link="<?= $model->getEmbedLink(); ?>" data-product_id="<?= $model->id; ?>">Copy Link</span>
                                    <div class="embed-box">
                                        <div class="title">Embed</div>
                                        <textarea class="source-code"></textarea>
                                        <div class="embed-panel">
                                            <div class="fields clearfix">
                                                <div class="group-width">
                                                    <label for="width">Width</label>
                                                    <input type="text" id="width" value="100%">
                                                </div>
                                                <div class="group-height">
                                                    <label for="height">Height</label>
                                                    <input type="text" id="height" value="500">
                                                </div>
                                            </div>
                                        </div>
                                        <button class="embed-close">X</button>
                                    </div>
                                </a>
                                <div class="like-block-right">
                                    <?php if (!\Yii::$app->user->isGuest) { ?>
                                        <?php if ($model->brandModel->getLikeStatus()) { ?>
                                            <button class="btn js-like active" data-type="<?= Likes::TYPE_BRAND; ?>"
                                                    data-object_id="<?= $model->brandModel->id; ?>"></button>
                                        <?php } else { ?>
                                            <button class="btn js-like not_active" data-type="<?= Likes::TYPE_BRAND; ?>"
                                                    data-object_id="<?= $model->brandModel->id; ?>"></button>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <button class="btn not_active js-login-modal"></button>
                                    <?php } ?>
                                    <span class="pad_like_icon js-like-count">
                                        <?= $model->brandModel->getLikesCount(); ?>
                                    </span>
                                </div>
                                <div class="hidden-lg hidden-md bord_boot-advert">
                                    <div class="follow-advert-button">
                                        <?php if (!\Yii::$app->user->isGuest) { ?>
                                            <?php if (Follower::getFollowerStatus($model->brandModel->id, Follower::TYPE_BRAND)) { ?>
                                                <button class="btn js-follow active public11_bottom22" data-type="brand"
                                                        data-object_id="<?= $model->brandModel->id; ?>"><span
                                                            class="follow">Following</span> advertiser
                                                </button>
                                            <?php } else { ?>
                                                <button class="btn js-follow not_active" data-type="brand"
                                                        data-object_id="<?= $model->brandModel->id; ?>"><span
                                                            class="follow">Follow</span> advertiser
                                                </button>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <button class="btn not_active js-login-modal"><span
                                                        class="follow">Follow</span>
                                                advertiser
                                            </button>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="hidden-lg hidden-md bord_boot-cat">
                                    <div class="div_fash">
                                        <div class="clas_img-categ-name">
                                            <img class="" src="/images/trt.png">
                                            <span class="pad_fash_icon">
                                    <?= $model->category ? HTML::a($model->category->name, $model->category->getLink()) : ''; ?>
                                </span>
                                        </div>
                                        <div class="clas_tags-product">
                                            <div class="pad_tags_name">
                                                <?php if ($model->productsTagsModel) { ?>
                                                    <?php foreach ($model->productsTagsModel as $key => $productTags) { ?>
                                                        <?= ($key != 0 ? ' ' : '') .
                                                        '<span href="' . $productTags->getLink() . '">' . $productTags->name . '</span>'; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="adv_class_div-icon">
                                            <?php $class = ''; ?>
                                            <?php if (!\Yii::$app->user->isGuest) { ?>
                                                <?php if ($model->getLikeStatus()) { ?>
                                                    <?php $class = 'js-like active'; ?>
                                                <?php } else { ?>
                                                    <?php $class = 'js-like not_active'; ?>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php $class = 'js-login-modal'; ?>
                                            <?php } ?>
                                            <a class="icon_like <?= $class; ?>" data-title="Like"
                                               data-type="<?= Likes::TYPE_PRODUCT; ?>"
                                               data-object_id="<?= $model->id; ?>">
                                                <img class="img_col" src="/images/pub3.png">
                                                <span class="js-like-count">
                                        <?= $model->getLikeCount(); ?>
                                    </span>
                                            </a>
                                            <a>
                                                <img class="style_img_col" src="/images/col1.png">
                                            </a>
                                            <span class="">
                                    <?= $model->getViewCount(); ?>
                                </span>
                                            <a>
                                                <img class="style_img_mess"
                                                     style="width: 19px;height: 16px;position: relative;top: 2px;"
                                                     src="/images/maseg.png">
                                            </a>
                                            <span class="">
                                    <?= $model->getCommentCount(); ?>
                                </span>
                                            <a href="<?= $model->getLink(); ?>">
                                                <img class="" src="/images/pub2.png">
                                            </a>
                                            <span class="">
                                    <?= $model->getCollectionCount(); ?>
                                </span>
                                            <a class="js-my-collection-<?= $model->id; ?> <?= \Yii::$app->user->isGuest ? 'js-login-modal' : 'add-to-collection'; ?> "
                                               data-product_id="<?= $model->id; ?>">
                                                <img class="img_ico img_ico-last" src="/images/+.png">
                                                <span class="pad_span2_right_222 pad_span23_right_22225 js-my-collection-count">
                                        <?= \Yii::$app->user->isGuest ? 0 : $model->getMyCollectionCount(); ?>
                                    </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="hidden-lg hidden-md bord_boot-public">
                                    <div class="div_right_opt">
                                        <div class="public-img-folow">
                                            <?php if ($model->companyModel) { ?>
                                                <a href="<?= $model->companyModel->getLink() ?>">
                                                    <img class="img_veng"
                                                         src="<?= $model->companyModel->getImageSrc(); ?>">
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <div class="follow-advert-button">
                                            <?php if (!\Yii::$app->user->isGuest && $model->companyModel) { ?>
                                                <?php if (Follower::getFollowerStatus($model->companyModel->id, Follower::TYPE_COMPANY)) { ?>
                                                    <button class="btn js-follow active"
                                                            data-type="<?= Follower::TYPE_COMPANY; ?>"
                                                            data-object_id="<?= $model->companyModel->id; ?>">
                                                        <span class="follow">Following</span> publication
                                                    </button>
                                                <?php } else { ?>
                                                    <button class="btn js-follow not_active"
                                                            data-type="<?= Follower::TYPE_COMPANY; ?>"
                                                            data-object_id="<?= $model->companyModel->id; ?>">
                                                        <span class="follow">Follow</span> publication
                                                    </button>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <button class="btn not_active js-login-modal">
                                                    <span class="follow">Follow</span> publication
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="related-adverts-block">
                            <div class="releat_adverts">Related Adverts</div>
                            <?= frontend\widgets\RecentProduct::widget(['limit' => 3]); ?>
                        </div>
                        <div class="block_advert_comments">
                            <div class="col_cole_f">
                                <?php $form = ActiveForm::begin(['id' => 'add-comment-form', 'enableAjaxValidation' => false, 'enableClientValidation' => false, 'validateOnSubmit' => false]); ?>
                                <div class="coments_ww">
                                    <div class="coments_1">
                                        <span>Comments</span>
                                    </div>
                                    <div class="col-lg-12 padding_0 coment_border">
                                        <div class="clas_bord1"></div>
                                        <div class="clas_bord2"></div>
                                    </div>
                                </div>
                                <?php if (!\Yii::$app->user->isGuest) { ?>
                                    <?= $form->field($modelNewProductComment, 'product_id')->hiddenInput(['value' => $model->id])->label(false) ?>
                                    <?= $form->field($modelNewProductComment, 'description')->textarea(['maxlength' => true, 'placeholder' => 'Join the discussion...'])->label(false) ?>
                                <?php } else { ?>
                                    <div class="cursor-none">
                                        <?= $form->field($modelNewProductComment, 'product_id')->hiddenInput(['value' => $model->id])->label(false) ?>
                                        <?= $form->field($modelNewProductComment, 'description')->textarea(['maxlength' => true, 'placeholder' => 'Join the discussion...'])->label(false) ?>
                                    </div>
                                <?php } ?>
                                <?php if (!\Yii::$app->user->isGuest) { ?>
                                    <div style="display: block">
                                        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary bat_comm_creat']) ?>
                                    </div>
                                <?php } else { ?>
                                    <div style="display: block">
                                        <?= Html::a(Yii::t('app', 'Create'), '', ['class' => 'btn btn-primary js-login-modal pull-right bat_comm_creat']) ?>
                                    </div>
                                <?php } ?>
                                <?php ActiveForm::end(); ?>
                                <div class="js-comment-container comment_mar">
                                    <?php foreach ($modelProductComment as $comment) { ?>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="js-comment">
                                                    <a href="#" class="user_name">
                                                        <?= $comment->user ? $comment->user->getNameAndSurname() : 'John Doe'; ?>
                                                    </a>
                                                    <span class="point"> </span>
                                                    <span>
                                                          <?= $comment->created_at; ?>
                                                        </span>
                                                    <p>
                                                        <?= $comment->description; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-11 col-xs-12 no-padding poz-absol-top">
                <?php if ($model->brandModel) { ?>
                    <div class="bord_boot">
                        <div class="clas_opus_adver">
                                <span class="sp_adv_opus">
                                    <?= date('M d, Y', strtotime($model->created_at)); ?>
                                </span>
                        </div>
                        <div class="opus_adver-product">
                            <?php if ($model->parametr1) { ?>
                                <div class="advert-page-detal">Advert Page DetaIls</div>
                                <div class="page-size">
                                    <span class="page-siz-left div-float-left">
                                          <?= $model->getAttributeLabel('parametr1'); ?>: </span>
                                    <span class="div-float-left">
                                          <?= $model->parametr1; ?>
                                    </span>
                                </div>
                            <?php } ?>
                            <?php if ($model->parametr2) { ?>
                                <div class="page-back-gray">
                                    <span class="page-siz-left div-float-left">
                                          <?= $model->getAttributeLabel('parametr2'); ?>: </span>
                                    <div class="div-float-left">
                                        <?= $model->parametr2; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($model->parametr3) { ?>
                                <div class="page-size">
                                    <span class="page-siz-left div-float-left">
                                          <?= $model->getAttributeLabel('parametr3'); ?>: </span>
                                    <span class="div-float-left">
                                          <?= $model->parametr3; ?>
                                </div>
                            <?php } ?>
                            <?php if ($model->parametr4) { ?>
                                <div class="page-back-gray">
                                    <span class="page-siz-left div-float-left">
                                            <?= $model->getAttributeLabel('parametr4'); ?>: </span>
                                    <div class="div-float-left">
                                        <?= $model->parametr4; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($model->parametr3) { ?>
                                <div class="page-size">
                                    <span class="page-siz-left div-float-left">
                                          <?= $model->getAttributeLabel('parametr3'); ?>: </span>
                                    <span class="div-float-left">
                                          <?= $model->parametr3; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="hidden-sm hidden-xs bord_boot-cat">
                        <div class="div_fash">
                            <div class="clas_img-categ-name">
                                <img class="" src="/images/trt.png">
                                <span class="pad_fash_icon">
                                    <?= $model->category ? HTML::a($model->category->name, $model->category->getLink()) : ''; ?>
                                </span>
                            </div>
                            <div class="clas_tags-product">
                                <div class="pad_tags_name">
                                    <?php if ($model->productsTagsModel) { ?>
                                        <?php foreach ($model->productsTagsModel as $key => $productTags) { ?>
                                            <?= ($key != 0 ? ' ' : '') .
                                            '<a href="' . $productTags->getLink() . '">' . $productTags->name . '</a>'; ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="adv_class_div-icon">
                                <?php $class = ''; ?>
                                <?php if (!\Yii::$app->user->isGuest) { ?>
                                    <?php if ($model->getLikeStatus()) { ?>
                                        <?php $class = 'js-like active'; ?>
                                    <?php } else { ?>
                                        <?php $class = 'js-like not_active'; ?>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?php $class = 'js-login-modal'; ?>
                                <?php } ?>
                                <a class="icon_like <?= $class; ?>" data-title="Like"
                                   data-type="<?= Likes::TYPE_PRODUCT; ?>" data-object_id="<?= $model->id; ?>">
                                    <img class="img_col" src="/images/pub3.png">
                                    <span class="js-like-count">
                                        <?= $model->getLikeCount(); ?>
                                    </span>
                                </a>
                                <a>
                                    <img class="style_img_col" src="/images/col1.png">
                                </a>
                                <span class="">
                                    <?= $model->getViewCount(); ?>
                                </span>
                                <a>
                                    <img class="style_img_mess"
                                         style="width: 19px;height: 16px;position: relative;top: 2px;"
                                         src="/images/maseg.png">
                                </a>
                                <span class="">
                                    <?= $model->getCommentCount(); ?>
                                </span>
                                <a href="<?= $model->getLink(); ?>">
                                    <img class="" src="/images/pub2.png">
                                </a>
                                <span class="">
                                    <?= $model->getCollectionCount(); ?>
                                </span>
                                <a class="js-my-collection-<?= $model->id; ?> <?= \Yii::$app->user->isGuest ? 'js-login-modal' : 'add-to-collection'; ?> "
                                   data-product_id="<?= $model->id; ?>">
                                    <img class="img_ico img_ico-last" src="/images/+.png">
                                    <span class="pad_span2_right_222 pad_span23_right_22225 js-my-collection-count">
                                        <?= \Yii::$app->user->isGuest ? 0 : $model->getMyCollectionCount(); ?>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="hidden-sm hidden-xs bord_boot-advert">
                        <div class="div_img_2">
                            <a href="<?= $model->brandModel->getLink(); ?>">
                                <img class="cla_im_advert" src="<?= $model->brandModel->getImageSrc(); ?>">
                            </a>
                            <div style="clear: both"></div>
                        </div>
                        <div class="float_le_advert_hover" style="width: 100%">
                            <p>
                                <a href="<?= $model->brandModel->getLink(); ?>">
                                    <?= $model->brandModel->name; ?>
                                </a>
                            </p>
                        </div>
                        <div class="follow-advert-button">
                            <?php if (!\Yii::$app->user->isGuest) { ?>
                                <?php if (Follower::getFollowerStatus($model->brandModel->id, Follower::TYPE_BRAND)) { ?>
                                    <button class="btn js-follow active public11_bottom22" data-type="brand"
                                            data-object_id="<?= $model->brandModel->id; ?>"><span
                                                class="follow">Following</span> advertiser
                                    </button>
                                <?php } else { ?>
                                    <button class="btn js-follow not_active" data-type="brand"
                                            data-object_id="<?= $model->brandModel->id; ?>"><span
                                                class="follow">Follow</span> advertiser
                                    </button>
                                <?php } ?>
                            <?php } else { ?>
                                <button class="btn not_active js-login-modal"><span class="follow">Follow</span>
                                    advertiser
                                </button>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="hidden-xs hidden-sm bord_boot-public">
                    <div class="div_right_opt">
                        <div class="public-img-folow">
                            <?php if ($model->companyModel) { ?>
                                <a href="<?= $model->companyModel->getLink() ?>">
                                    <img class="img_veng" src="<?= $model->companyModel->getImageSrc(); ?>">
                                </a>
                            <?php } ?>
                        </div>
                        <div class="follow-advert-button">
                            <?php if (!\Yii::$app->user->isGuest && $model->companyModel) { ?>
                                <?php if (Follower::getFollowerStatus($model->companyModel->id, Follower::TYPE_COMPANY)) { ?>
                                    <button class="btn js-follow active"
                                            data-type="<?= Follower::TYPE_COMPANY; ?>"
                                            data-object_id="<?= $model->companyModel->id; ?>">
                                        <span class="follow">Following</span> publication
                                    </button>
                                <?php } else { ?>
                                    <button class="btn js-follow not_active"
                                            data-type="<?= Follower::TYPE_COMPANY; ?>"
                                            data-object_id="<?= $model->companyModel->id; ?>">
                                        <span class="follow">Follow</span> publication
                                    </button>
                                <?php } ?>
                            <?php } else { ?>
                                <button class="btn not_active js-login-modal">
                                    <span class="follow">Follow</span> publication
                                </button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.js-add-to-collection-modal', function () {
        BootstrapModal(
            'create-collection-modal',
            'Create collection',
            BootstrapModalIFrame('/profile/add-to-collection?id=<?= $model->id; ?>', 300)
        );
    });
    $('#create-collection-modal').on('hidden.bs.modal', function () {
        var text = $('#create-collection-modal iframe').contents().find('.js-status-create').text();
        if (text == 'create') {
            location.reload();
        }
    })

    $(document).on('click', '#create-collection-modal .js-modal-save', function (e) {
        $('#create-collection-modal iframe').contents().find('.js-form-button-hide').trigger('click');
    });
</script>