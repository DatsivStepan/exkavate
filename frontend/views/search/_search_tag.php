<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contant">
    <div class="row height_row">
        <?php $search_ads = '?search-tag=' . (isset($_GET['search-tag']) ? $_GET['search-tag'] : ''); ?>
        <div>
            <div class="div-float-left"><span class="span_text_sort">Sort:</span><span class="span_text_by">By</span></div>
            <div class="div-float-left">
                <?php $href = 'created_at'; ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <?php if ($_GET['sort'] == 'created_at') { ?>
                        <?php $href = '-created_at'; ?>
                        <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_top"></span></a>
                    <?php } elseif ($_GET['sort'] == '-created_at') { ?>
                        <?php $href = 'created_at'; ?>
                        <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_bottom"></span></a>
                    <?php } ?>
                <?php } ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>">
                    <span class="span_text_by_date">Date</span>
                </a>
            </div>
            <div class="div-float-left">
                <?php $href = 'commentsCount'; ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <?php if ($_GET['sort'] == 'commentsCount') { ?>
                        <?php $href = '-commentsCount'; ?>
                        <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_top44"></span></a>
                    <?php } elseif ($_GET['sort'] == '-commentsCount') { ?>
                        <?php $href = 'commentsCount'; ?>
                        <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_bottom44"></span></a>
                    <?php } ?>
                <?php } ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>">
                    <span class="span_text_comments bold">Comments</span>
                </a>
            </div>
            <div class="div-float-left">
                <?php $href = 'viewsCount'; ?>
                <?php if (isset($_GET['sort'])) { ?>
                    <?php if ($_GET['sort'] == 'viewsCount') { ?>
                        <?php $href = '-viewsCount'; ?>
                        <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_top44"></span></a>
                    <?php } elseif ($_GET['sort'] == '-viewsCount') { ?>
                        <?php $href = 'viewsCount'; ?>
                        <a href="<?= $search_ads; ?>&sort=<?= $href; ?>"><span class="span_arrow_bottom44"></span></a>
                    <?php } ?>
                <?php } ?>
                <a href="<?= $search_ads; ?>&sort=<?= $href; ?>">
                    <span class="span_text_comments">Views</span>
                </a>
            </div>
        </div>
    </div>
</div>
