<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;
use common\models\Likes;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Search - ' . (isset($_GET['search-ads']) ? $_GET['search-ads'] : '');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog background-margin-top">
    <div class="container">
        <div class="category-index row">
            <div class="input-group col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 col-xs-12 inpur-search-gro">
                <input type="text" class="form-control js-page-header-search-value"
                       value="<?= $searchModel->search_value ?>" placeholder="Name" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2"></span>
            </div>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog back_gray">
    <div class="container">
        <div class="category-index row">
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-3 col-sm-3 padding-right-null pos-n-categ">
                <?= $this->render('_search', [
                    'model' => $searchModel,
                ]); ?>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-9">
                <div class="col-ld-12 padding_0 wight_med_catal">
                    <?php foreach ($dataProvider->getModels() as $product) { ?>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-container-padding product-one-all-no-home">
                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                        </div>
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                </div>
                <div class="bottom_pagination" style="clear: both;display: block">
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $(".span_arrow_top").click(function () {
            $(".span_arrow_top").toggleClass("span_arrow_bottom2");
        });

        $("#buttom_search").click(function () {
            $("#textSearch").val("");
            $("#textSearch").focus();
        });

        $(document).on('change', '.js-page-header-search-value', function () {
            $('.js-form-search-value').val($(this).val()).trigger('change')
        })
    });
</script>