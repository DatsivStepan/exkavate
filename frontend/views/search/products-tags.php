<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;
use common\models\Likes;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Search - ' . (isset($_GET['search-ads']) ? $_GET['search-ads'] : '');
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_pag_cat">
    <div class="container">
        <div class="row">
            <h1 class="h1_search-tags">
                <?= $searchModel->tag; ?>
            </h1>
        </div>
        <div class="col-sm-12 padding_0">
            <?= $this->render('//category/_search', [
                'model' => $searchModel
            ]); ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="backgroung_page_catalog back_gray" style="padding-top: 29px">
    <div class="container">
        <div class="di2s_web222 roles_dis_row rig_collec_hom back_gray">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary rig_collec_hom padding_0">
                <div class="right_colum_collection rig_collec_hom row_col_ad">
                    <?php foreach ($dataProvider->getModels() as $product) { ?>
                        <div class="col-20-proz col-md-3 col-sm-4 col-xs-6 product-container-padding whidth_media_1400 product-one-all-no-home product-fiv-pading"
                             style="margin-bottom: 15px">
                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                        </div>
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                </div>
                <div class="bottom_pagination" style="clear: both;display: block">
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function () {

        $(this).find(".span_arrow_top").click(function () {
            $(".span_arrow_top").toggleClass("span_arrow_bottom2");
        });

    });
</script>