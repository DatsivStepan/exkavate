<?php

use yii\helpers\Html;
use common\models\Category;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use common\models\Company;
use frontend\models\ProductsSearch;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
$get = \Yii::$app->request->get();
$dates = ProductsSearch::$datesArray;
?>
<div class="clearfix"></div>
<div class="par_right-0">
    <hr class="hr-category hidden-xs">
    <hr class="hr-category2 hidden-lg hidden-md hidden-sm">
    <?php $form = ActiveForm::begin([
        'id' => 'js-product-search-form',
        'method' => 'get'
    ]); ?>
    <a class="category_name category-refine-by cat-reg-by" href="#1a" data-toggle="tab">
        REFINE BY <span class="clear-all-category hidden-xs">Clear all</span>
        <div class="hidden-lg hidden-md hidden-sm arr_top">
        </div>
    </a>
    <div class="list_group_cat">
        <h4 class="dates-search">Dates</h4>
        <div class="sort-date-search">
            <div class="hide">
                <?= $form->field($model, 'dates')->dropDownList($dates, ['class' => 'js-dropdown-dates']); ?>
            </div>
            <?php foreach ($dates as $value => $text) { ?>
                <?php $class = $model->dates && $model->dates == $value ? 'active' : null; ?>
                <?= Html::a($text, null, ['class' => 'js-dates-sort ' . $class, 'data-value' => $value]); ?><br>
            <?php } ?>
        </div>


        <div class="clearfix"></div>
        <h4 class="dates-categ">Categories</h4>
        <div class="list-group">
            <?php foreach (Category::find()->all() as $category) { ?>
                <div class="<?php //= (in_array($category->id, $category_id)) ? 'active' : ''; ?> col_sidb">
                    <?= CheckboxX::widget([
                        'name' => 'ProductsSearch[category_id][' . $category->id . ']',
                        'value' => is_array($model->category_id) && in_array($category->id, $model->category_id) ? 1 : 0,
                        'options' => [
                            'id' => 'category_' . $category->id,
                            'class' => 'js-checkbox-category',
                            'data-category_id' => $category->id,
                        ],
                        'pluginOptions' => [
                            'threeState' => false,
                        ],
                    ]);
                    ?>
                    <a class="fonts_title_categ <?php //= (in_array($category->id, $category_id)) ? 'active' : ''; ?>">
                        <div class="icon_like2" data-title="<?= $category->name; ?>">
                            <?= $category->name; ?> <span>(<?= $category->getProductCount(); ?>)</span>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>

        <div class="clearfix"></div>
        <h4 class="dates-public">Publications</h4>
        <div class="list-group">
            <?php foreach (Company::find()->all() as $publication) { ?>
                <div class="<?php //= (in_array($category->id, $category_id)) ? 'active' : ''; ?> col_sidb">
                    <?= CheckboxX::widget([
                        'name' => 'ProductsSearch[publication_id][' . $publication->id . ']',
                        'value' => is_array($model->publication_id) && in_array($publication->id, $model->publication_id) ? 1 : 0,
                        'options' => [
                            'id' => 'publication_' . $publication->id,
                            'class' => 'js-checkbox-publication',
                            'data-publication_id' => $publication->id,
                        ],
                        'pluginOptions' => [
                            'threeState' => false,
                        ],
                    ]);
                    ?>
                    <a class="fonts_title_categ <?php //= (in_array($category->id, $category_id)) ? 'active' : ''; ?>">
                        <div class="icon_like2" data-title="<?= $publication->name; ?>">
                            <?= $publication->name; ?>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
        <div class="clear-all-category-xs hidden-lg hidden-md hidden-sm">Clear all</div>
        <hr class="category-hr-menu">
    </div>
    <div class="col-lg-10 col-md-9 col-sm-8 hidden-xs">
        <?= $form->field($model, 'search_value')->hiddenInput(['placeholder' => 'Search', 'class' => 'js-form-search-value'])->label(false) ?>
    </div>
    <?php $form = ActiveForm::end(); ?>

</div>
<script>
    $(function () {
        $(document).on('change', '.js-checkbox-publication, .js-checkbox-category, .js-dropdown-dates, .js-form-search-value', function () {
            $('#js-product-search-form').submit()
        })

        $(document).on('click', '.js-dates-sort', function () {
            $('.js-dropdown-dates option[value="' + $(this).data('value') + '"]').trigger('change')
        })
    })
    $(document).ready(function () {
        $(".arr_top").click(function () {
            $(".arr_top").toggleClass("arr_bot");
            $(".list_group_cat").toggleClass("list_group_category_display");
        });
    });
</script>