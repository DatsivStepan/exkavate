<?php

use frontend\widgets\HomeCategories;
use frontend\assets\HomeAsset;

HomeAsset::register($this);

/* @var $this yii\web\View */

$this->title = $model->name;
?>
<br>
<div class="container">
    <div class="section-title-about"><?= $model->name; ?><br></div>
    <div class="categories-box_about">
        <?php if ($model->slug == 'about') { ?>
            <img class="img-about-pag" src="/images/layer-2033.png">
        <?php } ?>
        <div class="col-sm-8 col-sm-offset-2">
            <?= $model->content; ?>
        </div>
    </div>
</div>