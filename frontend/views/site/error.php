<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">
    <div class="sec-h1-error"><h1>#404</h1><br></div>
    <div class="categories-box_about">
        <div class="col-sm-8 col-sm-offset-2 class-err">
            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>
            <br>
            <p>
                The above error occurred while the Web server was processing your request.
            </p>
            <p>
                Please contact us if you think this is a server error. Thank you!
            </p>
        </div>
    </div>
</div>
