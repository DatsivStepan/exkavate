<?php
use yii\helpers\Html;
use common\models\Configuration;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$modelSetting = ArrayHelper::map(Configuration::find()->all(), 'key', 'value');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<footer class="footer foor_hei_0">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-xs-center">
                <span class="footer-join-mail-title">Subscribe to the newsletter</span>        
            </div>
            <div class="col-md-6 custom-positin-relative">
                <?php $form = ActiveForm::begin(['id' => 'form-newslater2', 'enableAjaxValidation' => false, 'enableClientValidation' => false, 'validateOnSubmit' => false]); ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center footer-form-style">
                    <input class="js-newslater-email" placeholder="Email..."
                           required="required" type="email" name="email"
                           autocomplete="off">
                    <?= Html::submitButton('Subscribe', ['class' => 'btn btn-primary ', 'name' => 'newslater-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="alert js-alert-newslater-status alert-dismissible alert_fade_sign_up alert-subcribe-footer fade"
                     role="alert">
                        <span class="message">
                            <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                        </span>
                    <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="col-md-3 text-right text-xs-center">
                <span class="footer-right-text-style">Follow Exkavate</span>
                <a target="_blank" href="<?= $modelSetting['facebook']; ?>">
                    <img src="/images/fb-icon-copy.png" class="footer-icon-style"></img>
                </a>
                <a target="_blank" href="<?= $modelSetting['twitter']; ?>">
                    <img src="/images/tw-icon-copy.png" class="footer-icon-style"></img>
                </a>   
            </div>
        </div>
    </div>
    <div class="footer-middle-line"></div>
    <div class="container">    
        <div class="row">
            <div class="col-md-3 text-left text-xs-center">
                <a href="/">
                    <img class="footer-logo-style" src="/images/exkavate21.png" style="height: 23px;"></img>
                </a>
            </div>
            <div class="col-md-6 text-center hidden-xs">
                <span class="copyright-style">Copyright (c) <?= date('Y') ?> Exkavate</span>
            </div>    
            <div class="col-md-3 text-right text-xs-center">
                <span class="terms-info"><a href="/pages/terms_of_use">Terms of Use</a></span>
                <span class="policy-info"><a href="/pages/privacy_policy">Privacy Policy</a></span>
            </div>    
            <div class="col-md-6 text-center hidden-sm hidden-md hidden-lg" style="margin-top: 21px;">
                <span class="copyright-style">Copyright (c) <?= date('Y') ?> Exkavate</span>
            </div>    
        </div>

    </div>
</footer>
