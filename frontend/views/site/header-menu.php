<?php
use yii\helpers\Html;
use common\models\Configuration;
use yii\helpers\ArrayHelper;
use common\components\AdminMenuUrlManager;
use yii\widgets\ActiveForm;
use common\models\LoginForm;
use frontend\models\SignupForm;
$modelMenuUrl = new AdminMenuUrlManager();

$modelSetting = ArrayHelper::map(Configuration::find()->all(), 'key', 'value');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="navbar-fixed-top navbar" role="navigation">
    <div style="border-bottom: 1px solid #e1e1e1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                <div class="logo">
                    <a href="/"></a>
                </div>
                    <div class="menu-button" id=""></div>
                <div class="search-header">
                    <!--<form method="get" action="/search/products">-->
                        <button class="dropdown-toggle" type="button" id="dropdownMenu12"
                                data-toggle="dropdown">
                            <img style="width: 100%;" src="/images/ic_search_black_24px.png" alt="search">
                        </button>
                        <div class="dropdown-menu dropdown_menu22" aria-labelledby="dropdownMenu12">
                            <section class="search search22">
                                <div class="row">
                                    <div class="col-sm-12 xs-padding-right-0">
                                        <form method="get" action="/search/products">
                                            <div class="div_clas_tr"></div>
                                            <div class="form-group">
                                                <input id="search-klock" class="input_search_ico" type="text" name="ProductsSearch[search_value]"
                                                        autocomplete="off" value="Search">
                                                <button class="btn-search-header">
                                                    <img class="img_pos_ab" src="/images/search_icon22.png" alt="search">
                                                </button>
                                            </div>
                                        </form>
                                        <!--      <div class=""><a class="a_style23" href="/category/index">Advanced Search<span class="arr_mar_left"> > </span></a></div>-->
                                    </div>
                                </div>
                            </section>
                        </div>
                    <!--</form>-->
                </div>
                <div class="hidden-xs socials">
                    <a target="_blank" href="<?= $modelSetting['facebook']; ?>">
                        <div class="fb"></div>
                    </a>
                    <a target="_blank" href="<?= $modelSetting['twitter']; ?>">
                        <div class="tw"></div>
                    </a>
                </div>
                    <div class="clearfix"></div>        
            </div>
        </div>
    </div>
    </div>    
<!--            <div class="bach_3px"></div>-->
    <div class="mobile-menu-modal-container"></div>
        <div class="mobile-menu hidden-sm hidden-md hidden-lg">
            <div class="mobile-close-menu text-right">
                <img style="width: 15px;height:15px;" src="/images/ic_close_black_24px.png">
            </div>
            <div class="mobile-login-container">
                    <ul class="authorization">
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <li class="signup" data-toggle="modal" data-target="#signupModal">Sign Up</li>
                            <li class="vertical-line">/</li>
                            <li class="login" data-toggle="modal" data-target="#loginModal">Log In</li>
                        <?php } elseif ((Yii::$app->user->identity->assignmentType == LoginForm::TYPE_COMPANY_ADMIN) || (Yii::$app->user->identity->assignmentType == LoginForm::TYPE_COMPANY_MANAGER)) { ?>
                            <li class="signup  text-center sign_li2"
                                style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                                <?= Html::beginForm(['/site/logout'], 'post'); ?>
                                <?= Html::submitButton(
                                    'Logout',
                                    ['style' => 'background: none;outline: none;border: 0px;box-shadow: none; color:black;', 'title' => Yii::$app->user->identity->email]
                                ); ?>
                                <?= Html::endForm() ?>
                            </li>
                        <?php } else { ?>
                            <div class="dropdown">
                                <button style=""
                                        class="login-btn-style btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    My Exkavate
                                    <span class="login-angle"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                </button>
                                <ul class="dropdown-menu whit_60 whit_mob" aria-labelledby="dropdownMenu1">
                                    <li class="signup  text-center sign_li"
                                        style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                                        <a href="/profile">Profile</a>
                                    </li>
                                    <li class="signup  text-center sign_li"
                                        style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                                        <a href="/profile/update">Settings</a>
                                    </li>
                                    <?= Html::beginForm(['/site/logout'], 'post'); ?>
                                    <li class="signup  text-center sign_li2"
                                        style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                                        <?= Html::submitButton(
                                            'Logout',
                                            ['style' => 'background: none;outline: none;border: 0px;box-shadow: none; color:black;', 'title' => Yii::$app->user->identity->email]
                                        ); ?>
                                    </li>
                                    <?= Html::endForm() ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </ul>
                </div>
            <div class="mobile-social-icons text-center">
                <a target="_blank" href="<?= $modelSetting['facebook']; ?>">
                    <div class="fb"></div>
                </a>
                <a target="_blank" href="<?= $modelSetting['twitter']; ?>">
                    <div class="tw"></div>
                </a>   
            </div>
            <div class="mobile-menu-container">
                <ul class="menu">
                    <li role="presentation" class="menu-item <?= $modelMenuUrl->checkUrl(['']); ?>"><a
                                href="/">Home</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['news/index']); ?>"><a
                                href="/news/index">News</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['pages/about']); ?>"><a
                                href="/pages/about">About</a></li>
                    <li role="presentation" class="menu-item <?= $modelMenuUrl->checkUrl(['category']); ?>">
                        <a href="/category">Categories</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['collections/index']); ?>"><a
                                href="/collections/index">Collections</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['advertisers/index']); ?>"><a
                                href="/advertisers/index">Advertisers</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['publications/index']); ?>"><a
                                href="/publications/index">Publications</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['trendings/index']); ?>"><a
                                href="/trendings/index">Trending</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['products/last-reviewed']); ?>"><a
                                href="/products/last-reviewed">Last reviewed</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    <div class="container hidden-xs">
        <div class="row">
            <div class="col-lg-12">
                <ul class="menu">
                    <li role="presentation" class="menu-item <?= $modelMenuUrl->checkUrl(['']); ?>"><a
                                href="/">Home</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['news/index']); ?>"><a
                                href="/news/index">News</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['pages/about']); ?>"><a
                                href="/pages/about">About</a></li>
                    <li role="presentation" class="menu-item <?= $modelMenuUrl->checkUrl(['category']); ?>">
                        <a href="/category">Categories</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['collections/index']); ?>  <?= Yii::$app->controller->id . Yii::$app->controller->action->id == 'collectionsview' ? 'active' : ''; ?>"><a
                                href="/collections/index">Collections</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['advertisers/index']); ?> <?= Yii::$app->controller->id . Yii::$app->controller->action->id == 'productsview' ? 'active' : ''; ?>"><a
                                href="/advertisers/index">Advertisers</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['publications/index']); ?>  <?= Yii::$app->controller->id . Yii::$app->controller->action->id == 'publicationsview' ? 'active' : ''; ?>"><a
                                href="/publications/index">Publications</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['trendings/index']); ?>  <?= Yii::$app->controller->id . Yii::$app->controller->action->id == 'trendingsview' ? 'active' : ''; ?>"><a
                                href="/trendings/index">Trending</a></li>
                    <li role="presentation"
                        class="menu-item <?= $modelMenuUrl->checkUrl(['products/last-reviewed']); ?>"><a
                                href="/products/last-reviewed">Last reviewed</a></li>
                </ul>
                <ul class="authorization">
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <li class="signup" data-toggle="modal" data-target="#signupModal">Sign Up</li>
                        <li class="vertical-line">/</li>
                        <li class="login" data-toggle="modal" data-target="#loginModal">Log In</li>
                    <?php } elseif ((Yii::$app->user->identity->assignmentType == LoginForm::TYPE_COMPANY_ADMIN) || (Yii::$app->user->identity->assignmentType == LoginForm::TYPE_COMPANY_MANAGER)) { ?>
                        <li class="signup  text-center sign_li2"
                            style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                            <?= Html::beginForm(['/site/logout'], 'post'); ?>
                                <?= Html::submitButton(
                                    'Logout',
                                    ['style' => 'background: none;outline: none;border: 0px;box-shadow: none; color:black;', 'title' => Yii::$app->user->identity->email]
                                ); ?>
                            <?= Html::endForm() ?>
                        </li>
                    <?php } else { ?>
                        <div class="dropdown">
                            <button style=""
                                    class="login-btn-style btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                My Exkavate
                                <span class="login-angle"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                            </button>
                            <ul class="dropdown-menu whit_60" aria-labelledby="dropdownMenu1">
                                <li class="signup  text-center sign_li"
                                    style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                                    <a href="/profile">Profile</a>
                                </li>
                                <li class="signup  text-center sign_li"
                                    style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                                    <a href="/profile/update">Settings</a>
                                </li>
                                <?= Html::beginForm(['/site/logout'], 'post'); ?>
                                <li class="signup  text-center sign_li2"
                                    style="margin:0px;width:100%;font-family: 'RobotoSlab-Regular';">
                                    <?= Html::submitButton(
                                        'Logout',
                                        ['style' => 'background: none;outline: none;border: 0px;box-shadow: none; color:black;', 'title' => Yii::$app->user->identity->email]
                                    ); ?>
                                </li>
                                <?= Html::endForm() ?>
                            </ul>
                        </div>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</nav>

<!-- 
    //
    //modals begin
    //
-->
<div class="triangle"></div>
<div class="colection-popap"></div>
<?php if (Yii::$app->user->isGuest) { ?>
    <?php $modelLogin = new LoginForm(); ?>
    <!-- Modal -->
    <div id="loginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <button type="button" class="close close-modal-btn" data-dismiss="modal"></button>
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:0px ">
                <div class="modal-body">
                    <div class="sectionTitle titl-h1" style="margin-top: 40px">Log in<br></div>
                    <div class="row">
                        <div class="box-center">
                            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableAjaxValidation' => false, 'enableClientValidation' => false, 'validateOnSubmit' => false]); ?>
                                <?= $form->field($modelLogin, 'username')->textInput(['autofocus' => true])->label('Email'); ?>
                                <?= $form->field($modelLogin, 'password')->passwordInput()->label('Password'); ?>
                                <?php $modelLogin->rememberMe = 0; ?>
                                <?= $form->field($modelLogin, 'rememberMe', ['template' => '{input}{label}'])->checkbox([], false); ?>
                                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']); ?>
                            <?php ActiveForm::end(); ?>
                            <div class="register-link-passw">
                                <a href="/site/request-password-reset" class="registr_forgot_passw">Forgot your
                                    password?</a>
                            </div>
                            <div class="register-link">
                                Not a member yet? <a class="js-register-link">Register now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div id="signupModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <button type="button" class="close close-modal-btn" data-dismiss="modal"></button>
            <?php $modelSignUp = new SignupForm(); ?>
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:0px ">
                <div class="modal-body">
                    <div class="sectionTitle titl-h1" style="margin-top: 40px">Create your account <br></div>
                    <div class="row">
                        <div class="box-center">
                            <?php $form = ActiveForm::begin([
                                    'id' => 'form-signup',
                                // 'enableClientValidation' => true,
                                'enableAjaxValidation' => true,
                                'action' => \yii\helpers\Url::to('/site/signup', true)
                              ]); ?>

                            <?= $form->field($modelSignUp, 'username')->textInput(['autofocus' => true])->label('Username'); ?>

                            <?= $form->field($modelSignUp, 'email')->textInput(['autofocus' => true])->label('Email'); ?>

                            <?= $form->field($modelSignUp, 'password')->passwordInput()->label('Password'); ?>

                            <?=
                            $form->field($modelSignUp, 'reCaptcha')->widget(
                                \himiklab\yii2\recaptcha\ReCaptcha::className(),
                                [
                                    'name' => 'reCaptcha',
                                    'siteKey' => '6LfhATIUAAAAAD6jLtKEDAgG0BZj_8weyfDovLtA'
                                ]
                            )->label('');
                            ?>

                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                            <div class="register-link reg-link">
                                Are you a member? <a class="js-login-link">Login now</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php } ?>

<!-- 
    //
    //modals end
    //
-->