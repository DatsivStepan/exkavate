<?php

use frontend\widgets\HomeCategories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Configuration;
use frontend\assets\HomeAsset;
use yii\widgets\ActiveForm;
use frontend\widgets\NewsSliderHome;

HomeAsset::register($this);

$modelSetting = ArrayHelper::map(Configuration::find()->all(), 'key', 'value');
/* @var $this yii\web\View */


$this->title = 'Exkavate';
?>

<?php if ($sliderItems) { ?>
    <section class="maine-slider">
        <div class="owl-carousel owl-theme" id="maine-slider" style="position: relative">
            <?php foreach ($sliderItems as $items) { ?>
                <div class="item">
                    <img data-u="image" src="/<?= $items->img_src; ?>"/>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>
<section class="search">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 search-first-container">
                <div class="search-head">Search Ads</div>
                <form method="get" action="/search/products">
                    <div class="form-group">
                        <input type="text"  style="border: none;
    width: 100%;
    height: 70px;
    font-size: 35px;
    font-family: MarkProLight;
    font-weight: 400;
    margin-top: 10px;" name="ProductsSearch[search_value]" placeholder="Type Keyword ...">
                    </div>
                </form>
                <!--      <div class=""><a class="a_style23" href="/category/index">Advanced Search<span class="arr_mar_left"> > </span></a></div>-->
            </div>

            <div class="col-sm-6">
                <div class="search-head" style="">Trending</div>
                <div class="tags-box">
                    <?php foreach ($trends as $trend) { ?>
                        <div class="tag-item">
                            <a href="<?= $trend->getLink(); ?>">
                                <?= $trend->name; ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="popular-widgets">
    <div class="popular-widgets-maine-container">
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#latest" data-toggle="tab">
                        Latest Ads
                    </a>
                </li>
                <li>
                    <a href="#featured" data-toggle="tab">
                        Featured Ads
                    </a>
                </li>
                <li>
                    <a href="#popular" data-toggle="tab">
                        Popular Ads
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="popular-widgets-container">
        <div class="container">
            <svg class="hidden">
                <defs>
                    <path id="tabshape" d="M80,60C34,53.5,64.417,0,0,0v60H80z"></path>
                </defs>
            </svg>
            <div class="tab-content" style="">
                <div class="tab-pane fade in active" id="latest">
                    <div class="owl-carousel owl-theme" id="carousel1">
                        <?= frontend\widgets\ProductsForSliderHome::widget(['limit' => 10, 'type' => 'latest']); ?>
                    </div>
                    <div class="popular-widgets-navigation">
                        <div class="adv-controls hidden-xs hidden-sm hidden-md">
                            <div class="adv-control-prev" id="adv-prev1"></div>
                            <div class="adv-control-next" id="adv-next1"></div>
                        </div>
                    </div>
                    <div class="all-adv hidden-xs">
                        <a href="/category">View All Latest Ads <i class="fa fa-angle-right"
                                                                   style="height:17px;font-size:31px;padding-bottom:0;display:inline-block;padding-left: 6px;"
                                                                   aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="tab-pane fade" id="featured">
                    <div class="owl-carousel owl-theme" id="carousel2">
                        <?= frontend\widgets\ProductsForSliderHome::widget(['limit' => 10, 'type' => 'fuatured']); ?>
                    </div>
                    <div class="popular-widgets-navigation">
                        <div class="adv-controls hidden-xs hidden-sm hidden-md">
                            <div class="adv-control-prev" id="adv-prev2"></div>
                            <div class="adv-control-next" id="adv-next2"></div>
                        </div>
                    </div>
                    <div class="all-adv hidden-xs">
                        <a href="/">View All Featured Ads <i class="fa fa-angle-right"
                                                             style="height:17px;font-size:31px;padding-bottom:0;display:inline-block;padding-left: 6px;"
                                                             aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="tab-pane fade" id="popular">
                    <div class="owl-carousel owl-theme" id="carousel3">
                        <?= frontend\widgets\ProductsForSliderHome::widget(['limit' => 10, 'type' => 'popular']); ?>
                    </div>
                    <div class="popular-widgets-navigation">
                        <div class="adv-controls hidden-sm hidden-md">
                            <div class="adv-control-prev" id="adv-prev3"></div>
                            <div class="adv-control-next" id="adv-next3"></div>
                        </div>
                    </div>
                    <div class="all-adv hidden-xs">
                        <a href="/">View All Popular Ads <i class="fa fa-angle-right"
                                                            style="height:17px;font-size:31px;padding-bottom:0;display:inline-block;padding-left: 6px;"
                                                            aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="join-mail">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="join-maile-container">
                    <div><span class="join_mail_title">Exkavate Daily</span></div>
                    <div><span class="join_mail_text">Get the latest updates on ads, announcements, and notices</span>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'form-newslater', 'enableAjaxValidation' => false, 'enableClientValidation' => false, 'validateOnSubmit' => false]); ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <input placeholder="Email..." class="inpus_su_madal2 js-newslater-email" required="required"
                                   type="email" name="email" autocomplete="off">
                            <?= Html::submitButton('Subscribe', ['class' => 'btn btn-primary subscrib_cs', 'name' => 'newslater-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <!-- Modal -->
            <!--          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="join-mail-icon-grupe">
                <a target="_blank" href="<?= $modelSetting['facebook']; ?>">
                    <div class="join-mail-fb-icon"></div>
                </a>
                <a target="_blank" href="<?= $modelSetting['twitter']; ?>">
                    <div class="join-mail-tw-icon"></div>
                </a>
                <a target="_blank" href="<?= $modelSetting['google_plus']; ?>">
                  <div class="join-mail-google-icon"></div>
                </a>
            </div>
          </div>-->
            <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-3 col-md-2">
            </div>
        </div>
    </div>
</section>
<section class="howWorks">
    <div class="container">
        <div class="howWorks-title">Discover Exkavate<br></div>
        <div class="owl-carousel owl-theme hidden-sm hidden-md hidden-lg" id="owlHoweWorks">
            <div class="item">
                <div class="col-xs-12 col-sm-6 work-block find">
                    <div class="work-block-title">Find</div>
                    <div class="work-block-text">
                        The largest selection of ads from your favorite newspapers and magazines is right here
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-xs-12 col-sm-6 work-block save">
                    <div class="work-block-title">Save</div>
                    <div class="work-block-text">
                        Never miss a thing. Bookmark ads in personal folders and create Collections to organize them.
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-xs-12 col-sm-6 work-block follow">
                    <div class="work-block-title">Follow</div>
                    <div class="work-block-text">
                        Follow advertisers, categories, and topics to get the latest scoops
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-xs-12 col-sm-6 work-block share">
                    <div class="work-block-title">Share</div>
                    <div class="work-block-text">
                        Broadcast the best deals, job opportunities, civic notices, events and more to family,
                        colleagues
                        and friends
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-xs-12 col-sm-6 work-block subscribe">
                    <div class="work-block-title">Subscribe</div>
                    <div class="work-block-text">
                        Stay up to date when you sign up for the Exkavate newsletter.
                    </div>
                </div>
            </div>
        </div>
        <div class="row hidden-xs">
            <div class="col-xs-12 col-sm-6 work-block find">
                <div class="work-block-title">Find</div>
                <div class="work-block-text">
                    The largest selection of ads from your favorite newspapers and magazines is right here
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 work-block save">
                <div class="work-block-title">Save</div>
                <div class="work-block-text">
                    Never miss a thing. Bookmark ads in personal folders and create Collections to organize them.
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 work-block follow">
                <div class="work-block-title">Follow</div>
                <div class="work-block-text">
                    Follow advertisers, categories, and topics to get the latest scoops
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 work-block share">
                <div class="work-block-title">Share</div>
                <div class="work-block-text">
                    Broadcast the best deals, job opportunities, civic notices, events and more to family, colleagues
                    and friends
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 work-block subscribe">
                <div class="work-block-title">Subscribe</div>
                <div class="work-block-text">
                    Stay up to date when you sign up for the Exkavate newsletter.
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeCategories::widget(); ?>
<section class="home-new">
    <div class="container">
        <div class="row text-center">
            <span class="home-news-maine-title">News</span>
        </div>
    </div>
    <?= NewsSliderHome::widget(['limit' => 20]); ?>
</section>
<section class="publicatioin">
    <div class="container">
        <div class="sectionTitle">Publications<br></div>
        <div class="publication-slider-container">
            <div class="owl-carousel owl-theme owl-loaded " id="owl-pulications">
                <?php foreach ($publications as $publication) { ?>
                    <?php //if ($publication->getProductsCount() > 0) { ?>
                    <div class="item">
                        <a href="<?= $publication->getLink(); ?>">
                            <img style="width:60%;" src="<?= $publication->getImageSrc(); ?>">
                        </a>
                    </div>
                    <?php //} ?>
                <?php } ?>
            </div>
        </div>
        <div id="pub-prev1"></div>
        <div id="pub-next1"></div>
    </div>
</section>



