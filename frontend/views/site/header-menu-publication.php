<?php
use yii\helpers\Html;
use common\models\Configuration;
use yii\helpers\ArrayHelper;
use common\components\AdminMenuUrlManager;
use yii\widgets\ActiveForm;
use common\models\LoginForm;
use frontend\models\SignupForm;
$modelMenuUrl = new AdminMenuUrlManager();

$modelSetting = ArrayHelper::map(Configuration::find()->all(), 'key', 'value');
?>
<nav class="navbar-fixed-top navbar" role="navigation">
     
    <div class="container hidden-xs">
        <div class="row">
            <div class="col-lg-12">
                <ul class="menu">
                    <li role="presentation" class="menu-item <?= $modelMenuUrl->checkUrl(['']); ?>"><a
                            href="view?id=<?= $id; ?>">Home</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</nav>

<!-- 
    //
    //modals begin
    //
-->
<div class="triangle"></div>
<div class="colection-popap"></div>
<?php if (Yii::$app->user->isGuest) { ?>
    <?php $modelLogin = new LoginForm(); ?>
    <!-- Modal -->
    <div id="loginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <button type="button" class="close close-modal-btn" data-dismiss="modal"></button>
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:0px ">
                <div class="modal-body">
                    <div class="sectionTitle titl-h1" style="margin-top: 40px">Log in<br></div>
                    <div class="row">
                        <div class="box-center">
                            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableAjaxValidation' => false, 'enableClientValidation' => false, 'validateOnSubmit' => false]); ?>
                            <?= $form->field($modelLogin, 'username')->textInput(['autofocus' => true])->label('Email'); ?>
                            <?= $form->field($modelLogin, 'password')->passwordInput()->label('Password'); ?>
                            <?php $modelLogin->rememberMe = 0; ?>
                            <?= $form->field($modelLogin, 'rememberMe', ['template' => '{input}{label}'])->checkbox([], false); ?>
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']); ?>
                            <?php ActiveForm::end(); ?>
                            <div class="register-link-passw">
                                <a href="/site/request-password-reset" class="registr_forgot_passw">Forgot your
                                    password?</a>
                            </div>
                            <div class="register-link">
                                Not a member yet? <a class="js-register-link">Register now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div id="signupModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <button type="button" class="close close-modal-btn" data-dismiss="modal"></button>
            <?php $modelSignUp = new SignupForm(); ?>
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:0px ">
                <div class="modal-body">
                    <div class="sectionTitle titl-h1" style="margin-top: 40px">Create your account <br></div>
                    <div class="row">
                        <div class="box-center">
                            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'enableAjaxValidation' => false, 'enableClientValidation' => false, 'validateOnSubmit' => false]); ?>

                            <?= $form->field($modelSignUp, 'username')->textInput(['autofocus' => true])->label('Username'); ?>

                            <?= $form->field($modelSignUp, 'email')->textInput(['autofocus' => true])->label('Email'); ?>

                            <?= $form->field($modelSignUp, 'password')->passwordInput()->label('Password'); ?>

                            <?=
                            $form->field($modelSignUp, 'reCaptcha')->widget(
                                \himiklab\yii2\recaptcha\ReCaptcha::className(),
                                [
                                    'name' => 'reCaptcha',
                                    'siteKey' => '6LfhATIUAAAAAD6jLtKEDAgG0BZj_8weyfDovLtA'
                                ]
                            )->label('');
                            ?>

                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                            <div class="register-link reg-link">
                                Are you a member? <a class="js-login-link">Login now</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php } ?>

<!-- 
    //
    //modals end
    //
-->