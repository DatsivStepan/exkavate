<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
         <?= $this->render('_header');?>
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 padding_0 media_wight_1200">
                <ul class="left_sidebar">
                    <h2>Obi Onyx</h2>
                    <h3>Member since: <span>March 4, 2017</span></h3>
                    <hr>
                    <li class="list_style">
                        <img src="/images/89200-200.png" class="img_category">
                        <a href="/category/list/1" class="fonts_title_profile">
                            <div class="categ_name">OKs</div>
                        </a>
                        <span class="number_catal_prof">2233</span>
                    </li>
                    <li class="list_style">
                        <img src="/images/1122.png" class="img_category">
                        <a href="/category/list/1" class="fonts_title_profile">
                            <div class="categ_name">Following</div>
                            <a href="/" class="arr_1" onclick="menuOpen('s_mn_1');return(false)"></a>
                        </a>
                        <span class="number_catal_prof">2233</span>
                    </li>
                    <div id="m_body" style="display: block">
                        <li>
                            <ul id="s_mn_1">
                                <hr>
                                <li class="list_style">
                                    <img src="/images/89200-200.png" class="img_category">
                                    <a href="/category/list/1" class="fonts_title_profile">
                                        <div class="categ_name">Advertisers</div>
                                    </a>
                                    <span class="number_catal_prof">2233</span>
                                </li>
                                <li class="list_style">
                                    <img src="/images/89200-200.png" class="img_category">
                                    <a href="/category/list/1" class="fonts_title_profile">
                                        <div class="categ_name">Publications</div>
                                    </a>
                                    <span class="number_catal_prof">2233</span>
                                </li>
                                <li class="list_style">
                                    <img src="/images/89200-200.png" class="img_category">
                                    <a href="/category/list/1" class="fonts_title_profile">
                                        <div class="categ_name">Collections</div>
                                    </a>
                                    <span class="number_catal_prof">2233</span>
                                </li>
                                <li class="list_style">
                                    <img src="/images/89200-200.png" class="img_category">
                                    <a href="/category/list/1" class="fonts_title_profile">
                                        <div class="categ_name">Trends </div>
                                    </a>
                                    <span class="number_catal_prof">2233</span>
                                </li>
                                <hr>
                            </ul>
                        </li>
                    </div>
                    <li class="list_style">
                        <img src="/images/89200-200.png" class="img_category">
                        <a href="/category/list/1" class="fonts_title_profile">
                            <div class="categ_name">My Collections</div>
                        </a>
                        <span class="number_catal_prof">2233</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-8 col-sm-7 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column">
                    <ul class="breadcrumbs hidden-xs  col-sm-12 col-md-12 col-lg-12">
                        <li class="breadcrumb_text">
                            <a class="a_breadc_arrow" href="/">
                                Breadcrumb
                            </a>
                        </li>
                        <li class="breadcrumb_text">
                            <a class="a_breadc_arrow" href="/">
                                Test
                            </a>
                        </li>
                        <li class="breadcrumb_text_li">Test</li>
                    </ul>
                    <div class="col-lg-12 padding_0">
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 padding_0">
                            <div class="tovar_profile_one">
                                <div>
                                    <img class="img_product_profile" src="/images/tovar-59.png">
                                    <p class="data_tovar_profil">10.02.2015</p>
                                    <p class="name_tovar_prof">Slyppis</p>
                                </div>
                                <div class="div_prof_bott">
                                    <a href="/"><img class="" src="/images/okey.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/111.png"></a>
                                    <span class="pad_span_right">24</span>
                                    <a href="/"><img class="" src="/images/222.png"></a>
                                    <span class="pad_span_right">1</span>
                                    <a href="/"><img class="" src="/images/333.png"></a>
                                    <span class="pad_span_right">0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom_pagination">
                        <a href="#"><img class="ar_left_pagin" src="/images/ar_left_pagin.png"></a>
                        <div class="pagination">
                            <a class="pagin_a" href="#">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#">4</a>
                            <a href="#">...</a>
                            <a href="#">15</a>
                        </div>
                            <a href="#"><img class="ar_right_pagin" src="/images/ar_right_pagin.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none">
        <p>
            <?= Html::a('Update', ['update'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete'], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                'email:email',
                'created_at:datetime',
                'description:ntext',
            ],
        ]) ?>

    </div>
</div>
</div>

<script type="text/javascript">
    function menuOpen(id) {
        if (document.getElementById(id).style.display == "none") {
            document.getElementById(id).style.display = "block";
            $(".arr_1").toggleClass("arr_2");
        } else {
            document.getElementById(id).style.display = "none";
            $(".arr_1").toggleClass("arr_2");
        }
    }
</script>