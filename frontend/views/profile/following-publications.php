<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use common\models\Likes;
use common\models\Follower;
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Publications';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = ['label' => 'Following', 'url' => ['']];
$this->params['breadcrumbs'][] = $this->title;
?>
  <div class="wrap profile-pages">
    <div class="container ">
      <?= $this->render('_profile_menu'); ?>
      <?php foreach ($dataProvider->getModels() as $publications) { ?>
      <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 advert-box">
          <?= $this->render('/templates/_publication', ['publications' => $publications])?>
        </div>
        <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">
          <div class="row">
            <div class="profile-products cleafix">
              <?php foreach ($publications->getCompanyProductWithLimit(3) as $product) { ?>
              <div class="profile-product-item col-lg-3 col-md-4 col-sm-4 my-col-xs">
                <?= $this->render('/templates/_product', ['product' => $product])?>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <?php if(!$dataProvider->getModels()){?>
      <p class="text-center text_empty">Empty</p>
      <?php }?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-sm-offset-4 bottom-pagination">
                <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
            </div>
        </div>
    </div>
  </div>