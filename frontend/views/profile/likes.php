<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;
use common\models\Likes;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'OKs';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
  <div class="wrap profile-pages">
    <div class="container ">
      <?= $this->render('_profile_menu'); ?>
      <div class="row">
        <div class="profile-products cleafix">
          <?php foreach ($dataProvider->getModels() as $product) { ?>
          <div class="profile-product-item my-col-lg my-col-md my-col-sm my-col-xs">
            <?= $this->render('/templates/_product', ['product' => $product])?>
          </div>
          <?php } ?>
        </div>
        <?php if(!$dataProvider->getModels()) {?>
        <p class="text-center text_empty">Empty</p>
        <?php }?>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-4 bottom-pagination">
          <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
        </div>
      </div>
    </div>
  </div>
  </div>