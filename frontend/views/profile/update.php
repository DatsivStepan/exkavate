<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['users/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
  <div class="row settings-page" style="margin: 0">
    <div class="col-xs-10 col-sm-8 col-md-6 col-lg-4 col-xs-offset-1 col-sm-offset-2 col-md-offset-3 col-lg-offset-4">
      <h1 class="">
        <?= Html::encode($this->title) ?>
      </h1>

      <?php $form = ActiveForm::begin(); ?>
      <?php if (Yii::$app->session->hasFlash('dataSave')) { ?>
      <?= Alert::widget([
          'options' => ['class' => 'alert-success'],
          'body' => 'Updated!',
        ]) ?>
      <?php } ?>

      <?= $form->field($model, 'email')->input('email') ->label(true) ?>
      <?= $form->field($model, 'newslater')->checkbox(['template' => '{input}{label}{error}{hint}',])->label('Newsletter') ?>

      <?php if (Yii::$app->session->hasFlash('passwordSave')) { ?>
      <?= Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => 'Password changed!',
      ]) ?>
      <?php } ?>

      <?php if (Yii::$app->session->hasFlash('passwordNotSave')) { ?>
        <?= Alert::widget([
          'options' => ['class' => 'alert-danger'],
          'body' => 'Old password is wrong!',
        ]) ?>
      <?php } ?>
      <?= $form->field($model, 'old_password')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('old_password')])->label(false) ?>
      <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
      <?= $form->field($model, 'password_repeat')->textInput(['maxlength' => true, 'placeholder' => 'Verify new Password'])->label(false) ?>
      <div class="form-group">
        <?= Html::submitButton('Update', [
          'class' => 'btn btn-success save-changes pull-right',
          'name' => 'updatePassword'
        ]) ?>
      </div>
      <?php ActiveForm::end(); ?>
    </div>

  </div>