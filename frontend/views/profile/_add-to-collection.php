<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\Alert;
use yii\widgets\ActiveForm;
use common\components\BootstrapModal;

/* @var $this yii\web\View */
/* @var $model common\models\User */

BootstrapModal::widget([
    ['id' => 'create-collection-modal', 'header' => 'Create collection', 'footer' => true],
]);

?>
<div class="brand-create">
    <?php if (Yii::$app->session->hasFlash('collection_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => 'Collection saved!',
        ]) ?>
    <?php } ?>

    <?php if (Yii::$app->session->hasFlash('collection_not_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => 'Collection not saved!',
        ]) ?>
    <?php } ?>
    

    <div class="brand-form">

        <?php $form = ActiveForm::begin(); ?>

            <div class="form-group">
                <label>Product</label>
                <?= Html::textInput('product_name', $modelProduct->name, ['class' => 'form-control', 'disabled'=> true]); ?>
            </div>
    
            <div class="form-group">
                <div class="col-xs-10">
                    <?= Select2::widget([
                        'data' => $collectionsArray,
                        'value' => $choiseCollectionsArray,
                        'language' => 'en',
                        'name' => 'collections',
                        'size' => Select2::MEDIUM,
                        'options' => ['placeholder' => 'Add to Collections...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-xs-2">
                    <a class="btn btn-success js-create-collection"><span class="glyphicon glyphicon-plus"></span></a>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success js-form-button-hide hide']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>
<script>
    $(document).on('click','.js-create-collection', function(e){
        e.preventDefault();
        BootstrapModal(
            'create-collection-modal',
            'Create collection',
            BootstrapModalIFrame('/profile/create-collection', 200)
        );
    });
    $(document).on('click', '#create-collection-modal .js-modal-save', function (e) {
        $('#create-collection-modal iframe').contents().find('.js-form-button-hide').trigger('click');
    });
</script>