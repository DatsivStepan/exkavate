<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use common\models\Likes;
use common\models\Follower;
use common\components\BootstrapModal;
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'My collections';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = 'Following';
$this->params['breadcrumbs'][] = $this->title;
BootstrapModal::widget([
    ['id' => 'create-collection-modal', 'header' => 'Create collection', 'footer' => true],
]);
?>

  <div class="wrap profile-pages">
    <div class="container ">
      <?= $this->render('_profile_menu'); ?>
        <?php foreach ($dataProvider->getModels() as $collection) { ?>
        <div class="row">
          <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 advert-box">
            <div class="colection-sidebar">
              <a class="colection-name" href="<?= $collection->getLink(); ?>">
                <?= $collection->name; ?>
              </a>
              <div class="colection-description">
                <?= $collection->description; ?>
              </div>

              <!-- <?php if (!\Yii::$app->user->isGuest) { ?>
    <?php if ($collection->getFollowStatus()) { ?>
    <button class="btn js-follow active public_bottom333" style="margin-bottom: 141px;" data-type="<?= Follower::TYPE_COLLECTION; ?>"
      data-object_id="<?= $collection->id; ?>">
      <span class="follow">Following</span>
    </button>
    <?php } else { ?>
    <button class="btn js-follow public_bottom333 not_active" style="margin-bottom: 141px;" data-type="<?= Follower::TYPE_COLLECTION; ?>"
      data-object_id="<?= $collection->id; ?>">
      <span class="follow">Follow</span>
    </button>
    <?php } ?>
    <?php } else { ?>
    <button class="btn not_active js-login-modal">
      <span class="follow">Follow</span>
    </button>
    <?php } ?> -->

              <div style="clear: both"></div>
              <div class="colection-icons">
                  <div class="colection-icon">
                    <a>
                      <img class="" src="/images/col1.png">
                    </a>
                    <?= $collection->getViewsCount(); ?>
                  </div>
                  <div class="colection-icon">
                    <a href="<?= $collection->getLink(); ?>">
                      <img class="" src="/images/08_collections.png">
                    </a>
                    <?= $collection->getDownloadCount(); ?>
                  </div>
              </div>
              <a href="<?= $collection->getLink(); ?>" class="all-colections">
                View All
              </a>
            </div>
          </div>
          <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">
            <div class="row">
              <div class="profile-products cleafix">
                <?php foreach ($collection->getColectionProductWithLimit(3) as $product) { ?>
                <div class="profile-product-item col-lg-3 col-md-4 col-sm-4 my-col-xs">
                  <?= $this->render('/templates/_product', ['product' => $product])?>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php if(!$dataProvider->getModels()){?>
        <p class="text-center text_empty">Empty</p>
        <?php }?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-sm-offset-4 bottom-pagination">
                <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
            </div>
        </div>
    </div>
  </div>