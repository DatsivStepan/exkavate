<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Likes;
use common\models\Follower;
use common\models\Collections;
use common\components\AdminMenuUrlManager;

$modelMenuUrl = new AdminMenuUrlManager();
/* @var $this yii\web\View */
/* @var $model common\models\User */

?>
  <div class="row">
    <div class="col-xs-12 member-info">
      <h1 class="member-info-name text-center">
        <?= (\Yii::$app->user->identity->first_name) ? \Yii::$app->user->identity->first_name. ' ' . \Yii::$app->user->identity->last_name : \Yii::$app->user->identity->username; ?>
      </h1>
      <span class="member-info-date text-center">Member since:
        <?= date('d M, Y', (\Yii::$app->user->identity->created_at)); ?>
      </span>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
      <div profile-menu="" class="row category-menu">
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/likes']); ?>">
          <a href="/profile/likes">
            <div class="count-items">
              <?= Likes::myLikesCount(Likes::TYPE_PRODUCT); ?>
            </div>
            <p class="p-title-profil-oks">OKs</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/following-advertisers']); ?>">
          <a href="/profile/following-advertisers">
            <div class="count-items">
              <?= Follower::getMyFollowerCount(Follower::TYPE_BRAND); ?>
            </div>
              <p class="p-title-profil">Advertisers</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/following-publications']); ?>">
          <a href="/profile/following-publications" class="fonts_title_profile">
            <div class="count-items">
              <?= Follower::getMyFollowerCount(Follower::TYPE_COMPANY); ?>
            </div>
              <p class="p-title-profil">Publications</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/following-collections']); ?>">
          <a href="/profile/following-collections" class="fonts_title_profile">
            <div class="count-items">
              <?= Follower::getMyFollowerCollectionCount(); ?>
            </div>
              <p class="p-title-profil">Collections</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/following-trends']); ?>">
          <a href="/profile/following-trends" class="fonts_title_profile">
            <div class="count-items">
              <?= Follower::getMyFollowerTrendsCount(); ?>
            </div>
              <p class="p-title-profil">Trends</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/my-collections']); ?>">
          <a href="/profile/my-collections" class="fonts_title_profile">
            <div class="count-items">
              <?= Collections::getMyCollectionCount();?>
            </div>
              <p class="p-title-profil">My Collections</p>
          </a>
        </div>
      </div>
    </div>
  </div>