<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Likes;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Last reviewed');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top no-padding">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <?php foreach ($productsModel as $product) { ?>
                        <div class="col-20-proz col-md-3 col-sm-4 col-xs-6 product-container-padding whidth_media_1400 product-one-all-no-home product-fiv-pading" style="margin-bottom: 15px">
                            <?= $this->render('//templates/_product', ['product' => $product, 'type' => 'home']) ?>
                        </div>
                    <?php } ?>
                    <?php if (!$productsModel) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>

                </div>
                <div class="bottom_pagination" style="clear: both;display: block">
                    <?= LinkPager::widget(['pagination' => $pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
    
