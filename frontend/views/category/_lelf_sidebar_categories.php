<?php

use yii\helpers\Html;
use common\models\Category;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<hr class="hr-category hidden-xs">
<div>
    <a class="category_name category-refine-by" href="#1a" data-toggle="tab">
        REFINE BY <span class="clear-all-category js-clear-category-choise hidden-xs">Clear all</span>
        <div class="hidden-lg hidden-md hidden-sm arr_top">
        </div>
    </a>

    <div class="list-group list_group_cat">
        <?php foreach ($model as $category) { ?>
            <?php if ($category->getProductCount() > 0) { ?>
                <div class="<?= (in_array($category->id, $category_id)) ? 'active' : ''; ?> col_sidb">
                    <?= CheckboxX::widget([
                        'name' => 'category_' . $category->id,
                        'value' => in_array($category->id, $category_id),
                        'options' => [
                            'id' => 'category_' . $category->id,
                            'class' => 'js-checkbox-category',
                            'data-category_id' => $category->id,
                        ],
                        'pluginOptions' => [
                            'threeState' => false,
                        ],
                    ]);
                    ?>

                    <a class="fonts_title_categ <?= (in_array($category->id, $category_id)) ? 'active' : ''; ?>">
                        <div class="icon_like2" data-title="<?= $category->name; ?>">
                            <?= $category->name; ?> <span>(<?= $category->getProductCount(); ?>)</span>
                        </div>
                    </a>
                </div>
            <?php } ?>
        <?php } ?>
        <div class="clear-all-category-xs hidden-lg hidden-md hidden-sm">Clear all</div>
        <hr class="category-hr-menu">
    </div>
</div>
<script>
    $(function () {
        $('.js-checkbox-category').change(function () {
            var categoryChangeValue = $(this).data('category_id');
            $('#productssearch-category_id [type=checkbox]').each(function (index) {
                if (categoryChangeValue == $(this).val()) {
                    $(this).trigger('click');
                }
            })
        })
    });
    
    $(document).ready(function () {

        $(".arr_top").click(function () {
            $(".arr_top").toggleClass("arr_bot");
            $(".list_group_cat").toggleClass("list_group_category_display");
        });

        $(document).on('click', '.js-clear-category-choise', function () {
            $('#productssearch-category_id [type=checkbox]').prop( "checked", false);
            $('#js-product-search-form').submit()
        });

    });
</script>