<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Likes;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Category');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog">
    <div class="container">
        <div class="category-index row">
            <div class='col-sm-12'>
                <?= $this->render('_search', ['model' => $searchModel, 'modelCategories' => $modelCategories]); ?>
            </div>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog back_gray">
    <div class="container">
        <div class="category-index">
            <div class='col-lg-2 col-md-3 col-sm-3 padding-right-null pos-n-categ select-sort-pad'>
                <?= $this->render('_lelf_sidebar_categories', ['model' => $modelCategories, 'category_id' => $searchModel->category_id ? $searchModel->category_id : []]); ?>
            </div>
            <div class='col-lg-10 col-md-9 col-sm-9' style="margin-bottom: 30px;">
                <div class="col-ld-12 padding_0 wight_med_catal">
                    <?php foreach ($productsModel as $product) { ?>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-container-padding product-one-all-no-home categoty-name-height">
                            <?= $this->render('//templates/_product', ['product' => $product, 'type' => 'home']) ?>
                        </div>
                    <?php } ?>
                    <?php if (!$productsModel) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>

                </div>
                <div class="bottom_pagination" style="clear: both;display: block">
                    <div class="col-sm-2 col-xs-12" style="padding:0px;">
                        <?= Html::dropDownList('paginCount', 
                                $searchModel->paginCount, 
                                \frontend\models\ProductsSearch::$arrayPagination, 
                                [
                                    'class' => 'form-control js-pagin-count-select class-plag-count',
                                    'style' => 'margin:20px 0px;',
                                ])?>
                    </div>
                    <div class="col-sm-10 col-xs-12" style="padding-left:40px;"><?= LinkPager::widget(['pagination' => $pagination]); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>