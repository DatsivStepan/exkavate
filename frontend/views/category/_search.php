<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm,
    yii\helpers\ArrayHelper,
    frontend\models\ProductsSearch;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
$get = \Yii::$app->request->get();
?>
<div class="contant public-view-search" style="padding: 29px 0px 24px 0;">

    <?php $form = ActiveForm::begin([
        'id' => 'js-product-search-form',
        'method' => 'get'
    ]); ?>
    <?php if($modelCategories) { ?> 
        <div class="hide">
            <?= $form->field($model, 'category_id')->checkboxList(
                    ArrayHelper::map($modelCategories, 'id', 'name'),
                    [
                        'class' => 'js-checkbox-form-category'
                    ]
                )->label(false) ?>
        </div>
    <?php } ?>
        <div class="hide">
            <?= $form->field($model, 'paginCount')->dropDownList(ProductsSearch::$arrayPagination)->label(false) ?>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 select-sort-date select-sort-pad">
            <?= Html::dropDownList(
                'sort',
                $get ? (array_key_exists('sort', $get) ? $get['sort'] : null) : null,
                [
                    'created_at' => 'Date descending',
                    '-created_at' => 'Date ascending',
                    '-viewsCount' => 'Number of Views',
                    '-commentsCount' => 'Number of Comments',
                ]
            ) ?>
            <hr class="hr-category2 hidden-lg hidden-md hidden-sm">
        </div>
        <div class="col-lg-10 col-md-9 col-sm-8 hidden-xs whid-mob" style="width: 34%;float: right;padding-right: 0px">
            <?= $form->field($model, 'search_value')->textInput(['placeholder' => 'Search'])->label(false) ?>
        </div>
    <?php $form = ActiveForm::end(); ?>

</div>
<script>
    $(function(){
        $(document).on('change', '[name=sort], #productssearch-category_id [type=checkbox] ', function(){
            $('#js-product-search-form').submit()
        })
        
        $(document).on('change', '.js-pagin-count-select', function(){
            $('#productssearch-pagincount option[value="' + $(this).val() + '"]').prop('selected', true)
            $('#js-product-search-form').submit()
        })
    })
</script>