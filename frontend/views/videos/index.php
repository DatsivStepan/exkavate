<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\VideosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Videos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog">
    <div class="container">
        <div class="category-index">
            <div class="row">
                <h1 class="h1_profile_title"><?= Html::encode($this->title) ?>
                    <hr>
                </h1>
            </div>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_0">
                <?php foreach ($dataProvider->getModels() as $video) { ?>
                    <?php if ($video->link) { ?>
                        <a href='<?= $video->getLink(); ?>'>
                            <?php
                            parse_str(parse_url($video->link, PHP_URL_QUERY), $parameters);
                            $link = $parameters['v'];
                            ?>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding_0">
                                <div class="tovar_profile_one111 video_profile_one">
                                    <div class="im_ju_3">
                                        <img src="//img.youtube.com/vi/<?= $link; ?>/hqdefault.jpg"
                                             width="100%">
                                        <p class="data_tovar_catal111"
                                           style="margin-top: 15px;"><?= $video->getCreatedDate(); ?></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                <?php } ?>
                <?php /*= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'link' => [
                                    'label' => 'Link',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        if ($model->link) {
                                            parse_str(parse_url($model->link, PHP_URL_QUERY), $parameters);
                                            $link = $parameters['v'];
                                            return Html::tag('iframe', '', [
                                                        'src' => 'https://www.youtube.com/embed/' . $link, 
                                                        'style' => 'width:200px; height:150px;', 'frameborder' => '0', 'allowfullscreen'=> 'true']);
                                        } else {
                                            return null;
                                        }
                                    }
                                ],
                                'description:ntext',
                                'created_at',
                                // 'updated_at',
                                //['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); */ ?>
            </div>
        </div>
    </div>
</div>
