<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Video */

$this->title = 'Video';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Videos'), 'url' => ['index']];
?>
<div class='container'>
    <div style="margin-top: 25px;">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <div class="video-view">
        <?php parse_str(parse_url($model->link, PHP_URL_QUERY), $parameters);
        $link = $parameters['v']; ?>

        <?= Html::tag('iframe', '', [
            'src' => 'https://www.youtube.com/embed/' . $link,
            'style' => 'width:100%; height:500px', 'frameborder' => '0', 'allowfullscreen' => 'true']) ?>

        <p class="data_video_one"><?= $model->getCreatedDate(); ?></p>
        <p class="name_video_one"><?= $model->description; ?></p>
    </div>
</div>
