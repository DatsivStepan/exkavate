<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Likes;
use yii\widgets\LinkPager;
use frontend\assets\HomeAsset;
use common\models\Follower;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TrendingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
HomeAsset::register($this);
$this->title = Yii::t('app', 'Trends');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <div class="col-sm-12 padding_0">
            <?= $this->render('_search', ['model' => $searchModel]) ?>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?php foreach ($dataProvider->getModels() as $trend) { ?>
                        <div class="row ro_marg_0">
                            <div class="block-left-brand padding-collec-nul">
                                <?= $this->render('//templates/_trend', ['trend' => $trend]); ?>
                            </div>
                            <div class="block-right-brand block-band-rig">
                                <div class="owl-carousel owl-theme owl-loaded js-products-carousel"
                                     data-slides="<?= count($trend->getTrendProductWithLimit(12)); ?>"
                                     data-id="<?= $trend->id ?>" id="owl-trend<?= $trend->id ?>">
                                    <?php foreach ($trend->getTrendProductWithLimit(12) as $product) { ?>
                                        <div class="item product-one-all-no-home product-one-all-adverts">
                                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!--                            <a class="a_colecction a_col3 a_col3_coll2" href="-->
                            <? //= $trend->getLink(); ?><!--"><img-->
                            <!--                                        src="/images/ar_lef1.png">View all</a>-->
                        </div>
                        <hr class="hidden-xs hr-product-col">
                    <?php } ?>
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(this).find(".span_arrow_top").click(function () {
            $(".span_arrow_top").toggleClass("span_arrow_bottom2");
        });
    });
    $(document).ready(function () {
        $(this).find(".span_arrow_top2").click(function () {
            $(".span_arrow_top2").toggleClass("span_arrow_bottom2");
        });
    });
    $(document).ready(function () {
        $(this).find(".span_arrow_bottom").click(function () {
            $(".span_arrow_bottom").toggleClass("span_arrow_top2");
        });

        $("#buttom_search").click(function () {
            $("#textSearch").focus();
        });
    });
    $(document).ready(function () {
        $(".js-products-carousel").each(function () {
            var id = $(this).data("id");
            var slides = $(this).data("slides");
            var width = $( window ).width();
            $('#owl-trend' + id).owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: true,
                center: (slides == 1 && width <= 768)?true:false,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 2
                    },
                    1200: {
                        items: 4
                    }
                }
            });
        });
    });
</script>
