<?php

use common\models\Likes,
    common\models\Follower;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="colection-sidebar">
    <a class="colection-name" href="<?= $collection->getLink(); ?>">
        <?= $collection->name; ?>
    </a>
    <div class="colection-description">
        <?= $collection->description; ?>
    </div>
    <div class="hidden-sm hidden-md hidden-lg colection-sidebar coll-dis">
        <div class="colection-icons">
            <!--        <div class="collec_icon2">-->
            <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
            <!--            <span class="pad_span_collec2">-->
            <?php //= $collection->getLikesCount(); ?>
            <!--</span>-->
            <!--        </div>-->
            <div class="colection-icon">
                <a>
                    <img class="" src="/images/col1.png">
                </a>
                <?= $collection->getViewsCount(); ?>
            </div>
            <div class="colection-icon">
                <a href="<?= $collection->getLink(); ?>">
                    <img class="" src="/images/08_collections.png">
                </a>
                <?= $collection->getDownloadCount(); ?>
            </div>
        </div>

        <?php if (!\Yii::$app->user->isGuest) { ?>
            <?php $class = $collection->getFollowStatus() ? 'active' : 'not_active'; ?>
            <button class="hidden-lg hidden-md hidden-sm follow-btn collec-prod js-follow <?= $class; ?>"
                    data-type="<?= Follower::TYPE_COLLECTION; ?>"
                    data-object_id="<?= $collection->id; ?>">
                <span class="follow">Following</span>
            </button>
        <?php } else { ?>
            <button class="hidden-lg hidden-md hidden-sm follow-btn not_active js-login-modal collec-prod">
                <span class="follow">Follow</span>
            </button>
        <?php } ?>

        <a href="<?= $collection->getLink(); ?>" class="all-colections">
            View All
        </a>
    </div>
    <div style="clear: both"></div>
    <div class="hidden-xs coll-dis">
        <div class="colection-icons">
            <!--        <div class="collec_icon2">-->
            <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
            <!--            <span class="pad_span_collec2">-->
            <?php //= $collection->getLikesCount(); ?>
            <!--</span>-->
            <!--        </div>-->
            <div class="colection-icon">
                <a>
                    <img class="" src="/images/col12.png">
                </a>
                <?= $collection->getViewsCount(); ?>
            </div>
            <div class="colection-icon">
                <a href="<?= $collection->getLink(); ?>">
                    <img class="" src="/images/08_collections.png">
                </a>
                <?= $collection->getDownloadCount(); ?>
            </div>
        </div>

        <?php if (!\Yii::$app->user->isGuest) { ?>
            <?php $class = $collection->getFollowStatus() ? 'active' : 'not_active'; ?>
            <button class="hidden-lg hidden-md hidden-sm follow-btn collec-prod js-follow <?= $class; ?>"
                    data-type="<?= Follower::TYPE_COLLECTION; ?>" data-object_id="<?= $collection->id; ?>">
                <span class="follow">Following</span>
            </button>
        <?php } else { ?>
            <button class="hidden-lg hidden-md hidden-sm follow-btn not_active js-login-modal collec-prod">
                <span class="follow">Follow</span>
            </button>
        <?php } ?>

        <a href="<?= $collection->getLink(); ?>" class="all-colections">
            View All
        </a>
    </div>
</div>