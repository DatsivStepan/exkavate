<?php
use common\models\Likes;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

    <div class="tovar_profile_one111 tovar_profile_one65 js-product-block product-one-to-all">
        <div class="im_ju_3">
            <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink()?>">
                <div class="img_ki_44"
                     style="background: url(<?= $product->getImageSrc(); ?>) center no-repeat;background-size:100%;"></div>
            </a>
            <p class="data_tovar_catal111">
                <?= $product->getCreatedDate('d.m.Y'); ?>
                <!--<a><img src="/images/wwe.png"></a>-->
            </p>
            <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink()?>"
               class="name_naw_catal111"><?= $product->name; ?></a>
        </div>
        <?php //if ($type == 'home') { ?>
            <?php if($product->brandModel) { ?>
                <div class="paper-title-container">
                  <span class="paper-title"><?= $product->brandModel->getCategoryName(); ?></span>
                  <span class="paper-text"><a href="<?= !$link ? $product->brandModel->getLink() : null; ?>"><?= $product->brandModel->name; ?></a></span>
                </div>
            <?php } ?>
        <?php //} ?>
        <div class="div_catal_bott111 div_bot_56">
            <?php $class = ''; ?>
            <?php if (!\Yii::$app->user->isGuest) { ?>
                <?php if ($product->getLikeStatus()) { ?>
                    <?php $class = 'js-like active'; ?>
                <?php } else { ?>
                    <?php $class = 'js-like not_active'; ?>
                <?php } ?>
            <?php } else{ ?>
                    <?php $class = 'js-login-modal'; ?>
            <?php } ?>
            <a class="icon_like <?= $class; ?>" data-title="Like" data-type="<?= Likes::TYPE_PRODUCT; ?>" data-object_id="<?= $product->id; ?>">
                <img style="width: 11px;" class="img_ico" src="/images/hand_grey.png">
                <span class="pad_span2_right_222 pad_span23_right_22225 js-like-count">
                    <?= $product->getLikeCount(); ?>
                </span>
            </a>
            <a>
                <img class="img_ico" src="/images/111.png">
                <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getViewCount(); ?>
                </span>
            </a>
            <a>
                <img class="img_ico" src="/images/222.png">
                <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCommentCount(); ?>
                </span>
            </a>
            <a href="<?= $link ? null : $product->getLinkToProductColletion(); ?>">
                <img class="img_ico" src="/images/333.png">
                <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCollectionCount(); ?>
                </span>
            </a>
            <a  class="js-my-collection-<?= $product->id; ?> <?= \Yii::$app->user->isGuest ? 'js-login-modal' : 'add-to-collection'; ?> " data-product_id="<?= $product->id; ?>">
                <img class="img_ico img_ico-last" src="/images/+.png">
                <span class="pad_span2_right_222 pad_span23_right_22225 js-my-collection-count">
                    <?= \Yii::$app->user->isGuest ? 0 : $product->getMyCollectionCount(); ?>
                </span>
            </a>  
        </div>
    </div>
