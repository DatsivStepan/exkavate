<?php

use common\models\Likes;
use common\models\Follower;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
    <div class="adverties-sidebar">
        <a href="<?= $brand->getLink(); ?>" class="adverties-logo">
            <img src="/<?= $brand->img_src ? $brand->img_src : 'images/default_image.jpg' ?>">
        </a>
        <div class="hidden-sm hidden-md hidden-lg adverties-sidebar coll-dis">
                <div class="adverties-follow-count">
                    <a>
                        <img class="img_no_padd" src="/images/pub1.png">
                        <?= $brand->getFollowCount(); ?>
                    </a>
                </div>
                <?php if (!\Yii::$app->user->isGuest) { ?>
                    <?php $class = $brand->getFollowStatus() ? 'active' : 'not_active'; ?>
                    <button class="follow-btn js-follow <?= $class; ?>" data-type="<?= Follower::TYPE_BRAND; ?>"
                            data-object_id="<?= $brand->id; ?>">
                        <span class="follow">Following</span>
                    </button>
                <?php } else { ?>
                    <button class="follow-btn not_active js-login-modal">
                        <span class="follow">Follow</span>
                    </button>
                <?php } ?>

                <a href="<?= $brand->getLink(); ?>" class="all-adverties">
                    View All
                </a>
            </div>
        <div class="adverties-info">
            <div class="advrties-info-name">
                <?= $brand->name; ?>
            </div>
            <div class="advrties-info-category">
                <?= $brand->getCategoryName(); ?>
            </div>
        </div>
        <div class="hidden-xs coll-dis">
            <div class="adverties-follow-count">
                <a>
                    <img class="img_no_padd" src="/images/pub1.png">
                    <?= $brand->getFollowCount(); ?>
                </a>
            </div>

            <?php if (!\Yii::$app->user->isGuest) { ?>
                <?php $class = $brand->getFollowStatus() ? 'active' : 'not_active'; ?>
                <button class="follow-btn js-follow <?= $class; ?>" data-type="<?= Follower::TYPE_BRAND; ?>"
                        data-object_id="<?= $brand->id; ?>">
                    <span class="follow">Following</span>
                </button>
            <?php } else { ?>
                <button class="follow-btn not_active js-login-modal">
                    <span class="follow">Follow</span>
                </button>
            <?php } ?>

            <a href="<?= $brand->getLink(); ?>" class="all-adverties">
                View All
            </a>
        </div>
        <!-- <div class="pub_icon_bot">
      <div class="public_float_left">
        <div class="pub_icon1">
          <?php $class = ''; ?>
          <?php if (!\Yii::$app->user->isGuest) { ?>
          <?php if ($brand->getLikeStatus()) { ?>
          <?php $class = 'js-like active'; ?>
          <?php } else { ?>
          <?php $class = 'js-like not_active not-activ-brand'; ?>
          <?php } ?>
          <?php } else { ?>
          <?php $class = 'js-login-modal'; ?>
          <?php } ?>
          <a class="icon_like <?= $class; ?>" data-title="Like" data-type="<?= Likes::TYPE_BRAND; ?>" data-object_id="<?= $brand->id; ?>"
            style="background: none;padding: 0;">
            <img class="img_paddd11 img_no_padd" src="/images/pub3.png">
            <span class="pad_span_like_count js-like-count">
              <?= $brand->getLikesCount(); ?>
            </span>
          </a>
        </div>
        <div class="pub_icon1">
          <a>
            <img class="img_no_padd" src="/images/pub1.png">
            <span class="data_advert_count">
              <?= $brand->getFollowCount(); ?>
            </span>
          </a>
        </div>
      </div>
      <div class="public_float_right">
        <div class="pub_icon1">
          <a>
            <img class="" src="/images/col1.png">
            <span class="data_advert_count pad_right">
              <?= $brand->getViewsCount(); ?>
            </span>
          </a>
        </div>
        <div class="pub_icon1">
          <a href="<?= $brand->getLink(); ?>">
            <img class="img_paddd11" src="/images/pub2.png">
            <span class="">
              <?= $brand->getCollectionsCount(); ?>
            </span>
          </a>
        </div>
      </div>

    </div> -->
    </div>


<?php
/* //type profile_template
 * <a href="<?= $brand->getLink(); ?>" class="public_bottom_view">View All</a>
    */ ?>