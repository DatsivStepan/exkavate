<?php
use common\models\Likes;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<?php $class = ''; ?>
<?php if (!\Yii::$app->user->isGuest) { ?>
    <?php if ($brand->getLikeStatus()) { ?>
        <?php $class = 'js-like active'; ?>
    <?php } else { ?>
        <?php $class = 'js-like not_active'; ?>
    <?php } ?>
<?php } else{ ?>
        <?php $class = 'js-login-modal'; ?>
<?php } ?>
<a class="icon_like <?= $class; ?> clas_merg" data-title="Like" data-type="<?= Likes::TYPE_BRAND; ?>" data-object_id="<?= $brand->id; ?>"
   style="background: none;padding: 0;">
    <div class="bacg_img"></div>
    <span class="js-like-count span_icon_brand">
        <?= $brand->getLikesCount(); ?>
    </span>
</a>

<a class="clas_merg"><img class="" src="/images/col1.png"></a>
<span class="span_icon_brand"><?= $brand->getViewsCount(); ?></span>

<a class="clas_merg"><img class="" src="/images/pub1.png"></a>
<span class="span_icon_brand"><?= $brand->getFollowCount(); ?></span>

<a class="clas_merg" href="<?= $brand->getLink(); ?>"><img class="" src="/images/pub2.png"></a>
<span class="span_icon_brand"><?= $brand->getCollectionsCount(); ?></span>