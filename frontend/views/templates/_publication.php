<?php

use common\models\Follower;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- <div class="tovar_profile_one public_one one_adv_223 tovar_profile_one11123 border_0_adverts">
    <a href="<?= $publications->getLink(); ?>">
      <img class="img_public" src="/<?= $publications->logo_src ? $publications->logo_src : 'images/default_image.jpg' ?>">
      <p class="public_name">
        <?= $publications->name; ?>
      </p>
    </a>
    <?php if (!\Yii::$app->user->isGuest) { ?>
    <?php if ($publications->getFollowStatus()) { ?>
    <button class="btn js-follow active public_bottom333" style="margin-bottom: 141px;" data-type="<?= Follower::TYPE_COMPANY; ?>"
      data-object_id="<?= $publications->id; ?>">
      <span class="follow">Following</span> Publication
    </button>
    <?php } else { ?>
    <button class="btn js-follow public_bottom333 not_active" style="margin-bottom: 141px;" data-type="<?= Follower::TYPE_COMPANY; ?>"
      data-object_id="<?= $publications->id; ?>">
      <span class="follow">Follow</span> Publication
    </button>
    <?php } ?>
    <?php } else { ?>
    <button class="btn not_active js-login-modal">
      <span class="follow">Follow</span> Publication
    </button>
    <?php } ?>
    <a class="btn btn-primary" href="<?= $publications->getLink(); ?>">View All</a>
    <div class="pub_icon_bot">
      <div class="public_float_left">
        <div class="pub_icon1">
          <a class="icon_like" data-title="Like">
            <img class="img_paddd11 img_no_padd" src="/images/pub3.png">
            <span class="data_advert_count">
              <?= $publications->getLikesCount(); ?>
            </span>
          </a>
        </div>


        <div class="pub_icon1">
          <a>
            <img class="img_paddd3 img_no_padd" src="/images/pub1.png">
            <span class="data_advert_count">
              <?= $publications->getFollowCount(); ?>
            </span>
          </a>
        </div>
      </div>
      <div class="public_float_right">
        <div class="pub_icon1">
          <a>
            <img class="" src="/images/col1.png">
            <span class="data_advert_count">
              <?= $publications->getViewsCount(); ?>
            </span>
          </a>
        </div>
        <div class="pub_icon1">
          <a href="<?= $publications->getLink(); ?>">
            <img class="img_paddd4" src="/images/pub2.png">
            <span class="data_advert_count">
              <?= $publications->getCollectionsCount(); ?>
            </span>
          </a>
        </div>
      </div>

    </div>
  </div> -->


<div class="publications-sidebar">
    <a href="<?= $publications->getLink(); ?>" class="publications-logo">
        <img src="/<?= $publications->logo_src ? $publications->logo_src : 'images/default_image.jpg' ?>">
    </a>
    <div class="hidden-sm hidden-md hidden-lg publications-sidebar coll-dis">
        <div class="adverties-follow-count">
            <a class="text-center">
                <img class="img_no_padd" src="/images/pub1.png">
                <span><?= $publications->getFollowCount(); ?></span>
            </a>
        </div>
        <?php if (!\Yii::$app->user->isGuest) { ?>
            <?php $class = $publications->getFollowStatus() ? 'active' : 'not_active'; ?>
            <button class="follow-btn js-follow <?= $class; ?>"
                    data-type="<?= Follower::TYPE_COMPANY; ?>"
                    data-object_id="<?= $publications->id; ?>">
                <span class="follow">Following</span>
            </button>
        <?php } else { ?>
            <button class="follow-btn not_active js-login-modal">
                <span class="follow">Follow</span>
            </button>
        <?php } ?>

        <a href="<?= $publications->getLink(); ?>" class="all-publications">
            View All
        </a>
    </div>
    <div class="publications-info">
        <div class="publications-info-name">
            <?= $publications->name; ?>
        </div>
    </div>
    <div class="hidden-xs coll-dis">
        <div class="adverties-follow-count">
            <a class="text-center">
                <img class="img_no_padd" src="/images/pub1.png">
                <span><?= $publications->getFollowCount(); ?></span>
            </a>
        </div>
        <?php if (!\Yii::$app->user->isGuest) { ?>
            <?php $class = $publications->getFollowStatus() ? 'active' : 'not_active'; ?>
            <button class="follow-btn js-follow <?= $class; ?>" data-type="<?= Follower::TYPE_COMPANY; ?>"
                    data-object_id="<?= $publications->id; ?>">
                <span class="follow">Following</span>
            </button>
        <?php } else { ?>
            <button class="follow-btn not_active js-login-modal">
                <span class="follow">Follow</span>
            </button>
        <?php } ?>

        <a href="<?= $publications->getLink(); ?>" class="all-publications">
            View All
        </a>
    </div>
    <!---->
    <!--    <div class="publications-follow-count">-->
    <!--      <a>-->
    <!--        <img class="img_no_padd" src="/images/pub1.png">-->
    <!--        --><? //= $publications->getFollowCount(); ?>
    <!--      </a>-->
    <!--    </div>-->
</div>