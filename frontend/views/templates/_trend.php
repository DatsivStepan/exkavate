<?php

use common\models\Follower;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="trend-sidebar">
    <a class="trend-name" href="<?= $trend->getLink(); ?>">
        <?= $trend->name; ?>
    </a>
    <div class="trend-description">
        <?= $trend->description; ?>
    </div>
    <div class="hidden-sm hidden-md hidden-lg trend-sidebar coll-dis">
        <div class="trend-icons">
            <!--        <div class="collec_icon2">-->
            <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
            <!--            <span class="pad_span_collec2">-->
            <? //= $collection->getLikesCount(); ?>
            <!--</span>-->
            <!--        </div>-->
            <div class="trend-icon">
                <a>
                    <img class="" src="/images/pub1.png">
                </a>
                <?= $trend->getFollowCount() ?>
            </div>
            <div class="trend-icon">
                <a href="<?= $trend->getLink(); ?>">
                    <img class="" src="/images/08_collections.png">
                </a>
                <?= $trend->getDownloadCount(); ?>
            </div>
        </div>

        <?php if (!\Yii::$app->user->isGuest) { ?>
            <?php $class = $trend->getFollowStatus() ? 'active' : 'not_active'; ?>
            <button class="follow-btn js-follow <?= $class; ?>"
                    data-type="<?= Follower::TYPE_TRENDING; ?>"
                    data-object_id="<?= $trend->id; ?>">
                <span class="follow">Following</span>
            </button>
        <?php } else { ?>
            <button class="follow-btn not_active js-login-modal">
                <span class="follow">Follow</span>
            </button>
        <?php } ?>

        <a href="<?= $trend->getLink(); ?>" class="all-trend">
            View All
        </a>
    </div>
    <div style="clear: both"></div>
    <div class="hidden-xs coll-dis">
        <div class="trend-icons">
            <!--        <div class="collec_icon2">-->
            <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
            <!--            <span class="pad_span_collec2">-->
            <? //= $collection->getLikesCount(); ?>
            <!--</span>-->
            <!--        </div>-->
            <div class="trend-icon">
                <a>
                    <img class="" src="/images/pub1.png">
                </a>
                <?= $trend->getFollowCount() ?>
            </div>
            <div class="trend-icon">
                <a href="<?= $trend->getLink(); ?>">
                    <img class="" src="/images/08_collections.png">
                </a>
                <?= $trend->getDownloadCount(); ?>
            </div>
        </div>

        <?php if (!\Yii::$app->user->isGuest) { ?>
            <?php $class = $trend->getFollowStatus() ? 'active' : 'not_active'; ?>
            <button class="follow-btn js-follow <?= $class; ?>" data-type="<?= Follower::TYPE_TRENDING; ?>"
                    data-object_id="<?= $trend->id; ?>">
                <span class="follow">Following</span>
            </button>
        <?php } else { ?>
            <button class="follow-btn not_active js-login-modal">
                <span class="follow">Follow</span>
            </button>
        <?php } ?>

        <a href="<?= $trend->getLink(); ?>" class="all-trend">
            View All
        </a>
    </div>
</div>