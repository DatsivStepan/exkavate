<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use frontend\assets\HomeAsset;
use common\models\Likes;
use common\models\Follower;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PublicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
HomeAsset::register($this);
$this->title = Yii::t('app', 'Companies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <div class="row">
            <div class="mar_bat-crie"></div>
        </div>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <div class="col-sm-12 padding_0">
            <?= $this->render('_search', ['model' => $searchModel]) ?>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog back_pag_cat back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <?php foreach ($dataProvider->getModels() as $publications) { ?>
                        <div class="row row_mar_00">
                            <div class="block-left-brand padding-collec-nul">
                                <?= $this->render('//templates/_publication', ['publications' => $publications]); ?>
                            </div>
                            <div class="block-right-brand block-band-rig">
                                <div class="owl-carousel owl-theme owl-loaded js-products-carousel"
                                     data-slides="<?= count($publications->getCompanyProductWithLimit(12)); ?>"
                                     data-id="<?= $publications->id ?>" id="owl-products<?= $publications->id ?>">
                                    <?php foreach ($publications->getCompanyProductWithLimit(12) as $product) { ?>
                                        <div class="item product-one-all-no-home product-one-all-adverts">
                                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
<!--                        <hr class="hr-product-col">-->
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".js-products-carousel").each(function () {
            var id = $(this).data("id");
            var slides = $(this).data("slides");
            var width = $( window ).width();
            $('#owl-products' + id).owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: true,
                center: (slides == 1 && width <= 768)?true:false,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 2
                    },
                    1200: {
                        items: 4
                    }
                }
            });
        });
    });
</script>