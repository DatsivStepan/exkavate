<?php

use yii\helpers\Html,
    yii\widgets\ActiveForm,
    yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
$get = \Yii::$app->request->get();
?>
<div class="contant row cont-ro-pad" style="padding: 29px 0px 24px 0;">

    <?php $form = ActiveForm::begin([
        'id' => 'js-publications-search-form',
        'method' => 'get'
    ]); ?>

    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 select-sort-date select-adver">
        <?= Html::dropDownList(
            'sort',
            $get ? (array_key_exists('sort', $get) ? $get['sort'] : null) : null,
            [
                'created_at' => 'Sort by Date',
                '-created_at' => 'Date ascending',
                '-viewsCount' => 'Number of Views',
                '-likesCount' => 'Number of Likes',
                '-followCount' => 'Number of Follows',
            ]
        ) ?>
    </div>

    <div class="col-lg-10 col-md-9 col-sm-8 col-form-group">
        <?= $form->field($model, 'search_value')->textInput(['placeholder' => 'Search'])->label(false) ?>
    </div>

    <?php $form = ActiveForm::end(); ?>

</div>
<script>
    $(function () {
        $(document).on('change', '#publicationssearch-search_value, [name=sort]', function () {
            $('#js-publications-search-form').submit()
        })
    })
</script>