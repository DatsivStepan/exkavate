<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Likes;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model common\models\Collections */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Collections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <div class="section-title-view-prod" style="background: none">
            <?= $this->title ?>
        </div>
        <div class="col-sm-12 padding_0">
            <?= $this->render('/category/_search', ['model' => $searchModel, 'modelCategories' => $modelCategories]); ?>
        </div>
    </div>
</div>
<div class="backgroung_page_catalog back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top padding_0">
                <div class="right_colum_collection rig_collec_hom row_col_ad">
                    <?php foreach ($dataProvider->getModels() as $product) { ?>
                        <div class="col-20-proz col-md-3 col-sm-4 col-xs-6 product-container-padding whidth_media_1400 product-one-all-no-home product-fiv-pading"
                             style="margin-bottom: 15px">
                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                        </div>
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="bottom_pagination" style="clear: both;display: block">
            <div class="col-sm-2 col-xs-12" style="padding:0px;">
                <?= Html::dropDownList('paginCount',
                    $searchModel->paginCount,
                    \frontend\models\ProductsSearch::$arrayPagination,
                    [
                        'class' => 'form-control js-pagin-count-select class-plag-count class-plag-col-count',
                        'style' => 'margin:20px 0px;',
                    ]) ?>
            </div>
            <div class="col-sm-10 col-xs-12"
                 style="padding-left:40px;"><?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
            </div>
        </div>

    </div>
</div>
