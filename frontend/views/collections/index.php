<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Likes;
use yii\widgets\LinkPager;
use frontend\assets\HomeAsset;
use yii\widgets\Breadcrumbs;
use common\models\Follower;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CollectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
HomeAsset::register($this);
$this->title = Yii::t('app', 'Collections');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <?= $this->render('_search', ['model' => $searchModel]) ?>
    </div>
</div>
<div class="backgroung_page_catalog back_pag_cat back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <?php foreach ($dataProvider->getModels() as $collection) { ?>
                        <div class="row ro_marg_0">
                            <div class="block-left-brand padding-collec-nul">
                                <?= $this->render('//templates/_collection', ['collection' => $collection]) ?>
                            </div>
                            <div class="block-right-brand">
                                <div class="owl-carousel owl-theme owl-loaded js-products-carousel"
                                     data-slides="<?= count($collection->getColectionProductWithLimit(12)); ?>"
                                     data-id="<?= $collection->id ?>" id="owl-products<?= $collection->id ?>">
                                    <?php foreach ($collection->getColectionProductWithLimit(12) as $product) { ?>
                                        <div class="item product-one-all-no-home product-one-all-adverts prod_coll">
                                            <?= $this->render('//templates/_product', ['product' => $product]) ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <hr class="hidden-xs hr-product-col">
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(this).find(".span_arrow_top").click(function () {
            $(".span_arrow_top").toggleClass("span_arrow_bottom2");
        });
    });
    $(document).ready(function () {
        $(this).find(".span_arrow_top2").click(function () {
            $(".span_arrow_top2").toggleClass("span_arrow_bottom2");
        });
    });
    $(document).ready(function () {
        $(this).find(".span_arrow_bottom").click(function () {
            $(".span_arrow_bottom").toggleClass("span_arrow_top2");
        });
        $("#buttom_search").click(function () {
            $("#textSearch").focus();
        });
    });
    $(document).ready(function () {
        $(".js-products-carousel").each(function () {
            var id = $(this).data("id");
            var slides = $(this).data("slides");
            var width = $( window ).width();
            $('#owl-products' + id).owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: true,
                center: (slides == 1 && width <= 768)?true:false,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 2
                    },
                    1200: {
                        items: 4
                    }
                }
            });
        });
    });
</script>