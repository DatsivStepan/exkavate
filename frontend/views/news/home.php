<?php

use yii\helpers\Html;
use frontend\widgets\RecentNews;
use common\components\AdminMenuUrlManager;
use frontend\assets\HomeAsset;
use yii\widgets\ActiveForm;

HomeAsset::register($this);
$modelMenuUrl = new AdminMenuUrlManager();
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\news\models\SearchNews */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backg-news">
    <div class="container menu-news">
        <?= Html::a('News Home', '/news/index' , ['class' => 'btn btn-primary btn-prim-mob-menu '. $modelMenuUrl->checkUrl('news/index')]); ?>
        <div class="hidden-lg hidden-md hidden-sm arr_top-news"></div>
        <?php foreach ($categoriesNews as $categoriesId => $categoriesName) { ?>
            <?= Html::a($categoriesName, '/news/index/' . $categoriesId, ['class' => 'btn btn-primary btn-prim-mob ' . $modelMenuUrl->checkUrl('news/index/' . $categoriesId)]); ?>
        <?php } ?>
    </div>
</div>
<div class="container">
    <?php foreach ($dataProvider->getModels() as $key => $newsCategory) { ?>
                <div class="news-index" style="margin-top: 30px">
                    <?php if ($categorySliderNews = $newsCategory->getAllSliderNews()) { ?>
                        <div class="hidden-xs" style="position:relative;">
                            <div class="owl-carousel owl-theme owl-loaded news-index-car" id="news-block">
                                <?php foreach ($categorySliderNews as $news) { ?>
                                    <div class="item">
                                        <div class="col-sm-12 no-padding" style="position:relative;margin-bottom: 100px;">
                                            <?= Html::tag('img', '', ['src' => $news->getImages(), 'style' => 'width: 100%;height: 550px;object-fit: fill;-o-object-fit: fill;']); ?>
                                        </div>
                                        <div class="col-sm-8 col-sm-offset-2 back-whit-news">
                                            <button class="butom_print"><?= $news->categoryModel ? Html::a($news->categoryModel->name, $news->categoryModel->getLink()) : ''; ?></button>
                                            <p class="news-data-great"><?= $news->getDateCreate('M. d.Y'); ?></p>
                                            <p class="news-short-description"><?= Html::a($news->title, $news->getLink()); ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($newsCategory->getNewsWithLimit(3)) { ?>
                        <div class="">
                            <div class="col-sm-9 no-padding">
                                <?php foreach ($newsCategory->getNewsWithLimit(3) as $key => $news) { ?>
                                    <div class="row" style="margin-bottom: 27px">
                                        <div class="col-sm-6" style="position:relative;">
                                            <?= Html::tag('img', '', ['src' => $news->getImages(), 'class' => 'img-new-we', 'style' => 'width:100%;height: 264px;box-shadow: 1px 1px 17px #9194a1;']); ?>
                                            <button class="butom_print-news"><?= Html::a($news->categoryModel->name, $news->categoryModel->getLink()); ?></button>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="get-data-create-news"><?= $news->getDateCreate('M. d.Y'); ?></p>
                                            <p class="title-news"><?= Html::a($news->title, $news->getLink()); ?></p>
                                            <p class="hidden-xs descrip-news"><?= $news->short_description; ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-sm-3 no-padding">
                                <div class="col-sm-12 no-padding form-news-letr">
                                    <?php $form = ActiveForm::begin(['id' => 'form-newslater2', 'enableAjaxValidation' => false, 'enableClientValidation' => false, 'validateOnSubmit' => false]); ?>
                                        <div class="block-sibscribe">
                                            <div class="alert js-alert-newslater-status alert-dismissible alert_fade_sign_up fade" role="alert" style="width:100%;margin-left:0px;">
                                                <span class="message">
                                                    <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                                                </span>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <p class="title-subsc">Get the Exkavate newsletter</p>
                                            <p class="descrip-subsc">For the latest ads, notices and news</p>
                                            <input type="text" class="js-newslater-email" required="required" type="email" name="email" autocomplete="off" placeholder="Email...">
                                            <?= Html::submitButton('Subscribe') ?>
                                        </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php } ?>
                </div>
    <?php } ?>
</div>
<script>
    $(document).ready(function () {
        $(".arr_top-news").click(function () {
            $(".arr_top-news").toggleClass("arr_bot-news");
            $(".btn-prim-mob").toggleClass("list_group-news");
        });
    });
    $(document).ready(function () {
        $('.news-index-car').owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });
    });
    
    $( "#form-newslater2" ).submit(function( event ) {
        event.preventDefault();
        var email = $(this).find('.js-newslater-email').val();
        $.ajax({
            url: '/site/save-newslater-email',
            method:'post',
            data:{email:email},
            dataType: 'json',
            success: function (response) {
                if(response.status){
                    $('.js-alert-newslater-status').addClass('alert-success');
                    $('.js-alert-newslater-status').addClass('show');
                    $('.js-alert-newslater-status').removeClass('fade');
                } else {
                    $('.js-alert-newslater-status').addClass('alert-danger');
                    $('.js-alert-newslater-status').addClass('show');
                    $('.js-alert-newslater-status').removeClass('fade');
                }
                $('.js-newslater-email').val('')
                $('.js-alert-newslater-status span.message').html(response.message);
            }
        })
    });
</script>