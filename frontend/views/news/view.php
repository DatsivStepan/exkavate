<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\widgets\RecentNews;
use yii\widgets\Breadcrumbs;
use bigpaulie\social\share\Share;


/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News Home'), 'url' => ['index']];
if ($model->categoryModel) {
    $this->params['breadcrumbs'][] = ['label' => $model->categoryModel->name, 'url' => [$model->categoryModel->getLink()]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backg-news">
    <div class="container menu-news">
        <?= Html::a('News Home', '/news/index/' . $categoriesId, ['class' => 'btn btn-primary btn-prim-mob-menu active']); ?>
        <div class="hidden-lg hidden-md hidden-sm arr_top-news"></div>
        <?php foreach ($categoriesNews as $categoriesId => $categoriesName) { ?>
            <?= Html::a($categoriesName, '/news/index/' . $categoriesId, ['class' => 'btn btn-primary btn-prim-mob']); ?>
        <?php } ?>
    </div>
</div>
<div class="container">
    <div class="news-index" style="margin-top: 45px">
        <div class="hidden-lg hidden-md hidden-sm news-menu-link">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <div class="col-sm-9 padding-left-null pad-rig-nesw-one">

            <div class="hidden-xs news-menu-link">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
            <div class="title-one-news">
                <?= Html::a($model->title, $model->getLink()); ?>
            </div>
            <p class="date-one-news"><?= $model->getDateCreate('M. d.Y');; ?></p>
            <?= Html::tag('img', '', ['src' => $model->getImages(), 'style' => 'width:100%;']); ?>
            <figcaption class="featured-image-caption">
                <div class="content-width-responsive">
                    <img class="icon-camera" src="/images/camera-icon.png">
                    <?= $model->image_description; ?>
                </div>
            </figcaption>
            <div class="clearfix"></div>
            <div class="backg-new-view">
                <div class="col-sm-9 col-xs-12">
                    <div class="descripthin-one-news"><?= $model->description; ?></div>
                </div>
                <div class="col-sm-3 col-xs-12 padding_0" style="margin-top: 25px">
                    <div><span class="style-share">SHARE</span></div>
                    <div class="icon-inet">
                    <style>
                        ul#socials-share{
                            list-style: none;
                            padding: 0;
                        }
                        ul#socials-share li{
                            display: inline-block;
                            margin-right: 10px;
                        }
                        ul#socials-share{
                            margin-bottom: 20px;
                        }
                    </style>
                    <div class="share-buttons colored item-meta-row">
                    <?= Share::widget([
                        'networks' => [
                            'facebook' => 'https://www.facebook.com/sharer/sharer.php?u={url}',
                            'google-plus' => 'https://plus.google.com/share?url={url}',
                            'twitter' => 'https://twitter.com/home?status={url}',
                            'linkedin' => 'https://www.linkedin.com/shareArticle?mini=true&url={url}',
                        ],
                        'type' => Share::TYPE_EXTRA_SMALL,
                        'htmlOptions' => ['id'=>'socials-share']
                    ]);?>
                    <br>
                    </div>
                        <!-- <div class="share-buttons colored item-meta-row">
                            <a title="Share on twitter" href="/" target="_blank" class="share-action icon-twitter">
                                <img src="/images/294655-16.png">
                            </a>
                            <a title="Share on facebook" href="/" target="_blank" class="share-action icon-facebook">
                                <img src="/images/317727-16.png">
                            </a>
                            <a title="Share on linkedin" href="/" target="_blank" class="share-action icon-linkedin">
                                <img src="/images/386655-16.png">
                            </a>
                            <a title="Share on email" href="/" target="_blank" class="share-action icon-email">
                                <img src="/images/images-gmail.png">
                            </a>
                            </div>
                         -->
                    </div>
                    <div><span class="style-share">WRITTEN BY</span></div>
                    <div class="icon-inet">
                        <div class="share-buttons colored item-meta-row">
                            <span class="author-name">
                                <?= $model->avtor_name; ?>
                            </span>
                        </div>
                    </div>
<!--                    -->
<!--                    <div><span class="style-share" style="color: #a1a1a1;">--><?php//= $model->getTimeAgo(); ?><!--</span></div>-->
                    <div class="clas_tags-product">
                        <p class="tags-news-one">TAGS</p>
                        <div class="pad_tags_name margin-bott-news">
                            <?php foreach ($model->newsTagsModel as $tags) { ?>
                                <a href="<?= $tags->getLink(); ?>"><?= $tags->name; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3 no-padding" style="margin-top: 35px">
            <div class="block-sibscribe">
                <p class="title-subsc">Get the Exkavate newsletter</p>
                <p class="descrip-subsc">For the latest ads, notices and news</p>
                <input>
                <button>Subscribe</button>
            </div>
        </div>
    </div>
</div>

</div>
<script>
    $(document).ready(function () {
        $(".arr_top-news").click(function () {
            $(".arr_top-news").toggleClass("arr_bot-news");
            $(".btn-prim-mob").toggleClass("list_group-news");
        });
    });
</script>