<?php

namespace frontend\modules\frame\controllers;

use Yii;
use common\models\Company;
use frontend\modules\frame\models\PublicationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Products;

/**
 * PublicationController implements the CRUD actions for Company model.
 */
class PublicationController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionProduct($id)
    {
        $this->layout = '/iframe-publication';
        $model = $this->findProduct($id);
        $company = $this->findModel($model->company_id);
        Yii::$app->params['companyId'] = $company->id;
        Yii::$app->params['page'] = 'product';
        
        $user_ip = Yii::$app->request->userIP;
        $modeViews = \common\models\Views::find()->where([
            'object_id' => $model->id,
            'type' => 'product'
        ])->andWhere([
            'OR',
            ['user_ip' => $user_ip],
            ['user_id' => !\Yii::$app->user->isGuest ? \Yii::$app->user->id : null]
        ])->one();
        if(!$modeViews){
            $modeViews = new \common\models\Views();
            $modeViews->object_id = $model->id;
            $modeViews->user_ip = $user_ip;
            $modeViews->user_id = !\Yii::$app->user->isGuest ? \Yii::$app->user->id : null;
            $modeViews->type = 'product';
            $modeViews->save();
        }
        
        $modelProductComment = \common\models\ProductsComments::find()->where(['product_id' => $id,'parent_id' => null])->orderBy('created_at DESC')->all();
        $modelNewProductComment = new \common\models\ProductsComments();
        return $this->render('product', [
            'model' => $model,
            'modelNewProductComment' => $modelNewProductComment,
            'modelProductComment' => $modelProductComment,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        Yii::$app->params['companyId'] = $id;
        Yii::$app->params['link'] = 'product?id';
        $this->layout = '/iframe-publication';
        
        $model = $this->findModel($id);
        $modelCategories = \common\models\Category::find()->all();
        
        $searchModel = new \frontend\models\ProductsSearch();
        $searchModel->publication_id = $id;

        $dataProvider = $searchModel->searchPublicationsProducts(Yii::$app->request->queryParams);
        return $this->render('view', [
            'model' => $model,
            'modelCategories' => $modelCategories,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findProduct($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
