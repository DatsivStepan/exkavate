<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;
use common\models\Follower;
use common\models\Likes;
use frontend\assets\AddAdsAsset;

AddAdsAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\Company */

Yii::$app->view->registerJsFile('/js/plugins/sweetalert.min.js');
Yii::$app->view->registerJsFile('/js/plugins/shablon/web-animations.js');
Yii::$app->view->registerJsFile('/js/plugins/shablon/hammer-2.0.8.min.js');
Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.4.1/velocity.min.js');
Yii::$app->view->registerJsFile('/js/plugins/shablon/grid_view.js');

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        .premium .form-group {
            text-align: left;
        }

        .grid {
            position: relative;
            margin: 20px 0;
            border: 1px solid transparent;
            -moz-box-sizing: content-box;
            -webkit-box-sizing: content-box;
            box-sizing: content-box;
        }

        .item {
            position: absolute;
            line-height: 100px;
            margin: 0px;
            z-index: 1;
        }

        .item.muuri-dragging,
        .item.muuri-releasing {
            z-index: 9999;
        }

        .item.muuri-dragging {
            cursor: move;
        }

        .item.muuri-hidden {
            z-index: 0;
        }

        .item-normal {
            height: 325px;
        }

        .item-horizontal, .item-vertical {
            height: 650px;
        }

        .item-content {
            position: relative;
            width: 100%;
            height: 100%;
            padding: 10px;
        }

    </style>
    <div class="backgroung_page_catalog">
        <div class="container">
            <div class="category-index">
                <div class="">
                    <div class="col_sm-12 catal_cat_public-view padding_0">
                        <div class="public_view-img">
                            <div class="block-pub-left">
                                <a>
                                    <img src="<?= $model->getImageSrc(); ?>">
                                </a>
                                <span class="div_catal_public-view">
                                    <img class="class-img-public-view" src="/images/pub1.png">
                                    <span class="pad_span_right">
                                        <?= $model->getFollowCount(); ?>
                                    </span>
                                </span>
                            </div>
                            <div class="block-pub-right">
                                <span class="a_name-pub"><?= $model->name; ?></span>
                                <?php if (!\Yii::$app->user->isGuest) { ?>
                                    <?php if ($model->getFollowStatus()) { ?>
                                        <button class="btn js-follow public_bottom113 active" data-type="brand"
                                                data-object_id="<?= $model->id; ?>"><span
                                                    class="follow">Following</span>
                                        </button>
                                    <?php } else { ?>
                                        <button class="btn js-follow public_bottom113 not_active" data-type="brand"
                                                data-object_id="<?= $model->id; ?>"><span
                                                    class="follow">Follow</span>
                                        </button>
                                    <?php } ?>
                                <?php } else { ?>
                                    <button class="btn not_active js-login-modal"><span class="follow">Follow</span>
                                    </button>
                                <?php } ?>
                                <input type="hidden" id="js-company-template" value='<?= $model->template; ?>'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="backgroung_page_catalog">
        <div class="container">
            <div class="category-index row">
                <?= $this->render('//category/_search', ['model' => $searchModel, 'modelCategories' => $modelCategories]); ?>
            </div>
        </div>
    </div>
    <div class="backgroung_page_catalog back_gray">
        <div class="container">
            <div class="category-index row">
                <div class='col-lg-2 col-md-3 col-sm-3 padding-right-null pos-n-categ'>
                    <?= $this->render('//category/_lelf_sidebar_categories', ['model' => $modelCategories, 'category_id' => $searchModel->category_id ? $searchModel->category_id : []]); ?>
                </div>
                <div class='col-lg-10 col-md-9 col-sm-9'>
                    <div class="col-ld-12 padding_0 wight_med_catal">
                        <?php foreach ($dataProvider->getModels() as $product) { ?>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-container-padding product-one-all-no-home">
                                <?= $this->render('//templates/_product', ['product' => $product, 'link' => '/frame/publication/product']) ?>
                            </div>
                        <?php } ?>

                        <?php if (!$dataProvider->getModels()) { ?>
                            <p class="text-center">Not found</p>
                        <?php } ?>
                    </div>
                    <div class="bottom_pagination" style="clear: both;display: block">
                        <div class="col-sm-2 col-xs-12" style="padding:0px;">
                            <?= Html::dropDownList('paginCount',
                                $searchModel->paginCount,
                                \frontend\models\ProductsSearch::$arrayPagination,
                                [
                                    'class' => 'form-control js-pagin-count-select class-plag-count',
                                    'style' => 'margin:20px 0 45px 0;',
                                ]) ?>
                        </div>
                        <div class="col-sm-10 col-xs-12"
                        <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php /* ?>
 * <div class="row">
                <div class='col-lg-3 col-md-3 col-sm-3'>
                    <?= $this->render('//category/_lelf_sidebar_categories', ['model' => $modelCategories, 'category_id' => $searchModel->category_id ? $searchModel->category_id : []]); ?>
                </div>
                <div class='col-lg-9 col-md-9 col-sm-9'>
                    <div class="right_colum_collection rig_collec_hom">
                        <?php if (($model->getActivePremiumCount() == 0) || (isset($_GET['page']) && ($_GET['page'] > 1))) {?>
                            <?php foreach ($dataProvider->getModels() as $product) { ?>
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-container-padding">
                                    <?= $this->render('//templates/_product', ['product' => $product])?>
                                </div>
                            <?php } ?>
                            <?php if (!$dataProvider->getModels()) { ?>
                                <p class="text-center">Not found</p>
                            <?php } ?>
                        <?php } else { ?>
                                <div class="grid">
                                    
                                </div>
                        <?php } ?>
                                
                        <?php foreach($dataProvider->getModels() as $key => $pr) { ?>
                            <?php break; ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="bottom_pagination" style="clear: both;display: block">
                <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
            </div>
<? */ ?>