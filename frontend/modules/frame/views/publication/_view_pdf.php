<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Products */

?>
<div class="products-view">
    <input type="hidden" id="pdf_file_src" value="/<?= $model->file_src; ?>">
    <div id="flipbookContainer">
        
    </div>
</div>