$(document).ready(function(){
    var product_id = $("#add-comment-form").find('#productscomments-product_id').val();

    if ($.cookie('last_reviewed_data')) {
        var arrayLastReviewed = jQuery.parseJSON($.cookie('last_reviewed_data'))
        if ( jQuery.isArray(arrayLastReviewed) ) { 
            
            if ($.inArray( product_id, arrayLastReviewed ) < 0) {
                arrayLastReviewed.unshift(product_id);

                $.cookie('last_reviewed_data', JSON.stringify(arrayLastReviewed), {
                    expires: 365,
                    path: '/',
                });
            }
        }
    } else {
        var arrayLastReviewed = [];
        arrayLastReviewed.unshift(product_id);

        $.cookie('last_reviewed_data', JSON.stringify(arrayLastReviewed), {
            expires: 365,
            path: '/',
        });
    }
    
    //
    //comment begin
    $(document).on('click', '.answerToComment', function(){
        $('#answerCommentForm').modal('show');
        var comment_id = $(this).data('comment_id');
        $('#add-answer-to-comment').find('#commentanswer-parent_id').val(comment_id);
        
    })
    
    $( "#add-comment-form " ).submit(function( event ) {
        event.preventDefault();
        var thisElement = $(this);
        var description = $(this).find('#productscomments-description').val();
        var product_id = $(this).find('#productscomments-product_id').val();
        var parent_id = null;
        
        $.ajax({
            url: '/products/create-product-comment',
            method:'post',
            data:{description:description, product_id:product_id, parent_id:parent_id},
            dataType: 'json',
            success: function (response) {
                if(response.status){
                    thisElement.find('#productscomments-description').val('')
                    
                    var comment = response.comment;
                    $('.js-comment-container').prepend('<div class="row">'+
                            '<div class="col-sm-12">'+
                                '<div class="content-comment-cards-goods">'+
                                    '<a href="#" class="user_name">'+comment['username']+'</a>'+
                                        ' <span class="point">.</span> <span>'+comment['created_at']+'</span>'+
                                    '<p>'+comment['description']+'</p>'+
                                    '<div style="padding-left:40px;" class="js-comment-childs childs-comment-'+comment['id']+'-container">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>');
                }
            }
        })
    });
    
    
    $( "#add-answer-to-comment " ).submit(function( event ) {
        event.preventDefault();
        var thisElement = $(this);
        var description = $(this).find('#commentanswer-description').val();
        var product_id = $(this).find('#commentanswer-product_id').val();
        var parent_id = $(this).find('#commentanswer-parent_id').val();
        $.ajax({
            url: '/products/create-product-comment',
            method:'post',
            data:{description:description, product_id:product_id, parent_id:parent_id},
            dataType: 'json',
            success: function (response) {
                if(response.status){
                    thisElement.find('#productscomments-description').val('')
                    
                    var comment = response.comment;
                    $('.js-comment-childs.childs-comment-'+parent_id+'-container').append('<div class="row">'+
                            '<div class="col-sm-12">'+
                                '<div class="content-comment-cards-goods">'+
                                    '<a href="#" class="user_name">'+comment['username']+'</a>'+
                                        ' - <span>'+comment['created_at']+'</span>'+
                                    '<p>'+comment['description']+'</p>'+
                                    '<a class="answerToComment" data-comment_id="'+comment['id']+'">Reply</a>'+
                                '</div>'+
                            '</div>'+
                        '</div><hr>');
                        $('#answerCommentForm').modal('hide');
                }
            }
        })
    });
    //comment end
    //
    $(".embed-close").click(function(e) {
        e.preventDefault();
        $(".embed-box").toggleClass('open');
    });
    $(".js-copy-link").click(function(e) {
        e.preventDefault();
        changeEmbed($(".embed-box"), $(".js-copy-link"));
        $(".embed-box").toggleClass('open');
       
    });

    $(".embed-panel .fields input").keyup(function(e) {
        e.preventDefault();
        changeEmbed($(".embed-box"), $(".js-copy-link"));
    });

    function changeEmbed (box, el){
        var link = el.data('link');
        var width = box.find('input#width').val();
        var height = box.find('input#height').val();
        var sourceCode = '<iframe src="' + link +'" frameborder="0" width="' + width + '" height="' + height + '"></iframe>';
        $(".embed-box .source-code").text(sourceCode);
    }
})
