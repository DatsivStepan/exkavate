 jQuery(document).ready(function() {
    $('#maine-slider').owlCarousel({
        center: true,
        items:2,
        loop:true,
        margin:20,
        nav:true,
        navText: [],
        autoWidth:true,
        responsive:{
            600:{
                items:2
            },
            1000:{
                items:2
            }
        }
    });
     
    $('#owl-pulications').owlCarousel({
    loop:false,
    margin:20,
    nav:true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});
});
function CategoriesSlide() {
    var hItem = 60;
    var hBox = 0;
    var minHeight = $(".categories-box").height();
    var toggleStatus = false;
    var toggleText = ['Expand Categories  <i class="fa fa-angle-right" aria-hidden="true"></i>', 'Collapse categories <i class="fa fa-angle-right" aria-hidden="true"></i>'];
    var toggleBtn = $(".categories .toggle-categories");

    this.init = function() {
      hBox = $(".categories-box > .row").height();
      $( ".categories-box .category-items" ).each(function(i, el) {
        if(i >= 7){
          hItem = $(el).height();
          toggleBtn.show();
        }
      });

      toggleBtn.on('click', function(e){
        $(".categories-box").animate({
          height: toggleStatus ? minHeight : hBox
        }, 500, function(){
          toggleBtn.html(toggleText[parseInt(+toggleStatus)]);
        });
        toggleStatus = !toggleStatus;
        e.preventDefault();
      });
    };
  
  }


$(document).ready(function() {
  

  new CategoriesSlide().init();

  var owl1 = $('#carousel1').owlCarousel({
    loop: false,
    margin: 24,
    responsiveClass: true,
    
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });
  owl1.on('changed.owl.carousel', function(event) {
    $('#carousel1 .item').show();
  });
  $('#adv-prev1').click(function() {
    owl1.trigger('prev.owl.carousel');
  });

  $('#adv-next1').click(function() {
    owl1.trigger('next.owl.carousel', [300]);
  });

  var owl2 = $('#carousel2').owlCarousel({
    loop: false,
    margin: 24,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });
  owl2.on('changed.owl.carousel', function(event) {
    $('#carousel2 .item').show();
  });
  $('#adv-prev2').click(function() {
    owl1.trigger('prev.owl.carousel');
  })

  $('#adv-next2').click(function() {
    owl1.trigger('next.owl.carousel', [300]);
  })

  var owl3 = $('#carousel3').owlCarousel({
    loop: false,
    margin: 24,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });
  owl3.on('changed.owl.carousel', function(event) {
    $('#carousel3 .item').show();
  });
  $('#adv-prev3').click(function() {
    owl1.trigger('prev.owl.carousel');
  })

  $('#adv-next3').click(function() {
    owl3.trigger('next.owl.carousel', [300]);
  })
});