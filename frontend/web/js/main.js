$(document).ready(function(){
    $("#search-klock").click(function () {
        $(".input_search_ico").val("");
    });
if($('#owl-news-mobile').length){
    $('#owl-news-mobile').owlCarousel({
    loop:true,
    nav:false,
    center:true,
    items:2,
    autoWidth:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:1
        },
        1000:{
            items:5
        }
    }
});
}    
if($('#owlHoweWorks').length){
    $('#owlHoweWorks').owlCarousel({
    loop:false,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});
}    
if($('#owl-news').length){
    $('#owl-news').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});
}
        $(document).on('click', '.js-last-reviewed', function() {
        $('.js-last-reviewed-block').toggle()
        if ($.cookie('last_reviewed_data')) {
            var arrayLastReviewed = jQuery.parseJSON($.cookie('last_reviewed_data'))
            if ( jQuery.isArray(arrayLastReviewed) ) {
                $.ajax({
                    url: '/category/get-products-by-id/',
                    type: 'POST',
                    data:{products_id:arrayLastReviewed},
                    dataType: 'json',
                    success: function (response) {
                        $('.js-last-reviewed-block').html('')
                        if (response.status) {
                            $.each(response.data, function( index, product ) {
                                $('.js-last-reviewed-block').prepend('' + 
                                            '<a href="'+product['link']+'" style="display:block;">' +
                                                '<img style="width:20%;height:30px;float:left;" src="'+product['image']+'">' +
                                                '<p style="width:80%;height:30px;float:left;">'+product['name']+'</p>' +
                                            '</a><hr>' +
                                        '')
                            });
                        } else {
                            $('.js-last-reviewed-block').html('<h5 class="text-center">Empty</h5>')
                        }
                    }
                })
            } else {
                $('.js-last-reviewed-block').html('<h5 class="text-center">Empty</h5>')
            }
        } else {
                $('.js-last-reviewed-block').html('<h5 class="text-center">Empty</h5>')
        }
    });

    $('.colection-popap').on('click', '.message-block', function(){
        $(this).html('')
    })

    $('.colection-popap').on('click', '.js-popap-add-button', function(){
        var thisElement = $(this)
        var value = $('.colection-popap').find('.js-popap-add-input').val();
        if(value.length > 3){
            thisElement.prop('disabled', true);
            $.ajax({
                url: '/profile/add-my-collection/',
                type: 'POST',
                data:{value:value},
                dataType: 'json',
                success: function (response) {
                    thisElement.prop('disabled', false);
                    $('.colection-popap').find('.message-block').html('')
                    if(response.status){
                        $('.colection-popap').find('.js-popap-add-input').removeClass('error')
                        $('.colection-popap').find('.js-popap-add-input').val('')
                        var key = response.id;
                        $('.colection-popap').find('.message-block').html(response.message)
                        $('.colection-popap').find('.js-collection-list').prepend(
                            '<div>'+
                                '<input type="checkbox"  data-collection_id="'+key+'"  id="collection'+key+'" name="choiseCollectionInput" value="'+key+'">'+
                                '<label for="collection'+key+'">'+value+'</label>'+
                            '</div>');
                    } else {
                        $('.colection-popap').find('.message-block').html(response.message)
                    }
                }
            });
        } else {
            $('.colection-popap').find('.js-popap-add-input').addClass('error')
        }
    });
    
    $('.colection-popap').on('click', '.js-save-choise-collection', function(){
        var product_id = $(this).data('product_id')
        var arrayCollection = [];
        $('.colection-popap .js-collection-list').find('[name=choiseCollectionInput]').each(function() {
            if($( this ).is(':checked')){
                arrayCollection.push($( this ).data('collection_id'));
            }
        });
        
        $.ajax({
            url: '/profile/save-collection-choise/' + product_id,
            type: 'POST',
            data:{arrayCollection:arrayCollection},
            dataType: 'json',
            success: function (response) {
                if(response.status){
                    $('.colection-popap').hide();
                    $('.js-my-collection-' + product_id).find('.js-my-collection-count').text(response.count);
                }
            }
        });
        
    })
    
    
    $('.add-to-collection').on('click', function(event) {
        $('.colection-popap').html('');
        event.stopPropagation();
        var thisElement = $(this);
        
        $('.colection-popap').css({
            width:thisElement.closest('.js-product-block').outerWidth(true) + 15,
            top:thisElement.offset().top + 45,
            left:thisElement.closest('.js-product-block').offset().left
        }).show();
        $('.triangle').css({
            top:thisElement.offset().top + 32,
            left:thisElement.offset().left - 4
        }).show();
        $('body').addClass('open-colection-popap');
        
        var product_id = thisElement.data('product_id');
        $.ajax({
            url: '/profile/get-collection-choise/' + product_id,
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                $('.colection-popap').html('');
                var title = $('<p/>', {
                        class: 'c-title',
                    }).text('Add to Collections');
                $('.colection-popap').append(title);
                
                var formAdd = $('<div/>', {
                        class: 'form-block',
                    }).append(
                        $('<input/>', {
                            type: 'text',
                            placeholder: 'Create...',
                            class: 'form-input js-popap-add-input',
                        }),
                        $('<button/>', {
                            class: 'btn btn-default js-popap-add-button',
                        }).html('<img class="" src="/images/ic_add_black_24px.png">'),
                        
                        $('<div/>', {
                            class: 'clearfix',
                        })
                    );
                $('.colection-popap').append(formAdd);
                $('.colection-popap').append('<div class="message-block"></div>');
                
                var collectionList = $('<div/>', {
                    class: 'js-collection-list collection-scrol-bar', 
                });
                    $.each( response.collectionArray, function( key, value ) {
                        var ch = '';
                        if ($.inArray(parseInt(key), response.choiseCollectionArray) >= 0) {
                            ch = 'checked';
                        }
                        collectionList.append(
                                '<div> <span for="collection'+key+'" class="chek-colection"></span>'+
                                    '<input type="checkbox" '+ch+' data-collection_id="'+key+'"  id="collection'+key+'" name="choiseCollectionInput" value="'+key+'">'+
                                    '<label for="collection'+key+'">'+value+'</label>'+
                                '</div>'
                        )
                    })
                $('.colection-popap').append(collectionList);
                
                
                var saveChoiseCollectionButton = $('<div/>', {
                    class: 'collection-bnt-container',
                }).html($('<button/>', {
                    class: 'btn btn-primary js-save-choise-collection',
                    'data-product_id': product_id
                }).text('Save'));
                $('.colection-popap').append(saveChoiseCollectionButton);
            }
        })
        
        
   });


    $('body').on('click', function(){
        if ( $("body").hasClass("open-colection-popap")) {
            $('.colection-popap').hide();
            $('.triangle').css('display', 'none');
            $('body').removeClass('open-colection-popap');
        };
    }); 
  $('.colection-popap').on('click', function(event){
    event.stopPropagation();
  }); 
    
    $(document).on('click','.js-login-modal', function(e){
        e.preventDefault();
        $('#loginModal').modal('show');
    });
    
        $(document).on('click', '.js-follow.not_active', function(e){
            e.preventDefault()
            var object_id =  $(this).data('object_id');
            var type =  $(this).data('type');
            $.ajax({
                url: '/site/follow',
                type: 'POST',
                data:{object_id:object_id, type:type},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $('.js-follow').each(function( index ) {
                            if (($( this ).data('object_id') == object_id) && ($( this ).data('type') == type)){
                                $( this ).removeClass('not_active')
                                $( this ).addClass('active')
                                $( this ).find('span.follow').text('Unfollow');
                            }
                        });
                    }
                }
            })
        })
        
        $(document).on('click', '.js-follow.active', function(e){
            e.preventDefault()
            var thisButton = $(this);
            var object_id =  $(this).data('object_id');
            var type =  $(this).data('type');
            $.ajax({
                url: '/site/unfollow',
                type: 'POST',
                data:{object_id:object_id, type:type},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $('.js-follow').each(function( index ) {
                            if (($( this ).data('object_id') == object_id) && ($( this ).data('type') == type)){
                                $( this ).removeClass('active')
                                $( this ).addClass('not_active')
                                $( this ).find('span.follow').text('Follow');
                            }
                        });
                    }
                }
            })
        })
    
        $(document).on('click', '.js-like.not_active', function(){
            var object_id =  $(this).data('object_id');
            var type =  $(this).data('type');
            $.ajax({
                url: '/site/like',
                type: 'POST',
                data:{object_id:object_id, type:type},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $('.js-like').each(function( index ) {
                            if (($( this ).data('object_id') == object_id) && ($( this ).data('type') == type)){
                                $( this ).removeClass('not_active')
                                $( this ).addClass('active')
                                var like_count = $( this ).parent().find('.js-like-count').text();
                                $( this ).parent().find('.js-like-count').text(parseInt(like_count) + 1);
                            }
                        });
                    }
                }
            })
        })
        
        $(document).on('click', '.js-like.active', function(){
            var thisButton = $(this);
            var object_id =  $(this).data('object_id');
            var type =  $(this).data('type');
            $.ajax({
                url: '/site/unlike',
                type: 'POST',
                data:{object_id:object_id, type:type},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $('.js-like').each(function( index ) {
                            if (($( this ).data('object_id') == object_id) && ($( this ).data('type') == type)){
                                $( this ).removeClass('active')
                                $( this ).addClass('not_active')
                                var like_count = $( this ).parent().find('.js-like-count').text();
                                $( this ).parent().find('.js-like-count').text(parseInt(like_count) - 1);
                            }
                        });
                    }
                }
            })
        })
    
    
    
    
    
    
    $(document).on('click', '.js-register-link', function(){
        $('#loginModal').modal('hide');
        $('#signupModal').modal('show');
    })
    $(document).on('click', '.js-login-link', function(){
        $('#signupModal').modal('hide');
        $('#loginModal').modal('show');
    })
    
    $( "#form-newslater" ).submit(function( event ) {
        event.preventDefault();
        var thisElement = $(this)
        var email = $(this).find('.js-newslater-email').val();
        $.ajax({
            url: '/site/save-newslater-email',
            method:'post',
            data:{email:email},
            dataType: 'json',
            success: function (response) {
                //$( "#myModal3" ).modal('hide')
                if(response.status){
                    thisElement.find('input[type=email]').val('')
                    
                    thisElement.append('<div class="alert js-alert-newslater-status alert-dismissible alert-success show alert_fade_sign_up alert-subscribe-home" role="alert">'+
                            '<span class="message">'+
                                '<strong>Holy guacamole!</strong> You should check in on some of those fields below.'+
                            '</span>'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                '<span aria-hidden="true">&times;</span>'+
                            '</button>'+
                        '</div>')
                    
//                    thisElement.find('.js-alert-newslater-status').addClass('alert-success');
//                    thisElement.find('.js-alert-newslater-status').addClass('show');
//                    thisElement.find('.js-alert-newslater-status').removeClass('fade');
                } else {
                    thisElement.append('<div class="alert js-alert-newslater-status alert-dismissible alert-danger show alert_fade_sign_up alert-subscribe-home" role="alert">'+
                            '<span class="message">'+
                                '<strong>Holy guacamole!</strong> You should check in on some of those fields below.'+
                            '</span>'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                '<span aria-hidden="true">&times;</span>'+
                            '</button>'+
                        '</div>')
//                    thisElement.find('.js-alert-newslater-status').addClass('alert-danger');
//                    thisElement.find('.js-alert-newslater-status').addClass('show');
//                    thisElement.find('.js-alert-newslater-status').removeClass('fade');
                }
                thisElement.find('.js-alert-newslater-status span.message').html(response.message);
            }
        })
    });
    $( "#form-newslater2" ).submit(function( event ) {
        event.preventDefault();
        var thisElement = $(this)
        var email = $(this).find('.js-newslater-email').val();
        $.ajax({
            url: '/site/save-newslater-email',
            method:'post',
            data:{email:email},
            dataType: 'json',
            success: function (response) {
//                $( "#myModal3" ).modal('hide')
                if(response.status){
                    thisElement.find('input[type=email]').val('')
                    
                    thisElement.append('<div class="alert js-alert-newslater-status alert-dismissible alert-success show alert_fade_sign_up alert-subscribe-home" role="alert">'+
                            '<span class="message">'+
                                '<strong>Holy guacamole!</strong> You should check in on some of those fields below.'+
                            '</span>'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                '<span aria-hidden="true">&times;</span>'+
                            '</button>'+
                        '</div>')
                } else {
                    thisElement.append('<div class="alert js-alert-newslater-status alert-dismissible alert-danger show alert_fade_sign_up alert-subscribe-home" role="alert">'+
                            '<span class="message">'+
                                '<strong>Holy guacamole!</strong> You should check in on some of those fields below.'+
                            '</span>'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                '<span aria-hidden="true">&times;</span>'+
                            '</button>'+
                        '</div>')
                }
                thisElement.find('.js-alert-newslater-status span.message').html(response.message);
            }
        })
    });
    
    $( "#login-form" ).submit(function( event ) {
        event.preventDefault();
        var username = $(this).find('#loginform-username').val();
        var password = $(this).find('#loginform-password').val();
        var remember = $(this).find('#loginform-rememberme').val();
        $.ajax({
            url: '/site/ajax-login',
            method:'post',
            data:{LoginForm:{username:username, password:password, remember:remember}},
            dataType: 'json',
            success: function (response) {
                if(response.status){
                    location.reload();
                } else {
                    $('#loginform-password').parent().find('.help-block').html('<p>Incorrect username or password.</p>')
                    $('#loginform-password').parent().find('.help-block').addClass('help-block-error');
                    $('#loginform-password').css('border', '1px solid #a94442');
                    $('#loginform-username').css('border', '1px solid #a94442');
                }
            }
        })
    });
    $(document).on('keypress', '#loginform-username, #loginform-password', function(){
        $('#loginform-password').parent().find('.help-block').html('')
        $('#loginform-password').parent().find('.help-block').removeClass('help-block-error')
        $('#loginform-password').css('border', '1px solid #c7c7c7');
        $('#loginform-username').css('border', '1px solid #c7c7c7');
    })
    
//    $( "#form-signup" ).submit(function( event ) {
//        event.preventDefault();
//        var username = $(this).find('#signupform-username').val();
//        var password = $(this).find('#signupform-password').val();
//        var email = $(this).find('#signupform-email').val();
//        var reCaptcha = $(this).find('#signupform-recaptcha').val();
//        $.ajax({
//            url: '/site/ajax-signup',
//            method:'post',
//            data:{SignupForm:{username:username, password:password, email:email, type:'user', reCaptcha:reCaptcha}},
//            dataType: 'json',
//            success: function (response) {
//                if(response.status){
//                    location.reload();
//                }
//            }
//        });
//    });

    $(".menu-button").click(function(e){
        e.preventDefault();
        $('.mobile-menu-modal-container').show();
        $('.mobile-menu').show().animate({
            right:'90%'
        },500);
    });
    $(".mobile-close-menu").click(function(){
        $('.mobile-menu-modal-container').hide();
        $('.mobile-menu').hide('slow').css({right:0});
    });
});
