$(function () {

    var m = {};
    function formTemplate(template, type, product_id){
        var arrayValue = [];
        template.forEach(function (item, i) {
            var element = {};
            element.product_id = item.product_id;
            element.type = item.type;
            arrayValue.push(element)
        });

        var number = 4;
        if (type == 'horizontal') {
            number = number+4;
        }
        console.log(arrayValue);
        var newArray = [];
        var reverseArray = arrayValue.reverse()
        reverseArray.forEach(function (item, i) {
            if (item.type == 'normal') {
                if(number > 0) {
                    number = number - 1;
                } else {
                    var element = {};
                    element.product_id = item.product_id;
                    element.type = item.type;
                    newArray.push(element)
                }
            }else {
                var element = {};
                element.product_id = item.product_id;
                element.type = item.type;
                newArray.push(element)
            }
        });
        arrayValue = newArray.reverse();
        arrayValue.push({type:type, product_id:product_id, text:'your new ads'});

        return arrayValue;
    }
    

    function getDefaultTemplate(type, product_id){
        var arrayValue = [];
        var productCount = $('.productCount').text();
        if (type == 'horizontal') {
            arrayValue.push({type:type, product_id:product_id, text:'your new ads'});
        } else {
            arrayValue.push({type:type, product_id:product_id, text:'your new ads'});
        }

        for (var i = 0; i < productCount; i++) {
            var element = {};
            element.product_id = null;
            element.type = 'normal';
            arrayValue.push(element)
        }
        
        return arrayValue;
    }

//    var pType = 'vertical';
//    if ($('#products-premium_type').val() == 2) { 
//        pType = 'horizontal';
//    }
//    var pId = $('#products-id').val();
//    var TemplateValue = getDefaultTemplate(pType, pId);
//    if ($('#products-template').val()) {
//        TemplateValue = formTemplate(jQuery.parseJSON($('#products-template').val()), pType, pId)
//    }
    var TemplateValue = jQuery.parseJSON($('#products-template').val())
    
    
  var $grid = $('.grid');
  var $root = $('html');
  var uuid = 0;
  var grid = null;

  // Bind events.
  $('.demo-init').on('click', init);
  $('.demo-destroy').on('click', destroy);
  $('.demo-show').on('click', show);
  $('.demo-hide').on('click', hide);
  $('.demo-add').on('click', add);
  $('.demo-remove').on('click', remove);
  $('.demo-refresh').on('click', refresh);
  $('.demo-layout').on('click', layout);
  $('.demo-synchronize').on('click', synchronize);

  // Init.
    init();
  //
  // Helper utilities
  //

    function saveGrid(){
        var templateArray = [];
        grid.getItems().forEach(function (item, i) {
            item = item._element;
            templateArray.push({
                type:item.getAttribute('data-type'),
                product_id:item.getAttribute('data-product_id')
            });
        });

        $('#products-template').val(JSON.stringify(templateArray))
    }

  function generateElements(shablon) {


        var ret = [];
	var itemElem = $("div");
	
	$.each( shablon, function( key, value ) {
            
            var text = ++uuid;
            if (value.text) {
                text = value.text;
            }
            var stuffDiv = document.createElement('div');
              stuffDiv.setAttribute('class', 'card');
              stuffDiv.appendChild(document.createTextNode(text));

              var innerDiv = document.createElement('div');
              innerDiv.setAttribute('class', 'item-content');
              innerDiv.appendChild(stuffDiv);

              var outerDiv = document.createElement('div');
              var width = Math.floor(Math.random() * 2) + 1;
              var height = Math.floor(Math.random() * 2) + 1;
              outerDiv.setAttribute('class', 'item item-'+value.type);
              outerDiv.setAttribute('data-id', key);
              outerDiv.setAttribute('data-type', value.type);
              outerDiv.setAttribute('data-product_id', value.product_id);
              outerDiv.appendChild(innerDiv);

              ret.push(outerDiv);
	})

    return ret;
}

  function init() {

    if (!grid) {

      var dragCounter = 0;

      grid = new Muuri({
        container: $grid.get(0),
        items: generateElements(TemplateValue),
        dragEnabled: true,
        dragReleaseEasing: 'ease-in',
        dragContainer: document.body
      });

        grid
        .on('dragstart', function () {
          ++dragCounter;
          $root.addClass('dragging');
        })
        .on('dragend', function () {
            if (--dragCounter < 1) {
              $root.removeClass('dragging');
            }
            saveGrid();
        });
        

    }

  }

  function destroy() {

    if (grid) {
      grid.destroy();
      $grid.empty();
      grid = null;
      uuid = 0;
    }

  }

  function show() {

    if (grid) {
      grid.show(grid.get('inactive').slice(0, 5), function (items) {
        console.log('CALLBACK: Hide ' + items.length + ' items');
      });
    }

  }

  function hide() {

    if (grid) {
      grid.hide(grid.get('active').slice(0, 5), function (items) {
        console.log('CALLBACK: Hide ' + items.length + ' items');
      });
    }

  }

  function add() {

    if (grid) {
      var items = generateElements({1:'vertical', 2:'normal', 3:'normal', 4:'normal', 5:'normal', 6:'horizontal'});
      items.forEach(function (item) {
        item.style.display = 'none';
      });
      grid.show(grid.add(items), function (items) {
        console.log('CALLBACK: Added ' + items.length + ' items');
      });
    }

  }

  function remove() {

    if (grid) {
      grid.hide(grid.get('active').slice(0, 5), function (items) {
        grid.remove(items, true);
        console.log('CALLBACK: Removed ' + items.length + ' items');
      });
    }

  }

  function layout() {

    if (grid) {
      grid.layout(function () {
        console.log('CALLBACK: Layout');
      });
    }

  }

  function refresh() {

    if (grid) {
      grid.refresh();
    }

  }

  function synchronize() {

    if (grid) {
      grid.synchronize();
    }

  }

});
