$(function () {

    var m = {};
    function formTemplate(template, type, product_id){
        var arrayValue = [];
        template.forEach(function (item, i) {
            var element = {};
            element.product_id = item.product_id;
            element.type = item.type;
            arrayValue.push(element)
        });

        var number = 4;
        if (type == 'horizontal') {
            number = number+4;
        }
        var newArray = [];
        var reverseArray = arrayValue.reverse()
        reverseArray.forEach(function (item, i) {
            if (item.type == 'normal') {
                if(number > 0) {
                    number = number - 1;
                } else {
                    var element = {};
                    element.product_id = item.product_id;
                    element.type = item.type;
                    newArray.push(element)
                }
            }else {
                var element = {};
                element.product_id = item.product_id;
                element.type = item.type;
                newArray.push(element)
            }
        });
        arrayValue = newArray.reverse();
        arrayValue.push({type:type, product_id:product_id});

        return arrayValue;
    }
    

    function getDefaultTemplate(){
        var arrayValue = [];
        var number = 44;

        for (var i = 0; i < number; i++) {
            var element = {};
            element.product_id = null;
            element.type = 'normal';
            arrayValue.push(element)
        }
        
        return arrayValue;
    }
    var TemplateValue = getDefaultTemplate();
    if ($('#js-company-template').val()) {
        console.log($('#js-company-template').val())
        TemplateValue = jQuery.parseJSON($('#js-company-template').val())
    }
    
//    console.log(TemplateValue);
    
  var $grid = $('.grid');
  var $root = $('html');
  var uuid = 0;
  var grid = null;

  // Bind events.
  $('.demo-init').on('click', init);
  $('.demo-destroy').on('click', destroy);
  $('.demo-show').on('click', show);
  $('.demo-hide').on('click', hide);
  $('.demo-add').on('click', add);
  $('.demo-remove').on('click', remove);
  $('.demo-refresh').on('click', refresh);
  $('.demo-layout').on('click', layout);
  $('.demo-synchronize').on('click', synchronize);

  // Init.
    init();
  //
  // Helper utilities
  //

    function saveGrid(){
        var templateArray = [];
        grid.getItems().forEach(function (item, i) {
            item = item._element;
            templateArray.push({
                type:item.getAttribute('data-type'),
                product_id:item.getAttribute('data-product_id')
            });
        });

        $('#products-template').val(JSON.stringify(templateArray))
        console.log(templateArray);
    }

  function generateElements(shablon) {


        var ret = [];
	var itemElem = $("div");
	$.each( shablon, function( key, value ) {
            var product = [];
            if (value.type != 'normal') {
                $.ajax({
                    url:'/products/get-product-data/' + value.product_id,
                    dataType:'json',
                }).done(function(response){
                    if (response.status) {
                        $('.productBlock' + value.product_id).find('.img_ki_44').css('background', 'url('+response.data.img+') center no-repeat');
                        $('.productBlock' + value.product_id).find('.data_tovar_catal111').text(response.data.date);
                        $('.productBlock' + value.product_id).find('.name_naw_catal111').text(response.data.name);
                        $('.productBlock' + value.product_id).find('.name_naw_catal111').attr('href', response.data.link);
                    }
                });
            }

            var divTovar = $('<div/>', {
                class:'tovar_profile_one111 tovar_profile_one65 js-product-block productBlock'+value.product_id,
            }).append(
                $('<div/>', {
                    class:'im_ju_3'
                }).append(
                        $('<a/>', {
                            href:'/products/view/7'
                        }).append(
                            $('<div/>', {
                                class:'img_ki_44',
                                style:"background: url(/publisher/files/products/1/1511209810_7412.png) center no-repeat"
                            }),
                        ),
                        $('<p/>', {
                            class:'data_tovar_catal111',
                            text:"04 Nov, 2017"
                        }),
                        $('<a/>', {
                            class:'name_naw_catal111',
                            text:"name product",
                            href:"/products/view/7"
                        }),
                ),
                $('<div/>', {
                    class:'div_catal_bott111 div_bot_56'
                }).append(
                    $('<a/>', {
                        class:'icon_like js-like-link-block',
                        'data-title': "Like",
                        'data-type':"product",
                        'data-object_id':"7",
                    }).append(
                        $('<div/>', {
                            class:'bacg_img',
                        }),
                        $('<span/>', {
                            class:'pad_span2_right_222 pad_span23_right_22225 js-like-count',
                            text:"5",
                        }),
                    ),
                    $('<a/>', {
                        class:'js-views-link-block',
                    }).append(
                        $('<img/>', {
                            class:'img_ico',
                            src:'/images/111.png',
                        }),
                        $('<span/>', {
                            class:'pad_span2_right_222 pad_span23_right_22225 js-like-count',
                            text:"2",
                        }),
                    ),
                    $('<a/>', {
                        class:'js-comment-link-block',
                    }).append(
                        $('<img/>', {
                            class:'img_ico',
                            src:'/images/222.png',
                        }),
                        $('<span/>', {
                            class:'pad_span2_right_222 pad_span23_right_22225 js-like-count',
                            text:"2",
                        }),
                    ),
                    $('<a/>',{
                        class:'js-collection-link-block',
                        'data-object_id':"7",
                    }).append(
                        $('<img/>', {
                            class:'img_ico img_ico-last',
                            src:'/images/+.png',
                        }),
                        $('<span/>', {
                            class:'pad_span2_right_222 pad_span23_right_22225 js-my-collection-count',
                            text:"0",
                        }),
                    ),
                )
            );

            var outerDiv = $('<div/>', {
                class:'item item-'+value.type,
                'data-id':key,
                'data-type':value.type,
                'data-product_id':value.product_id,
            });
            outerDiv.append(divTovar)
              ret.push(outerDiv.get(0));
	})

    return ret;
}

  function init() {

    if (!grid) {

      var dragCounter = 0;

      grid = new Muuri({
        container: $grid.get(0),
        items: generateElements(TemplateValue),
//        dragEnabled: true,
//        dragReleaseEasing: 'ease-in',
//        dragContainer: document.body
      });

//        grid
//        .on('dragstart', function () {
//          ++dragCounter;
//          $root.addClass('dragging');
//        })
//        .on('dragend', function () {
//            if (--dragCounter < 1) {
//              $root.removeClass('dragging');
//            }
//            saveGrid();
//        });
        

    }

  }

  function destroy() {

    if (grid) {
      grid.destroy();
      $grid.empty();
      grid = null;
      uuid = 0;
    }

  }

  function show() {

    if (grid) {
      grid.show(grid.get('inactive').slice(0, 5), function (items) {
        console.log('CALLBACK: Hide ' + items.length + ' items');
      });
    }

  }

  function hide() {

    if (grid) {
      grid.hide(grid.get('active').slice(0, 5), function (items) {
        console.log('CALLBACK: Hide ' + items.length + ' items');
      });
    }

  }

  function add() {

    if (grid) {
      var items = generateElements({1:'vertical', 2:'normal', 3:'normal', 4:'normal', 5:'normal', 6:'horizontal'});
      items.forEach(function (item) {
        item.style.display = 'none';
      });
      grid.show(grid.add(items), function (items) {
        console.log('CALLBACK: Added ' + items.length + ' items');
      });
    }

  }

  function remove() {

    if (grid) {
      grid.hide(grid.get('active').slice(0, 5), function (items) {
        grid.remove(items, true);
        console.log('CALLBACK: Removed ' + items.length + ' items');
      });
    }

  }

  function layout() {

    if (grid) {
      grid.layout(function () {
        console.log('CALLBACK: Layout');
      });
    }

  }

  function refresh() {

    if (grid) {
      grid.refresh();
    }

  }

  function synchronize() {

    if (grid) {
      grid.synchronize();
    }

  }

});
