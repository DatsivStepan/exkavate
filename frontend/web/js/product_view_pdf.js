
    var flipBook;

    jQuery(document).ready(function () {

        //make sure this file is hosted in localhost or any other server
        var pdf = $('#pdf_file_src').val();

        var options = {
            webgl:true,
            height: 800,
            duration: 800,
            enableDebugLog : true,
            direction: DFLIP.DIRECTION.LTR,
            autoEnableOutline:false //auto open the outline/bookmarks tab
        };

        /**
         * outline is basically a array of json object as:
         * {title:"title to appear",dest:"url as string or page as number",items:[]}
         * items is the same process again array of json objects
         */
//        options.outline = [
//            {title: "Page 1", dest: 1},
//            {title: "Page 2", dest:2},
//            {title: "StackOverflow", dest: "https://stackoverflow.com/",items:[
//                {title:"My Profile",dest :"https://stackoverflow.com/users/6687403/deepak-ghimire"},
//                {title:"Page 4",dest:4}
//            ]}
//        ];
        options.onFlip= function(){
            if(flipBook.stage && flipBook.stage.canvas) $(".example-menu-header").html(flipBook.stage.canvas.width());
        };
        flipBook = $("#flipbookContainer").flipBook(pdf, options);
});