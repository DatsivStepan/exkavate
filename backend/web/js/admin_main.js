$(document).ready(function(){
    
    $(document).on('click', '#category-parent_id_list li', function(){
        $('#category-parent_id_name').val($(this).text())
        $('#category-parent_id').val($(this).data('category_id'))
    })
    
    $(document).on('keyup', '#category-parent_id_name', function(){
        var key_value = $(this).val();
        $.ajax({
            type: 'POST',
            url: '/admin/catalog/category/get-category-for-name',
            data: {key_value:key_value},
            dataType:'json',
            success: function(response){
                $('#category-parent_id_list').html('');
                for(var category_id in response){
                    $('#category-parent_id_list').append('<li data-category_id="'+category_id+'">'+response[category_id]+'</li>');
//                    console.log(response[category_id])
                }
                console.log(response);
            }
        })
    })
    
    $(document).on('change','#product-category_id', function(){
        var category_id = $(this).val();
        $.ajax({
            type: 'POST',
            url: '../../../site/getchildcategory',
            data: {category_id:category_id},
            dataType:'json',
            success: function(response){
                $('#product-sub_category').show()
                $('#product-sub_category').html('')
                for(var categ_id in response){
                    var category = response[categ_id];
                    $('#product-sub_category').append('<option value="'+category['id']+'">'+category['name']+'</option>');
                }
            }
        });
        
    })


    if($('.addSliderImage').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "/admin/sliders/sliders/savesliderphoto",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".addSliderImage",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.previewSlidersImage').attr('src', '/'+response.xhr.response)
                $('#sliders-img_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }

    if($('.addCategoryImage').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "/admin/category/category/savecategoryphoto",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".addCategoryImage",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.previewCategoryImage').attr('src', '/'+response.xhr.response)
                $('#category-img_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }

    if($('.addCompanyLogo').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "/admin/company/company/save-company-logo",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".addCompanyLogo",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.previewCompanyLogo').attr('src', '/'+response.xhr.response)
                $('#company-logo_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }
    
    if($('.addProductsImage').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "/admin/products/products/save-products-img",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".addProductsImage",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.previewProductsImage').attr('src', '/'+response.xhr.response)
                $('#products-img_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }
    
    if($('.addProductsFileSrc').length){
        var previewNode = document.querySelector("#template2");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneFIle = new Dropzone('.addProductsFileSrc', {
            url: "/admin/products/products/save-products-file",
            previewTemplate: previewTemplate,
            acceptedFiles:'application/pdf, image/*',
            previewsContainer: "#previews2",
            clickable: ".addProductsFileSrc",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzoneFIle.on("maxfilesexceeded", function(file) {
                myDropzoneFIle.removeAllFiles();
                myDropzoneFIle.addFile(file);
        });

        myDropzoneFIle.on("totaluploadprogress", function(progress) {
            $(".progress").show()
            document.querySelector(".progress .progress-bar").style.width = progress + "%";
        });

        myDropzoneFIle.on("complete", function(response) {
            if (response.status == 'success') {
                $(".progress").hide()
                $('.previewProductsFileSrc').html('<a href="'+response.xhr.response+'">File link</a>')
                $('#products-file_src').val(response.xhr.response);
            }
        });

        myDropzoneFIle.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }
})