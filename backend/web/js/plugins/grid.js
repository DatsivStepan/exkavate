$(function () {

    var m = {};
    var TemplateValue = jQuery.parseJSON($('#company-template').val())
    
  var $grid = $('.grid');
  var $root = $('html');
  var uuid = 0;
  var grid = null;

  // Bind events.
  $('.demo-init').on('click', init);
  $('.demo-destroy').on('click', destroy);
  $('.demo-show').on('click', show);
  $('.demo-hide').on('click', hide);
  $('.demo-add').on('click', add);
  $('.demo-remove').on('click', remove);
  $('.demo-refresh').on('click', refresh);
  $('.demo-layout').on('click', layout);
  $('.demo-synchronize').on('click', synchronize);

  // Init.
    init();
  //
  // Helper utilities
  //

    function saveGrid(){
        var templateArray = [];
        grid.getItems().forEach(function (item, i) {
            item = item._element;
            templateArray.push({
                type:item.getAttribute('data-type'),
                product_id:item.getAttribute('data-product_id')
            });
        });
        
        $('#company-template').val(JSON.stringify(templateArray))
    }

    function generateElements(shablon, indexStart = 0) {


        var ret = [];
	var itemElem = $("div");
	
	$.each( shablon, function( key, value ) {
            key = indexStart ? indexStart - 1 : key;

            var text = ++uuid;
            if (value.text) {
                text = value.text;
            }

            var stuffDiv = document.createElement('div');
              stuffDiv.setAttribute('class', 'card');
              if ((value.type == 'vertical') || (value.type == 'horizontal')) {
                  stuffDiv.setAttribute('style', 'background-color:#367fa9;');
                    stuffDiv.innerHTML = '<p>'+text+'</p>';
              } else {
                    stuffDiv.innerHTML = text;
              }

              var innerDiv = document.createElement('div');
              innerDiv.setAttribute('class', 'item-content');
              innerDiv.appendChild(stuffDiv);

              var outerDiv = document.createElement('div');
              var width = Math.floor(Math.random() * 2) + 1;
              var height = Math.floor(Math.random() * 2) + 1;
              outerDiv.setAttribute('class', 'item item-'+value.type);
              outerDiv.setAttribute('data-id', key);
              outerDiv.setAttribute('data-type', value.type);
              outerDiv.setAttribute('data-product_id', value.product_id);
              outerDiv.appendChild(innerDiv);

              ret.push(outerDiv);
	})

        return ret;
    }

    $(document).on('click', '.js-remove-product-button', function(){
        var key = $(this).data('id')
        $.ajax({
            type: 'GET',
            url: '/admin/company/company/change-product-status/' + key,
            data:{status:'deactive'},
            dataType:'json',
            success: function(response){
                if (response.status) {
                    var elementDel = $('.grid .item[data-product_id=' + key + ']');

                    grid.getItems().forEach(function (item, i) {
                        if (item._element.getAttribute('data-product_id') == key) {
                            if (elementDel.length) {
                                remove(item)
                            }
                        }
                    });
                    $('#premiumList').yiiGridView('applyFilter')
                }
            }
        })
    })

    $(document).on('click', '.js-active-product-button', function(){
        var key = $(this).data('id')
        var type = $(this).data('type')
        var text = $(this).closest('tr').find('.js-product-name').text()
        $.ajax({
            type: 'GET',
            url: '/admin/company/company/change-product-status/' + key,
            data:{status:'active'},
            dataType:'json',
            success: function(response){
                if (response.status) {
                    var itemId = $('.grid .item[data-product_id=' + key + ']').length;
                    if (itemId) {
                    } else {
                        add(key, type, text)
                    }
                    $('#premiumList').yiiGridView('applyFilter')
                }
            }
        })
    })

  function init() {

    if (!grid) {

      var dragCounter = 0;

      grid = new Muuri({
        container: $grid.get(0),
        items: generateElements(TemplateValue),
        dragEnabled: true,
        dragReleaseEasing: 'ease-in',
        dragContainer: document.body
      });

        grid
        .on('dragstart', function () {
          ++dragCounter;
          $root.addClass('dragging');
        })
        .on('dragend', function () {
            if (--dragCounter < 1) {
              $root.removeClass('dragging');
            }
            saveGrid();
        });
        

    }

  }

  function destroy() {

    if (grid) {
      grid.destroy();
      $grid.empty();
      grid = null;
      uuid = 0;
    }

  }

    function show() {

        if (grid) {

            grid.show(grid.getItems(), function (items) {
                console.log('CALLBACK: Hide ' + items.length + ' items');
            });
        }

    }

    function hide(item) {

        if (grid) {
            grid.hide(item, function (items) {
              console.log('CALLBACK: Hide ' + items.length + ' items');
            });
        }

    }

    function add(productId, type, text) {
        if (grid) {
            var templateArray = [];

            templateArray.push({
                type:type,
                product_id:productId,
                text : text
            });

            var items = generateElements(templateArray,  grid.getItems().length + 1);
            items.forEach(function (item) {
                item.style.display = 'none';
            });
            
            grid.show(grid.add(items, grid.getItems().length + 1), function (items) {
                console.log('CALLBACK: Added ' + items.length + ' items');
            });
            
            saveGrid()
        }

    }

    function remove(item) {
        if (grid) {
//            grid.hide(item)
            grid.remove(item, {removeElements: true});
            saveGrid()
        }

    }

  function layout() {

    if (grid) {
      grid.layout(function () {
        console.log('CALLBACK: Layout');
      });
    }

  }

  function refresh() {

    if (grid) {
      grid.refresh();
    }

  }

  function synchronize() {

    if (grid) {
      grid.synchronize();
    }

  }

});
