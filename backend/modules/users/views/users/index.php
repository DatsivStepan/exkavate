<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\users\models\SearchUsers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        <div class="user-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Fistr and Last name',
                        'attribute' => 'filterUsername',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getNameAndSurname();
                        }
                    ],
                    'email:email',
                    [
                        'label' => 'User Role',
                        'attribute' => 'userRole',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'userRole',
                            'data' => \common\models\auth\AuthItem::getAuthRoleArray(),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'value' => function ($model) {
                            $modelRoles = $model->userRoleModel;
                            return $modelRoles ? $modelRoles->itemName->description : null;
                        },
                    ],
                    [
                        'label' => 'Publication',
                        'attribute' => 'company_id',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'company_id',
                            'data' => \common\models\Company::getAllInArrayMap(),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->companyModel ? $model->companyModel->name : null;
                        },
                        'format' => 'raw',
                    ],
                     'created_at',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </section>
