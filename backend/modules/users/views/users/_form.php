<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Company;
use common\models\auth\AuthItem;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$classColSM6 = ['options' => ['class' => 'col-sm-6']];
$classColSM12 = ['options' => ['class' => 'col-sm-12']];
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= $form->field($model, 'first_name', $classColSM6)->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'last_name', $classColSM6)->textInput(['autofocus' => true]) ?>
    </div>
    <?= $form->field($model, 'email', $classColSM12) ?>
    
    <?php $model->role = $model->getUserRoleItemName(); ?>
    <?= $form->field($model, 'role', $classColSM12)
            ->dropDownList(AuthItem::getAuthRoleArray())->label('User type') ?>
    
    <?= $form->field($model, 'company_id', $classColSM12)->dropDownList(Company::getCompanyListArray(), ['prompt' => 'Choise publisher...']); ?>
    
    <?= $form->field($model, 'password', $classColSM12)->passwordInput() ?>
    
    <?php //= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
