<?php

namespace backend\modules\users\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * SearchUsers represents the model behind the search form about `common\models\User`.
 */
class SearchUsers extends User
{
    public $userRole;
    public $filterUsername;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'status', 'created_at', 'updated_at', 'company_id'
                ],
                'integer'
            ],
            [
                [
                    'username', 'auth_key', 'password_hash', 'password_reset_token',
                    'email', 'description', 'userRole', 'first_name', 'last_name', 'filterUsername'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = User::find()
                ->alias('user')
                ->joinWith(['userRoleModel urm']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'urm.item_name' => $this->userRole,
            'user.status' => $this->status,
            'user.company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'user.username', $this->username])
            ->andFilterWhere(['like', 'CONCAT(user.first_name, " ", user.last_name)', $this->filterUsername])
            ->andFilterWhere(['like', 'user.email', $this->email]);

        return $dataProvider;
    }
}
