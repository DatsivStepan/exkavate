<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Collections */
/* @var $form yii\widgets\ActiveForm */
$classColSM5 = ['options' => ['class' => 'col-sm-5']];
?>

<div class="collections-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div>
        <?= $form->field($model, 'company_id', $classColSM5)->dropDownList(common\models\Company::getCompanyListArray(), ['prompt' => 'Choise publisher...']); ?>
        <div class="col-sm-2 text-center" style="font-size:30px;">OR</div>
        <?= $form->field($model, 'user_id', $classColSM5)->dropDownList(common\models\Company::getCompanyListArray(), ['prompt' => 'Choise users...']); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
