<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use froala\froalaeditor\FroalaEditorWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .fr-box .fr-wrapper a[href^="https://www.froala.com"] {
        opacity: 0  !important;
        height: 0 !important;
        padding: 0 !important;
        font-size: 0px !important; 
    }
</style>
<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= FroalaEditorWidget::widget([
            'model' => $model,
            'attribute' => 'content',
            'options' => [
                // html attributes
                'id'=>'content'
            ],
            'clientOptions' => [
                'toolbarInline' => false,
                'theme' => 'royal', //optional: dark, red, gray, royal
                'language' => 'en_gb', // optional: ar, bs, cs, da, de, en_ca, en_gb, en_us ...,
                'toolbarButtons' => [
                    'fullscreen', 'bold', 'italic', 'fontFamily', 'color', 'underline', 'quote', 'fontSize',  'align', 
                    'formatOL', 'formatUL', 'outdent', 'indent',  '|', 'paragraphFormat', 'insertImage', 'html'],
                'imageUploadParam' => 'file',
                'imageMaxSize' => 1024 * 1024 * 50,
                'imageUploadURL' => Url::to(['/news/news/save-image'])
            ]
        ]); ?>

        <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
