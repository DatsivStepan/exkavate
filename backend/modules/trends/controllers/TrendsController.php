<?php

namespace backend\modules\trends\controllers;

use Yii;
use common\models\Trends;
use backend\modules\trends\models\TrendsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use backend\modules\products\models\SearchProducts;

/**
 * TrendsController implements the CRUD actions for Trends model.
 */
class TrendsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trends models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrendsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trends model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelNew = new \common\models\ProductsTrends;
        $modelNew->trend_id = $model->id;
        if ($modelNew->load(Yii::$app->request->post()) && $modelNew->save()) {
            Yii::$app->session->setFlash('successData');
            $modelNew = new \common\models\ProductsTrends;
        }
        
        $searchModel = new SearchProducts();
        $searchModel->trendId = $model->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('view', [
            'modelNew' => $modelNew,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionDeleteWithTrend($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $trendId = Yii::$app->request->get('trend');
        
        $model = \common\models\ProductsTrends::findOne(['product_id' => $id, 'trend_id' => $trendId]);

        return ['status' => $model->delete()];
    }
    
    /**
     * Creates a new Trends model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trends();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Trends model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    
    public function actionAddTrend()
    {
        $this->layout = '/modal';
        $model = new Trends();
        $model->company_id = Yii::$app->request->get('company_id');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successData');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdateTrend($id)
    {
        $this->layout = '/modal';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('successData');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    /**
     * Deletes an existing Trends model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        return ['status' => $model->delete()];
    }

    /**
     * Finds the Trends model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trends the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trends::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
