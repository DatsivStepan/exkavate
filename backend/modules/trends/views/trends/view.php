<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Collections */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>
    <!-- Main content -->
    <section class="invoice">
        
        <div class="collections-view">

            <p>
                <?= Html::a(Yii::t('app', 'Update'), null, [
                    'class' => 'btn btn-primary js-update-company-trend js-reload-trend',
                    'data-id' => $model->id
                ]) ?>
            </p>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table" style="max-width:500px;">
                        <tr>
                            <td><b>Slug:</b></td>
                            <td><?= $model->slug; ?></td>
                        </tr>
                        <tr>
                            <td><b>Publisher:</b></td>
                            <td><?= $model->getCompanyName(); ?></td>
                        </tr>
                        <tr>
                            <td><b>Created at:</b></td>
                            <td><?= $model->created_at; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            
            
            <?php Pjax::begin(['id' => 'pjax-add-c-l', 'enablePushState' => false]); ?>

                <?php if (Yii::$app->session->hasFlash('successData')) { ?>
                    <script>
                        $('#trendList').yiiGridView("applyFilter");
                    </script>
                <?php } ?>

                <div class="tournament-form well">
                    <h3>Add ads to trend</h3>
                    <?php $form = ActiveForm::begin(['id' => 'c-l-form', 'options' => ['data-pjax' => 'pjax-add-c-l']]); ?>

                        <?= $form->field($modelNew, 'product_id', ['options' =>['class' => 'col-sm-10']])->widget(Select2::classname(), [
                            'data' => $model->getProductNotIn(),
                            'language' => 'en',
                            'size' => Select2::MEDIUM,
                            'options' => ['placeholder' => 'Select Ads...'],
                            'pluginOptions' => [
                    //            'allowClear' => true,
                            ],
                        ])->label(false); ?>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <?= Html::submitButton('Add', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php ActiveForm::end(); ?>
                </div>

            <?php Pjax::end();  ?>
            
            
            
            <div class="row">
                <div class="col-sm-12">
                    <h3>Ads</h3>
                </div>
            </div>
            <?= GridView::widget([
                'id' => 'trendList',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    [
                        'attribute' => 'category_id',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'category_id',
                            'data' => \common\models\Category::getAllInArrayMap(),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->category ? $model->category->name : null;
                        },
                        'format' => 'raw',
                    ],
                    [
                        'label' => 'Added to the system',
                        'value' => function ($model) {
                            return $model->created_at;
                        }
                    ],
                    [
                        'value' => function ($model) {
                            return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']) . ' Delete with trend',
                                null,
                                [
                                    'data-id' => $model->id,
                                    'class' => 'btn btn-xs btn-danger js-delete-with-trend',
                                    'data-pjax' => 0
                                ]);
                        },
                        'format' => 'raw'
                    ],

//                    [
//                        'class' => 'yii\grid\ActionColumn',
//                        'template' => '{update} {delete}',
//                        'buttons' => [
//                            'update' => function ($url, $model) use ($searchModel) {
//                                return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
//                                    Url::to(['/products/products/update/?id=' . $model->id]),
//                                    [
//                                        'data-id' => $model->id,
//                                        'class' => 'btn btn-xs btn-primary',
//                                        'data-pjax' => 0
//                                    ]);
//                            },
//                            'delete' => function ($url, $model) {
//                                return Html::a(
//                                    Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
//                                    '#',
//                                    [
//                                        'data-id' => $model->id,
//                                        'class' => 'btn btn-xs btn-danger js-delete-company-producct',
//                                    ]);
//                            }
//                        ],
//                    ],
                ],
            ]); ?>

        </div>
        
    </section>
    <script>
        $(function(){
                 $(document).on('click', '.js-update-company-trend', function(e){
                    e.preventDefault();
                    var id = $(this).data('id')
                    $.confirm({
                        id: 'updateCompanyTrend',
                        title: 'Update trend',
                        content: "url:/admin/trends/trends/update-trend?id=" + id ,
                        containerFluid: true
                    });
                });

                $(document).on('click', '.js-delete-with-trend', function(e){
                    e.preventDefault();

                    if (confirm('Do you really want to delete ads with this collection?')) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= Url::to(['/trends/trends/delete-with-trend/']); ?>?id=' + $(this).data('id') + '&trend=<?= $model->id?>',
                            dataType:'json',
                            success: function(response){
                                if (response.status) {
                                    location.reload();
                                }
                            }
                        });
                    }
                });
        })
    </script>