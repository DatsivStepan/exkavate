<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin(['id' => 'pjax-add-c', 'enablePushState' => false]); ?>

    <?php if (Yii::$app->session->hasFlash('successData')) { ?>
        <script>
            if ($modal.addCompanyTrend) {
                $modal.addCompanyTrend.close();
            }
            if ($modal.updateCompanyTrend) {
                $modal.updateCompanyTrend.close();
            }
            if ($('.js-reload-trend').length) {
                location.reload();
            }
            $('#js-company-trend-table').yiiGridView("applyFilter");
        </script>
    <?php } ?>

    <div class="tournament-form">

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['data-pjax' => 'pjax-add-c']]); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php Pjax::end();  ?>