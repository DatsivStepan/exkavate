<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\brands\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Advertisers');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>
    <!-- Main content -->
    <section class="invoice">
        
        <div class="brands-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a(Yii::t('app', 'Create Adverts'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::tag('img', '', [
                                'src' => $model->getImageSrc(),
                                'class' => 'img-thumbnail',
                                'style' => 'width:50px;',
                            ]);
                        }
                    ],
                    'name',
                    [
                        'attribute' => 'category_id',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'category_id',
                            'data' => \common\models\Brands::$category,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->category_id ? \common\models\Brands::$category[$model->category_id] : null;
                        },
                        'format' => 'raw',
                    ],
                    // 'company_id',
                    // 'created_at',
                    // 'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                    ]
                ],
            ]); ?>
        </div>
        
    </section>
