<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Brands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brands-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div style="width:150px;">
        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
        <img src="<?= $model->getImageSrc(); ?>" class="js-preview-brands-image img-thumbnail"><br>
        <a class="btn btn-sm btn-success js-add-brands-image" style="width:100%">Change image</a><br><br>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
        'data' => \common\models\Brands::$category,
        'language' => 'en',
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Select Category...'],
        'pluginOptions' => [
//            'allowClear' => true,
        ],
    ])->label(false); ?>

    <?= $form->field($model, 'company_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
