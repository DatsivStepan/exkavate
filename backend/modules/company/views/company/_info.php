<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-4">
        <img src="<?= $model->getImageSrc(); ?>" style="width:100%;">
    </div>
    <div class="col-sm-8">
        <h2><?= $model->name; ?></h2>
        <table class="table">
            <tr>
                <td><b>address</b></td>
                <td><?= $model->address; ?></td>
            </tr>
            <tr>
                <td><b>phone</b></td>
                <td><?= $model->phone; ?></td>
            </tr>
            <tr>
                <td><b>site</b></td>
                <td><?= $model->site; ?></td>
            </tr>
        </table>
    </div>
</div>