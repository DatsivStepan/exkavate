<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>
    <!-- Main content -->
    <section class="invoice">
        
        <div class="company-view">

            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <?= $this->render('_info', [
                'model' => $model
            ])?>

            <?= $this->render('_tabs', [
                'model' => $model
            ])?>
            <div class="clearfix">
                <div style="margin-top: 40px;margin-bottom: 20px">
                    <?= Html::a(Yii::t('app', 'Create Advert'), ['/products/products/create?company_id=' . $model->id], ['class' => 'btn btn-success pull-right']) ?>
                </div>
            </div>
            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
//                    [
//                        'label' => 'Publication',
//                        'attribute' => 'company_id',
//                        'filterType' => 'kartik\select2\Select2',
//                        'filterWidgetOptions' => [
//                            'model' => $searchModel,
//                            'attribute' => 'company_id',
//                            'data' => \common\models\Company::getAllInArrayMap(),
//                            'options' => [
//                                'placeholder' => Yii::t('app', 'Select...'),
//                            ],
//                            'pluginOptions' => [
//                                'allowClear' => true,
//                            ],
//                        ],
//                        'value' => function ($model) {
//                            return $model->companyModel ? $model->companyModel->name : null;
//                        },
//                        'format' => 'raw',
//                    ],
                    [
                        'attribute' => 'category_id',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'category_id',
                            'data' => \common\models\Category::getAllInArrayMap(),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->category ? $model->category->name : null;
                        },
                        'format' => 'raw',
                    ],
                    [
                        'label' => 'Added to the system',
                        'value' => function ($model) {
                            return $model->created_at;
                        }
                    ],

//                    [
//                        'class' => 'yii\grid\ActionColumn',
//                        'template' => '{update} {delete}',
//                        'buttons' => [
//                            'update' => function ($url, $model) use ($searchModel) {
//                                return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
//                                    Url::to(['/products/products/update/?id=' . $model->id]),
//                                    [
//                                        'data-id' => $model->id,
//                                        'class' => 'btn btn-xs btn-primary',
//                                    ]);
//                            },
//                            'delete' => function ($url, $model) {
//                                return Html::a(
//                                    Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
//                                    '#',
//                                    [
//                                        'data-id' => $model->id,
//                                        'class' => 'btn btn-xs btn-danger  js-delete-company-producct',
//                                    ]);
//                            }
//                        ],
//                    ],
                ],
            ]); ?>

        </div>

    </section>