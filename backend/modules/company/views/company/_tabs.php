<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-tabs">
    <ul class="nav nav-tabs">
        <li class="<?= $modelMenuUrl->checkUrl('company/company/' . $model->id) . ' ' . $modelMenuUrl->checkUrl('company/company/view/' . $model->id); ?>">
            <a href="<?= Url::to(['/company/company/view/' . $model->id]); ?>">Ads</a>
        </li>
        <li class="<?= $modelMenuUrl->checkUrl('company/company/premium/' . $model->id); ?>">
            <a href="<?= Url::to(['/company/company/premium/' . $model->id]); ?>">Premium ads</a>
        </li>
        <li class="<?= $modelMenuUrl->checkUrl('company/company/collections/' . $model->id); ?>">
            <a href="<?= Url::to(['/company/company/collections/' . $model->id]); ?>">Collection</a>
        </li>
        <li class="<?= $modelMenuUrl->checkUrl('company/company/trends/' . $model->id); ?>">
            <a href="<?= Url::to(['/company/company/trends/' . $model->id]); ?>">Trends</a>
        </li>
    </ul>
</div>