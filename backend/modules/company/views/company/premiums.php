<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use kartik\grid\GridView;
use yii\helpers\Url;
use common\models\Products;
use kartik\form\ActiveForm;
$baseUrl = (stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://') . Yii::$app->getRequest()->serverName;

Yii::$app->view->registerCssFile($baseUrl . '/css/plugins/shablon/demo-grid.css');
Yii::$app->view->registerCssFile($baseUrl . '/css/plugins/shablon/main.css');
Yii::$app->view->registerCssFile($baseUrl . '/css/plugins/shablon/demo-kanban.css');
Yii::$app->view->registerJsFile($baseUrl . '/js/plugins/sweetalert.min.js');
Yii::$app->view->registerJsFile($baseUrl . '/js/plugins/shablon/web-animations.js');
Yii::$app->view->registerJsFile($baseUrl . '/js/plugins/shablon/hammer-2.0.8.min.js');
Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.4.1/velocity.min.js');
Yii::$app->view->registerJsFile('/admin/js/plugins/muuri-0.5.1.js');
Yii::$app->view->registerJsFile('/admin/js/plugins/grid.js');

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>
    <!-- Main content -->
    <section class="invoice">
        
        <div class="company-view">

            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <?= $this->render('_info', [
                'model' => $model
            ])?>

            <?= $this->render('_tabs', [
                'model' => $model
            ])?>

            <div class="clearfix"></div>
            
                <div style="margin-top: 40px;margin-bottom: 20px">
                    <?= Html::a(Yii::t('app', 'Create premium'), Url::to(['/products/premium/create', 'company_id' => $model->id]), [
                        'class' => 'btn btn-success pull-right',
                        'target' => '_blank'
                    ]) ?>
                </div>
            
            <div class="clearfix"></div>
            
                <?= GridView::widget([
                    'id' => 'premiumList',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax' => true,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<span class="js-product-name">' . $model->name . '</span>';
                            }
                        ],
                        [
                            'attribute' => 'category_id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->category ? $model->category->name : null;
                            }
                        ],
                        [
                            'attribute' => 'premium_status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Products::$premiumStatusArray[$model->premium_status];
                            }
                        ],
                        [
                            'attribute' => 'premium_type',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $type = null;
                                if (Products::PREMIUM_TYPE_HORIZONTALLY == $model->premium_type) {
                                    $type = Products::$typesTranslate[Products::PREMIUM_TYPE_HORIZONTALLY];
                                } elseif (Products::PREMIUM_TYPE_VERTICALLY == $model->premium_type) {
                                    $type = Products::$typesTranslate[Products::PREMIUM_TYPE_VERTICALLY];
                                }
                                return $type;
                            }
                        ],
                        'created_at',
                        [
                            'format' => 'raw',
                            'value' => function ($model) {
                                $text = 'Active';
                                $class = 'success js-active-product-button';
                                $buttonTwo = '';
                                if ($model->premium_status == Products::PREMIUM_STATUS_ACTIVE) {
                                    $text =  'Deactive';
                                    $class = 'danger js-remove-product-button';
                                }elseif ($model->premium_status == Products::PREMIUM_STATUS_ON_CONFIRMATION) {
                                    $buttonTwo = ' ' . Html::button('Deactive', [
                                        'class' => 'btn btn-danger js-remove-product-button',
                                        'data-id' => $model->id,
                                    ]);
                                }
                                return Html::button($text, [
                                    'class' => 'btn btn-' . $class,
                                    'data-type' => Products::$types[$model->premium_type],
                                    'data-id' => $model->id,
                                ]) . $buttonTwo;
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'text-nowrap'],
                            'template' => '{delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::button(
                                        '<i class="glyphicon glyphicon-trash"></i>',
                                        null,
                                        [
                                            'data-pjax' => 0, 
                                            'class' => 'btn btn-danger js-delete-element'
                                        ]
                                    );
                                },
                            ]
                        ],
                    ],
                ]); ?>
            
            <div class="clearfix"></div>
            
            
            <div class="products-form">
                <div class="col-sm-8 col-sm-offset-2">
                    <?php $form = ActiveForm::begin(); ?>
                        <?= Html::submitButton(Yii::t('app', 'Save Template'), ['class' => 'pull-right btn btn-primary bat_coll_creat']) ?>
                        <div class="premium text-left">
                            <div class="row" style="border: 1px solid transparent;padding:0px;padding-top:15px;margin:0px;">
                                <?= $form->field($model, 'template')->hiddenInput(['value' => json_encode($model->getTemplate())])->label(false); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br>

                    <?php ActiveForm::end(); ?>
                    <section id="demo" style="width:500px;margin:0 auto;">
                            <div class="grid"></div>
                    </section>
                </div>
                <div class="clearfix" style="margin-bottom: 40px"></div>
            </div>

        </div>

    </section>
    
    <style>
        .premium .form-group{
            text-align: left;
        }
        .grid {
            position: relative;
            margin: 20px 0;
            border: 1px solid transparent;
            -moz-box-sizing: content-box;
            -webkit-box-sizing: content-box;
            box-sizing: content-box;
          }
          .item .item-content {
              line-height:19px !important;
          }
          .item {
            position: absolute;
            line-height: 100px;
            margin: 0px;
            z-index: 1;
          }
          .item.muuri-dragging,
          .item.muuri-releasing {
            z-index: 9999;
          }
          .item.muuri-dragging {
            cursor: move;
          }
          .item.muuri-hidden {
            z-index: 0;
          }
          .item-content {
            position: relative;
            width: 100%;
            height: 100%;
            padding:10px;
          }
          .item[data-product_id='<?= $model->id; ?>']{
              background-color: #21a2dc;
          }
            .item-normal{
                height:150px;
                width:124px;
                margin:0px;
            }
            .item-vertical{
                height:300px;
                width:248px;
                margin:0px;
            }
            .item-horizontal{
                height:300px;
                width:496px;
                margin:0px;
            }
    </style>