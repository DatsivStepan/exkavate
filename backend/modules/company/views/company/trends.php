<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>
    <!-- Main content -->
    <section class="invoice">
        
        <div class="company-view">

            <p>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <?= $this->render('_info', [
                'model' => $model
            ])?>

            <?= $this->render('_tabs', [
                'model' => $model
            ])?>
            <div class="clearfix">
                <div style="margin-top: 40px;margin-bottom: 20px">
                    <?= Html::a(Yii::t('app', 'Create Trend'), null, ['class' => 'btn btn-success pull-right js-add-company-trend']) ?>
                </div>
            </div>
            
            <?= GridView::widget([
                'id' => 'js-company-trend-table',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    [
                        'label' => 'Ads count',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getProductsCount();
                        }
                    ],
                    'created_at',
                    // 'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']),
                                    Url::to(['/trends/trends/view/' . $model->id]),
                                    [
                                        'class' => 'btn btn-xs btn-primary',
                                        'target' => '_blank',
                                        'data-pjax' => 0,
                                    ]);
                            },
                            'update' => function ($url, $model) {
                                return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
                                    null,
                                    [
                                        'data-id' => $model->id,
                                        'class' => 'btn btn-xs btn-primary js-update-company-trend',
                                    ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a(
                                    Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
                                    '#',
                                    [
                                        'data-id' => $model->id,
                                        'class' => 'btn btn-xs btn-danger  js-delete-company-trend',
                                    ]);
                            }
                        ],
                    ],
                ],
            ]); ?>

        </div>

    </section>
    
    <script>
    $(function(){
            $(document).on('click', '.js-update-company-trend', function(e){
                e.preventDefault();
                var id = $(this).data('id')
                $.confirm({
                    id: 'updateCompanyTrend',
                    title: 'Update trend',
                    content: "url:/admin/trends/trends/update-trend?id=" + id + '&company_id=<?= $model->id?>',
                    containerFluid: true
                });
            });

            $(document).on('click', '.js-add-company-trend', function(e){
                e.preventDefault();
                var trendId = $(this).data('id')
                $.confirm({
                    id: 'addCompanyTrend',
                    title: 'Create trend',
                    content: "url:/admin/trends/trends/add-trend?id=" + trendId + '&company_id=<?= $model->id?>',
                    containerFluid: true
                });
            });

            $(document).on('click', '.js-delete-company-trend', function(e){
                e.preventDefault();

                if (confirm('Do you really want to delete this trend?')) {
                    $.ajax({
                        type: 'POST',
                        url: '<?= Url::to(['/trends/trends/delete/']); ?>?id=' + $(this).data('id'),
                        dataType:'json',
                        success: function(response){
                            if (response.status) {
                                $('#js-company-trend-table').yiiGridView("applyFilter");
                            }
                        }
                    });
                }
            });
    })
</script>