<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Sliders */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sliders',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sliders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>

    <!-- Main content -->
    <section class="invoice">
        
        <div class="sliders-update">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
        
    </section>
