<?php

namespace backend\modules\products\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Company;

/**
 * SearchProducts represents the model behind the search form about `common\models\Products`.
 */
class SearchPremium extends Company
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'template'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'logo_src', 'address', 'site', 'file_src'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find()
                ->alias('company')
                ->select(['company.*', 'premCount' => 'COUNT(premProduct.id)'])
                ->joinWith(['premiumProducts premProduct'])
                ->orderBy(['premProduct.id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'company_id' => $this->company_id,
//            'category_id' => $this->category_id,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//        ]);
//
//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'short_content', $this->short_content])
//            ->andFilterWhere(['like', 'content', $this->content])
//            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
}
