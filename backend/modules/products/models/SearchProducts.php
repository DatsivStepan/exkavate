<?php

namespace backend\modules\products\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

/**
 * SearchProducts represents the model behind the search form about `common\models\Products`.
 */
class SearchProducts extends Products
{
    public $collectionId;
    public $trendId;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'company_id', 'category_id', 'brand_id', 'premium_type',
                    'collectionId', 'trendId', 'premium_status'
                ],
                'integer'
            ],
            [
                [
                    'name', 'short_content', 'content', 'img_src', 'created_at', 'updated_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find()
                ->alias('product')
                ->joinWith(['productsCollectionsModel pcm'])
                ->joinWith(['productsTrendsModel ptm'])
                ->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->collectionId) {
            $query->andFilterWhere(['pcm.collection_id' => $this->collectionId]);
        }
        if ($this->trendId) {
            $query->andFilterWhere(['ptm.trend_id' => $this->trendId]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product.id' => $this->id,
            'product.company_id' => $this->company_id,
            'product.brand_id' => $this->brand_id,
            'product.category_id' => $this->category_id,
            'product.created_at' => $this->created_at,
            'product.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->name])
            ->andFilterWhere(['like', 'product.short_content', $this->short_content])
            ->andFilterWhere(['like', 'product.content', $this->content])
            ->andFilterWhere(['like', 'product.img_src', $this->img_src]);

        return $dataProvider;
    }

    public function searchPremium($params = null)
    {
        $query = Products::find()
                ->where(['premium' => 1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        if ($params) {
            $this->load($params);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'category_id' => $this->category_id,
            'premium_status' => $this->premium_status,
            'premium_type' => $this->premium_type,
        ]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'short_content', $this->short_content])
//            ->andFilterWhere(['like', 'content', $this->content])
//            ->andFilterWhere(['like', 'img_src', $this->img_src]);

        return $dataProvider;
    }
}
