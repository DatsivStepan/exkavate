<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;
use kartik\checkbox\CheckboxX;
use common\models\Products;
use common\components\BootstrapModal;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */

//BootstrapModal::widget([
//    ['id' => 'create-collection-modal', 'header' => 'Create collection', 'footer' => true],
//    ['id' => 'create-trend-modal', 'header' => 'Create trend', 'footer' => true],
//    ['id' => 'create-brand-modal', 'header' => 'Create brand', 'footer' => true],
//]);
$colSm6 = ['options' => ['class' => 'col-sm-6']];
?>
<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="clearfix"></div>
    <div class="form-group" style="margin-bottom:20px;">
        <div id="buttom_css" class="col-sm-4 col-xs-12 text-xs-center padding_0">
            <?= $form->field($model, 'file_src')->hiddeninput()->label(false); ?>
            <a class="btn btn-primary addProductsFileSrc bat_coll_creat"><?= $model->isNewRecord ? Yii::t('app', 'Upload new advert') : Yii::t('app', 'Change file'); ?></a>
            <div class="previewProductsFileSrc"
                 style="display:inline-block;"><?= ($model->file_src) ? '<a href="/' . $model->file_src . '">' . $model->name . '</a>' : '' ?></div>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="display:none;margin-top:20px;">
              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress=""></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div style="width:150px;">
        <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
        <img src="<?= $model->getImageSrc(); ?>" class="js-preview-products-image img-thumbnail"><br>
        <a class="btn btn-sm btn-success js-add-products-image" style="width:100%">Change image</a><br><br>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name'])->label(false) ?>
    <?= $form->field($model, 'content')->textarea(['rows' => 2, 'placeholder' => 'Description'])->label(false) ?>

    <div class="clearfix"></div>
    <?= $form->field($model, 'category_id', $colSm6)->widget(Select2::classname(), [
        'data' => common\models\Category::getAllInArrayMap(),
        'language' => 'en',
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Select Category...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label(false); ?>

    <?= $form->field($model, 'brand_id', $colSm6)->widget(Select2::classname(), [
        'data' => \common\models\Brands::getAllInArrayMap(),
        'language' => 'en',
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Select Advertiser...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label(false); ?>
    
    <div class="clearfix"></div>
    <?= $form->field($model, 'collections', $colSm6)->widget(Select2::classname(), [
        'data' => \common\models\Collections::getAllInArrayMap($model->company_id),
        'value' => $model->collections,
        'language' => 'en',
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Add to Collections...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true
        ],
    ])->label(false); ?>

    <?= $form->field($model, 'trends', $colSm6)->widget(Select2::classname(), [
        'data' => \common\models\Trends::getAllInArrayMap($model->company_id),
        'language' => 'en',
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Add to Trending Topic...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true
        ],
    ])->label(false); ?>
    
    <div class="clearfix"></div>
    <?= $form->field($model, 'tags')->widget(Select2::classname(), [
            'data' => $tagsArray,
            'options' => ['placeholder' => 'Tag ...', 'multiple' => true],
            'maintainOrder' => true,
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 16
            ],
        ])->label(false); ?>
    
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'parametr1')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('parametr1')])->label(false) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'parametr2')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('parametr2')])->label(false) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'parametr3')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('parametr3')])->label(false) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'parametr4')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('parametr4')])->label(false) ?>
        </div>
    </div>

    <div class="input_name_cl"><?php //= $form->field($modelVideo, 'link')->textInput(['maxlength' => true, 'placeholder' => 'Link for YouTube video'])->label(false) ?></div>
    <div class="input_name_cl"><?php //= $form->field($modelVideo, 'description')->textarea(['rows' => 2, 'maxlength' => true, 'placeholder' => 'Video description'])->label(false) ?></div>
    

    <div class="input_data_embargo">
        <?= $form->field($model, 'embargo_advert')->widget(DatePicker::className(), [
            'options' => ['value' => date('Y-m-d')],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ]
        ]) ?>
    </div>
    
    
    <div class="clearfix"></div>
    <br>

    <div class="form-group">
        <div class=" mar_bat-crie">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success bat_coll_creat' : 'btn btn-primary bat_coll_creat']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">
            </div>
        </div>
    </div>
</div>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews2">
            <div id="template2" class="file-row">
            </div>
        </div>
    </div>
</div>

