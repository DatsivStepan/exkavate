<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
use common\models\Products;
use common\models\Category;
use common\models\Company;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\company\models\SearchCompany */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Premium Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?= Html::encode($this->title) ?>
      </h1>
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </section>
    <!-- Main content -->
    <section class="invoice">
        
        <div class="company-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a(Yii::t('app', 'Create Premium'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'Logo',
//                        'attribute' => 'logo_src',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::tag('img', '', [
                                'src' => $model->getImageSrc(),
                                'class' => 'img-thumbnail',
                                'style' => 'width:100px;',
                            ]);
                        }
                    ],
                    'name',
                    [
                        'attribute' => 'category_id',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'category_id',
                            'data' => Category::getAllInArrayMap(),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->category ? $model->category->name : null;
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'company_id',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'company_id',
                            'data' => Company::getAllInArrayMap(),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'value' => function ($model) {
                            return $model->companyModel ? $model->companyModel->name : '';
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'premium_type',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'premium_type',
                            'data' => Products::$typesTranslate,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            $type = null;
                            if (Products::PREMIUM_TYPE_HORIZONTALLY == $model->premium_type) {
                                $type = Products::$typesTranslate[Products::PREMIUM_TYPE_HORIZONTALLY];
                            } elseif (Products::PREMIUM_TYPE_VERTICALLY == $model->premium_type) {
                                $type = Products::$typesTranslate[Products::PREMIUM_TYPE_VERTICALLY];
                            }
                            return $type;
                        }
                    ],
                    [
                        'attribute' => 'premium_status',
                        'filterType' => 'kartik\select2\Select2',
                        'filterWidgetOptions' => [
                            'model' => $searchModel,
                            'attribute' => 'premium_status',
                            'data' => Products::$premiumStatusArray,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Select...'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'format' => 'raw',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Products::$premiumStatusArray[$model->premium_status];
                        }
                    ],
                    'created_at',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['class' => 'text-nowrap'],
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a(
                                    null,
                                    '/admin/company/company/premium/' . $model->company_id,
                                    ['data-pjax' => 0, 'class' => 'glyphicon glyphicon-eye-open']
                                );
                            },
                        ]
                    ],
//                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>

    </section>