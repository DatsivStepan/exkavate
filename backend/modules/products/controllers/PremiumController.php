<?php

namespace backend\modules\products\controllers;

use Yii;
use common\models\Company;
use backend\modules\products\models\SearchPremium;
use backend\modules\products\models\SearchProducts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Products;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class PremiumController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchProducts();
        //$searchModel->premium_status = Products::PREMIUM_STATUS_ON_CONFIRMATION;
        $dataProvider = $searchModel->searchPremium(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('SaveData');
        }
        
        $searchModel = new SearchProducts();
        $searchModel->company_id = $model->id;
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchPremium();
        
        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($step = 1)
    {
        $model = new Products();
        $model->company_id = Yii::$app->request->get('company_id');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->save()){
                if ($modelC = Company::findOne($model->company_id)) {
                    $modelC->template = json_encode($modelC->getTemplate($model->premium_type, $model->id));
                    $modelC->save();
                }
                \Yii::$app->session->setFlash('adverts_save');
                return $this->redirect(['/company/company/premium/', 'id' => $model->company_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
