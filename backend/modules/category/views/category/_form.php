<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php /*= $form->field($model, 'description')->widget(TinyMce::className(), [
            'options' => ['rows' => 6],
            'language' => 'en',
            'clientOptions' => [
                'plugins' => [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
    ]); */ ?>

    <?php //= $form->field($model, 'parent_id')->dropDownList(common\models\Category::parentCategoriesList(), ['prompt' => 'Select ...']); ?>
    
    <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
    <img src="/<?= ($model->img_src)?$model->img_src:'images/default_avatar.jpg'; ?>" class="previewCategoryImage" style="width:150px;height:150px;"><br>
    <a class="btn btn-success addCategoryImage" >Change image</a><br><br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
