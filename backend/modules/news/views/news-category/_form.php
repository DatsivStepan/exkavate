<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model common\models\NewsCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-category-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="well">
            <h3>News featured</h3>
            <?php if ($model->newsModel) { ?>
                    <?php foreach ($model->newsModel as $news) { ?>
                        <div class="col-sm-3">

                            <?= CheckboxX::widget([
                                'name' => 'NewsCategory[featured][' . $news->id . ']',
                                'value'=> is_array($model->featured) ? (in_array($news->id, $model->featured) ? $model->featured[$news->id] : 0) : 0,
                                'pluginOptions' => ['threeState' => false],
                                'labelSettings' => [
                                    'label' => mb_substr($news->title, 0, 25, 'utf-8'),
                                    'position' => CheckboxX::LABEL_RIGHT,
                                ]
                            ]) ?>

                        </div>
                <?php } ?>
            <?php } ?>
            <div class="clearfix"></div>
        </div>

        <?= $form->field($model, 'sort')->textInput(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
