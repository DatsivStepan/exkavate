<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\NewsCategory;
use kartik\checkbox\CheckboxX;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div id="news-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'avtor_name')->textInput(['maxlength' => true])->label('Author Name'); ?>

        <?= $form->field($model, 'category_id')->dropDownList(NewsCategory::getAllInArrayMap()) ?>

        <div style="width:150px;height:150px;">
            <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
            <img src="<?= $model->getImages(); ?>" class="previewNewsImage img-thumbnail"><br>
            <a class="btn btn-sm btn-success addNewsImage" style="width:100%">Change image</a><br><br>
        </div>
        <?= $form->field($model, 'image_description')->textInput(['maxlength' => true]) ?>
    
        <?= $form->field($model, 'inHome', [
            'template' => '{label} {input}',
            'labelOptions' => ['class' => 'control-label'],
        ])->widget(CheckboxX::classname(), ['pluginOptions' => ['threeState' => false]])->label('Show in home'); ?>
    
        <?= $form->field($model, 'short_description')->textarea(['rows' => 2]) ?>
        
        <?= $form->field($model, 'tags')->widget(Select2::classname(), [
            'options' => ['placeholder' => 'Tag ...', 'multiple' => true],
            'maintainOrder' => true,
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 16
            ],
        ])->label(false); ?>
    
        <?= $form->field($model, 'description')->widget(TinyMce::className(), [
            'options' => ['rows' => 16],
            'language' => 'en_GB',
            'clientOptions' => [
                'plugins' => [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
            ]
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

</div>
<script>
        function jsFunctionToBeCalled(field_name, url, type, win){
            win.document.getElementById(field_name).value = 'my browser value';
        }
</script>