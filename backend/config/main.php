<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'news' => [
            'class' => 'backend\modules\news\Module',
        ],
        'language' => [
            'class' => 'backend\modules\language\Module',
        ],
        'modules' => [
        ],
        'sliders' => [
            'class' => 'backend\modules\sliders\Module',
        ],
        'brands' => [
            'class' => 'backend\modules\brands\Module',
        ],
        'collections' => [
            'class' => 'backend\modules\collections\Module',
        ],
        'trends' => [
            'class' => 'backend\modules\trends\Module',
        ],
        'configuration' => [
            'class' => 'backend\modules\configuration\Module',
        ],
        'pages' => [
            'class' => 'backend\modules\pages\Module',
        ],
        'users' => [
            'class' => 'backend\modules\users\Module',
        ],
        'category' => [
            'class' => 'backend\modules\category\Modules',
        ],
        'company' => [
            'class' => 'backend\modules\company\Module',
        ],
        'products' => [
            'class' => 'backend\modules\products\Module',
        ],
        'posts' => [
            'class' => 'backend\modules\post\Module',
        ],
    ],
    'components' => [
        'request' => [
			'baseUrl' => '/admin',
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                '<_c:[\w-]+>' => '<_c>/index',
                '<_c:[\w-]+>/<id:\d+>' => '<_c>/view',
                '<_c:[\w-]+>/<id:\d+>/<_a:[\w-]+>' => '<_c>/<_a>',
                '<_m>/<_c:[\w-]+>/<_a:[\w-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
            ],
        ],
    ],
    'params' => $params,
];
