<?php

use yii\helpers\Html;

?>
<header class="main-header">
    <!-- Logo -->
    <a href="/admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>E</b>XK</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Exkavate</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?= \Yii::$app->user->identity->username; ?></span>
            </a>
            <ul class="dropdown-menu">

              <!-- Menu Footer-->
              <li class="user-footer">
                <?= Html::beginForm(['/site/logout'], 'post', ['style' => 'width:100%;']); ?>
                    <?= Html::submitButton(
                        'Sign out',
                        ['class' => 'btn btn-default btn-flat', 'style' => 'width:100%;']
                    ); ?>
                <?= Html::endForm() ?>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>