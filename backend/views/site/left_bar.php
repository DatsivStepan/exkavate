<?php

use yii\helpers\Html;
use common\models\Configuration;
use common\components\AdminMenuUrlManager;
$modelMenuUrl = new AdminMenuUrlManager();
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
<!--          <img src="/admin/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        </div>
        <div class="pull-left info">
          <p style="position: relative;bottom: 11px;"><?= \Yii::$app->user->identity->username; ?></p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ADMIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Pages</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              
            <?php /* ?>
                <li class="<?= $modelMenuUrl->checkUrl('pages/pages/create'); ?>"><a href="/admin/pages/pages/create"><i class="fa fa-circle-o"></i> Add page</a></li>
            <?php */ ?>
            <li class="<?= $modelMenuUrl->checkUrl('pages/pages/index'); ?>"><a href="/admin/pages/pages/index"><i class="fa fa-circle-o"></i> List pages</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('users/users/create'); ?>"><a href="/admin/users/users/create"><i class="fa fa-circle-o"></i> Add user</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('users/users/index'); ?>"><a href="/admin/users/users/index"><i class="fa fa-circle-o"></i> List users</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-building"></i>
            <span>Publications</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('company/company/create'); ?>"><a href="/admin/company/company/create"><i class="fa fa-circle-o"></i> Add publication</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('company/company/index'); ?>"><a href="/admin/company/company/index"><i class="fa fa-circle-o"></i> List publications</a></li>
          </ul>
        </li>
        <?php /* ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Collections</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('collections/collections/create'); ?>"><a href="/admin/collections/collections/create"><i class="fa fa-circle-o"></i> Add collections</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('collections/collections/index'); ?>"><a href="/admin/collections/collections/index"><i class="fa fa-circle-o"></i> List collections</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Trends</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('trends/trends/create'); ?>"><a href="/admin/trends/trends/create"><i class="fa fa-circle-o"></i> Add trends</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('trends/trends/index'); ?>"><a href="/admin/trends/trends/index"><i class="fa fa-circle-o"></i> List trends</a></li>
          </ul>
        </li>
        <?php 
         */ ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Advertisers </span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('brands/brands/create'); ?>"><a href="/admin/brands/brands/create"><i class="fa fa-circle-o"></i> Add Advertiser</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('brands/brands/index'); ?>"><a href="/admin/brands/brands/index"><i class="fa fa-circle-o"></i> List Advertisers </a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i>
            <span>Ads categories</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('category/category/create'); ?>"><a href="/admin/category/category/create"><i class="fa fa-circle-o"></i> Add category</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('category/category/index'); ?>"><a href="/admin/category/category/index"><i class="fa fa-circle-o"></i> List categories</a></li>
          </ul>
        </li>
        <?php /* ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Ads</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('products/products/create'); ?>"><a href="/admin/products/products/create"><i class="fa fa-circle-o"></i> Add ads</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('products/products/index'); ?>"><a href="/admin/products/products/index"><i class="fa fa-circle-o"></i> List ads</a></li>
            <li><hr style="margin:10px 10px 20px 10px;"></li>
            <li class="<?= $modelMenuUrl->checkUrl('products/premium/create'); ?>"><a href="/admin/products/premium/create"><i class="fa fa-circle-o"></i> Add premium</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('products/premium/index'); ?>"><a href="/admin/products/premium/index"><i class="fa fa-circle-o"></i> List premiums</a></li>
          </ul>
        </li>
        <?php */ ?>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Premium Ads</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('products/premium/create'); ?>"><a href="/admin/products/premium/create"><i class="fa fa-circle-o"></i> Add premium</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('products/premium/index'); ?>"><a href="/admin/products/premium/index"><i class="fa fa-circle-o"></i> List premiums</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>News </span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('news/news/create'); ?>"><a href="/admin/news/news/create"><i class="fa fa-circle-o"></i> Add news</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('news/news/index'); ?>"><a href="/admin/news/news/index"><i class="fa fa-circle-o"></i> List news</a></li>
            <li style="padding:0px 10px;"><hr style="margin:5px 0px;"></li>
            <li class="<?= $modelMenuUrl->checkUrl('news/news-category/create'); ?>"><a href="/admin/news/news-category/create"><i class="fa fa-circle-o"></i> Add category news</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('news/news-category/index'); ?>"><a href="/admin/news/news-category/index"><i class="fa fa-circle-o"></i> List categories news</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Slider </span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?= $modelMenuUrl->checkUrl('sliders/sliders/create'); ?>"><a href="/admin/sliders/sliders/create"><i class="fa fa-circle-o"></i> Add item</a></li>
            <li class="<?= $modelMenuUrl->checkUrl('sliders/sliders/index'); ?>"><a href="/admin/sliders/sliders/index"><i class="fa fa-circle-o"></i> Items list</a></li>
          </ul>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-cogs"></i>
              <span>Settings</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="<?= $modelMenuUrl->checkUrl('configuration/configuration/index'); ?>">
                  <a href="/admin/configuration/configuration/index"><i class="fa fa-circle-o"></i> Configuration</a>
              </li>
              <?php /* ?>
              <li class="<?= $modelMenuUrl->checkUrl('language/source-message/index'); ?>">
                  <a href="/admin/language/source-message/index"><i class="fa fa-circle-o"></i> Translation</a>
              </li>
              <li class="<?= $modelMenuUrl->checkUrl('language/language/index'); ?>">
                  <a href="/admin/language/language/index"><i class="fa fa-circle-o"></i> Languages</a>
              </li>
              <?php */ ?>
            </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>