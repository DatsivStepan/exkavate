<?php

namespace common\rbac;

class Rbac
{
    const ADMIN = 'admin';
    const COMPANY_ADMIN = 'company_admin';
    const COMPANY_MANAGER = 'company_manager';
    const USER = 'user';    
}