<?php

namespace common\models;

use Yii,
    common\models\Follower,
    common\models\Products,
    common\models\Views,
    common\models\Likes,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%brends}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $img_src
 * @property integer $category_id
 * @property integer $company_id
 * @property string $created_at
 * @property string $updated_at
 */
class Brands extends \yii\db\ActiveRecord
{
    public static $category = [
        1 => 'Civic/ Social',
        2 => 'Educational',
        3 => 'Governmental',
        4 => 'Individual/ Personal',
        5 => 'Business Organization',
        6 => 'Non-Profit/ Non-Governmental Organization',
        7 => 'Religious'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%brands}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['category_id', 'company_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'img_src', 'slug'], 'string', 'max' => 255],
        ];
    }

    
    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'backend\models\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'img_src' => Yii::t('app', 'Img Src'),
            'category_id' => Yii::t('app', 'Category ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function getFollowStatus(){
        return Follower::findOne(['type' => Follower::TYPE_BRAND, 'object_id' => $this->id, 'user_id' => \Yii::$app->user->id]) ? true : false;
    }
    
    public function getLikeStatus(){
        return Likes::findOne(['type' => Follower::TYPE_BRAND, 'object_id' => $this->id, 'user_id' => \Yii::$app->user->id]) ? true : false;
    }
    
    /**
     * return string
     */
    public function getCategoryName()
    {
        return $this->category_id ? self::$category[$this->category_id] : '';
    }
    
    /**
     * return string
     */
    public function getLink()
    {
        return '/advertisers/view/' . $this->id;
    }
    
    /**
     * return string
     */
    public function getImageSrc()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_image.jpg';
    }
    
    /**
     * return integer
     */
    public static function getAllInArrayMap()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
    
    /**
     * return integer
     */
    public function getCollectionsCount()
    {
        return '0';
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getLikesCount()
    {
        return Likes::find()->where(['object_id' => $this->id, 'type' => Likes::TYPE_BRAND])
                ->distinct()
                ->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getViewsCount()
    {
        return Views::find()
                ->alias('vw')
                ->leftJoin('products product', 'product.id = vw.object_id')
                ->where(['product.brand_id' => $this->id, 'vw.type' => Views::TYPE_PRODUCT])
                ->distinct()
                ->count();
    }
    
    /**
     * return integer
     */
    public function getFollowCount()
    {
        return Follower::getCount($this->id, Follower::TYPE_BRAND);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getBrandsFollowers()
    {
        return $this->hasMany(Follower::className(), ['object_id' => 'id'])
                ->andOnCondition(['type' => Follower::TYPE_BRAND]);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getBrandProductWithLimit($limit)
    {
        return Products::find()->where(['brand_id' => $this->id])->limit($limit)->all();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getBrandProduct()
    {
        return $this->hasMany(Products::className(), ['brand_id' => 'id']);
    }
    
}
