<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property integer $category_id
 * @property string $img_src
 * @property string $created_at
 * @property string $updated_at
 */
class News extends \yii\db\ActiveRecord
{
    public $tags;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_description', 'description', 'image_description'], 'string'],
            [['category_id', 'inHome'], 'integer'],
            [['created_at', 'updated_at', 'tags'], 'safe'],
            [['title', 'img_src', 'avtor_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'short_description' => Yii::t('app', 'Short Description'),
            'description' => Yii::t('app', 'Description'),
            'category_id' => Yii::t('app', 'Category'),
            'img_src' => Yii::t('app', 'Img Src'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->tags = ArrayHelper::getColumn($this->newsTagsModel, 'name');
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        NewsTags::deleteAll(['news_id' => $this->id]);
        if ($this->tags) {
            foreach ($this->tags as $tag) {
                $modelNewPrTag = new NewsTags();
                $modelNewPrTag->news_id = $this->id;
                $modelNewPrTag->name = $tag;
                $modelNewPrTag->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }
    
    public function getImages()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_avatar.jpg';
    }
    
    public function getLink()
    {
        return '/news/view/' . $this->id;
    }
    
    public function getTimeAgo()
    {
        $string = '';
        $now = date_create(date('Y-m-d'));
        $datetime = date_create($this->created_at);
        $interval = date_diff($datetime, $now);

        $string = ($interval->y ? $interval->y . ($interval->y > 1 ?  ' years ' : ' year ') : '') .
                ($interval->m ? $interval->m . ($interval->m > 1 ?  ' months ' : ' month ') : '') . 
                ($interval->d ? $interval->d . ($interval->d > 1 ?  ' days ' : ' day ') : '') .
                ($interval->h ? $interval->h . ' hours ' : '') . ' ago';

        return $string;
    }
    
    public function getDateCreate($format = 'd M, Y')
    {
        return $this->created_at ? date($format, strtotime($this->created_at)) : '';
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryModel()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getNewsTagsModel()
    {
        return $this->hasMany(NewsTags::className(), ['news_id' => 'id']);
    }
    
}
