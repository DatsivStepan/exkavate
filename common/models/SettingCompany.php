<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%setting_company}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $subject
 * @property integer $status
 * @property integer $company_id
 * @property string $created_at
 * @property string $updated_at
 */
class SettingCompany extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%setting_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'company_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'subject' => Yii::t('app', 'Subject'),
            'status' => Yii::t('app', 'Status'),
            'company_id' => Yii::t('app', 'Company ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public static function getStatus($type, $company_id)
    {
        $model = self::findOne(['type' => $type, 'company_id' => $company_id]);
        return $model ? $model->status : true;
    }
}
