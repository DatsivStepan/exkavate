<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%views}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $object_id
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class Views extends \yii\db\ActiveRecord
{
    const TYPE_PRODUCT = 'product';
    public $nameC;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%views}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'object_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'user_ip'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public static function getViewsFromTo($from, $to, $company_id=null){
        $from = date('Y-m-d', strtotime($from));
        $to = date('Y-m-d', strtotime($to));
        
        return  ArrayHelper::getColumn(self::find()
                ->alias('vw')
                ->leftJoin('products product', 'product.id = vw.object_id')
                ->where(['product.company_id' => $company_id, 'vw.type' => self::TYPE_PRODUCT])
                ->andWhere(['between', 'vw.created_at', $from, $to])
                ->distinct()->all(), 'created_at');
    }
    
    public static function getCategoryViewsFromTo($from, $to, $company_id=null){
        $from = date('Y-m-d', strtotime($from));
        $to = date('Y-m-d', strtotime($to));
            $model = self::find()
                ->alias('vw')
                ->select(['nameC' => 'IFNULL(category.name, "")'])
                ->leftJoin('products product', 'product.id = vw.object_id')
                ->leftJoin('category', 'category.id = product.category_id')
                ->where(['product.company_id' => $company_id, 'vw.type' => self::TYPE_PRODUCT])
                ->andWhere(['between', 'vw.created_at', $from, $to])
                ->all();
            
            $array = [];
            foreach ($model as $mod) {
                if (array_key_exists($mod->nameC, $array)) {
                    $array[$mod->nameC] = $array[$mod->nameC] + 1;
                } else {
                    $array[$mod->nameC] = 1;
                }
            }
            return $array;
    }
    
}
