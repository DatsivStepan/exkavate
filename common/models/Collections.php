<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper,
    common\models\Follower;

/**
 * This is the model class for table "{{%collections}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property integer $company_id
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Collections extends \yii\db\ActiveRecord
{
    public $productsCount; 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%collections}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['company_id', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'backend\models\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Description'),
            'company_id' => Yii::t('app', 'Publication'),
            'user_id' => Yii::t('app', 'User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * return string
     */
    public static function getAllInArrayMap($company_id = null)
    {
        $query = self::find();
        if ($company_id) {
            $query->where(['company_id' => $company_id]);
        }
        return ArrayHelper::map($query->all(), 'id', 'name');
    }
    
    /**
     * return string
     */
    public function getLink()
    {
        return '/collections/view/' . $this->id;
    }
    
    /**
     * return integer
     */
    public function getProductNotIn()
    {
        $query = Products::find()
                ->alias('product')
                ->select(['id' => 'product.id'])
                ->joinWith(['productsCollectionsModel pcm'])
                ->where(['pcm.collection_id' => $this->id])
                ->all();

        $productId = ArrayHelper::getColumn($query, 'id');
        return ArrayHelper::map(Products::find()
                ->where(['company_id' => $this->company_id])
                ->andWhere(['not in', 'id', $productId])
                ->all(), 'id', 'name');
    }
    
    /**
     * return integer
     */
    public function getFollowStatus()
    {
        return Follower::find()->where(['object_id' => $this->id, 'type' => Follower::TYPE_COLLECTION])->count();
    }
    
    /**
     * return integer
     */
    public function getViewsCount()
    {
        return Views::find()
                ->alias('vw')
                ->leftJoin('products product', 'product.id = vw.object_id')
                ->leftJoin('products_collections', 'products_collections.product_id = product.id')
                ->where(['products_collections.collection_id' => $this->id, 'vw.type' => "product"])
                ->distinct()
                ->count();
    }
    
    /**
     * return integer
     */
    public function getLikesCount()
    {
        return Likes::find()
                ->alias('likes')
                ->leftJoin('products product', 'product.id = likes.object_id')
                ->leftJoin('products_collections', 'products_collections.product_id = product.id')
                ->where(['products_collections.collection_id' => $this->id, 'likes.type' => "product"])
                ->distinct()
                ->count();
    }
    
    /**
     * return integer
     */
    public function getDownloadCount()
    {
        return ProductsCollections::find()->where(['collection_id' => $this->id])->count();
    }
    
    /**
     * return integer
     */
    public function getProductsCount()
    {
        return ProductsCollections::find()->where(['collection_id' => $this->id])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getColectionProductWithLimit($limit)
    {
        return Products::find()
                ->alias('product')
                ->leftJoin('products_collections', 'products_collections.product_id = product.id')
                ->where(['products_collections.collection_id' => $this->id])->limit($limit)->all();
    }
    
    /**
     * return array
     */
    public static function getMyInArrayMap()
    {
        return ArrayHelper::map(self::find()->where(['user_id' => \Yii::$app->user->id])->all(), 'id', 'name');
    }
    
    public function getUserName()
    {
        return $this->userModel ? $this->userModel->email : '';
    }
    
    public function getCompanyName()
    {
        return $this->companyModel ? $this->companyModel->name : '';
    }
    
    public function getCompanyModel()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    
    public function getUserModel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * return integer
     */
    public static function getMyCollectionCount()
    {
        return self::find()->where(['user_id' => \Yii::$app->user->id])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getFollowCount()
    {
        return Follower::find()->where(['object_id' => $this->id, 'type' => Follower::TYPE_COLLECTION])->count();
    }
    
    /**
     * return array
     */
    public static function deleteMyChoiseForProduct($product_id)
    {
        $collections_ids = ArrayHelper::getColumn(self::find()
                ->alias('collection')
                ->leftJoin('products_collections', 'products_collections.collection_id = collection.id')
                ->where(['collection.user_id' => \Yii::$app->user->id, 'products_collections.product_id' => $product_id])->all(), 'id');
        return ProductsCollections::deleteAll(['collection_id' => $collections_ids, 'product_id' => $product_id]);
    }
    
    /**
     * return array
     */
    public static function getMyChoiseForProductCount($product_id)
    {
        return self::find()
                ->alias('collection')
                ->leftJoin('products_collections', 'products_collections.collection_id = collection.id')
                ->where(['collection.user_id' => \Yii::$app->user->id, 'products_collections.product_id' => $product_id])->count();
    }

    /**
     * return array
     */
    public static function getMyChoiseForProduct($product_id)
    {
        return ArrayHelper::getColumn(self::find()
                ->alias('collection')
                ->leftJoin('products_collections', 'products_collections.collection_id = collection.id')
                ->where(['collection.user_id' => \Yii::$app->user->id, 'products_collections.product_id' => $product_id])->all(), 'id');
    }
    
    
}
