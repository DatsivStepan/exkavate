<?php

namespace common\models;

use Yii,
    common\models\SettingCompany;

/**
 * This is the model class for table "{{%notifications}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $message
 * @property integer $company_id
 * @property integer $read
 * @property string $created_at
 * @property string $updated_at
 */
class Notifications extends \yii\db\ActiveRecord
{
    const TYPE_AD_RECEIVES_OK = 1;
    const TYPE_AD_CROSSES_RECOGNITION  = 2;
    const TYPE_NEW_COMPANY_FOLLOW = 3;
    const TYPE_AD_TO_COLLECTION = 4;
    const TYPE_AD_TO_TREND = 5;

    public static $types = [
        self::TYPE_AD_RECEIVES_OK,
        self::TYPE_AD_CROSSES_RECOGNITION,
        self::TYPE_NEW_COMPANY_FOLLOW,
        self::TYPE_AD_TO_COLLECTION,
        self::TYPE_AD_TO_TREND
    ];

    public static $typeSubjects = [
        self::TYPE_AD_RECEIVES_OK => 'Ad  receives an Ok',
        self::TYPE_AD_CROSSES_RECOGNITION => 'Ad crosses to higher recognition level',
        self::TYPE_NEW_COMPANY_FOLLOW => 'Publisher gets a new Follow',
        self::TYPE_AD_TO_COLLECTION => 'Ad gets added to a Collection',
        self::TYPE_AD_TO_TREND => 'Ad gets added to a Trending topic'
    ];

    public static $typeMessages = [
        self::TYPE_AD_RECEIVES_OK => ' ad receives ok', //'XYZ ad receives ok
        self::TYPE_AD_CROSSES_RECOGNITION => 'Ad receives ',
        self::TYPE_NEW_COMPANY_FOLLOW => 'New follower added',
        self::TYPE_AD_TO_COLLECTION => ' ad added to ',
        self::TYPE_AD_TO_TREND => ' ad added to '
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notifications}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'read', 'type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['subject'], 'string', 'max' => 100],
            [['message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'subject' => Yii::t('app', 'Subject'),
            'message' => Yii::t('app', 'Text'),
            'company_id' => Yii::t('app', 'Company ID'),
            'read' => Yii::t('app', 'Read'),
            'created_at' => Yii::t('app', 'Data'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public static function addNotification($type, $subject, $message, $company_id, $object_id){
        if (SettingCompany::getStatus($type, $company_id)) {
            $model = new Notifications();
            $model->user_id = \Yii::$app->user->id;
            $model->type = $type;
            $model->subject = $subject;
            $model->message = $message;
            $model->company_id = $company_id;
            $model->object_id = $object_id;

            return $model->save();
        }
    }

}
