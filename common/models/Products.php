<?php

namespace common\models;

use Yii,
    common\models\ProductsTags,
    common\models\ProductsCollections,
    common\models\ProductsTrends,
    common\models\Likes,
    common\models\Views,
    common\models\ProductsComments,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%products}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $company_id
 * @property integer $category_id
 * @property string $short_content
 * @property string $content
 * @property string $img_src
 * @property string $file_src
 * @property string $comments_count
 * @property string $viewers_count
 * @property string $likes_count
 * @property string $created_at
 * @property string $updated_at
 */
class Products extends \yii\db\ActiveRecord
{
    public $sort_date;
    public $collections;
    public $trends;
    public $tags;
    public $template;
    public $premiumId;

    const PREMIUM_MAX_COUNT = 3;

    const PREMIUM_STATUS_ACTIVE = 2;
    const PREMIUM_STATUS_NOT_ACTIVE = 0;
    const PREMIUM_STATUS_ON_CONFIRMATION = 1;

    const PREMIUM_TYPE_NORMAL = 'normal';
    const PREMIUM_TYPE_VERTICALLY = 1;
    const PREMIUM_TYPE_HORIZONTALLY = 2;
    
    public static $types = [
        self::PREMIUM_TYPE_VERTICALLY => 'vertical',
        self::PREMIUM_TYPE_HORIZONTALLY => 'horizontal'
    ];

    public static $typesTranslate = [
        self::PREMIUM_TYPE_VERTICALLY => 'Portrait - 2/2',
        self::PREMIUM_TYPE_HORIZONTALLY => 'Landscape - 2/4'
    ];
    
    public static $premiumStatusArray = [
        self::PREMIUM_STATUS_ACTIVE => 'Active',
        self::PREMIUM_STATUS_NOT_ACTIVE => 'Deactive',
        self::PREMIUM_STATUS_ON_CONFIRMATION => 'On confirmation'
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'file_src', 'company_id'], 'required'],
            [
                [
                    'company_id', 'category_id', 'brand_id', 'likes_count', 'viewers_count',
                    'comments_count', 'premium_status', 'premium_type' , 'premium'
                ], 
                'integer'
            ],
            [['short_content', 'content', 'sort_date', 
                'parametr1',
                'parametr2',
                'parametr3',
                'parametr4',
                ], 'string'],
            [['created_at', 'updated_at', 'tags', 'collections', 'trends', 'embargo'], 'safe'],
            [['name', 'img_src', 'file_src', 'slug'], 'string', 'max' => 255],
            [['template'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'company_id' => Yii::t('app', 'Company'),
            'category_id' => Yii::t('app', 'Category'),
            'short_content' => Yii::t('app', 'Short Content'),
            'content' => Yii::t('app', 'Content'),
            'img_src' => Yii::t('app', 'Image'),
            'file_src' => Yii::t('app', 'File'),
            'parametr1' => Yii::t('app', 'Unit size'),
            'parametr2' => Yii::t('app', 'Col x inches'),
            'parametr3' => Yii::t('app', 'Total col ins'),
            'parametr4' => Yii::t('app', 'Pg coverage'),
            'created_at' => Yii::t('app', 'Date'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'backend\models\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->collections = ArrayHelper::getColumn($this->productsCollectionsModel, 'collection_id');
        $this->trends = ArrayHelper::getColumn($this->productsTrendsModel, 'trend_id');
        $this->tags = ArrayHelper::getColumn($this->productsTagsModel, 'name');
//        $this->premiumId = self::find()->where([
//            'company_id' => $this->company_id,
//            'premium' => Self
//        ])->count();
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        ProductsCollections::deleteAll(['product_id' => $this->id, 'company_id' => $this->company_id]);
        if ($this->collections) {
            foreach ($this->collections as $collection_id) {
                $modelNewPrCollection = new ProductsCollections();
                $modelNewPrCollection->product_id = $this->id;
                $modelNewPrCollection->collection_id = $collection_id;
                $modelNewPrCollection->company_id = $this->company_id;
                $modelNewPrCollection->save();
            }
        }

        ProductsTrends::deleteAll(['product_id' => $this->id, 'company_id' => $this->company_id]);
        if ($this->trends) {
            foreach ($this->trends as $trend_id) {
                $modelNewPrTrend = new ProductsTrends();
                $modelNewPrTrend->product_id = $this->id;
                $modelNewPrTrend->trend_id = $trend_id;
                $modelNewPrTrend->company_id = $this->company_id;
                $modelNewPrTrend->save();
            }
        }

        ProductsTags::deleteAll(['product_id' => $this->id]);
        if ($this->tags) {
            foreach ($this->tags as $tag) {
                $modelNewPrTag = new ProductsTags();
                $modelNewPrTag->product_id = $this->id;
                $modelNewPrTag->name = $tag;
                $modelNewPrTag->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }
    
    public function getLikeStatus(){
        return Likes::findOne(['type' => 'product', 'object_id' => $this->id, 'user_id' => \Yii::$app->user->id]) ? true : false;
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getEmbedLink()
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . '/products/embed/' . $this->id;
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getLink()
    {
        return '/products/view/' . $this->id;
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getLinkToProductColletion()
    {
        return '/products/collections/' . $this->id;
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getRecognitionLevel()
    {
        return Likes::getRecognitionLevel($this->id);
    }
    
    
    //
    // count begin
    //

    /**
     * return integer
     */
    public function getMyCollectionCount()
    {
        return Collections::find()
                ->alias('collection')
                ->leftJoin('products_collections', 'products_collections.collection_id = collection.id')
                ->where(['collection.user_id' => \Yii::$app->user->id, 'products_collections.product_id' => $this->id])->count();
    }

    /**
     * return integer
     */
    public function getCollectionCount()
    {
        return ProductsCollections::find()
                ->alias('productCol')
                ->joinWith([
                'collection colection'
            ])->where(['productCol.product_id' => $this->id, 'productCol.company_id' => $this->company_id])->count();
    }
    
    /**
     * return string
     */
    public function getImageSrc()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_image.jpg';
    }
    
    /**
     * return string
     */
    public function getCreatedDate($format = 'd M, Y')
    {
        return $this->created_at ?
                date($format, strtotime($this->created_at)) : '';
    }

    /**
     * return integer
     */
    public function getLikeCount()
    {
        return Likes::getCount($this->id, Likes::TYPE_PRODUCT);
    }
    
    /**
     * return integer
     */
    public function getCommentCount()
    {
        return ProductsComments::find()->where(['product_id' => $this->id])->count();
    }
    
    /**
     * return integer
     */
    public function getViewCount()
    {
        return Views::find()->where([
            'object_id' => $this->id,
            'type' => 'product',
        ])->count();
    }
    //
    // count end
    //
    
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsCommentsModel()
    {
        return $this->hasMany(ProductsComments::className(), ['product_id' => 'id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getBrandModel()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsLikes()
    {
        return $this->hasMany(Likes::className(), ['object_id' => 'id'])->andOnCondition(['type' => Likes::TYPE_PRODUCT]);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsCollectionsModel()
    {
        return $this->hasMany(ProductsCollections::className(), ['product_id' => 'id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsTrendsModel()
    {
        return $this->hasMany(ProductsTrends::className(), ['product_id' => 'id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsTagsModel()
    {
        return $this->hasMany(ProductsTags::className(), ['product_id' => 'id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCollectionsModel()
    {
        return $this->hasMany(Collections::className(), ['product_id' => 'id']);
    }
}
