<?php

namespace common\models;

use Yii,
    common\models\Collections,
    common\models\Notifications,
    common\models\Products;

/**
 * This is the model class for table "{{%products_collections}}".
 *
 * @property integer $id
 * @property integer $collection_id
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductsCollections extends \yii\db\ActiveRecord
{
    public $name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_collections}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collection_id', 'product_id', 'user_id', 'company_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function afterFind() {
        parent::afterFind();
        $this->name = $this->collection ? $this->collection->name : '';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'collection_id' => Yii::t('app', 'Collection ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        Notifications::addNotification(
            Notifications::TYPE_AD_TO_COLLECTION, 
            Notifications::$typeSubjects[Notifications::TYPE_AD_TO_COLLECTION],
            ($this->productModel ? $this->productModel->name : '') . Notifications::$typeMessages[Notifications::TYPE_AD_TO_COLLECTION] . ' ' . ($this->collection ? $this->collection->name : '') . ' collection', 
            $this->productModel ? $this->productModel->company_id : null,
            $this->product_id
        );
    }
    
    public function beforeSave($insert)
    {
        $this->user_id = \Yii::$app->user->id;
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
     /**
     * return yii\data\ActiveDataProvider
     */
    public function getCollection()
    {
        return $this->hasOne(Collections::className(), ['id' => 'collection_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductModel()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

}
