<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%sliders}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_src
 * @property string $link
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 */
class Sliders extends \yii\db\ActiveRecord
{
    const SLIDER_TYPE_HOME = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sliders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img_src'], 'required'],
            [['type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'img_src', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'img_src' => Yii::t('app', 'Img Src'),
            'link' => Yii::t('app', 'Link'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * return string
     */
    public function getImageSrc()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_image.jpg';
    }
}
