<?php

namespace common\models;

use Yii,
    common\models\Follower,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%trends}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property integer $company_id
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Trends extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%trends}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['company_id', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }
    
    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'backend\models\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Description'),
            'company_id' => Yii::t('app', 'Company ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * return string
     */
    public function getLink()
    {
        return '/trendings/view/' . $this->id;
    }
    
    /**
     * return integer
     */
    public function getViewsCount()
    {
        return Views::find()
                ->alias('vw')
                ->leftJoin('products product', 'product.id = vw.object_id')
                ->leftJoin('products_trends', 'products_trends.product_id = product.id')
                ->where(['products_trends.trend_id' => $this->id, 'vw.type' => "product"])
                ->distinct()
                ->count();
    }
    
    /**
     * return integer
     */
    public function getLikesCount()
    {
        return Likes::find()
                ->alias('likes')
                ->leftJoin('products product', 'product.id = likes.object_id')
                ->leftJoin('products_trends', 'products_trends.product_id = product.id')
                ->where(['products_trends.trend_id' => $this->id, 'likes.type' => "product"])
                ->distinct()
                ->count();
    }
    
    /**
     * return integer
     */
    public function getProductNotIn()
    {
        $query = Products::find()
                ->alias('product')
                ->select(['id' => 'product.id'])
                ->joinWith(['productsTrendsModel pcm'])
                ->where(['pcm.trend_id' => $this->id])
                ->all();

        $productId = ArrayHelper::getColumn($query, 'id');
        return ArrayHelper::map(Products::find()
                ->where(['company_id' => $this->company_id])
                ->andWhere(['not in', 'id', $productId])
                ->all(), 'id', 'name');
    }
    
    /**
     * return integer
     */
    public function getDownloadCount()
    {
        return ProductsTrends::find()->where(['trend_id' => $this->id])->count();
    }
    
    public static function getAllInArrayMap($company_id = null)
    {
        $query = self::find();
        if ($company_id) {
            $query->where(['company_id' => $company_id]);
        }
        return ArrayHelper::map($query->all(), 'id', 'name');
    }
    
    /**
     * return integer
     */
    public function getFollowStatus()
    {
        return Follower::find()->where(['object_id' => $this->id, 'type' => Follower::TYPE_TRENDING])->count();
    }
    
    public function getCompanyName()
    {
        return $this->companyModel ? $this->companyModel->name : '';
    }
    
    public function getCompanyModel()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getFollowCount()
    {
        return Follower::find()->where(['object_id' => $this->id, 'type' => Follower::TYPE_TRENDING])->count();
    }
    /**
     * return integer
     */
    public function getProductsCount()
    {
        return ProductsTrends::find()->where(['trend_id' => $this->id])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getTrendProductWithLimit($limit)
    {
        return Products::find()
                ->alias('product')
                ->leftJoin('products_trends', 'products_trends.product_id = product.id')
                ->where(['products_trends.trend_id' => $this->id])->limit($limit)->all();
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public static function getTrendWithLimit($limit)
    {
        return self::find()->limit($limit)->all();
    }
}
