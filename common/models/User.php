<?php
namespace common\models;

use common\models\query\UserQuery;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\auth\AuthAssignment;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $first_name
 * @property integer $last_name
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $description
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    
    public $profile;
    public $assignmentType;
    public $role;
    public $old_password;
    public $password;
    public $password_repeat;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'unique'],
            ['email', 'email'],
            [['type', 'role'], 'string'],
            [['company_id', 'newslater'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'first_name', 'last_name'], 'string'],

            ['old_password', 'required'],
            ['password', 'required'],
            [['password', 'password_repeat'], 'string', 'min' => 6],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            
        ];
    }

    public function scenarios()
    {
        return [
            'user_data' => ['email', 'newslater'],
            'default' => ['email', 'password', 'role', 'company_id', 'first_name', 'last_name'],
            'reset_pass' => ['password_reset_token'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'old_password' => 'Enter Old Password',
            'password' => 'Enter New Password',
            'password_repeat' => 'Verify Old Password',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()->has('user-'.$id)) {
            return new self(Yii::$app->getSession()->get('user-'.$id));
        }else {
            return self::findOne($id);
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->joinWith('tokens t')
            ->andWhere(['t.token' => $token])
            ->andWhere(['>', 't.expired_at', time()])
            ->one();
    }
    
     public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            'username' => $service->getAttribute('name'),
            'authKey' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function afterFind() {
        $modelAuthAssignment = AuthAssignment::find()->where(['user_id' => (string)$this->id])->one();
        $this->assignmentType = $modelAuthAssignment ? $modelAuthAssignment->item_name : null;
        parent::afterFind();
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        if($this->role){
            $modelAuthAssignment = AuthAssignment::find()->where(['user_id' => (string)$this->id])->one();
            if(!$modelAuthAssignment){
                $modelAuthAssignment = new AuthAssignment();
            }
            $modelAuthAssignment->item_name = $this->role;
            $modelAuthAssignment->user_id = (string)$this->id;
            $modelAuthAssignment->save();
        }
        
    }

    /**
     * return string
     */
    public function getCreatedDate($format = 'd M, Y')
    {
        return $this->created_at ?
                date($format, strtotime($this->created_at)) : '';
    }
    
    public function generatePassword(){
        $newpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
        return $newpassword;        
    }
    
    /**
     * @return bool|string
     */
    public function getNameAndSurname()
    {
        $username = $this->email;
        if ($this->username) {
            $username = $this->username;
        }
        if ($this->first_name || $this->last_name) {
            $username = $this->first_name . ' ' . $this->last_name;
        }
        return $username; 
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoleModel()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoleItemName()
    {
        $model = AuthAssignment::findOne(['user_id' => $this->id]);
        return $model ? $model->item_name : null;
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }
    
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['user_id' => 'id']);
    }

//    public function fields()
//    {
//        return [
//            'id' => 'id',
//            'username' => 'username',
//            'password' => 'password',
//            'type' => 'type',
//            'email' => 'email',
//            'description' => 'description',
//        ];
//    }
}
