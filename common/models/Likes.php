<?php

namespace common\models;

use Yii,
    common\models\Notifications,
    yii\helpers\ArrayHelper,
    common\models\Products;

/**
 * This is the model class for table "{{%likes}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $object_id
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class Likes extends \yii\db\ActiveRecord
{
    const TYPE_PRODUCT = 'product';
    const TYPE_BRAND = 'brand';
    
    const RECOGNITION_LEVEL_1 = 9; //gold
    const RECOGNITION_LEVEL_2 = 6; //silver
    const RECOGNITION_LEVEL_3 = 3; //bronze

    public static $recognitionData = [
        self::RECOGNITION_LEVEL_1 => 'gold',
        self::RECOGNITION_LEVEL_2 => 'silver',
        self::RECOGNITION_LEVEL_3 => 'bronze',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%likes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'object_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 50],
        ];
    }

     
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        if ($this->type == self::TYPE_PRODUCT) {
            $objectName = $this->productModel->name;
            Notifications::addNotification(
                Notifications::TYPE_AD_RECEIVES_OK, 
                Notifications::$typeSubjects[Notifications::TYPE_AD_RECEIVES_OK], 
                $objectName . Notifications::$typeMessages[Notifications::TYPE_AD_RECEIVES_OK], 
                $this->productModel ? $this->productModel->company_id : null,
                $this->object_id
            );
            
            $count = self::getCount($this->object_id, self::TYPE_PRODUCT);
            $new_level = null;
            switch ($count) {
                case self::RECOGNITION_LEVEL_3:
                    $new_level = self::$recognitionData[self::RECOGNITION_LEVEL_3];
                case self::RECOGNITION_LEVEL_2:
                    $new_level = self::$recognitionData[self::RECOGNITION_LEVEL_2];
                case self::RECOGNITION_LEVEL_1:
                    $new_level = self::$recognitionData[self::RECOGNITION_LEVEL_1];
            }
            if ($new_level) {
                Notifications::addNotification(
                    Notifications::TYPE_AD_CROSSES_RECOGNITION,
                    Notifications::$typeSubjects[Notifications::TYPE_AD_CROSSES_RECOGNITION], 
                    Notifications::$typeMessages[Notifications::TYPE_AD_CROSSES_RECOGNITION] . 
                        ' ' . $new_level . ' star after crossing ' . number_format($count, 2, ',', '') . ' oks ', 
                    $this->productModel ? $this->productModel->company_id : null,
                    $this->object_id
                );
            }
        }
        
    }
    
    public static function getRecognitionLevel($product_id){
        $count = self::getCount($product_id, self::TYPE_PRODUCT);
        
        $level = null;
        if (($count >= self::RECOGNITION_LEVEL_3) && ($count < self::RECOGNITION_LEVEL_2)) {
            $level = self::$recognitionData[self::RECOGNITION_LEVEL_3];
        } elseif (($count >= self::RECOGNITION_LEVEL_2) && ($count < self::RECOGNITION_LEVEL_1)) {
            $level = self::$recognitionData[self::RECOGNITION_LEVEL_2];
        } elseif ($count >= self::RECOGNITION_LEVEL_1) {
            $level = self::$recognitionData[self::RECOGNITION_LEVEL_1];
        }
        return $level;
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductModel()
    {
        return $this->hasOne(Products::className(), ['id' => 'object_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
//    getLikeFromTo
    public static function getLikeFromTo($from, $to, $company_id=null){
        $from = date('Y-m-d', strtotime($from));
        $to = date('Y-m-d', strtotime($to));
        
        return  ArrayHelper::getColumn(self::find()
                ->alias('likes')
                ->leftJoin('products product', 'product.id = likes.object_id')
                ->where(['product.company_id' => $company_id, 'likes.type' => self::TYPE_PRODUCT])
                ->andWhere(['between', 'likes.created_at', $from, $to])
                ->distinct()->all(), 'created_at');
    }
    
    public static function getCount($object_id, $type){
        return self::find()->where(['object_id' => $object_id, 'type' => $type])->count();
    }
    
    public static function myLikesCount($type){
        return self::find()->where(['type' => $type, 'user_id' => \Yii::$app->user->id])->count();
    }
}
