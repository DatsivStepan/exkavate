<?php

namespace common\models;

use Yii,
    yii\helpers\Html,
    common\models\Follower,
    common\models\Collections,
    common\models\Products,
    common\models\Likes,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%companies}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $logo_src
 * @property string $address
 * @property string $site
 * @property string $phone
 * @property string $created_at
 * @property string $updated_at
 */
class Company extends \yii\db\ActiveRecord
{
    public $premCount;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['premCount'], 'integer'],
            [['description', 'template'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'logo_src', 'address', 'site', 'file_src'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'logo_src' => Yii::t('app', 'Logo Src'),
            'address' => Yii::t('app', 'Address'),
            'site' => Yii::t('app', 'Site'),
            'phone' => Yii::t('app', 'Phone'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }    
    
//    public function behaviors()
//    {
//        return [
//            'slug' => [
//                'class'         => 'backend\models\behaviors\Slug',
//                'in_attribute'  => 'name',
//                'out_attribute' => 'slug',
//                'translit'      => true
//            ]
//        ];
//    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    /**
     * return string
     */
    public function getImageSrc()
    {
        return $this->logo_src ? '/' . $this->logo_src : '/images/default_image.jpg';
    }
    
    public function getLink()
    {
        return '/publications/view/'. $this->id;
    }
    
    public static function getAllInArrayMap()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
    
    public static function getCompanyListArray()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
        
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCollectionsCount()
    {
        return Collections::find()->where(['company_id' => $this->id])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getLikesCount()
    {
        return Likes::find()
                ->alias('likes')
                ->leftJoin('products product', 'product.id = likes.object_id')
                ->where(['product.company_id' => $this->id, 'likes.type' => Likes::TYPE_PRODUCT])
                ->distinct()
                ->count();
    }

    public function getDefaultTemplate($premiumType, $productId) {
        $productCount = $this->getProductsCountWithOutPremium();
        $template = [];

        if ($productCount > 40) {
            $productCount = 40;
        }
        if ($productCount) {
            
            for($i = 1; $i <= (int)$productCount; $i++){
                $template[] = [
                    'type' => Products::PREMIUM_TYPE_NORMAL,
                    'product_id' => null,
                ];
            }
        }
        

        if ($premiumType || $productId) {
            $template[] = [
                'type' => Products::$types[$premiumType],
                'product_id' => $productId,
                'text' => 'your new ads'
            ];
        }


        return $template;
    }

    public function getPremiumBlockCount() {
        $products = Products::find()->where(['company_id' => $this->id, 'premium' => 1])->all();
        $count = 0;
        foreach ($products as $product) {
            if ($product->premium_type == Products::PREMIUM_TYPE_VERTICALLY) {
                $count = $count + 4;
            } elseif ($product->premium_type == Products::PREMIUM_TYPE_HORIZONTALLY) {
                $count = $count + 8;
            }
        }
        return $count;
    }

    public function getTemplateForOutput() {
        $template = json_decode($this->template);

        $products = Products::find()->where(['company_id' => $this->id])
                ->andWhere([
                    'OR',
                    ['premium' => 0],
                    ['premium' => null],
                    ['premium_status' => 0],
                ])
                ->orderBy(['id' => SORT_DESC])->all();
                
        $productCount = count($products);
        $newTemplate = [];
        $k = 0;
        foreach ($template as $temp) {
            if ($temp->type == Products::PREMIUM_TYPE_NORMAL) {
                if ($k <= $productCount){
                    $newTemplate[] = ['type' => 'normal', 'data' => $products[$k]];
                    $k++;
                }
            } else {
                if ($product = Products::find()->where(['id' => $temp->product_id, 'premium_status' => Products::PREMIUM_STATUS_ACTIVE])->one()) {
                    $newTemplate[] = ['type' => $temp->type, 'data' => $product];
                }
            }
        }

        if (array_key_exists($k, $products)) {
            $newTemplate[] = ['type' => 'normal', 'data' => $products[$k]];
            $k++;
        }
        if (array_key_exists($k, $products)) {
            $newTemplate[] = ['type' => 'normal', 'data' => $products[$k]];
            $k++;
        }
        if (array_key_exists($k, $products)) {
            $newTemplate[] = ['type' => 'normal', 'data' => $products[$k]];
        }
        
        return $newTemplate;
    }

    public function getTemplateForUpdate($premiumType = null, $productId = null) {
        if (!$this->template) {
            return [];
        }
        $template = json_decode($this->template);
        $newTemplate = [];
        $premiumBlockCount = $this->getPremiumBlockCount();
        $productCount = $this->getProductsCountWithOutPremium(44-(int)$premiumBlockCount);
        $k = $productCount;
        foreach ($template as $temp) {
            if ($temp->type == Products::PREMIUM_TYPE_NORMAL) {
                if ($k > 0){
                    $newTemplate[] = $temp;
                    $k = $k - 1;
                }
            } else {
                if ($product = Products::find()->where(['id' => $temp->product_id])->one()) {
                    $newTemplate[] = ['type' => $temp->type, 'product_id' => $temp->product_id, 'text' => $product->name];
                }
            }
        }

        if ($k > 0) {
            for($i = 1; $i <= (int)$k; $i++){
                $newTemplate[] = [
                    'type' => Products::PREMIUM_TYPE_NORMAL,
                    'product_id' => null,
                    'text' => 'your new ads'
                ];
            }
        }
        if ($premiumType || $productId) {
            $newTemplate[] = ['type' => Products::$types[$premiumType], 'product_id' => $productId, 'text' => 'your new ads'];
        }
        return $newTemplate;
    }

    public function getTemplate($premiumType = null, $productId = null) {
        if ($this->template) {
            return $this->getTemplateForUpdate($premiumType, $productId);
        } else {
            return $this->getDefaultTemplate($premiumType, $productId);
        }
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsCountWithOutPremium($limit=null)
    {
        return Products::find()->where(['company_id' => $this->id])
                ->andWhere([
                    'OR',
                    ['premium' => 0],
                    ['premium' => null],
                ])->limit($limit)
                ->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductsCount()
    {
        return Products::find()->where(['company_id' => $this->id])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getPremiumCount()
    {
        return Products::find()->where(['company_id' => $this->id, 'premium' => 1])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getEmbargoCount()
    {
        return Products::find()->where(['company_id' => $this->id])->andWhere(['>', 'embargo', date('Y-m-d')])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getAdsCount()
    {
        return Products::find()->where(['company_id' => $this->id])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getActivePremiumCount()
    {
        return Products::find()->where(['company_id' => $this->id, 'premium' => 1, 'premium_status' => Products::PREMIUM_STATUS_ACTIVE])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getViewsCount()
    {
        return Views::find()
                ->alias('vw')
                ->leftJoin('products product', 'product.id = vw.object_id')
                ->where(['product.company_id' => $this->id, 'vw.type' => Views::TYPE_PRODUCT])
                ->distinct()
                ->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getFollowCount()
    {
        return Follower::find()->where(['object_id' => $this->id, 'type' => Follower::TYPE_COMPANY])->count();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCompanyFollowers()
    {
        return $this->hasMany(Follower::className(), ['object_id' => 'id'])->andOnCondition(['type' => Follower::TYPE_COMPANY]);
    }
    
    public function getCompanyProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'company_id']);
    }
    
    public function getPremiumProducts()
    {
        return $this->hasMany(Products::className(), ['company_id' => 'id'])
                ->andOnCondition(['premium' => 1]);
    }
    
    public function getFollowStatus(){
        return Follower::findOne(['type' => Follower::TYPE_COMPANY, 'object_id' => $this->id, 'user_id' => \Yii::$app->user->id]) ? true : false;
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getCompanyProductWithLimit($limit)
    {
        return Products::find()->where(['company_id' => $this->id])->limit($limit)->all();
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public static function getCompanyWithLimit($limit)
    {
        return self::find()->limit($limit)->all();
    }

    public static function getShablonForFile($companyId)
    {
        $model = self::findOne($companyId);
        return '<html>'.
               '<head>'.
                   '<title>' . $model->name . '</title>'.
               '</head>'.
               '<body style="margin:0px;">'.
                    Html::tag('iframe', null, [
                        'src' => self::getFrameUrl($model->id),
                        'width' => '100%',
                        'height' => '100%',
                        'style' => 'border:0px',
                    ]).
               '</body>'.
               '</html>';
    }

    public static function getFrameUrl($id)
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . '/frame/publication/view?id=' . $id;
    }

    public function generateFile()
    {
        $ds          = DIRECTORY_SEPARATOR;
        $fileSrc = '/files/frame'.$ds.$this->id;
        $storeFolder = \Yii::getAlias('@webroot') . $fileSrc;
        if (!is_dir($storeFolder)) {
            mkdir($storeFolder, 0777, true);
        }
        $fileName = 'index.html';
        $fileSrc = $fileSrc . $ds . $fileName;
        $storeFolder = $storeFolder . $ds . $fileName;
        $fp = fopen($storeFolder, "w");
        fwrite($fp, self::getShablonForFile($this->id));
        fclose($fp);

        $this->file_src = 'publisher' . $fileSrc;
        $this->save();

        return true;
    }
}
