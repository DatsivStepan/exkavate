<?php

namespace common\models;

use Yii;
use common\models\Products;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $parent_id
 * @property string $img_src
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['parent_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'img_src', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'parent_id' => Yii::t('app', 'Parent category'),
            'img_src' => Yii::t('app', 'Img Src'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'backend\models\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
    
    public static function parentCategoriesList()
    {
        return ArrayHelper::map(self::find()
                ->where(['parent_id' => '0'])
                ->orWhere(['parent_id' => null])
                ->all(), 'id', 'name');
    }
    
    public static function getAllInArrayMap()
    {
        return ArrayHelper::map(self::find()
                ->all(), 'id', 'name');
    }

    public static function getProductsCountObject($object_id, $type, $category_id=null)
    {
        $count = 0;
        switch ($type) {
            case 'publisher':
                $count = Products::find()->where(['company_id' => $object_id])
                    ->andFilterWhere([ 'category_id' => $category_id])
                    ->count();
                break;
            case 'advertiser':
                $count = Products::find()->where(['brand_id' => $object_id])
                    ->andFilterWhere(['category_id' => $category_id])
                    ->count();
                break;
        }
        return $count;
    }
    
    public function getImageSrc()
    {
        return $this->img_src ? '/' . $this->img_src : '/images/default_image.jpg';
    }
    
    public function getLink(){
        return '/category?' . http_build_query(['ProductsSearch' => ['category_id' => [$this->id]]]);
    }

    public function getCategories(){
        return self::find()->all();
    }

    public function getProductCount(){
        return Products::find()->where(['category_id' => $this->id])->count();
    }

    public static function getAllProductCount(){
        return Products::find()->count();
    }
        
}
