<?php

namespace common\models;

use Yii,
    common\models\Notifications,
    common\models\Products,
    common\models\Trends;

/**
 * This is the model class for table "{{%products_trends}}".
 *
 * @property integer $id
 * @property integer $trend_id
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductsTrends extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_trends}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trend_id', 'product_id', 'user_id', 'company_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'trend_id' => Yii::t('app', 'Trend ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        Notifications::addNotification(
            Notifications::TYPE_AD_TO_TREND, 
            Notifications::$typeSubjects[Notifications::TYPE_AD_TO_TREND], 
            ($this->productModel ? $this->productModel->name : '') . Notifications::$typeMessages[Notifications::TYPE_AD_TO_TREND] . ' ' . ($this->trendModel ? $this->trendModel->name : '') . ' trending topic', 
            $this->productModel ? $this->productModel->company_id : null,
            $this->product_id
        );
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getTrendModel()
    {
        return $this->hasOne(Trends::className(), ['id' => 'trend_id']);
    }

    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductModel()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
    
    public function beforeSave($insert)
    {
        $this->user_id = \Yii::$app->user->id;
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }
}
