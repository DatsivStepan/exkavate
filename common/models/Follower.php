<?php

namespace common\models;

use Yii,
    common\models\Notifications,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%follower}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class Follower extends \yii\db\ActiveRecord
{
    const TYPE_COMPANY = 'company';
    const TYPE_BRAND = 'brand';
    const TYPE_COLLECTION = 'collection';
    const TYPE_TRENDING = 'trending';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%follower}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'object_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if ($this->type == self::TYPE_COMPANY) {
            Notifications::addNotification(
                Notifications::TYPE_NEW_COMPANY_FOLLOW, 
                Notifications::$typeSubjects[Notifications::TYPE_NEW_COMPANY_FOLLOW], 
                Notifications::$typeMessages[Notifications::TYPE_NEW_COMPANY_FOLLOW], 
                $this->object_id,
                $this->object_id
            );
        }
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public static function getFollowerFromTo($from, $to, $company_id=null){
        $from = date('Y-m-d', strtotime($from));
        $to = date('Y-m-d', strtotime($to));
        
        return ArrayHelper::getColumn(self::find()
                ->alias('follow')
                ->where(['between', 'follow.created_at', $from, $to])
                ->andWhere(["follow.type" => self::TYPE_COMPANY])
                ->andWhere(['follow.object_id' => $company_id])
                ->all(), 'created_at');
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getProductModel()
    {
        return $this->hasOne(Products::className(), ['id' => 'object_id']);
    }
    
    public static function getFollowerStatus($object_id, $type) {
        return self::findOne(['object_id' => $object_id, 'type' => $type, 'user_id' => \Yii::$app->user->id]) ? true : false;
    }
    
    public static function getCount($object_id, $type){
        return self::find()->where(['object_id' => $object_id, 'type' => $type])->count();
    }
    
    public static function getMyFollowerCount($type){
        return self::find()->where(['type' => $type, 'user_id' => \Yii::$app->user->id])->count();
    }
    
    public static function getMyFollowerCollectionCount(){
        return Collections::find()
                ->alias('collections')
                ->leftJoin('follower', "follower.object_id = collections.id AND follower.type = '" . self::TYPE_COLLECTION . "'" )
                ->where(['follower.user_id' => \Yii::$app->user->id])->count();
    }
    
    public static function getMyFollowerTrendsCount(){
        return Trends::find()
                ->alias('trends')
                ->leftJoin('follower', "follower.object_id = trends.id AND follower.type = '" . self::TYPE_TRENDING . "'" )
                ->where(['follower.user_id' => \Yii::$app->user->id])->count();
    }
}
