<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
/**
 * Login form
 */
class LoginForm extends Model
{
    const TYPE_USER = 'user';
    const TYPE_ADMIN = 'admin';
    const TYPE_COMPANY_ADMIN = 'company_admin';
    const TYPE_COMPANY_MANAGER = 'company_manager';
    
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rememberMe' => \Yii::t('app','Keep me logged in'),
        ];
    }
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function loginUser()
    {
        if ($this->validate() && $this->getUserUser()) {
            return Yii::$app->user->login($this->getUserUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    
    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function adminLogin()
    {
        if ($this->validate() && $this->getUserAdmin()) {
            return Yii::$app->user->login($this->getUserAdmin(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    
    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function loginPublisher()
    {
        if ($this->getUserPublisher()) {
            return Yii::$app->user->login($this->getUserPublisher(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::find()->where(['username' => $this->username])->orWhere(['email' => $this->username])->one();
        }
        
        return $this->_user;
    }
    
    protected function getUserUser()
    {
        if ($this->_user === null) {
            $this->_user = User::find()->where(['username' => $this->username])->orWhere(['email' => $this->username])->one();
        }

        if($this->_user->type == self::TYPE_USER){
            return $this->_user;            
        }else{
            return false;
        }
    }
    
    protected function getUserAdmin()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        if($this->_user->type == self::TYPE_ADMIN){
            return $this->_user;            
        }else{
            return false;
        }
    }
    
    protected function getUserPublisher()
    {
        if ($this->_user === null) {
            $this->_user = User::find()->where(['username' => $this->username])->orWhere(['email' => $this->username])->one();
        }

        if(($this->_user->assignmentType == self::TYPE_COMPANY_ADMIN) || ($this->_user->assignmentType == self::TYPE_COMPANY_MANAGER)){
            return $this->_user;
        }else{
            return false;
        }
    }
}
