<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products_comments}}".
 *
 * @property integer $id
 * @property string $description
 * @property integer $product_id
 * @property integer $parent_id
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductsComments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_comments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['product_id', 'parent_id', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $model = Products::findOne($this->product_id);
        if($model){
            $model->comments_count = $model->comments_count + 1;
            $model->save();
        }
    }
    
    public function afterDelete() {
        parent::afterDelete();
        $model = Products::findOne($this->product_id);
        if($model){
            $model->comments_count = $model->comments_count - 1;
            $model->save();            
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'product_id' => Yii::t('app', 'Product ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * return yii\data\ActiveDataProvider
     */
    public function getChildcomments()
    {
        return $this->hasMany(ProductsComments::className(), ['parent_id' => 'id'])->orderBy(['created_at' => SORT_ASC]);
    }
    
}
