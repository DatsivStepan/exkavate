<?php

namespace common\models;

use Yii,
    yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%news_category}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $img_src
 * @property string $featured
 * @property string $created_at
 * @property string $updated_at
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_category}}';
    }

    
    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $attribute = [
            'featured',
        ];
        if (in_array($name, $attribute)) {
            $this->setAttribute($name, serialize($value));
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        $attribute = [
            'featured',
        ];
        if (in_array($name, $attribute)) {
            return unserialize($this->getAttribute($name));
        }

        return parent::__get($name);
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'featured'], 'safe'],
            [['name', 'img_src'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'img_src' => Yii::t('app', 'Img Src'),
            'featured' => Yii::t('app', 'Featured'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    public static function getHomeSliderNews()
    {
        return News::findAll(['inHome' => 1]);
    }
    
    public static function getAllInArrayMap()
    {
        $query = self::find()->orderBy(['sort' => SORT_ASC]);
        
        return ArrayHelper::map($query->all(), 'id', 'name');
    }
    
    public function getLink()
    {
        return '/news/index/' . $this->id;
    }
    
    public function getFeaturedId()
    {
        $newsIdArray = null;
        if ($this->featured && is_array($this->featured)) {
            foreach ($this->featured as $newsId => $value) {
                if ($value == '1') {
                    $newsIdArray[] = $newsId;
                }
            }
        }
        return $newsIdArray;
    }
    
    public function getAllSliderNews()
    {
        $newsIdArray = $this->getFeaturedId();
        if ($newsIdArray) {
            return News::findAll($newsIdArray);
        }
        return null;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsWithLimit($limit)
    {
        return News::find()
                ->where(['category_id' => $this->id])
                ->limit($limit)
                ->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsModel()
    {
        return $this->hasMany(News::className(), ['category_id' => 'id']);
    }
}
