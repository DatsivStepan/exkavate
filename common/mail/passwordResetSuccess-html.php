<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$link = Yii::$app->urlManager->createAbsoluteUrl([]);
?>
<div class="password-reset">
    <div style="text-align:center;">
        <img src="http://exkavate.roketdev.site/images/logo.png" style="width:150px;margin:0 auto;">
    </div>
    <p>Your password change at Exkavate.com was successful.</p>
    <p>Your new password is <?= $password; ?></p>
    <p>The Exkavate Accounts team</p>
</div>
