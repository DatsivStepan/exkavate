<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->email) ?>,</p>

    <p><b>Your login:</b> - <?= $user->email; ?></p>
    <p><b>Your password:</b> - <?= $user->password?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
