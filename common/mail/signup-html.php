<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$link = Yii::$app->urlManager->createAbsoluteUrl([]);
?>
<div class="password-reset">
    <div style="text-align:center;">
        <img src="http://exkavate.roketdev.site/images/logo.png" style="width:150px;margin:0 auto;">
    </div>
    <p>Welcome to Exkavate.</p>
    <p>To log in when visiting our site just click Login or My Account at the top right corner of every page, and then enter your email address and password.</p>
    <p>Use the following values when prompted to log in:</p>
    <p>Email: <?= Html::encode($user->email) ?></p>
    <p>Password: <?= $user->password; ?></p>
    <p>If you have any questions, please feel free to contact us at info@exkavate.com or by phone at 0113 238 9520.</p>
    <p>Exkavate Accounts team</p>
</div>
