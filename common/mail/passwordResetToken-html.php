<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

<div class="password-reset">
    <div style="text-align:center;">
        <img src="http://exkavate.roketdev.site/images/logo.png" style="width:150px;margin:0 auto;">
    </div>
    <p>We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below.</p>
    <p>Click the link below to reset your password using our secure server:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>If you did not request to have your password reset you can safely ignore this email. Rest assured your Exkavate account is safe.</p>
    <p>If clicking the link does not work, you can copy and paste the link into your browser's address window, or retype it there. Once you have returned to Exkavate.com, you will see instructions for resetting your password.</p>
    <p>The Exkavate Accounts Team</p>

</div>
