<?php
/**
 * @author DatsivStepan
 */

namespace common\components;

use Yii;
use yii\helpers\ArrayHelper;

class FunctionHelper
{
    
    public static function getDateArrayFromTo($from, $to)
    {
        $from = date('Y-m-d', strtotime($from));
        $to = date('Y-m-d', strtotime($to));

        $dateFrom = date_create($from);
        $dateTo = date_create($to);
        $interval = date_diff($dateFrom, $dateTo);
        $dayIntervalCount = $interval->days;

        $arrayDates = [];
        if ($from == $to) {
            $arrayDates[] = $from;
        } else {
            $date = $from;
            $arrayDates[$from] = 0;
            for ($i = 0; $i < $dayIntervalCount; $i++) {
                $date = date('Y-m-d', strtotime($date . " +1 days"));
                $arrayDates[$date] = 0;
            }
        }
        
        return $arrayDates;
    }
}