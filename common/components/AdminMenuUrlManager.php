<?php
/**
 * @author DatsivStepan
 */

namespace common\components;

use Yii;


class AdminMenuUrlManager
{
    public function checkUrl($url)
    {
        if (is_array($url)) {
            foreach ($url as $link) {
                if (\Yii::$app->request->pathInfo == $link){
                    return 'active';
                }
            }
            return '';
        } else {
            return (\Yii::$app->request->pathInfo == $url)?'active':'';
        }
    }
}