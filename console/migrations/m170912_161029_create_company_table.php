<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `category`.
 */
class m170912_161029_create_company_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%companies}}', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'slug' => Schema::TYPE_STRING . '(255) NULL',
            'description' => $this->text(),
            'logo_src' => Schema::TYPE_STRING . '(255) NULL',
            'address' => Schema::TYPE_STRING . '(255) NULL',
            'site' => Schema::TYPE_STRING . '(255) NULL',
            'phone' => Schema::TYPE_STRING . '(16) NULL',
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%companies}}');
    }
}
