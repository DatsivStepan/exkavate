<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `category`.
 */
class m170828_135313_create_category_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'slug' => Schema::TYPE_STRING . '(255) NULL',
            'description' => $this->text(),
            'parent_id' => $this->integer()->Null(),
            'img_src' => Schema::TYPE_STRING . '(255) NULL',
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
