<?php

use yii\db\Migration;
use yii\db\Schema;

class m171104_222201_create_likes_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%likes}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => $this->integer()->Null(),
            'object_id' => $this->integer()->Null(),
            'type' => Schema::TYPE_STRING . '(50) NULL',
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%likes}}');
    }
}

