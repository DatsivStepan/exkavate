<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `category`.
 */
class m170925_192550_create_products_comments_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products_comments}}', [
            'id' => $this->primaryKey(),
            'description' => $this->text(),
            'product_id' => $this->integer()->Null(),
            'parent_id' => $this->integer()->Null(),
            'user_id' => $this->integer()->Null(),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%products_comments}}');
    }
}
