<?php

use yii\db\Migration;
use yii\db\Schema;

class m171110_200247_create_setting_company_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%setting_company}}', [
            'id' => Schema::TYPE_PK,
            'type' => $this->integer()->null(),
            'subject' => Schema::TYPE_STRING . '(255) NULL',
            'status' => $this->integer()->null(),
            'company_id' => $this->integer()->null(),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%setting_company}}');
    }
}
