<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `category`.
 */
class m170927_213746_create_products_views_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products_views}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->Null(),
            'user_id' => $this->integer()->Null(),
            'user_ip' => $this->integer()->Null(),
            'created_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%products_views}}');
    }
}
