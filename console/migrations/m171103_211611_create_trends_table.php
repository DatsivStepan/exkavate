<?php

use yii\db\Migration;
use yii\db\Schema;

class m171103_211611_create_trends_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%trends}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'slug' => Schema::TYPE_STRING . '(255) NULL',
            'description' => Schema::TYPE_TEXT . '(255) NULL',
            'company_id' => $this->integer()->Null(),
            'user_id' => $this->integer()->Null(),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%trends}}');
    }
}

