<?php

use yii\db\Migration;
use yii\db\Schema;

class m171103_230306_create_video_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%video}}', [
            'id' => Schema::TYPE_PK,
            'link' => Schema::TYPE_STRING . '(255) NULL',
            'description' => Schema::TYPE_TEXT . '(255) NULL',
            'product_id' => $this->integer()->Null(),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%video}}');
    }
}

