<?php

use yii\db\Migration;
use common\models\Products;

class m170927_212634_add_column_analitics_to_products extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Products::tableName(), 
                'viewers_count', $this->integer(11)->null()->after('file_src'));
        $this->addColumn(Products::tableName(), 
                'comments_count', $this->integer(11)->null()->after('file_src'));
        $this->addColumn(Products::tableName(), 
                'likes_count', $this->integer(11)->null()->after('file_src'));
    }

    public function safeDown()
    {
        echo "m170927_212634_add_column_analitics_to_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170927_212634_add_column_analitics_to_products cannot be reverted.\n";

        return false;
    }
    */
}

