<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `category`.
 */
class m170912_161316_create_products_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING . '(255) NULL',
            'slug' => Schema::TYPE_STRING . '(255) NULL',
            'company_id' => $this->integer()->Null(),
            'category_id' => $this->integer()->Null(),
            'brand_id' => $this->integer()->Null(),
            'short_content' => $this->text(),
            'content' => $this->text(),
            'img_src' => Schema::TYPE_STRING . '(255) NULL',
            'file_src' => Schema::TYPE_STRING . '(255) NULL',
            'embargo' => Schema::TYPE_DATE,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%products}}');
    }
}