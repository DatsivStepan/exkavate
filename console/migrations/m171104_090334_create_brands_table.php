<?php

use yii\db\Migration;
use yii\db\Schema;

class m171104_090334_create_brands_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%brands}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(100) NULL',
            'slug' => Schema::TYPE_STRING . '(100) NULL',
            'description' => Schema::TYPE_TEXT . '(255) NULL',
            'img_src' => Schema::TYPE_STRING . '(255) NULL',
            'category_id' => $this->integer()->Null(),
            'company_id' => $this->integer()->Null(),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%brands}}');
    }
}

