<?php

use yii\db\Migration;
use yii\db\Schema;

class m171104_145633_create_products_trends_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products_trends}}', [
            'id' => Schema::TYPE_PK,
            'trend_id' => $this->integer()->Null(),
            'product_id' => $this->integer()->Null(),
            'user_id' => $this->integer()->Null(),
            'company_id' => $this->integer()->Null(),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%products_trends}}');
    }
}

