<?php

use yii\db\Migration;
use yii\db\Schema;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->null()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'signup_type' => $this->string()->Null(),
            'type' => $this->string()->Null(),
            'first_name' => $this->string()->Null(),
            'last_name' => $this->string()->Null(),
            'social_id' => $this->string()->Null()->unique(),
            'company_id' => $this->integer()->Null(),
            'status' => $this->smallInteger()->null()->defaultValue(10),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
        
        $this->batchInsert('{{%user}}', ['username', 'first_name', 'last_name', 'auth_key', 'password_hash', 'password_reset_token', 'type', 'email', 'status', 'created_at', 'updated_at'], 
            [['admin', 'first_name', 'last_name', 'key', '$2y$13$IXNGyMNxFdXBH.i.2eD9wOnOsEZNiWONememsdcgktY.HfysxdDci', '', 'admin','adminnnn@gmail.com', '10', date("Y-m-d H:i:s"), date("Y-m-d H:i:s")]]);        
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
