<?php

use yii\db\Migration;
use yii\db\Schema;

class m171110_185255_create_notifications_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notifications}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => $this->integer()->null(),
            'type' => $this->integer()->null(),
            'object_id' => $this->integer()->null(),
            'subject' => Schema::TYPE_STRING . '(100) NULL',
            'message' => Schema::TYPE_STRING . '(255) NULL',
            'company_id' => $this->integer()->null(),
            'read' => $this->integer()->null(),
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%notifications}}');
    }
}

