<?php

namespace console\controllers;

use common\rbac\Rbac;
use common\rbac\rules\PostAuthorRule;
use common\rbac\rules\ProfileOwnerRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $role_admin = Yii::$app->authManager->createRole(Rbac::ADMIN);
        $role_admin->description = 'Admin';
        $auth->add($role_admin);
        
        $role_compnay_admin = Yii::$app->authManager->createRole(Rbac::COMPANY_ADMIN);
        $role_compnay_admin->description = 'Publisher admin';
        $auth->add($role_compnay_admin);
        $role_company_manager = Yii::$app->authManager->createRole(Rbac::COMPANY_MANAGER);
        $role_company_manager->description = 'Publisher manager';
        $auth->add($role_company_manager);
        $auth->addChild($role_compnay_admin, $role_company_manager);
        
        $role_user = Yii::$app->authManager->createRole(Rbac::USER);
        $role_user->description = 'User';
        $auth->add($role_user);
    }
}