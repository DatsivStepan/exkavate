<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="col-lg-4 col-md-2 col-sm-1 col-xs-0"></div>

    <div class="col-lg-4 col-md-6 col-sm-10 col-xs-12">
        <div class="sectionTitle titl-h1" style="margin-bottom: 20px!important;"><?= Html::encode($this->title) ?><br></div>
        <?php
        if (Yii::$app->getSession()->hasFlash('error')) {
            echo '<div class="alert alert-danger">' . Yii::$app->getSession()->getFlash('error') . '</div>';
        }
        ?>

        <div class="log_text_in">Please fill out the following fields to login:</div>
        <?php $form = ActiveForm::begin(['id' => 'login-publisher-form']); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Username']) ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password']) ?>
        <div id="login_form_remembMe">
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
        </div>

        <div id="login-form" class="form-group" style="margin-bottom: 20px">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <div class="register-link dd33_5">
            If you forgot your password you can <?= Html::a('reset it', '/site/request-password-reset') ?>.
        </div>
        <?php ActiveForm::end(); ?>
        <!--<p class="lead">Do you already have an account on one of these sites? Click the logo to log in with it here:</p>-->
        <?php //echo \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
    </div>
</div>
<div class="clearfix"></div>

