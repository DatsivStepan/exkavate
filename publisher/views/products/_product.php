<?php
use common\models\Likes;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

    <div class="tovar_profile_one111 tovar_profile_one65">
        <div class="im_ju_3">
            <a href="<?= $product->getLink(); ?>">
                <div class="img_ki_44"
                     style="background: url(<?= $product->getImageSrc(); ?>) center no-repeat"></div>
            </a>
            <p class="data_tovar_catal111">
                <?= $product->getCreatedDate(); ?>
                <!--<a><img src="/images/wwe.png"></a>-->
            </p>
            <a href="<?= $product->getLink(); ?>"
               class="name_naw_catal111"><?= $product->name; ?></a>
        </div>
        <div class="div_catal_bott111 div_bot_56">
            <?php $class = ''; ?>
            <?php if (!\Yii::$app->user->isGuest) { ?>
                <?php if ($product->getLikeStatus()) { ?>
                    <?php $class = 'js-like active'; ?>
                <?php } else { ?>
                    <?php $class = 'js-like not_active'; ?>
                <?php } ?>
            <?php } else{ ?>
                    <?php $class = 'js-login-modal'; ?>
            <?php } ?>
            <a class="icon_like <?= $class; ?>" data-title="Like" data-type="<?= Likes::TYPE_PRODUCT; ?>" data-object_id="<?= $product->id; ?>">
                <div class="bacg_img"></div>
                <span class="pad_span2_right_222 pad_span23_right_22225 js-like-count">
                    <?= $product->getLikeCount(); ?>
                </span>
            </a>
            <a>
                <img class="" src="/images/111.png">
                <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getViewCount(); ?>
                </span>
            </a>
            <a>
                <img class="" src="/images/222.png">
                <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCommentCount(); ?>
                </span>
            </a>
            <a href="<?= $product->getLink(); ?>">
                <img class="" src="/images/333.png">
                <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCollectionCount(); ?>
                </span>
            </a>
        </div>
    </div>
