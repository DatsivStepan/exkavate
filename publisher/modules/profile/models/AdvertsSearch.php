<?php

namespace publisher\modules\profile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\Likes;

/**
 * AdvertsSearch represents the model behind the search form about `common\models\Products`.
 */
class AdvertsSearch extends Products
{
    public $embargoStatus;
    public $recognitionsStatus;
    public $likesCount;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'category_id', 'likesCount', 'likes_count', 'comments_count', 'viewers_count'], 'integer'],
            [
                [
                    'name', 'slug', 'short_content', 'embargo', 'content',
                    'img_src', 'img_src2', 'file_src', 'created_at', 
                    'updated_at', 'embargoStatus'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $company_id)
    {
        $query = Products::find()
                ->alias('product')
                ->select([
                    'product.*', 
                    'likesCount' => "(SELECT COUNT(lk.id) FROM " . Likes::tableName() . " lk WHERE lk.object_id = product.id AND lk.type = '" . Likes::TYPE_PRODUCT . "')",
                ])
                ->where(['company_id' => $company_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if($this->embargoStatus){
            $query->andFilterWhere(['>', 'product.embargo', date('Y-m-d')]);
        }
        
        if($this->recognitionsStatus){
            $query->andFilterHaving(['>', 'likesCount', Likes::RECOGNITION_LEVEL_3]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'product.id' => $this->id,
            'product.premium' => $this->premium,
            'product.company_id' => $this->company_id,
            'product.category_id' => $this->category_id,
            'product.likes_count' => $this->likes_count,
            'product.comments_count' => $this->comments_count,
            'product.viewers_count' => $this->viewers_count,
            'product.created_at' => $this->created_at,
            'product.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'product.name', $this->name])
            ->andFilterWhere(['like', 'product.slug', $this->slug])
            ->andFilterWhere(['like', 'product.short_content', $this->short_content])
            ->andFilterWhere(['like', 'product.content', $this->content])
            ->andFilterWhere(['like', 'product.img_src', $this->img_src])
            ->andFilterWhere(['like', 'product.img_src2', $this->img_src])
            ->andFilterWhere(['like', 'product.file_src', $this->file_src]);

        return $dataProvider;
    }
}
