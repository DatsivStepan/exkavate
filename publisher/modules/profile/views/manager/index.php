<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manager');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="wrap profile-pages publisher-page">
    <div class="container ">
    <?= $this->render('_profile_menu'); ?>
        <div class="row manager-ads-height">
          <div class="profile-products cleafix prof-manag-pad">
            <?php foreach ($dataProvider->getModels() as $product) { ?>
            <div class="col-20-proz col-md-3 col-sm-4 col-xs-6 product-container-padding whidth_media_1400 product-one-all-no-home product-fiv-pading">
              <?= $this->renderFile(\Yii::getAlias('@frontend/views/templates/_product.php'), ['product' => $product])?>
            </div>
            <?php } ?>
          </div>
          <?php if(!$dataProvider->getModels()) {?>
          <p class="text-center manager-empty-ads">No Ads at this time</p>
          <?php }?>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-sm-offset-4 bottom-pagination">
            <?php LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
          </div>
        </div>
    </div>
  </div>