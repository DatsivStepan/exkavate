<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Oks');
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="row backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <?= $this->render('/manager/_header'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-md-4 col-sm-12 col-xs-12 padding_0 media_wight_1200">
                <?= $this->render('/manager/_profile_menu'); ?>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column right_colum_collection right_colum22_collection22">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12 padding_0">
                        <p class="p_total_followers">Total number of Oks - <span><?= $likeCount; ?></span></p>
                    </div>
                    <div id="from_daata22" class="col-lg-6 col-md-7 col-sm-7  col-xs-12 padding_0">
                        <button type="submit" class="btn btn-success filterAction bat_coll_creat float_right">Filter</button>
                        <?= DatePicker::widget([
                            'name' => 'from_date',
                            'type' => DatePicker::TYPE_RANGE,
                            'name2' => 'to_date',
                            'value' => date("Y-m-d", strtotime("-1 month")),
                            'value2' => date('Y-m-d'),
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => true,
                                'format' => 'yyyy-mm-dd',
                                'endDate' => '+1d',
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <div id="chart_div"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawBackgroundColor);

    function drawBackgroundColor() {
        var from = $('input[name=from_date]').val();
        var to = $('input[name=to_date]').val();
        
        var date1 = new Date(from);
        var date2 = new Date(to);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays <= 31) {
                $.ajax({
                        url: '/publisher/profile/manager/get-like-data',
                        type: 'POST',
                        data: {from: from, to: to},
                        dataType: 'json',
                        success: function (response) {
                            var array = [];
                            jQuery.map(response.data, function( i, a ) {
                                array.push([new Date(a), i]);
                            })
                            if (response.status) {
                                    var data = new google.visualization.DataTable();
                                    data.addColumn('date', 'Date');
                                    data.addColumn('number', 'Like');

                                    data.addRows(array);
                                    var options = {
                                        backgroundColor: 'transparent',
                                        legend: {position: 'none'},
                                        pointsVisible: {visible: true}
                                    };

                                    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                                    chart.draw(data, options);
                            }
                        }
                    })
        } else {
            swal("wrong data!", "The period of selection should be no more than a month!", "warning") 
        }
    }
    
    $(document).ready(function () {
        $(document).on('click', '.filterAction', function () {
            drawBackgroundColor()
        })
    })
</script>
