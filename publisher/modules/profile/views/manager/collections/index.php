<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use common\models\Likes,
    common\models\Follower;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\CollectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Collections');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/manager/_profile_menu'); ?>
        <?php foreach ($dataProvider->getModels() as $collection) { ?>
            <div class="row manager-ads-height">
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                    <div class="colection-sidebar">
                        <a class="colection-name" href="<?= $collection->getLink(); ?>">
                            <?= $collection->name; ?>
                        </a>
                        <div class="colection-description">
                            <?= $collection->description; ?>
                        </div>

                        <div style="clear: both"></div>
                        <div class="hidden-xs coll-dis">
                            <div class="colection-icons">
                                <!--        <div class="collec_icon2">-->
                                <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
                                <!--            <span class="pad_span_collec2">-->
                                <?php //= $collection->getLikesCount(); ?>
                                <!--</span>-->
                                <!--        </div>-->
                                <div class="colection-icon">
                                    <a>
                                        <img class="" src="/images/col1.png">
                                    </a>
                                    <?= $collection->getViewsCount(); ?>
                                </div>
                                <div class="colection-icon">
                                    <a href="<?= $collection->getLink(); ?>">
                                        <img class="" src="/images/08_collections.png">
                                    </a>
                                    <?= $collection->getDownloadCount(); ?>
                                </div>
                            </div>

                            <?php if (!\Yii::$app->user->isGuest) { ?>
                                <?php $class = $collection->getFollowStatus() ? 'active' : 'not_active'; ?>
                                <button class="hidden-lg hidden-md hidden-sm follow-btn collec-prod js-follow <?= $class; ?>"
                                        data-type="<?= Follower::TYPE_COLLECTION; ?>"
                                        data-object_id="<?= $collection->id; ?>">
                                    <span class="follow">Following</span>
                                </button>
                            <?php } else { ?>
                                <button class="hidden-lg hidden-md hidden-sm follow-btn not_active js-login-modal collec-prod">
                                    <span class="follow">Follow</span>
                                </button>
                            <?php } ?>

                            <a href="<?= $collection->getLink(); ?>" class="all-colections">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">
                    <div class="row">
                        <?php foreach ($collection->getColectionProductWithLimit(4) as $product) { ?>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 product-one-all-no-home product-one-all-adverts">
                                <div class="tovar_profile_one111 tovar_profile_one65 js-product-block product-one-to-all">
                                    <div class="im_ju_3">
                                        <a href="<?= $product->getLink(); ?>">
                                            <div class="img_ki_44"
                                                 style="background: url(<?= $product->getImageSrc(); ?>) center no-repeat"></div>
                                        </a>
                                        <p class="data_tovar_catal111">
                                            <?= $product->getCreatedDate(); ?>
                                            <!--<a><img src="/images/wwe.png"></a>-->
                                        </p>
                                        <a href="<?= $product->getLink(); ?>"
                                           class="name_naw_catal111"><?= $product->name; ?></a>
                                    </div>
                                    <?php if ($type == 'home') { ?>
                                        <?php if ($product->brandModel) { ?>
                                            <div class="paper-title-container">
                                                <span class="paper-title"><?= $product->brandModel->getCategoryName(); ?></span>
                                                <span class="paper-text"><a
                                                            href="<?= $product->brandModel->getLink(); ?>"><?= $product->brandModel->name; ?></a></span>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="div_catal_bott111 div_bot_56">
                                        <?php $class = ''; ?>
                                        <?php if (!\Yii::$app->user->isGuest) { ?>
                                            <?php if ($product->getLikeStatus()) { ?>
                                                <?php $class = 'js-like active'; ?>
                                            <?php } else { ?>
                                                <?php $class = 'js-like not_active'; ?>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?php $class = 'js-login-modal'; ?>
                                        <?php } ?>
                                        <a class="icon_like <?= $class; ?>" data-title="Like"
                                           data-type="<?= Likes::TYPE_PRODUCT; ?>"
                                           data-object_id="<?= $product->id; ?>">
                                            <img style="width: 11px;" class="img_ico" src="/images/hand_grey.png">
                                            <span class="pad_span2_right_222 pad_span23_right_22225 js-like-count">
                    <?= $product->getLikeCount(); ?>
                </span>
                                        </a>
                                        <a>
                                            <img class="img_ico" src="/images/111.png">
                                            <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getViewCount(); ?>
                </span>
                                        </a>
                                        <a>
                                            <img class="img_ico" src="/images/222.png">
                                            <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCommentCount(); ?>
                </span>
                                        </a>
                                        <a href="<?= $product->getLinkToProductColletion(); ?>">
                                            <img class="img_ico" src="/images/333.png">
                                            <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCollectionCount(); ?>
                </span>
                                        </a>
                                        <a class="js-my-collection-<?= $product->id; ?> <?= \Yii::$app->user->isGuest ? 'js-login-modal' : 'add-to-collection'; ?> "
                                           data-product_id="<?= $product->id; ?>">
                                            <img class="img_ico img_ico-last" src="/images/+.png">
                                            <span class="pad_span2_right_222 pad_span23_right_22225 js-my-collection-count">
                    <?= \Yii::$app->user->isGuest ? 0 : $product->getMyCollectionCount(); ?>
                </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="hidden-sm hidden-md hidden-lg colection-sidebar coll-dis">
                        <div class="colection-icons">
                            <!--        <div class="collec_icon2">-->
                            <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
                            <!--            <span class="pad_span_collec2">-->
                            <?php //= $collection->getLikesCount(); ?>
                            <!--</span>-->
                            <!--        </div>-->
                            <div class="colection-icon">
                                <a>
                                    <img class="" src="/images/col1.png">
                                </a>
                                <?= $collection->getViewsCount(); ?>
                            </div>
                            <div class="colection-icon">
                                <a href="<?= $collection->getLink(); ?>">
                                    <img class="" src="/images/08_collections.png">
                                </a>
                                <?= $collection->getDownloadCount(); ?>
                            </div>
                        </div>

                        <?php if (!\Yii::$app->user->isGuest) { ?>
                            <?php $class = $collection->getFollowStatus() ? 'active' : 'not_active'; ?>
                            <button class="hidden-lg hidden-md hidden-sm follow-btn collec-prod js-follow <?= $class; ?>"
                                    data-type="<?= Follower::TYPE_COLLECTION; ?>"
                                    data-object_id="<?= $collection->id; ?>">
                                <span class="follow">Following</span>
                            </button>
                        <?php } else { ?>
                            <button class="hidden-lg hidden-md hidden-sm follow-btn not_active js-login-modal collec-prod">
                                <span class="follow">Follow</span>
                            </button>
                        <?php } ?>

                        <a href="<?= $collection->getLink(); ?>" class="all-colections">
                            View All
                        </a>
                    </div>
                </div>
            </div>
            <hr class="hidden-xs hr-product-col">
        <?php } ?>
        <?php if (!$dataProvider->getModels()) { ?>
            <p class="text-center text_empty manager-empty-ads">No collections at this time</p>
            <div style="min-height: 350px"></div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-sm-offset-4 bottom-pagination">
                <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
            </div>
        </div>
    </div>
</div>
