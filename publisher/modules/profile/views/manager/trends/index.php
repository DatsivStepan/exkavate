<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\TrendsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Trends');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <?= $this->render('/manager/_header'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-md-4 col-sm-12 col-xs-12 padding_0 media_wight_1200">
                <?= $this->render('/manager/_profile_menu'); ?>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column right_colum_collection right_colum22_collection22">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Html::a(Yii::t('app', 'Create Trends'), [''], ['class' => 'btn btn-success js-create-collection bat_coll_creat']) ?>
                    <div class="col-lg-12 padding_0">
                        <?php foreach ($dataProvider->getModels() as $trend) { ?>
                            <div class="row ro_marg_0">
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 padding_0">
                                    <div class="tovar_profile_one collect_one">
                                        <div>
                                            <p class="collec_name">
                                                <a class="collec_name" href="<?= $trend->getLink(); ?>">
                                                    <?= $trend->name; ?>
                                                </a>
                                            </p>
                                            <p class="text_collec"><?= $trend->description; ?></p>
                                        </div>
                                        <div class="collec_bot" style="padding-left: 0px;">
                                            <div class="collec_icon2">
                                                <a href="/"><img class="" src="/images/col1.png"></a>
                                                <span class="pad_span_collec2"><?= $trend->getViewsCount(); ?></span>
                                            </div>
                                            <div class="collec_icon1" style="padding-left: 10px;">
                                                <a href="/"><img class="" src="/images/col2.png"></a>
                                                <span class="pad_span_collec1"><?= $trend->getLikesCount(); ?></span>
                                            </div>
                                            <div class="collec_icon3" style="padding-left: 10px;">
                                                <a href="/"><img class="" src="/images/col3.png"></a>
                                                <span class="pad_span_collec3 col_ic3"><?= $trend->getDownloadCount(); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <?php foreach ($trend->getTrendProductWithLimit(3) as $product) { ?>
                                    <div class="col-lg-3 col-md-4 col-sm-4 hidden-xs padding_0">
                                        <div class="tovar_profile_one_222">
                                            <a href="<?= $product->getLink(); ?>">
                                                <div>
                                                    <div class="im_ju_3">
                                                        <div class="img_cloll_33 marbot_3"
                                                             style="background: url(/<?= $product->img_src ? $product->img_src : 'images/default_image.jpg' ?>) center no-repeat"></div>
                                                    </div>
                                                    <p class="data_tovar_profil_222"><?= $product->created_at; ?></p>
                                                    <a class="name_tovar_prof_222" href="<?= $product->getLink(); ?>">
                                                        <?= $product->name; ?>
                                                    </a>
                                                </div>
                                                <div class="div_prof_bott_222">
                                                    <a href="/"><img class="" src="/images/okey.png"><span
                                                                class="pad_span22_right_222 col_33"><?= $product->getViewCount(); ?></span></a>
                                                    <a href="/"><img class="" src="/images/111.png"><span
                                                                class="pad_span22_right_222 col_33"><?= $product->getLikeCount(); ?></span></a>
                                                    <a href="/"><img class="" src="/images/222.png"><span
                                                                class="pad_span22_right_222 col_33"><?= $product->getCommentCount(); ?></span></a>
                                                    <a href="/"><img class="" src="/images/333.png"><span
                                                                class="pad_span22_right_222 col_33"><?= $product->getCollectionCount(); ?></span></a>

                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <a class="a_colecction a_col3" href="<?= $product->getLink(); ?>"><img src="/images/ar_lef1.png">View all</a>
                            </div>
                        <?php } ?>
                        <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
