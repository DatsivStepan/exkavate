<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
use common\models\Likes;
/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Recognitions');
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="row backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <?= $this->render('/manager/_header'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-md-4 col-sm-12 col-xs-12 padding_0 media_wight_1200">
                <?= $this->render('/manager/_profile_menu'); ?>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column right_colum_collection right_colum22_collection22">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <div class="col-sm-12 padding_0">
                            <?php foreach ($dataProvider->getModels() as $product) { ?>
                                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 padding_0">
                                        <div class="tovar_profile_one111">
                                            <div>
                                                <a href="<?= $product->getLink(); ?>">
                                                    <div class="im_ju_3">
                                                        <div class="img_cloll_33 marbot_3"
                                                             style="background: url(<?= $product->getImageSrc(); ?>) center no-repeat"></div>
                                                    </div>
                                                </a>
                                                <p class="data_tovar_catal111">
                                                    <?= $product->getCreatedDate(); ?>
                                                    <!--<a><img src="/images/wwe.png"></a>-->
                                                </p>
                                                <a href="<?= $product->getLink(); ?>" class="name_naw_catal111"><?= $product->name; ?></a>
                                                <?php if ($product->getRecognitionLevel()) { ?>
                                                <a style="text-decoration:none" class="dis_init222 dis_98"><img src="/images/<?= $product->getRecognitionLevel(); ?>.png">
                                                    <div class="blok_hidd blok_hidd3_rec">
                                                        <div class="div_clas_tr22"></div>
                                                        This ad has received over <?= array_search($product->getRecognitionLevel(), Likes::$recognitionData); ?> Oks’
                                                    </div>
                                                </a>
                                            <?php } ?>
                                            </div>
                                            <div class="div_catal_bott111">
                                                <a>
                                                    <img class="" src="/images/okey.png">
                                                    <span class="pad_span2_right_222 pad_span224 col_33">
                                                        <?= $product->getLikeCount(); ?>
                                                    </span>
                                                </a>
                                                <a>
                                                    <img class="" src="/images/111.png">
                                                    <span class="pad_span2_right_222 pad_span224 col_33">
                                                        <?= $product->getViewCount(); ?>
                                                    </span>
                                                </a>
                                                <a>
                                                    <img class="" src="/images/222.png">
                                                    <span class="pad_span2_right_222 pad_span224 col_33">
                                                        <?= $product->getCommentCount(); ?>
                                                    </span>
                                                </a>
                                                <a>
                                                    <img class="" src="/images/333.png">
                                                    <span class="pad_span2_right_222 pad_span224 col_33">
                                                        <?= $product->getCollectionCount(); ?>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                            <?php } ?>
                    </div>
                </div>
                <div class="bottom_pagination" style="clear: both;display: block">
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
