<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Embargo');
$this->params['breadcrumbs'][] = $this->title;
?>
  <div class="wrap profile-pages publisher-page">
    <div class="container ">
      <?= $this->render('/manager/_profile_menu'); ?>
        <div class="row manager-ads-height">
          <div class="profile-products cleafix prof-manag-pad">
            <?php foreach ($dataProvider->getModels() as $product) { ?>
            <div class="col-20-proz col-md-3 col-sm-4 col-xs-6 product-container-padding whidth_media_1400 product-one-all-no-home product-fiv-pading">
              <div class="live_data_3">
                <span class="live-span">
                  <img src="/images/red_icon.png">
                  <span class="liv23">Live</span>
                </span>
                <span class="ebbar-span">
                  <?= $product->embargo ? date('d M, Y', strtotime($product->embargo)) : ''?>
                </span>
              </div>
          
              <?= $this->renderFile(\Yii::getAlias('@frontend/views/templates/_product.php'), ['product' => $product])?>
            </div>
            <?php } ?>
          </div>
          <?php if(!$dataProvider->getModels()) {?>
          <p class="text-center manager-empty-ads">No Embargoed Ads at this time</p>
          <?php }?>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-sm-offset-4 bottom-pagination">
            <?php LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
          </div>
        </div>
    </div>
  </div>