<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\AdminMenuUrlManager;
use common\models\Company;

$modelMenuUrl = new AdminMenuUrlManager();

/* @var $this yii\web\View */
/* @var $model publisher\modules\profile\models\AdminSearch */
/* @var $form yii\widgets\ActiveForm */
$modelCompany = Company::findOne(\Yii::$app->user->identity->company_id);
?>

<div class="row">
    <div class="col-xs-12 member-info">
      <h1 class="member-info-name text-center">
        <?= (\Yii::$app->user->identity->first_name) ? \Yii::$app->user->identity->first_name. ' ' . \Yii::$app->user->identity->last_name : \Yii::$app->user->identity->username; ?>
      </h1>
      <span class="member-info-date text-center">Member since:
        <?= date('d M, Y', (\Yii::$app->user->identity->created_at)); ?>
      </span>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1 margin-bot">
      <div profile-menu="" class="row category-menu">
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager']); ?>">
          <a href="/publisher/profile/manager">
            <div class="count-items">
              <?= $modelCompany ? $modelCompany->getAdsCount() : '0';?>
            </div>
              <p class="p-title-profil">List of ads</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/views']); ?>">
          <a href="/publisher/profile/manager/views">
            <div class="count-items">
              <?= $modelCompany ? $modelCompany->getViewsCount() : '0';?>
            </div>
              <p class="p-title-profil">Visits</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/followers']); ?>">
          <a href="/publisher/profile/manager/followers" class="fonts_title_profile">
            <div class="count-items">
              <?= $modelCompany ? $modelCompany->getFollowCount() : '0';?>
            </div>
              <p class="p-title-profil">Followers</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/adverts/embargo']); ?>">
          <a href="/publisher/profile/manager/adverts/embargo" class="fonts_title_profile">
            <div class="count-items">
              <?= $modelCompany ? $modelCompany->getEmbargoCount() : '0';?>
            </div>
              <p class="p-title-profil">Embargoed Ads</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/adverts/premium']); ?>">
          <a href="/publisher/profile/manager/adverts/premium" class="fonts_title_profile">
            <div class="count-items">
              <?= $modelCompany ? $modelCompany->getPremiumCount() : '0';?>
            </div>
              <p class="p-title-profil">Premium Ads</p>
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/collections/index']); ?>">
          <a href="/publisher/profile/manager/collections/index" class="fonts_title_profile">
            <div class="count-items">
              <?= $modelCompany ? $modelCompany->getCollectionsCount() : '0';?>
            </div>
              <p class="p-title-profil">Collections</p>
          </a>
        </div>
        <!-- <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/adverts/recognitions']); ?>">
          <a href="/publisher/profile/manager/adverts/recognitions" class="fonts_title_profile">
            <div class="count-items">
              0
            </div>
            Recognitions
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/likes']); ?>">
          <a href="/publisher/profile/manager/likes" class="fonts_title_profile">
            <div class="count-items">
              0
            </div>
            Oks
          </a>
        </div>
        <div class="col-xs- 12 col-sm-4 col-md-2 category-menu-item <?= $modelMenuUrl->checkUrl(['profile/manager/trends/index']); ?>">
          <a href="/publisher/profile/manager/trends/index" class="fonts_title_profile">
            <div class="count-items">
              0
            </div>
            Trending
          </a>
        </div> -->
      </div>
    </div>
  </div>