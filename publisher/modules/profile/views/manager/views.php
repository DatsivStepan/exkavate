<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Follower');
$this->params['breadcrumbs'][] = $this->title;
?>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

  <div class="wrap profile-pages">
    <div class="container ">
      <?= $this->render('_profile_menu'); ?>
        <div class="row chart-conteiner" style="margin-top: 50px;">
          <div class="chart-conteiner-head">
            <div class="col-xs-12 col-sm-6 chart-conteiner-title">Total Visits</div>
            <div class="col-xs-12 col-sm-6 chart-conteiner-controls">
              <?= DatePicker::widget([
                'name' => 'from_date',
                'type' => DatePicker::TYPE_RANGE,
                'name2' => 'to_date',
                'value' => date("Y-m-d", strtotime("-1 month")),
                'value2' => date('Y-m-d'),
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy/mm/dd',
                    'endDate' => '+1d',
                ],
                'pluginEvents' => [
                  'changeDate' => 'function(e) { drawBackgroundColor(); }',
                ]
              ]);
              ?>
            </div>
          </div>
          <div class="col-xs-12">
            <div id="total-visits-chart" style="width: 100%; height: 400px;"></div>
          </div>
        </div>

        <div class="row chart-conteiner">
          <div class="chart-conteiner-head">
            <div class="col-xs-12 col-sm-6 chart-conteiner-title">Visits by Category</div>
            <div class="col-xs-12 col-sm-6 chart-conteiner-controls">
              <?= DatePicker::widget([
                'name' => 'from_date',
                'type' => DatePicker::TYPE_RANGE,
                'name2' => 'to_date',
                'value' => date("Y-m-d", strtotime("-1 month")),
                'value2' => date('Y-m-d'),
                'pluginOptions' => [
                    'autoclose' => true,
                    'todayHighlight' => true,
                    'format' => 'yyyy/mm/dd',
                    'endDate' => '+1d',
                ],
                'pluginEvents' => [
                  'changeDate' => 'function(e) { drawBackgroundColor(); }',
                ]
              ]);
              ?>
            </div>
          </div>
          <div class="col-xs-12">
            <div id="total-visits-chart2" style="width: 100%; height: 400px;"></div>
          </div>
        </div>
    </div>
  </div>

  <script type="text/javascript">
    google.charts.load('current', {
      packages: ['corechart', 'line']
    });
    google.charts.setOnLoadCallback(drawBackgroundColor);

    function drawBackgroundColor() {
      var from = $('input[name=from_date]').val();
      var to = $('input[name=to_date]').val();

      var date1 = new Date(from);
      var date2 = new Date(to);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      if (diffDays <= 31) {
        $.ajax({
          url: '/publisher/profile/manager/get-views-data',
          type: 'POST',
          data: {
            from: from,
            to: to
          },
          dataType: 'json',
          success: function (response) {
            var array = [];
            jQuery.map(response.data, function (i, a) {
              array.push([new Date(a), i]);
            })
            if (response.status) {
              var data = new google.visualization.DataTable();
              data.addColumn('date', 'Date');
              data.addColumn('number', 'Like');

              data.addRows(array);
              var options = {
                backgroundColor: 'transparent',
                legend: {
                  position: 'none'
                },
                pointsVisible: {
                  visible: true
                },
                animation: {
                  easing: 'linear'
                },
                pointShape: 'circle',
                pointSize: 6,
                areaOpacity: 0.6,
                dataOpacity: 0.5,
                fontSize: 12,
                colors: ['#ffaf35'],
                fontName: 'MarkProBold',
              };

              var chart = new google.visualization.AreaChart(document.getElementById('total-visits-chart'));
              chart.draw(data, options);
            }
          }
        })
      } else {
        swal("wrong data!", "The period of selection should be no more than a month!", "warning")
      }
    }

    //google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var from = $('input[name=from_date]').val();
      var to = $('input[name=to_date]').val();

      var date1 = new Date(from);
      var date2 = new Date(to);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      if (diffDays <= 31) {
        $.ajax({
          url: '/publisher/profile/manager/get-views-data',
          type: 'POST',
          data: {
            from: from,
            to: to
          },
          dataType: 'json',
          success: function (response) {
            var array = [];
            jQuery.map(response.data, function (i, a) {
              array.push([new Date(a), i]);
            })
            if (response.status) {
              var data = new google.visualization.DataTable();
              data.addColumn('date', 'Date');
              data.addColumn('number', 'Like');

              data.addRows(array);
              var options = {
                backgroundColor: 'transparent',
                legend: {
                  position: 'none'
                },
                pointsVisible: {
                  visible: true
                },
                animation: {
                  easing: 'linear'
                },
                pointShape: 'circle',
                pointSize: 6,
                areaOpacity: 0.6,
                dataOpacity: 0.5,
                fontSize: 12,
                colors: ['#5dc6ef'],
                fontName: 'MarkProBold',
              };

              var chart = new google.visualization.AreaChart(document.getElementById('total-visits-chart2'));
              chart.draw(data, options);
            }
          }
        })
      } else {
        swal("wrong data!", "The period of selection should be no more than a month!", "warning")
      }
    }
  </script>