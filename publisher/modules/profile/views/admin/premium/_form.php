<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;
use kartik\checkbox\CheckboxX;
use common\models\Products;
use common\components\BootstrapModal;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */

BootstrapModal::widget([
    ['id' => 'create-collection-modal', 'header' => 'Create collection', 'footer' => true],
    ['id' => 'create-trend-modal', 'header' => 'Create trend', 'footer' => true],
    ['id' => 'create-brand-modal', 'header' => 'Create brand', 'footer' => true],
]);
?>
<?php if (Yii::$app->session->hasFlash('adverts_save')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => 'Advert saved!',
    ]) ?>
<?php } ?>

<?php if (Yii::$app->session->hasFlash('adverts_not_save')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-danger'],
        'body' => 'Advert not saved!',
    ]) ?>
<?php } ?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div id="buttom_css" class="col-sm-4 col-xs-6 padding_0">
            <?= $form->field($model, 'file_src')->hiddeninput()->label(false); ?>
            <a class="btn btn-primary addProductsFileSrc bat_coll_creat"><?= $model->isNewRecord ? Yii::t('app', 'Upload new advert') : Yii::t('app', 'Change file'); ?></a>
            <div class="previewProductsFileSrc"
                 style="display:inline-block;"><?= ($model->file_src) ? '<a href="/' . $model->file_src . '">' . $model->name . '</a>' : '' ?></div>
                 
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="display:none;margin-top:20px;">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress=""></div>
            </div>
        </div>
        <div id="buttom_css_upload" class="col-sm-5 col-xs-6 ">
            <span class="span_text_kover_fot hidden-xs">Cover Photo</span>
            <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
            <a class="btn btn-success addProductsImage"><?= $model->isNewRecord ? Yii::t('app', 'Add image') : Yii::t('app', 'Change image'); ?></a>
            <div class="previewProductsImage" style="display:inline-block;">
                <?= ($model->img_src) ? '<a href="/' . $model->img_src . '">Image link</a>' : '' ?>
            </div>
        </div>
    </div>

    <div class="input_name_cl"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name'])->label(false) ?></div>
    <div class="input_name_cl"><?= $form->field($model, 'content')->textarea(['rows' => 2, 'placeholder' => 'Description'])->label(false) ?></div>

    <div class="clearfix"></div>
    <div class="input_name_cl"><?= $form->field($model, 'tags')->widget(Select2::classname(), [
            'data' => $tagsArray,
            'value' => ['dfd', 'dfdf'],
            'options' => ['placeholder' => 'Tag ...', 'multiple' => true],
            'maintainOrder' => true,
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 16
            ],
        ])->label(false); ?>
    </div>
    <div class="input_name_cl"><?= $form->field($model, 'category_id')->widget(Select2::classname(), [
            'data' => $categoriesArray,
            'language' => 'en',
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Select Category...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false); ?>
    </div>
    <div class="form-group">
        <div class="col-sm-11 col-xs-10 padding_0 pad_heigh222">
            <div class="input_name_cl">
                <?= $form->field($model, 'brand_id')->widget(Select2::classname(), [
                    'data' => $brandArray,
                    'language' => 'en',
                    'size' => Select2::MEDIUM,
                    'options' => ['placeholder' => 'Select Advertiser...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label(false); ?>
            </div>
        </div>
        <div class="col-sm-1 col-xs-2 padding_0 pad_heigh222">
            <div class="plus_hei22">
                <?= Html::a('<span class="glyphicon glyphicon-plus plus_hei_an_whi"></span>', null, ['class' => 'btn btn-success js-create-brand']); ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="form-group">
        <div class="col-sm-11 col-xs-10 padding_0 pad_heigh222">
            <div class="input_name_cl22">
                <?= $form->field($model, 'collections')->widget(Select2::classname(), [
                    'data' => $collectionsArray,
                    'value' => $model->collections,
                    'language' => 'en',
                    'size' => Select2::MEDIUM,
                    'options' => ['placeholder' => 'Add to Collections...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true
                    ],
                ])->label(false); ?>
            </div>
        </div>
        <div class="col-sm-1 col-xs-2 padding_0 pad_heigh222">
            <div class="plus_hei22">
                <?= Html::a('<span class="glyphicon glyphicon-plus plus_hei_an_whi"></span>', null, ['class' => 'btn btn-success js-create-collection']); ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="form-group">
        <div class="col-sm-11 col-xs-10 padding_0 pad_heigh222">
            <div class="input_name_cl22">
                <?= $form->field($model, 'trends')->widget(Select2::classname(), [
                    'data' => $trendsArray,
                    'language' => 'en',
                    'size' => Select2::MEDIUM,
                    'options' => ['placeholder' => 'Add to Trending Topic...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true
                    ],
                ])->label(false); ?>
            </div>
        </div>
        <div class="col-sm-1 col-xs-2 padding_0">
            <div class="plus_hei22">
                <?= Html::a('<span class="glyphicon glyphicon-plus plus_hei_an_whi"></span>', null, ['class' => 'js-create-trand btn btn-success']); ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="input_name_cl"><?= $form->field($modelVideo, 'link')->textInput(['maxlength' => true, 'placeholder' => 'Link for YouTube video'])->label(false) ?></div>
    <div class="input_name_cl"><?= $form->field($modelVideo, 'description')->textarea(['rows' => 2, 'maxlength' => true, 'placeholder' => 'Video description'])->label(false) ?></div>

    <div class="input_data_embargo"><?= $form->field($model, 'embargo_advert')->widget(DatePicker::className(), [
            'options' => ['value' => date('Y-m-d')],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ]
        ]) ?>
    </div>
    <div class="form-group">
        <div class="col-sm-12 col-xs-12 padding_0 pad_heigh222">
            <div class="input_name_cl22">
                <?= $form->field($model, 'premium')->hiddenInput(['value' => 1])->label(false) ?>
                <?= $form->field($model, 'premium_status')->hiddenInput(['value' => Products::PREMIUM_STATUS_ON_CONFIRMATION])->label(false) ?>
                <?= $form->field($model, 'premium_type')->dropdownList(Products::$types)->label(false) ?>
                <i></i>
            </div>
        </div>
    </div>
    
    <div class="clearfix"></div>
    <br>
    <div class="form-group">
        <div class="float_right mar_bat-crie">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'NEXT') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success bat_coll_creat' : 'btn btn-primary bat_coll_creat']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">
            </div>
        </div>
    </div>
</div>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews2">
            <div id="template2" class="file-row">
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
        
    if ($('.addProductsImage').length) {
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "/publisher/profile/admin/save-products-img",
            acceptedFiles: 'image/jpeg,image/png,image/gif',
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".addProductsImage",
            uploadMultiple: false,
            maxFiles: 1,
        });

        myDropzone.on("maxfilesexceeded", function (file) {
            myDropzone.removeAllFiles();
            myDropzone.addFile(file);
        });

        myDropzone.on("complete", function (response) {
            if (response.status == 'success') {
                var file_name = response.xhr.response;
                if (file_name.length > 15) {
                    file_name = '...' + file_name.substr(file_name.length - 15)
                }

                $('.previewProductsImage').html('<a target="_blank" href="/' + response.xhr.response + '">' + file_name + '</a>')
                $('#products-img_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function (response) {
            if (response.xhr != null) {
                //deleteFile(response.xhr.response);
            }
        });

    }

    if ($('.addProductsFileSrc').length) {
        var previewNode = document.querySelector("#template2");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneFIle = new Dropzone('.addProductsFileSrc', {
            url: "/publisher/profile/admin/save-products-file",
            previewTemplate: previewTemplate,
            acceptedFiles: 'image/*, application/pdf',
            previewsContainer: "#previews2",
            clickable: ".addProductsFileSrc",
            uploadMultiple: false,
            maxFiles: 1,
        });

        myDropzoneFIle.on("maxfilesexceeded", function (file) {
            myDropzoneFIle.removeAllFiles();
            myDropzoneFIle.addFile(file);
        });

        myDropzoneFIle.on("totaluploadprogress", function(progress) {
            $(".progress").show();
            document.querySelector(".progress .progress-bar").style.width = progress + "%";
        });

        myDropzoneFIle.on("complete", function (response) {
            if (response.status == 'success') {
                $(".progress").hide();
                var file_name = response.xhr.response;
                if (file_name.length > 15) {
                    file_name = '...' + file_name.substr(file_name.length - 15)
                }
                $('.previewProductsFileSrc').html('<a target="_blank" href="/' + response.xhr.response + '">' + file_name + '</a>')
                $('.field-products-file_src .help-block').hide();

                $('#products-file_src').val(response.xhr.response);
            }
        });

        myDropzoneFIle.on("removedfile", function (response) {
            if (response.xhr != null) {
                //deleteFile(response.xhr.response);
            }
        });

    }


    $(document).on('click', '.js-create-trand', function () {
        BootstrapModal(
            'create-trend-modal',
            'Create trend',
            BootstrapModalIFrame('/publisher/profile/admin/trends/create-trend', 300)
        );
    })
    $('#create-trend-modal').on('hidden.bs.modal', function (e) {
        $.ajax({
            url: '/publisher/profile/admin/trends/get-trends',
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                $('#products-trends').html('')
                $('#products-trends').append('<option value="">Add to Trending Topic...</option>')
                for (var brand_id in response) {
                    var brand_name = response[brand_id];
                    $('#products-trends').append('<option value="' + brand_id + '">' + brand_name + '</option>')
                }
                $('#products-trends').prop({selected: true}).trigger('init');
            }
        });
    });

    $(document).on('click', '#create-trend-modal .js-modal-save', function (e) {
        $('#create-trend-modal iframe').contents().find('.js-form-button-hide').trigger('click');
    });


    $(document).on('click', '.js-create-brand', function () {
        BootstrapModal(
            'create-brand-modal',
            'Create brand',
            BootstrapModalIFrame('/publisher/profile/admin/create-brand', 500)
        );
    })
    $('#create-brand-modal').on('hidden.bs.modal', function (e) {
        $.ajax({
            url: '/publisher/profile/admin/get-brands',
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                $('#products-brand_id').html('')
                $('#products-brand_id').append('<option value="">Select Advertiser...</option>')
                for (var brand_id in response) {
                    var brand_name = response[brand_id];
                    $('#products-brand_id').append('<option value="' + brand_id + '">' + brand_name + '</option>')
                }
                $('#products-brand_id').prop({selected: true}).trigger('init');
            }
        });
    });
    $(document).on('click', '#create-brand-modal .js-modal-save', function (e) {
        $('#create-brand-modal iframe').contents().find('.js-form-button-hide').trigger('click');
    });

    $(document).on('click', '.js-create-collection', function () {
        BootstrapModal(
            'create-collection-modal',
            'Create collection',
            BootstrapModalIFrame('/publisher/profile/admin/collections/create-collection', 300)
        );
    })
    $('#create-collection-modal').on('hidden.bs.modal', function (e) {
        $.ajax({
            url: '/publisher/profile/admin/collections/get-collections',
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                $('#products-collections').html('')
                $('#products-collections').append('<option value="">Add to collection...</option>')
                for (var brand_id in response) {
                    var brand_name = response[brand_id];
                    $('#products-collections').append('<option value="' + brand_id + '">' + brand_name + '</option>')
                }
                $('#products-collections').prop({selected: true}).trigger('init');
            }
        });
    });
    $(document).on('click', '#create-collection-modal .js-modal-save', function (e) {
        $('#create-collection-modal iframe').contents().find('.js-form-button-hide').trigger('click');
    });
})
</script>