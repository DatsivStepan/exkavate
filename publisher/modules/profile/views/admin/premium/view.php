<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-sm-offset-1 col-md-10 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column">
                    <div class="butto_var_del">
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary bat_coll_creat']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger bat_coll_creat234',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                    <div id="form_view_2">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'name',
                                'slug',
                                'company_id',
                                'category_id',
                                'short_content:ntext',
                                'content:ntext',
                                'img_src',
                                'file_src',
                                'likes_count',
                                'comments_count',
                                'viewers_count',
                                'created_at',
                                'updated_at',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
