<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
use common\models\Likes;
use yii\widgets\Pjax;
use common\models\Products;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\AdvertsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Premium advert');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="backgroung_page_catalog back_gray">
    <div class="container container_profile">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top no-padding">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <!--                    <div class="pad_rol_bread">-->
                    <!--                        --><? //= Breadcrumbs::widget([
                    //                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    //                        ]) ?>
                    <!--                    </div>-->
                    <div class="col-sm-12 no-padding">
                        <?= Html::a('Create premium', ['create'], ['class' => 'btn btn-primary creat-pran-botton' . ($dataProvider->count >= Products::PREMIUM_MAX_COUNT ? ' disabled ' : ''), 'style' => $dataProvider->count >= Products::PREMIUM_MAX_COUNT ? 'pointer-events: none;' : '']); ?>
                    </div>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <?php foreach ($dataProvider->getModels() as $product) { ?>
                        <div class="col-20-proz col-md-3 col-sm-4 col-xs-6 product-container-padding whidth_media_1400 product-one-all-no-home product-fiv-pading"
                             style="margin-bottom: 15px">
                            <div class="tovar_profile_one111 tovar_profile_one65 js-product-block product-one-to-all">
                                <div class="im_ju_3">
                                    <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink() ?>">
                                        <div class="img_ki_44"
                                             style="background: url(<?= $product->getImageSrc(); ?>) center no-repeat;background-size:100%;"></div>
                                    </a>
                                    <p class="data_tovar_catal111">
                                        <?= $product->getCreatedDate('d.m.Y'); ?>
                                        <!--<a><img src="/images/wwe.png"></a>-->
                                    </p>
                                    <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink() ?>"
                                       class="name_naw_catal111"><?= $product->name; ?></a>
                                </div>
                                <?php //if ($type == 'home') { ?>
                                <?php if ($product->brandModel) { ?>
                                    <div class="paper-title-container">
                                        <span class="paper-title"><?= $product->brandModel->getCategoryName(); ?></span>
                                        <span class="paper-text"><a
                                                    href="<?= !$link ? $product->brandModel->getLink() : null; ?>"><?= $product->brandModel->name; ?></a></span>
                                    </div>
                                <?php } ?>
                                <?php //} ?>
                                <div class="div_catal_bott111 div_bot_56">
                                    <?php $class = ''; ?>
                                    <?php if (!\Yii::$app->user->isGuest) { ?>
                                        <?php if ($product->getLikeStatus()) { ?>
                                            <?php $class = 'js-like active'; ?>
                                        <?php } else { ?>
                                            <?php $class = 'js-like not_active'; ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php $class = 'js-login-modal'; ?>
                                    <?php } ?>
                                    <a class="icon_like <?= $class; ?>" data-title="Like"
                                       data-type="<?= Likes::TYPE_PRODUCT; ?>" data-object_id="<?= $product->id; ?>">
                                        <img style="width: 11px;" class="img_ico" src="/images/hand_grey.png">
                                        <span class="pad_span2_right_222 pad_span23_right_22225 js-like-count">
                                            <?= $product->getLikeCount(); ?>
                                        </span>
                                    </a>
                                    <a>
                                        <img class="img_ico" src="/images/111.png">
                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                                            <?= $product->getViewCount(); ?>
                                        </span>
                                    </a>
                                    <a>
                                        <img class="img_ico" src="/images/222.png">
                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                                            <?= $product->getCommentCount(); ?>
                                        </span>
                                    </a>
                                    <a href="<?= $link ? null : $product->getLinkToProductColletion(); ?>">
                                        <img class="img_ico" src="/images/333.png">
                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                                            <?= $product->getCollectionCount(); ?>
                                        </span>
                                    </a>
                                    <a class="js-my-collection-<?= $product->id; ?> <?= \Yii::$app->user->isGuest ? 'js-login-modal' : 'add-to-collection'; ?> "
                                       data-product_id="<?= $product->id; ?>">
                                        <img class="img_ico img_ico-last" src="/images/+.png">
                                        <span class="pad_span2_right_222 pad_span23_right_22225 js-my-collection-count">
                                            <?= \Yii::$app->user->isGuest ? 0 : $product->getMyCollectionCount(); ?>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="bottom_pagination" style="clear: both;display: block">
                        <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
