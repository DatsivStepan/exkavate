<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;
use kartik\checkbox\CheckboxX;
use common\models\Products;
use frontend\assets\AddAdsAsset;
use common\components\BootstrapModal;
AddAdsAsset::register($this);

Yii::$app->view->registerJsFile('/js/plugins/sweetalert.min.js');
Yii::$app->view->registerJsFile('/js/plugins/shablon/web-animations.js');
Yii::$app->view->registerJsFile('/js/plugins/shablon/hammer-2.0.8.min.js');
Yii::$app->view->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/velocity/1.4.1/velocity.min.js');
Yii::$app->view->registerJsFile('/js/plugins/shablon/grid.js');

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */

BootstrapModal::widget([
    ['id' => 'create-collection-modal', 'header' => 'Create collection', 'footer' => true],
    ['id' => 'create-trend-modal', 'header' => 'Create trend', 'footer' => true],
    ['id' => 'create-brand-modal', 'header' => 'Create brand', 'footer' => true],
    ['id' => 'shablon-modal', 'header' => 'Create brand', 'footer' => true],
]);
?>
<?php if (Yii::$app->session->hasFlash('adverts_save')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => 'Advert saved!',
    ]) ?>
<?php } ?>

<?php if (Yii::$app->session->hasFlash('adverts_not_save')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-danger'],
        'body' => 'Advert not saved!',
    ]) ?>
<?php } ?>

<style>
    .premium .form-group{
        text-align: left;
    }
    .card {
        border-radius: 0px;
    }
    .grid {
        position: relative;
        margin: 20px 0;
        border: 1px solid transparent;
        -moz-box-sizing: content-box;
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
      }
      .item {
        position: absolute;
        line-height: 100px;
        margin: 0px;
        z-index: 1;
        border-radius:0px;
      }
      .item.muuri-dragging,
      .item.muuri-releasing {
        z-index: 9999;
      }
      .item.muuri-dragging {
        cursor: move;
      }
      .item.muuri-hidden {
        z-index: 0;
      }
      .item-content {
        position: relative;
        width: 100%;
        height: 100%;
        padding:10px;
      }
      .item[data-product_id='<?= $model->id; ?>']{
          background-color: #21a2dc;
      }
        .item-normal{
            height:150px;
            width:124px;
            margin:0px;
        }
        .item-vertical{
            height:300px;
            width:248px;
            margin:0px;
        }
        .item-horizontal{
            height:300px;
            width:496px;
            margin:0px;
        }
</style>
<div class="products-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="premium text-left">
                <div class="row" style="border: 1px solid transparent;padding:0px;padding-top:15px;margin:0px;">
                    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'premium')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'premium_type')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'template')->hiddenInput(['value' => json_encode($modelCompany->getTemplate($model->premium_type, $model->id))])->label(false); ?>

                    <section id="demo" style="width:500px;margin:0 auto;">
                            <div class="grid"></div>
                    </section>
                </div>
        </div>
        <div class="clearfix"></div>
        <br>

        <div class="form-group">
            <div class="float_right mar_bat-crie">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary bat_coll_creat']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>

</div>
