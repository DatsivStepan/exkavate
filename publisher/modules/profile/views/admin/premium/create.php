<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = Yii::t('app', 'Create Products');
$this->title = 'Add Premium';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Premium advert'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-sm-offset-1 col-md-10 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div id="form_2-st" class="right_column">
                    <?php if ($step == 1) { ?>
                        <?= $this->render('_form', [
                            'model' => $model,
                            'modelVideo' => $modelVideo,
                            'categoriesArray' => $categoriesArray,
                            'collectionsArray' => $collectionsArray,
                            'trendsArray' => $trendsArray,
                            'brandArray' => $brandArray,
                            'tagsArray' => $tagsArray,
                        ]) ?>
                    <?php } else { ?>
                        <?= $this->render('_form_2', [
                            'model' => $model,
                            'modelCompany' => $modelCompany,
                        ]); ?>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
</div>
