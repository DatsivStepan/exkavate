<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\User */

?>
<div class="brand-create">
    <?php if (Yii::$app->session->hasFlash('collection_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => 'Collection saved!',
        ]) ?>
    <?php } ?>

    <?php if (Yii::$app->session->hasFlash('collection_not_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => 'Collection not saved!',
        ]) ?>
    <?php } ?>
    

    <div class="brand-form">

        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        
            <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success js-form-button-hide hide' : 'js-form-button-hide btn btn-primary hide']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>