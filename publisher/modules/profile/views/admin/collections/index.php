<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use common\components\BootstrapModal;
use yii\widgets\Breadcrumbs;
use common\models\Likes,
     common\models\Follower;
use frontend\assets\HomeAsset;

HomeAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\CollectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

BootstrapModal::widget([
    ['id' => 'create-collection-modal', 'header' => 'Create collection', 'footer' => true],
]);

$this->title = Yii::t('app', 'Collections');
$this->title = 'Collections';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages" style="padding-bottom: 0px;">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
    </div>
</div>
<div class="backgroung_page_catalog back_pag_cat back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <div class="float_right">
                        <?= Html::button(Yii::t('app', 'Create collection'), ['class' => 'btn btn-success js-create-collection bat_coll_creat clas_pos_re']) ?>
                    </div>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <?php foreach ($dataProvider->getModels() as $collection) { ?>
                        <div class="row ro_marg_0">
                            <div class="block-left-brand padding-collec-nul">
                                <div class="colection-sidebar">
                                    <a class="colection-name" href="<?= $collection->getLink(); ?>">
                                        <?= $collection->name; ?>
                                    </a>
                                    <div class="colection-description">
                                        <?= $collection->description; ?>
                                    </div>

                                    <div style="clear: both"></div>
                                    <div class="hidden-xs coll-dis">
                                        <div class="colection-icons">
                                            <!--        <div class="collec_icon2">-->
                                            <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
                                            <!--            <span class="pad_span_collec2">-->
                                            <?php //= $collection->getLikesCount(); ?>
                                            <!--</span>-->
                                            <!--        </div>-->
                                            <div class="colection-icon">
                                                <a>
                                                    <img class="" src="/images/col12.png">
                                                </a>
                                                <?= $collection->getViewsCount(); ?>
                                            </div>
                                            <div class="colection-icon">
                                                <a href="<?= $collection->getLink(); ?>">
                                                    <img class="" src="/images/08_collections.png">
                                                </a>
                                                <?= $collection->getDownloadCount(); ?>
                                            </div>
                                        </div>

                                        <?php if (!\Yii::$app->user->isGuest) { ?>
                                            <?php $class = $collection->getFollowStatus() ? 'active' : 'not_active'; ?>
                                            <button class="hidden-lg hidden-md hidden-sm follow-btn collec-prod js-follow <?= $class; ?>"
                                                    data-type="<?= Follower::TYPE_COLLECTION; ?>" data-object_id="<?= $collection->id; ?>">
                                                <span class="follow">Following</span>
                                            </button>
                                        <?php } else { ?>
                                            <button class="hidden-lg hidden-md hidden-sm follow-btn not_active js-login-modal collec-prod">
                                                <span class="follow">Follow</span>
                                            </button>
                                        <?php } ?>

                                        <a href="<?= $collection->getLink(); ?>" class="all-colections">
                                            View All
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="block-right-brand">
                                <div class="owl-carousel owl-theme owl-loaded js-products-carousel"
                                     data-id="<?= $collection->id ?>" id="owl-products<?= $collection->id ?>">
                                    <?php foreach ($collection->getColectionProductWithLimit(12) as $product) { ?>
                                        <div class="item product-one-all-no-home product-one-all-adverts prod_coll">
                                            <div class="tovar_profile_one111 tovar_profile_one65 js-product-block product-one-to-all">
                                                <div class="im_ju_3">
                                                    <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink()?>">
                                                        <div class="img_ki_44"
                                                             style="background: url(<?= $product->getImageSrc(); ?>) center no-repeat;background-size:100%;"></div>
                                                    </a>
                                                    <p class="data_tovar_catal111">
                                                        <?= $product->getCreatedDate('d.m.Y'); ?>
                                                        <!--<a><img src="/images/wwe.png"></a>-->
                                                    </p>
                                                    <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink()?>"
                                                       class="name_naw_catal111"><?= $product->name; ?></a>
                                                </div>
                                                <?php //if ($type == 'home') { ?>
                                                <?php if($product->brandModel) { ?>
                                                    <div class="paper-title-container">
                                                        <span class="paper-title"><?= $product->brandModel->getCategoryName(); ?></span>
                                                        <span class="paper-text"><a href="<?= !$link ? $product->brandModel->getLink() : null; ?>"><?= $product->brandModel->name; ?></a></span>
                                                    </div>
                                                <?php } ?>
                                                <?php //} ?>
                                                <div class="div_catal_bott111 div_bot_56">
                                                    <?php $class = ''; ?>
                                                    <?php if (!\Yii::$app->user->isGuest) { ?>
                                                        <?php if ($product->getLikeStatus()) { ?>
                                                            <?php $class = 'js-like active'; ?>
                                                        <?php } else { ?>
                                                            <?php $class = 'js-like not_active'; ?>
                                                        <?php } ?>
                                                    <?php } else{ ?>
                                                        <?php $class = 'js-login-modal'; ?>
                                                    <?php } ?>
                                                    <a class="icon_like <?= $class; ?>" data-title="Like" data-type="<?= Likes::TYPE_PRODUCT; ?>" data-object_id="<?= $product->id; ?>">
                                                        <img style="width: 11px;" class="img_ico" src="/images/hand_grey.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225 js-like-count">
                    <?= $product->getLikeCount(); ?>
                </span>
                                                    </a>
                                                    <a>
                                                        <img class="img_ico" src="/images/111.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getViewCount(); ?>
                </span>
                                                    </a>
                                                    <a>
                                                        <img class="img_ico" src="/images/222.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCommentCount(); ?>
                </span>
                                                    </a>
                                                    <a href="<?= $link ? null : $product->getLinkToProductColletion(); ?>">
                                                        <img class="img_ico" src="/images/333.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCollectionCount(); ?>
                </span>
                                                    </a>
                                                    <a  class="js-my-collection-<?= $product->id; ?> <?= \Yii::$app->user->isGuest ? 'js-login-modal' : 'add-to-collection'; ?> " data-product_id="<?= $product->id; ?>">
                                                        <img class="img_ico img_ico-last" src="/images/+.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225 js-my-collection-count">
                    <?= \Yii::$app->user->isGuest ? 0 : $product->getMyCollectionCount(); ?>
                </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="hidden-sm hidden-md hidden-lg colection-sidebar coll-dis">
                                <div class="colection-icons">
                                    <!--        <div class="collec_icon2">-->
                                    <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
                                    <!--            <span class="pad_span_collec2">-->
                                    <?php //= $collection->getLikesCount(); ?>
                                    <!--</span>-->
                                    <!--        </div>-->
                                    <div class="colection-icon">
                                        <a>
                                            <img class="" src="/images/col1.png">
                                        </a>
                                        <?= $collection->getViewsCount(); ?>
                                    </div>
                                    <div class="colection-icon">
                                        <a href="<?= $collection->getLink(); ?>">
                                            <img class="" src="/images/08_collections.png">
                                        </a>
                                        <?= $collection->getDownloadCount(); ?>
                                    </div>
                                </div>

                                <?php if (!\Yii::$app->user->isGuest) { ?>
                                    <?php $class = $collection->getFollowStatus() ? 'active' : 'not_active'; ?>
                                    <button class="hidden-lg hidden-md hidden-sm follow-btn collec-prod js-follow <?= $class; ?>"
                                            data-type="<?= Follower::TYPE_COLLECTION; ?>"
                                            data-object_id="<?= $collection->id; ?>">
                                        <span class="follow">Following</span>
                                    </button>
                                <?php } else { ?>
                                    <button class="hidden-lg hidden-md hidden-sm follow-btn not_active js-login-modal collec-prod">
                                        <span class="follow">Follow</span>
                                    </button>
                                <?php } ?>

                                <a href="<?= $collection->getLink(); ?>" class="all-colections">
                                    View All
                                </a>
                            </div>
                        </div>
                        <hr class="hidden-xs hr-product-col">
                    <?php } ?>
                    <?php if (!$dataProvider->getModels()) { ?>
                        <p class="text-center">Not found</p>
                    <?php } ?>
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
     $(document).on('click', '.js-create-collection', function (e) {
        e.preventDefault()
        BootstrapModal(
            'create-collection-modal',
            'Create collection',
            BootstrapModalIFrame('/publisher/profile/admin/collections/create-collection', 300)
        );
    })

//    $('#create-collection-modal').on('hidden.bs.modal', function (e) {
//        $("#js-trend-list").yiiGridView("applyFilter");
//    });

    $(document).on('click', '#create-collection-modal .js-modal-save', function (e) {
        $('#create-collection-modal iframe').contents().find('.js-form-button-hide').trigger('click');
    });

    $(document).ready(function () {
        $(this).find(".span_arrow_top").click(function () {
            $(".span_arrow_top").toggleClass("span_arrow_bottom2");
        });
    });
    $(document).ready(function () {
        $(this).find(".span_arrow_top2").click(function () {
            $(".span_arrow_top2").toggleClass("span_arrow_bottom2");
        });
    });
    $(document).ready(function () {
        $(this).find(".span_arrow_bottom").click(function () {
            $(".span_arrow_bottom").toggleClass("span_arrow_top2");
        });
        $("#buttom_search").click(function () {
            $("#textSearch").focus();
        });
    });
    $(document).ready(function () {
        $(".js-products-carousel").each(function () {
            var id = $(this).data("id");
            $('#owl-products' + id).owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: true,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 2
                    },
                    1200: {
                        items: 4
                    }
                }
            });
        });
    });
</script>