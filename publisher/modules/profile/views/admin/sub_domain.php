<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\widgets\Breadcrumbs;
use common\models\SettingCompany;
use yii\bootstrap\Alert;
use common\models\Company;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\AdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->title = 'Sub-domain';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->session->hasFlash('saved')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => 'Settings saved!',
    ]) ?>
<?php } ?>
<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="di2s_web222 roles_dis_row row_mar_00">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column">
                    <?php if ($model->file_src) { ?>
                        <a href="/<?= $model->file_src; ?>" target="_blank" download>Download frame file</a>
                        <div class="well">
                            <?= htmlspecialchars('<html>'); ?><br>
                                <div style="padding-left: 20px">
                                    <?= htmlspecialchars('<header>'); ?><br>
                                        <div style="padding-left: 20px">
                                            <?= htmlspecialchars('<title>'); ?>
                                            <?= $model->name; ?>
                                            <?= htmlspecialchars('</title>'); ?><br>
                                        </div>
                                    <?= htmlspecialchars('</header>'); ?><br>
                                    <?= htmlspecialchars('<body style="margin:0px;">'); ?><br>
                                        <div style="padding-left: 20px">
                                            <?= htmlspecialchars('<iframe src="' . Company::getFrameUrl($model->id) . '" width="100%" height="100%" style="border:0px"></iframe>'); ?><br>
                                        </div>
                                    <?= htmlspecialchars('</body>'); ?><br>
                                </div>
                            <?= htmlspecialchars('</html>'); ?><br>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-12 text_sub_dom padding_0">You have no sub-domain yet. Please create new sub-domain</div>
                        
                        <?php $form = ActiveForm::begin(['method' => 'post']); ?>

                            <div class="form-group">
                                <?= Html::submitButton(
                                    Yii::t('app', 'Create'), 
                                    [
                                        'class' => 'btn btn-primary',
                                        'name' => 'generate'
                                    ]
                                ) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>