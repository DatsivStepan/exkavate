<?php

use yii\helpers\Html;
use yii\bootstrap\Alert;
/* @var $this yii\web\View */
/* @var $model common\models\User */
?>
<div class="user-update">
    <?php if (Yii::$app->session->hasFlash('manager_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => 'Manager updated!',
        ]) ?>
    <?php } ?>

    <?php if (Yii::$app->session->hasFlash('manager_not_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => 'Manager not updated!',
        ]) ?>
    <?php } ?>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
