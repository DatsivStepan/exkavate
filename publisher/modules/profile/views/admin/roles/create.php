<?php

use yii\helpers\Html;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\User */

?>
<div class="user-create">
    <?php if (Yii::$app->session->hasFlash('manager_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => 'Manager saved!',
        ]) ?>
    <?php } ?>

    <?php if (Yii::$app->session->hasFlash('manager_not_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => 'Manager not saved!',
        ]) ?>
    <?php } ?>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
