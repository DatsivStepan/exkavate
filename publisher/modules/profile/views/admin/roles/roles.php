<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use common\components\BootstrapModal;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

BootstrapModal::widget([
    ['id' => 'create-manager-modal', 'header' => 'Create manager', 'footer' => true],
    ['id' => 'update-manager-modal', 'header' => 'Update manager', 'footer' => true],
]);
$this->title = Yii::t('app', 'Users');
$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages">
    <div class="container ">
            <?= $this->render('/admin/_profile_menu'); ?>
        <div class="di2s_web222 roles_dis_row row_mar_00">
            <div class="col-md-12 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column roles_colum">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <div class="float_right">
                        <?= Html::button(Yii::t('app', 'Create manager'), ['class' => 'btn btn-success js-create-manager bat_coll_creat clas_pos_re']) ?>
                    </div>
                    <div id="clas_summ">
                        <?= GridView::widget([
                            'id' => 'manager-table',
                            'dataProvider' => $dataProvider,
                            'responsiveWrap' => false,
                            'tableOptions' => [
                                'id' => 'report_zap_table1_id1',
                            ],
                            'export' => false,
                            'pjax' => true,
                            'rowOptions' => function ($model) {
                                return [
                                    'data-manager_id' => $model->id,
                                    'class' => 'managerId' . $model->id,
                                    'style' => 'white-space: normal !important;'];
                            },
                            'columns' => [
                                'first_name',
                                'last_name',
                                'email:email',
                                [
                                    'label' => 'Role',
                                    'attribute' => 'userRole',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        $modelRoles = $model->userRoleModel;
                                        return $modelRoles ? $modelRoles->itemName->description : null;
                                    }
                                ],
                                [
                                    'label' => 'Date',
                                    'attribute' => 'created_at',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return $model->getCreatedDate();
                                    }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update} {delete}',
                                    'buttons' => [
                                        'delete' => function ($url, $model) {
                                            return Html::a(
                                                '<span class="sp_poz-ab"></span>',
                                                '/publisher/profile/admin/roles/delete?id=' . $model->id,
                                                [
                                                    'title' => "Delete",
                                                    'aria-label' => "Delete",
                                                    'data-pjax' => 0,
                                                    'data-confirm' => "Are you sure you want to delete this item?",
                                                    'data-method' => "post",
                                                ]
                                            );
                                        },
                                        'update' => function ($url, $model) {
                                            return Html::tag('span',
                                                '<i class=""></i>',
                                                ['class' => 'js-update-manager class-g']
                                            );
                                        }
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(document).on('click', '.js-create-manager', function () {
            BootstrapModal(
                'create-manager-modal',
                'Create manager',
                BootstrapModalIFrame('/publisher/profile/admin/roles/create', 300)
            );
        })

        $(document).on('click', '#create-manager-modal .js-modal-save', function (e) {
            $('#create-manager-modal iframe').contents().find('.js-form-button-hide').trigger('click');
        });


        $(document).on('click', '.js-update-manager', function () {
            BootstrapModal(
                'update-manager-modal',
                'Update manager',
                BootstrapModalIFrame('/publisher/profile/admin/roles/update?id=' + $(this).closest('tr').data('manager_id'), 300)
            );
        })

        $(document).on('click', '#update-manager-modal .js-modal-save', function (e) {
            $('#update-manager-modal iframe').contents().find('.js-form-button-hide').trigger('click');
        });

        $('#create-manager-modal, #update-manager-modal').on('hidden.bs.modal', function (e) {
            $("#manager-table").yiiGridView("applyFilter");
        });
    })
</script>