<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\AdminMenuUrlManager;

$modelMenuUrl = new AdminMenuUrlManager();

/* @var $this yii\web\View */
/* @var $model publisher\modules\profile\models\AdminSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-xs-12 member-info">
        <h1 class="member-info-name text-center">
            <?= (\Yii::$app->user->identity->first_name) ? \Yii::$app->user->identity->first_name. ' ' . \Yii::$app->user->identity->last_name : \Yii::$app->user->identity->username; ?>
        </h1>
        <span class="member-info-date text-center">Member since:
            <?= date('d M, Y', (\Yii::$app->user->identity->created_at)); ?>
      </span>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 whith-profil-menu-admin">
        <div profile-menu="" class="row category-menu">
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/notifications']); ?>">
                <a href="/publisher/profile/admin/notifications">
                    <p class="p-title-profil">Notifications</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/setting']); ?>">
                <a href="/publisher/profile/admin/setting">
                    <p class="p-title-profil">Settings</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/roles/index']); ?>">
                <a href="/publisher/profile/admin/roles/index" class="fonts_title_profile">
                    <p class="p-title-profil">Roles</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/adverts/create']); ?>">
                <a href="/publisher/profile/admin/adverts/create" class="fonts_title_profile">
                    <p class="p-title-profil">Add Advert</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/adverts/index']); ?>">
                <a href="/publisher/profile/admin/adverts/index" class="fonts_title_profile">
                    <p class="p-title-profil">Advert History</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/collections/index']); ?>">
                <a href="/publisher/profile/admin/collections/index" class="fonts_title_profile">
                    <p class="p-title-profil">Collections</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/trends/index']); ?>">
                <a href="/publisher/profile/admin/trends/index" class="fonts_title_profile">
                    <p class="p-title-profil">Trending</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/premium/index', 'profile/admin/premium/create-second-step', 'profile/admin/premium/create']); ?>">
                <a href="/publisher/profile/admin/premium/index" class="fonts_title_profile">
                    <p class="p-title-profil">Premium advert</p>
                </a>
            </div>
            <div class="col-xs-12 col-sm-4 col-main category-menu-item <?= $modelMenuUrl->checkUrl(['profile/admin/sub_domain']); ?>">
                <a href="/publisher/profile/admin/sub_domain" class="fonts_title_profile">
                    <p class="p-title-profil">Sub-domain</p>
                </a>
            </div>
        </div>
    </div>
</div>