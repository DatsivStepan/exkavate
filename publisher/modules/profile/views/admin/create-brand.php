<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\User */

?>
<style>
    .wrap{
        overflow: hidden;
    }
</style>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>

<div class="brand-create">
    <?php if (Yii::$app->session->hasFlash('brand_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-success'],
            'body' => 'Manager saved!',
        ]) ?>
    <?php } ?>

    <?php if (Yii::$app->session->hasFlash('brand_not_save')) { ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => 'Manager not saved!',
        ]) ?>
    <?php } ?>
    

    <div class="brand-form">

        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'img_src')->hiddeninput()->label(false); ?>
            <img src="/<?= ($model->img_src) ? $model->img_src : 'images/default_avatar.jpg'; ?>" class="previewBrandImage" style="width:150px;height:150px;"><br>
            <a class="btn btn-success addBrandImage" >Change image</a>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model, 'category_id')->dropDownList(common\models\Brands::$category)->label('Category') ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success js-form-button-hide hide' : 'js-form-button-hide btn btn-primary hide']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>
<script>
    if($('.addBrandImage').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "/admin/brands/brands/savebrandsphoto",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".addBrandImage",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.previewBrandImage').attr('src', '/'+response.xhr.response)
                $('#brands-img_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }
</script>