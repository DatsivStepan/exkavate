<?php

/* @var $this yii\web\View */

$js = <<<script
    parent.$(".modal").modal("hide");
    parent.$("#list").yiiGridView("applyFilter");
script;

$this->registerJs($js);
