<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\LinkPager;
use common\components\BootstrapModal;
use yii\widgets\Breadcrumbs;
use common\models\Follower;
use common\models\Likes;
use frontend\assets\HomeAsset;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\TrendsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
HomeAsset::register($this);
BootstrapModal::widget([
    ['id' => 'create-trend-modal', 'header' => 'Create collection', 'footer' => true],
]);

$this->title = Yii::t('app', 'Trends');
$this->title = 'Trends';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages" style="padding-bottom: 0px;">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
    </div>
</div>
<div class="backgroung_page_catalog back_gray">
    <div class="container container_profile">
        <div class="di2s_web222 roles_dis_row rig_collec_hom">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container_profile_tovary collec_hom_pad-top">
                <div class="right_column right_colum_collection rig_collec_hom">
                    <div class="float_right">
                        <?= Html::button(Yii::t('app', 'Create trend'), ['class' => 'btn btn-success js-create-trend bat_coll_creat clas_pos_re']) ?>
                    </div>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?php foreach ($dataProvider->getModels() as $trend) { ?>
                        <div class="row ro_marg_0">
                            <div class="block-left-brand padding-collec-nul">
                                <div class="trend-sidebar">
                                    <a class="trend-name" href="<?= $trend->getLink(); ?>">
                                        <?= $trend->name; ?>
                                    </a>
                                    <div class="trend-description">
                                        <?= $trend->description; ?>
                                    </div>

                                    <div style="clear: both"></div>
                                    <div class="hidden-xs coll-dis">
                                        <div class="trend-icons">
                                            <!--        <div class="collec_icon2">-->
                                            <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
                                            <!--            <span class="pad_span_collec2">-->
                                            <? //= $collection->getLikesCount(); ?>
                                            <!--</span>-->
                                            <!--        </div>-->
                                            <div class="trend-icon">
                                                <a>
                                                    <img class="" src="/images/pub1.png">
                                                </a>
                                                <?= $trend->getFollowCount() ?>
                                            </div>
                                            <div class="trend-icon">
                                                <a href="<?= $trend->getLink(); ?>">
                                                    <img class="" src="/images/08_collections.png">
                                                </a>
                                                <?= $trend->getDownloadCount(); ?>
                                            </div>
                                        </div>

                                        <?php if (!\Yii::$app->user->isGuest) { ?>
                                            <?php $class = $trend->getFollowStatus() ? 'active' : 'not_active'; ?>
                                            <button class="follow-btn js-follow <?= $class; ?>" data-type="<?= Follower::TYPE_TRENDING; ?>"
                                                    data-object_id="<?= $trend->id; ?>">
                                                <span class="follow">Following</span>
                                            </button>
                                        <?php } else { ?>
                                            <button class="follow-btn not_active js-login-modal">
                                                <span class="follow">Follow</span>
                                            </button>
                                        <?php } ?>

                                        <a href="<?= $trend->getLink(); ?>" class="all-trend">
                                            View All
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="block-right-brand block-band-rig">
                                <div class="owl-carousel owl-theme owl-loaded js-products-carousel"
                                     data-id="<?= $trend->id ?>" id="owl-trend<?= $trend->id ?>">
                                    <?php foreach ($trend->getTrendProductWithLimit(12) as $product) { ?>
                                        <div class="item product-one-all-no-home product-one-all-adverts">
                                            <div class="tovar_profile_one111 tovar_profile_one65 js-product-block product-one-to-all">
                                                <div class="im_ju_3">
                                                    <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink()?>">
                                                        <div class="img_ki_44"
                                                             style="background: url(<?= $product->getImageSrc(); ?>) center no-repeat;background-size:100%;"></div>
                                                    </a>
                                                    <p class="data_tovar_catal111">
                                                        <?= $product->getCreatedDate('d.m.Y'); ?>
                                                        <!--<a><img src="/images/wwe.png"></a>-->
                                                    </p>
                                                    <a href="<?= $link ? $link . '?id=' . $product->id : $product->getLink()?>"
                                                       class="name_naw_catal111"><?= $product->name; ?></a>
                                                </div>
                                                <?php //if ($type == 'home') { ?>
                                                <?php if($product->brandModel) { ?>
                                                    <div class="paper-title-container">
                                                        <span class="paper-title"><?= $product->brandModel->getCategoryName(); ?></span>
                                                        <span class="paper-text"><a href="<?= !$link ? $product->brandModel->getLink() : null; ?>"><?= $product->brandModel->name; ?></a></span>
                                                    </div>
                                                <?php } ?>
                                                <?php //} ?>
                                                <div class="div_catal_bott111 div_bot_56">
                                                    <?php $class = ''; ?>
                                                    <?php if (!\Yii::$app->user->isGuest) { ?>
                                                        <?php if ($product->getLikeStatus()) { ?>
                                                            <?php $class = 'js-like active'; ?>
                                                        <?php } else { ?>
                                                            <?php $class = 'js-like not_active'; ?>
                                                        <?php } ?>
                                                    <?php } else{ ?>
                                                        <?php $class = 'js-login-modal'; ?>
                                                    <?php } ?>
                                                    <a class="icon_like <?= $class; ?>" data-title="Like" data-type="<?= Likes::TYPE_PRODUCT; ?>" data-object_id="<?= $product->id; ?>">
                                                        <img style="width: 11px;" class="img_ico" src="/images/hand_grey.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225 js-like-count">
                    <?= $product->getLikeCount(); ?>
                </span>
                                                    </a>
                                                    <a>
                                                        <img class="img_ico" src="/images/111.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getViewCount(); ?>
                </span>
                                                    </a>
                                                    <a>
                                                        <img class="img_ico" src="/images/222.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCommentCount(); ?>
                </span>
                                                    </a>
                                                    <a href="<?= $link ? null : $product->getLinkToProductColletion(); ?>">
                                                        <img class="img_ico" src="/images/333.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225">
                    <?= $product->getCollectionCount(); ?>
                </span>
                                                    </a>
                                                    <a  class="js-my-collection-<?= $product->id; ?> <?= \Yii::$app->user->isGuest ? 'js-login-modal' : 'add-to-collection'; ?> " data-product_id="<?= $product->id; ?>">
                                                        <img class="img_ico img_ico-last" src="/images/+.png">
                                                        <span class="pad_span2_right_222 pad_span23_right_22225 js-my-collection-count">
                    <?= \Yii::$app->user->isGuest ? 0 : $product->getMyCollectionCount(); ?>
                </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="hidden-sm hidden-md hidden-lg trend-sidebar coll-dis">
                                <div class="trend-icons">
                                    <!--        <div class="collec_icon2">-->
                                    <!--            <a class="icon_like" data-title="Like"><img class="" src="/images/col2.png"></a>-->
                                    <!--            <span class="pad_span_collec2">-->
                                    <? //= $collection->getLikesCount(); ?>
                                    <!--</span>-->
                                    <!--        </div>-->
                                    <div class="trend-icon">
                                        <a>
                                            <img class="" src="/images/pub1.png">
                                        </a>
                                        <?= $trend->getFollowCount() ?>
                                    </div>
                                    <div class="trend-icon">
                                        <a href="<?= $trend->getLink(); ?>">
                                            <img class="" src="/images/08_collections.png">
                                        </a>
                                        <?= $trend->getDownloadCount(); ?>
                                    </div>
                                </div>

                                <?php if (!\Yii::$app->user->isGuest) { ?>
                                    <?php $class = $trend->getFollowStatus() ? 'active' : 'not_active'; ?>
                                    <button class="follow-btn js-follow <?= $class; ?>"
                                            data-type="<?= Follower::TYPE_TRENDING; ?>"
                                            data-object_id="<?= $trend->id; ?>">
                                        <span class="follow">Following</span>
                                    </button>
                                <?php } else { ?>
                                    <button class="follow-btn not_active js-login-modal">
                                        <span class="follow">Follow</span>
                                    </button>
                                <?php } ?>

                                <a href="<?= $trend->getLink(); ?>" class="all-trend">
                                    View All
                                </a>
                            </div>
                            <!--                            <a class="a_colecction a_col3 a_col3_coll2" href="-->
                            <? //= $trend->getLink(); ?><!--"><img-->
                            <!--                                        src="/images/ar_lef1.png">View all</a>-->
                        </div>
                        <hr class="hidden-xs hr-product-col">
                    <?php } ?>
                    <?= LinkPager::widget(['pagination' => $dataProvider->pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.js-create-trend', function (e) {
        e.preventDefault()
        BootstrapModal(
            'create-trend-modal',
            'Create trend',
            BootstrapModalIFrame('/publisher/profile/admin/trends/create-trend', 300)
        );
    })

    $('#create-trend-modal').on('hidden.bs.modal', function (e) {
        $("#js-trend-list").yiiGridView("applyFilter");
    });

    $(document).on('click', '#create-trend-modal .js-modal-save', function (e) {
        $('#create-trend-modal iframe').contents().find('.js-form-button-hide').trigger('click');
    });

    $(document).ready(function () {
        $(".js-products-carousel").each(function () {
            var id = $(this).data("id");
            $('#owl-trend' + id).owlCarousel({
                loop: false,
                margin: 10,
                nav: true,
                dots: true,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 2
                    },
                    1200: {
                        items: 4
                    }
                }
            });
        });
    });
</script>
