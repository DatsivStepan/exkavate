<?php

/* @var $this yii\web\View */

$js = <<<script
    parent.$(".modal").modal("hide");
    parent.location.reload();
script;

$this->registerJs($js);
