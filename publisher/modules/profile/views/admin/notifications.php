<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\AdvertsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Notifications');
$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="di2s_web222 roles_dis_row row_mar_00">
            <div class="col-md-12 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column roles_colum">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 select-sort-date select-sort_da">
                            <?= HTML::dropDownList('filterC', '', [1 => 'Mark read', 2 => 'Mark unread', 3 => 'Remove'], ['id' => 'filterC', 'class' => 'form-control', 'prompt' => 'Select...']); ?>
                        </div>
                    </div>
                    <div id="clas_summ">
                        <?= GridView::widget([
                            'id' => 'notifications_design',
                            'dataProvider' => $dataProvider,
                            'pjax' => true,
                            //            'filterModel' => $searchModel,
                            'rowOptions' => function ($model) {
                                return ['style' => 'font-weight: ' . ($model->read == 1 ? 'normal;' : 'bold; ')];
                            },
                            'columns' => [
                                [
                                    'header' => CheckboxX::widget([
                                        'name' => 'notificationC',
                                        'options' => ['id' => 'notificationC'],
                                        'pluginOptions' => [
                                            'threeState' => false,
                                        ],
                                    ]),
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return CheckboxX::widget([
                                            'name' => 'notification_' . $model->id,
                                            'options' => [
                                                'id' => 'not_' . $model->id,
                                                'class' => 'js-checkbox-notes',
                                                'data-not_id' => $model->id,
                                            ],
                                            'pluginOptions' => [
                                                'threeState' => false,
                                            ],
                                        ]);
                                    }
                                ],
                                [
                                    'attribute' => 'subject',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return $model->subject;
                                    }
                                ],
                                [
                                    'label' => 'Description',
                                    'attribute' => 'message',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return $model->message;
                                    }
                                ],
                                [
                                    'label' => 'Date',
                                    'attribute' => 'created_at',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return date('d/m/Y H:i', strtotime($model->created_at));
                                    }
                                ],
                            ],
                        ]); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(document).on('change', '#notificationC', function () {
            if ($(this).val() == 1) {
                $('.js-checkbox-notes').each(function (index) {
                    $(this).trigger('click');
                });
            } else {
                $('.js-checkbox-notes').each(function (index) {
                    $(this).trigger('click');
                });
            }
        })

        $(document).on('change', '#filterC', function () {
            var checkArray = [];
            $('.js-checkbox-notes').each(function (index) {
                if ($(this).val() == 1) {
                    checkArray.push($(this).data('not_id'));
                }
            });
            var functionId = $(this).val();
            $.ajax({
                url: '/publisher/profile/admin/function-notification',
                type: 'POST',
                data: {functionId: functionId, checkArray: checkArray},
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        document.location.reload(true);
                    }
                }
            })
        })
    })
</script>