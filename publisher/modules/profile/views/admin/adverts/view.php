<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row backgroung_page_catalog back_pag_cat">
    <div class="container container_profile">
        <div class="row">
            <h1 class="h1_profile_title">CMS Features
                <hr>
            </h1>
            <div class="bott_edit_share222">
            </div>
        </div>
        <h1 class="h1_tex_updet"><?= Html::encode($this->title) ?></h1>

        <div class="butto_var_del">
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary bat_coll_creat']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger bat_coll_creat234',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div id="form_view_2">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'slug',
                    'company_id',
                    'category_id',
                    'short_content:ntext',
                    'content:ntext',
                    'img_src',
                    'file_src',
                    'likes_count',
                    'comments_count',
                    'viewers_count',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>
        </div>
    </div>
</div>
