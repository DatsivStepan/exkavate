<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\AdvertsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Advert History');
$this->title = 'Advert History';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-md-12 col-sm-12 col-xs-12 container_profile_tovary padding_0 pad-mob-0">
                <div class="right_column roles_colum">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <div id="clas_summ_adver_history">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'pjax' => true,
//            'filterModel' => $searchModel,
                            'columns' => [
                                'name',
                                [
                                    'label' => 'Cover Photo',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return $model->img_src ? Html::tag('img', '', ['src' => '/' . $model->img_src, 'style' => 'max-width:50px;']) : null;
                                    }
                                ],
                                'created_at',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{show} {update} {delete}',
                                    'buttons' => [
                                        'delete' => function ($url, $model) {
                                            return Html::a(
                                                '<span class="glyphicon glyphicon-trash"></span>',
                                                '/publisher/profile/admin/adverts/delete?id=' . $model->id,
                                                [
                                                    'title' => "Delete",
                                                    'aria-label' => "Delete",
                                                    'data-pjax' => 0,
                                                    'data-confirm' => "Are you sure you want to delete this item?",
                                                    'data-method' => "post",
                                                ]
                                            );
                                        },
                                        'update' => function ($url, $model) {
                                            return Html::a(
                                                '<i class="glyphicon glyphicon-pencil"></i>',
                                                ['update', 'id' => $model->id],
                                                [
                                                    'data-pjax' => 0,
                                                    'class' => 'js-update-manager'
                                                ]
                                            );
                                        },
                                        'show' => function ($url, $model) {
                                            return Html::a(
                                                '<i class="glyphicon glyphicon-eye-open"></i>',
                                                ['update', 'id' => $model->id],
                                                [
                                                    'data-pjax' => 0,
                                                    'class' => 'js-update-manager'
                                                ]
                                            );
                                        }
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
