<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = Yii::t('app', 'Create Products');
$this->title = 'Add Advert';
//$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->session->hasFlash('adverts_save')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => 'Advert saved!',
    ]) ?>
<?php } ?>

<?php if (Yii::$app->session->hasFlash('adverts_not_save')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-danger'],
        'body' => 'Advert not saved!',
    ]) ?>
<?php } ?>
<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>

                    <?= $this->render('_form', [
                        'model' => $model,
                        'modelVideo' => $modelVideo,
                        'categoriesArray' => $categoriesArray,
                        'collectionsArray' => $collectionsArray,
                        'trendsArray' => $trendsArray,
                        'brandArray' => $brandArray,
                        'tagsArray' => $tagsArray,
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
</div>
