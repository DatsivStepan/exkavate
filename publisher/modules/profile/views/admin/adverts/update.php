<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Products',
    ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="row di2s_web222 roles_dis_row row_mar_00">
            <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>

                    <?= $this->render('_form', [
                        'model' => $model,
                        'modelVideo' => $modelVideo,
                        'categoriesArray' => $categoriesArray,
                        'collectionsArray' => $collectionsArray,
                        'trendsArray' => $trendsArray,
                        'brandArray' => $brandArray,
                        'tagsArray' => $tagsArray,
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>
