<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\widgets\Breadcrumbs;
use common\models\SettingCompany;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $searchModel publisher\modules\profile\models\AdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->session->hasFlash('saved')) { ?>
    <?= Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => 'Settings saved!',
    ]) ?>
<?php } ?>

<div class="wrap profile-pages">
    <div class="container ">
        <?= $this->render('/admin/_profile_menu'); ?>
        <div class="di2s_web222 roles_dis_row row_mar_00">
            <div class="col-lg-4 col-lg-offset-4 col-md-12 col-sm-12 col-xs-12 container_profile_tovary padding_0">
                <div class="right_column">
                    <?php ActiveForm::begin(); ?>
                    <?php foreach ($notifications as $typeId => $typeName) { ?>
                        <div class="main-checkbox-style_pab_sett">
                            <?= CheckboxX::widget([
                                'name' => 'Notification[' . $typeId . ']',
                                'value' => SettingCompany::getStatus($typeId, $company_id) ? 1 : null,
                                'options' => ['id' => 'not_' . $typeId],
                                'pluginOptions' => [
                                    'threeState' => false,
                                ],
                            ]); ?>
                            <label class="cbx-label" for="not_<?= $typeId; ?>"><?= $typeName; ?></label>
                        </div>
                    <?php } ?>
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary pull-right bat_coll_creat save-bot-crea']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>