<?php

namespace publisher\modules\profile\controllers\manager;

use Yii;
use common\models\Collections;
use publisher\modules\profile\models\CollectionsSearch;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CollectionsController implements the CRUD actions for Collections model.
 */
class CollectionsController extends Controller
{
    public $company_id;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (\Yii::$app->user->can('company_manager') && \Yii::$app->user->identity->company_id){
            $this->company_id = \Yii::$app->user->identity->company_id;
            $this->enableCsrfValidation = false;
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
        return $this;
    }
    
    /**
     * Lists all Collections models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CollectionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Collections model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Collections the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Collections::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
