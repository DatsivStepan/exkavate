<?php

namespace publisher\modules\profile\controllers\manager;

use Yii;
use common\models\Products;
use publisher\modules\profile\models\AdvertsSearch;
use yii\web\Controller;
use common\models\Category;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Trends;
use common\models\Video;
use common\models\Brands;
use common\models\ProductsTags;
use yii\helpers\ArrayHelper;
use common\models\Collections;

/**
 * AdvertsController implements the CRUD actions for Products model.
 */
class AdvertsController extends Controller
{
    public $company_id;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (\Yii::$app->user->can('company_manager') && \Yii::$app->user->identity->company_id){
            $this->company_id = \Yii::$app->user->identity->company_id;
            $this->enableCsrfValidation = false;
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
        return $this;
    }
    
    public function actionPremium()
    {
        $searchModel = new AdvertsSearch();
        $searchModel->premium = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->company_id);

        return $this->render('premium', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionEmbargo()
    {
        $searchModel = new AdvertsSearch();
        $searchModel->embargoStatus = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->company_id);

        return $this->render('embargo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRecognitions()
    {
        $searchModel = new AdvertsSearch();
        $searchModel->recognitionsStatus = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->company_id);

        return $this->render('recognitions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
