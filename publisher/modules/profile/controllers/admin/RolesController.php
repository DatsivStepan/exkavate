<?php

namespace publisher\modules\profile\controllers\admin;

use Yii;
use common\models\User;
use publisher\modules\profile\models\ManagerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\rbac\Rbac;

/**
 * AdminController implements the CRUD actions for User model.
 */
class RolesController extends Controller
{
    public $company_id;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (\Yii::$app->user->can('company_admin') && \Yii::$app->user->identity->company_id){
            $this->company_id = \Yii::$app->user->identity->company_id;
            $this->enableCsrfValidation = false;
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
        return $this;
    }
    
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->company_id);

        return $this->render('roles', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '/iframe';
        
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
            $model->company_id = $this->company_id;
            $model->role = Rbac::COMPANY_MANAGER;
            $model->password = $model->generatePassword();
            $model->setPassword($model->password);
            $model->generateAuthKey();
            $model->status = User::STATUS_ACTIVE;
            if ($model->save()) {
                if (Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'passwordManagerSend-html'],
                        ['user' => $model]
                    )
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                    ->setTo($model->email)
                    ->setSubject('Manager register')
                    ->send()){
                        \Yii::$app->session->setFlash('email_send');
                    }
                \Yii::$app->session->setFlash('manager_save');
            } else {
                \Yii::$app->session->setFlash('manager_not_save');
            }
        } 
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = '/iframe';
        
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('manager_save');
            } else {
                \Yii::$app->session->setFlash('manager_not_save');
            }
        } 
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
//
// Roles end    
//    
    
    
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
