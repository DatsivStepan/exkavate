<?php

namespace publisher\modules\profile\controllers\admin;

use Yii;
use common\models\Products;
use publisher\modules\profile\models\AdvertsSearch;
use yii\web\Controller;
use common\models\Category;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Trends;
use common\models\Video;
use common\models\Brands;
use common\models\ProductsTags;
use yii\helpers\ArrayHelper;
use common\models\Collections;

/**
 * AdvertsController implements the CRUD actions for Products model.
 */
class AdvertsController extends Controller
{
    public $company_id;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (\Yii::$app->user->can('company_admin') && \Yii::$app->user->identity->company_id){
            $this->company_id = \Yii::$app->user->identity->company_id;
            $this->enableCsrfValidation = false;
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
        return $this;
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdvertsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->company_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $modelVideo = new Video;
        if ($model->load(Yii::$app->request->post())) {
            $model->company_id = $this->company_id;
            if ($model->save()){
                if ($modelVideo->load(Yii::$app->request->post())) {
                    $modelVideo->product_id = $model->id;
                    $modelVideo->save();
                    return $this->redirect('/publisher/profile/admin/adverts/index');
                    \Yii::$app->session->setFlash('adverts_save');
                } else {
                    \Yii::$app->session->setFlash('adverts_not_save');
                }
            }
        } 

        $collectionsArray = ArrayHelper::map(Collections::findAll(['company_id' => $this->company_id]), 'id', 'name');
        $trendsArray = ArrayHelper::map(Trends::findAll(['company_id' => $this->company_id]), 'id', 'name');
        $brandArray = ArrayHelper::map(Brands::find()->all(), 'id', 'name');
        $categoriesArray = Category::getAllInArrayMap();
        $tagsArray = ArrayHelper::map(ProductsTags::find()->all(), 'name', 'name');
        
        return $this->render('create', [
            'model' => $model,
            'tagsArray' => $tagsArray,
            'brandArray' => $brandArray,
            'modelVideo' => $modelVideo,
            'categoriesArray' => $categoriesArray,
            'collectionsArray' => $collectionsArray,
            'trendsArray' => $trendsArray,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelVideo = Video::findOne(['product_id' => $model->id]);
        if (!$modelVideo) {
            $modelVideo = new Video;
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if ($modelVideo->load(Yii::$app->request->post())) {
                    $modelVideo->product_id = $model->id;
                    $modelVideo->save();
                }
                \Yii::$app->session->setFlash('adverts_save');
            } else {
                \Yii::$app->session->setFlash('adverts_not_save');
            }
        }
        $collectionsArray = ArrayHelper::map(Collections::findAll(['company_id' => $this->company_id]), 'id', 'name');
        $trendsArray = ArrayHelper::map(Trends::findAll(['company_id' => $this->company_id]), 'id', 'name');
        $brandArray = ArrayHelper::map(Brands::find()->all(), 'id', 'name');
        $tagsArray = ArrayHelper::map(ProductsTags::find()->all(), 'name', 'name');
        
        $categoriesArray = Category::getAllInArrayMap();
        return $this->render('update', [
            'model' => $model,
            'modelVideo' => $modelVideo,
            'tagsArray' => $tagsArray,
            'brandArray' => $brandArray,
            'categoriesArray' => $categoriesArray,
            'collectionsArray' => $collectionsArray,
            'trendsArray' => $trendsArray,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
