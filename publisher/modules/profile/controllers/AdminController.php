<?php

namespace publisher\modules\profile\controllers;

use Yii;
use common\models\User;
use publisher\modules\profile\models\ManagerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\rbac\Rbac;
use common\models\SettingCompany;
use yii\web\Response;
use common\models\Notifications;
use publisher\modules\profile\models\NotificationsSearch;
use yii\helpers\ArrayHelper;
use common\models\Collections;
use common\models\Brands;
use common\models\Trends;

/**
 * AdminController implements the CRUD actions for User model.
 */
class AdminController extends Controller
{
    public $company_id;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (\Yii::$app->user->can('company_admin') && \Yii::$app->user->identity->company_id){
            $this->company_id = \Yii::$app->user->identity->company_id;
            $this->enableCsrfValidation = false;
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
        return $this;
    }
    
    public function actionIndex()
    {
        return $this->redirect('admin/notifications');
    }
    
    public function actionNotifications()
    {
        $searchModel = new NotificationsSearch();
        $searchModel->company_id = $this->company_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = false;
                
        return $this->render('notifications', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSub_domain()
    {
        $model = $this->findCompany($this->company_id);

        if (Yii::$app->request->isPost && (Yii::$app->request->post('generate') !== null)) {
            $model->generateFile();
        }

        return $this->render('sub_domain', [
            'model' => $model
        ]);
    }

    public function actionFunctionNotification()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = ['status' => false];
        if (\Yii::$app->request->isPost && \Yii::$app->request->post('functionId')) {
            $checkArray = \Yii::$app->request->post('checkArray');
            $modelNotification = Notifications::findAll($checkArray);
            switch (\Yii::$app->request->post('functionId')) {
                case 1:
                    foreach ($modelNotification  as $notification) {
                        $notification->read = 1;
                        $notification->save();
                    }
                    $res = ['status' => true];
                    break;
                case 2:
                    foreach ($modelNotification  as $notification) {
                        $notification->read = 0;
                        $notification->save();
                    }
                    $res = ['status' => true];
                    break;
                case 3:
                    Notifications::deleteAll(['id' => $checkArray]);
                    $res = ['status' => true];
                    break;
            }
        }
        
        return $res;
    }
    
    /**
     * Creates a new Collection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSetting()
    {
        $arrayNotifications = [];
        if (Notifications::$types) {
            foreach(Notifications::$types as $typeId) {
                $arrayNotifications[$typeId] = Notifications::$typeSubjects[$typeId];
            }
        }
        
        if (Yii::$app->request->isPost) {
            foreach(Notifications::$types as $typeId) {
                
                $model = SettingCompany::findOne(['company_id' => $this->company_id, 'type' => $typeId]);
                if(!$model){
                    $model = new SettingCompany();
                    $model->type = $typeId;
                    $model->subject = Notifications::$typeSubjects[$typeId];
                    $model->company_id = $this->company_id;
                }
                if (\Yii::$app->request->post('Notification')[$typeId]){
                    $model->status = 1;
                } else {
                    $model->status = 0;
                }
                $model->save();
            }
            \Yii::$app->session->setFlash('saved');
        }
        
        return $this->render('setting', [
            'notifications' => $arrayNotifications,
            'company_id' => $this->company_id
        ]);
    }
    
    /**
     * Creates a new Collection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetBrands()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $modelBrands = ArrayHelper::map(Brands::find()->all(), 'id', 'name');
        
        return $modelBrands;
    }
    
    
    /**
     * Creates a new Collection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateBrand()
    {
        $this->layout = '/iframe';

        $model = new Brands();
        if ($model->load(Yii::$app->request->post())) {
            $model->company_id = $this->company_id;
            if ($model->save()) {
                return $this->render('_close');
                \Yii::$app->session->setFlash('brand_save');
            } else {
                \Yii::$app->session->setFlash('brand_not_save');
            }
        }
        return $this->render('create-brand', [
            'model' => $model,
        ]);
    }
    
    public function actionSaveProductsImg()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/files/products/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.$this->company_id)){
              mkdir($storeFolder.$ds.$this->company_id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.$this->company_id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5
            move_uploaded_file($tempFile,$targetFile); //6
            return 'publisher/files/products/'.$this->company_id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    public function actionSaveProductsFile()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/files/products/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.$this->company_id)){
              mkdir($storeFolder.$ds.$this->company_id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.$this->company_id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.pdf';  //5
            move_uploaded_file($tempFile,$targetFile); //6
            return 'publisher/files/products/'.$this->company_id.'/'.$for_name.'_'.$randNumber.'.pdf'; //5
        }
    }
    
    
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findCompany($id)
    {
        if (($model = \common\models\Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
