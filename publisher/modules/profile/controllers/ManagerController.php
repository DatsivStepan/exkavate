<?php

namespace publisher\modules\profile\controllers;

use Yii;
use common\models\User;
use publisher\modules\profile\models\ManagerSearch;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\FunctionHelper;
use common\models\Follower;
use common\models\Likes;
use common\models\Company;
use common\models\Views;
use publisher\modules\profile\models\AdvertsSearch;

/**
 * ManagerController implements the CRUD actions for User model.
 */
class ManagerController extends Controller
{
     public $company_id;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
        if (\Yii::$app->user->can('company_manager') && \Yii::$app->user->identity->company_id){
            $this->company_id = \Yii::$app->user->identity->company_id;
            $this->enableCsrfValidation = false;
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
        return $this;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdvertsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->company_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionViews()
    {
        $models = Company::findOne($this->company_id);
        return $this->render('views', [
            'viewCount' => $models->getViewsCount()
        ]);
    }

    public function actionGetViewsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = ['status' => false, 'data' => null];
        
        
        if (\Yii::$app->request->isPost && ($from = \Yii::$app->request->post('from')) && ($to = \Yii::$app->request->post('to'))) {
            $dateArray = FunctionHelper::getDateArrayFromTo($from, $to);

            $folllowerFromTo = Views::getViewsFromTo($from, $to, $this->company_id);
            foreach($folllowerFromTo as $date){
                $date = date('Y-m-d', strtotime($date));
                $dateArray[$date] = $dateArray[$date] + 1;
            }
            $res['data'] = $dateArray;
            $res['status'] = true;
        }
        return $res;
    }
    
    public function actionGetCategoryViewsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = ['status' => false, 'data' => null];
        
        if (\Yii::$app->request->isPost && ($from = \Yii::$app->request->post('from')) && ($to = \Yii::$app->request->post('to'))) {
            $viewsFromTo = Views::getCategoryViewsFromTo($from, $to, $this->company_id);
            $res['data'] = $viewsFromTo;
            $res['status'] = true;
        }
        return $res;
    }

    /**
     * @return mixed
     */
    public function actionLikes()
    {
        $models = Company::findOne($this->company_id);
        return $this->render('likes', [
            'likeCount' => $models->getLikesCount()
        ]);
    }

    public function actionGetLikeData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = ['status' => false, 'data' => null];
        
        if (\Yii::$app->request->isPost && ($from = \Yii::$app->request->post('from')) && ($to = \Yii::$app->request->post('to'))) {
            $dateArray = FunctionHelper::getDateArrayFromTo($from, $to);

            $folllowerFromTo = Likes::getLikeFromTo($from, $to, $this->company_id);
            foreach($folllowerFromTo as $date){
                $date = date('Y-m-d', strtotime($date));
                $dateArray[$date] = $dateArray[$date] + 1;
            }
            $res['data'] = $dateArray;
            $res['status'] = true;
        }
        return $res;
    }

    /**
     * @return mixed
     */
    public function actionFollowers()
    {
        $followerCount = Follower::getCount($this->company_id, Follower::TYPE_COMPANY);
        return $this->render('followers', [
            'followerCount' => $followerCount
        ]);
    }

    public function actionGetFollowerData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = ['status' => false, 'data' => null];
        
        if (\Yii::$app->request->isPost && ($from = \Yii::$app->request->post('from')) && ($to = \Yii::$app->request->post('to'))) {
            $dateArray = FunctionHelper::getDateArrayFromTo($from, $to);

            $folllowerFromTo = Follower::getFollowerFromTo($from, $to, $this->company_id);
            foreach($folllowerFromTo as $date){
                $date = date('Y-m-d', strtotime($date));
                $dateArray[$date] = $dateArray[$date] + 1;
            }
            $res['data'] = $dateArray;
            $res['status'] = true;
        }
        return $res;
    }
    
    
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
