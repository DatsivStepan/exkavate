<?php

namespace publisher\modules\profile\controllers;

use yii\web\Controller;

/**
 * Default controller for the `profile` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action) {
        if (\Yii::$app->user->can('company_admin') || \Yii::$app->user->can('company_manager')) {
            $this->enableCsrfValidation = false;
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
        return $this;
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->can('company_admin')) {
            return $this->redirect(['/profile/admin']);
        }
        if (\Yii::$app->user->can('company_manager')) {
            return $this->redirect(['/profile/manager']);
        }
    }
}
